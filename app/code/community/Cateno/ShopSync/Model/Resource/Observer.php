<?php
/**
 * Cateno ShopSync Observer
 *
 * @category Model Mysql4
 * Cateno_OrderApproval
 * Ressource für Observer-Pattern
 *
 * @version 1.0.0
 */
//require 'Mage/Core/Controller/Front/Action.php';
class Cateno_ShopSync_Model_Resource_Observer
{

	public function NachLagerEnde($observer)
	{
		Mage::log($observer -> getCounter());
	}

	public function NachArtikelKategorieEnde($observer)
	{
		Mage::log($observer -> getCounter());
	}

	public function NachArtikelEnde($observer)
	{
		Mage::log($observer -> getCounter());
	}

	public function NachAdressenEnde($observer)
	{
		Mage::log($observer -> getCounter());
	}

	public function NachAdressenListe($observer)
	{
		Mage::log('NachAdressenListe');
		Mage::log($observer -> getCustomerIds());
	}

	public function NachAdresse($observer)
	{
		Mage::log('NachAdressen');
		Mage::log($observer -> getCustomerId());
	}
}
