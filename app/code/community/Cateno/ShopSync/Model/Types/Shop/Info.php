<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Shop-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Shop_Info extends Varien_Object
{
	/**
	 * @var string
	 */
	public $DbPrefix;

    /**
     * Tabellen object
     * @var Cateno_ShopSync_Model_Types_Tabellen
     */
	public $Tabellen;

    /**
     * Sprachen object
     * @var Cateno_ShopSync_Model_Types_Sprachen
     */
	public $sprachen;

    /**
     * Modul object
     * @var Cateno_ShopSync_Model_Types_Modul
     */
	public $versandArten;

    /**
     * Modul object
     * @var Cateno_ShopSync_Model_Types_Modul
     */
	public $zahlungsOptionen;

    /**
     * Modul object
     * @var Cateno_ShopSync_Model_Types_Modul
     */
	public $ZusammenfassungsModule;

    /**
     * Status object
     * @var Cateno_ShopSync_Model_Types_Vorgang_Status
     */
	public $vorgangStatus;

    /**
     * @var Integer
     */
	public $shopSystem;

	/**
	 * @var boolean
	 */
	public $ArtikelBilderPfad;

    /**
     * Gruppen object
     * @var Cateno_ShopSync_Model_Types_Artikel_Preis_Gruppen
     */
	public $artikelPreisGruppen;

    /**
     * Shops object
     * @var Cateno_ShopSync_Model_Types_Multi_Shops
     */
	public $MultiShops;
}
