<?php

 $mageFilename = "../../../../../../../../app/Mage.php";
 require_once $mageFilename; 
 umask(0);
 Mage::app();
 $activeprofile = basename(__FILE__, '.css.php');
 $baseconfig = Mage::helper("jmbasetheme")->getactiveprofile($activeprofile);
 header("Content-type: text/css; charset: UTF-8");
?>


/* Base settings
body#bd {
   background-color: <?php echo $baseconfig["bgolor"]; ?>;
   background-image:url("images/<?php echo $baseconfig["bgimage"]; ?>");
}
*/

<!--#ja-mycart .btn-toggle.active,-->
<!--#ja-mycart .btn-toggle:hover {-->
<!--   background-color:--><?php //echo $baseconfig["color"]; ?><!--;-->
<!--}-->

a,
.shop-access a:hover, /*Quick Access---*/
.shop-access a:active,
.shop-access a:focus,

#nav li.active a,   /* 0 Level */
#nav li.over a,
#nav a:hover,

#ja-pathway ul li a,  /*breadcrums---*/

.ja-spotlight .col-1 .block-content a, /* Spotlight */
.ja-spotlight .block .block-content a:hover,
.ja-spotlight .block .block-content a:focus,
.ja-spotlight .block .block-content a:active,
.socail-footer a:hover,
.ja-topsl_bottom li span.color-text3,
.ja-topsl_bottom li span.color-text2,

#ja-footer li a, /* FOOTER */

.pager .pages .current, /* Pager magento 1.4*/

.ratings .rating-links a:hover, /*RATINGS */
.ratings .rating-links a:focus,
.ratings .rating-links a:active,

.price, /*PRICES */
.regular-price .price,
.special-price .price,

a.minimal-price-link .price, /*Minimal price (as low as)---*/

.price-excluding-tax .price, /*Excluding tax---*/

.price-including-tax .price, /*Including tax---*/

.add-to-links a:focus, /*Add To---*/

.block-cart .subtotal .price, /*Mini cart - top---*/

ol#cart-sidebar .price,

.block-navigation #ja-sidenav li a:active, /*Layered Navigation---*/

.product-name a:hover, /*Product---*/
.product-name a:focus,
.product-name a:active,
.products-grid li.item:hover .product-name a, 

.product-essential .availability.in-stock span,

ul.ja-tab-navigator li a:hover, /*PRODUCT TABS */
ul.ja-tab-navigator li a:focus,
ul.ja-tab-navigator li a:active,

.email-friend a:hover, /*Product Info---*/
.email-friend a:focus,
.add-to-links a:hover,
.add-to-links a:focus,

.giftmessages .gift-header, /*Gift Messages----*/

button.btn-cart:hover,
button.btn-cart:focus,
button.button.btn-cart:hover,
button.button.btn-cart:focus,
.products-grid li.item:hover button.btn-cart,

#multiship-addresses-table button, /*Multiple Addresses checkout---*/
.place-order-box .grand-total .price, 

.block-account li a:hover, /*My Account navigation---*/
.block-account li a:active,
.block-account li a:focus,
.block-account li.current,

.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active,
.block-navigation #ja-sidenav ul.level0 li a:hover,
.block-navigation #ja-sidenav ul.level0 li a:focus,
.block-navigation #ja-sidenav ul.level0 li a:active,
.block-navigation #ja-sidenav ul.level0 li.active a,

.jm-megamenu ul.level1 li.mega a.mega span.menu-title,

.jm-megamenu .megamenu.level2 li.mega a.mega:hover span.menu-title,

.jm-megamenu .category-products .products-list li.item a:hover,

.jmzin-content:hover .product-name a, /* JM Categories List ---*/

.block-navigation #ja-sidenav li.active a,

<!--.jm-product-deals .products-list .deal-infor-wrap .sale-ends, /* JM Daily Deals ---*/-->
<!--.jm-product-deals .products-grid .deals-info li strong.sale-ends, -->
<!--.jm-product-deals .products-grid .deals-info li strong.discount, -->
<!--.jm-product-deals .products-grid .deals-info li strong.save,-->
<!--.jm-product-deals .products-list .add-to-links a:hover,-->
<!--.jm-product-deals .products-list .add-to-links a:focus,-->
<!--.jm-product-deals .products-list .add-to-links a:active {-->
<!--	color:--><?php //echo $baseconfig["color"]; ?><!--;-->
<!--}-->

<!--.input-text:focus, select:focus, textarea:focus,-->
<!--#ja-search .input-text:focus,-->
<!--.block-subscribe .input-box input:focus,-->
<!-