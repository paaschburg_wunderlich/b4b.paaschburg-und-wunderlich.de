<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Versandinformationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Versand
{
	/**
	 * @var string
	 */
	public $BelegNr;

	/**
	 * @var string
	 */
	public $PktNr;

	/**
	 * @var string
	 */
	public $VsdArtInfo;

    /**
	 * @var Date
	 */
	public $Dat;

	public function __construct($BelegNr = '', $PktNr = '', $VsdArtInfo = '', $Dat = '')
	{
		$this->BelegNr = $BelegNr;
		$this->PktNr = $PktNr;
		$this->VsdArtInfo = $VsdArtInfo;
		$this->Dat = $Dat;
	}
}
