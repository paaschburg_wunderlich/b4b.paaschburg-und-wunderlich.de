<?php
/**
 * Created by PhpStorm.
 * User: STENDEL, Wolf Schmidt - Schmidt Medienservice
 * Date: 29.04.2016
 * Time: 11:18
 */

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

require_once __DIR__ . '/inc/functions.inc.php';
$class = NEW B4b_Abfragen();

$start = microtime(true);

//ini_set('memory_limit','4G');
ini_set('display_errors', '1');
error_reporting(1);

define ('IMPORT_PATH', Mage::getBaseDir('media') . '/erp-tool/');
define ('IMPORT_FILE', 'de_product_update.sdf');
define ('CLEAR_FILE', 'not_online.sdf');

define ('LOG_PATH', Mage::getBaseDir() . '/var/log/');
define ('LOG_FILE', 'update_product.log');
define ('WRITE_LOGFILE', true);

if(WRITE_LOGFILE == true) {
    $logFileHandle = $class->openFile(LOG_PATH, LOG_FILE, 'a');
} else { $logFileHandle = NULL; }

$fileRowCount = 0;
$actionCount = 0;

Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
$de = Mage::app()->getStore('default')->getId();

$cat = Mage::getModel('catalog/category');
$catTree = $cat->getTreeModel()->load();
$catIds = $catTree->getCollection()->getAllIds();

$processElastic = Mage::getSingleton('index/indexer')->getProcessByCode('catalogsearch_fulltext');
$processElastic->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();

$message = '*** Start ***';
$class->printMessage($message, $logFileHandle);

/** Als erstes Shop-Artikel bereinigen */
if (($fileHandle = $class->openFile(IMPORT_PATH, CLEAR_FILE, 'r')) !== FALSE) {
    $countActive = 0;
    $countInactive = 0;
    $inactiveProducts = array();
    while (($inactive = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {
        $theDeleteObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($inactive[0]));
        if ($theDeleteObj->getSku() != "") {
            $message = $theDeleteObj->getSku() . " fliegt raus";
//            $class->printMessage($message, $logFileHandle);
            echo $message . "\r\n";
            $theDeleteObj->delete();
            $countInactive++;
        } else {
            $countActive++;
        }
    }
}

$end = microtime(true);
$runtime = number_format($end - $start, 2);

$message = "Laufzeit für die Bereinigung [" . number_format($end - $start, 2) . "] Sekunden. Es gibt  " . $countActive . " aktive Artikel. " . $countInactive . " Artikel sollen nicht zu B4B DE\n";
$class->printMessage($message, $logFileHandle);

/** Reindex Produkt->Kategorie */
$message = "Reindiziere Produkt zu Kategorie";
$class->printMessage($message, $logFileHandle);
$reindexStart = microtime(true);
$processCategory = Mage::getModel('index/indexer')->getProcessByCode('catalog_category_product');
$processCategory->reindexAll();
$reindexEnd = microtime(true);
$message = "Produkt zur Kategorie neu aufgebaut in [ " . number_format($reindexEnd - $reindexStart, 2) . " ] Sekunden.";
$class->printMessage($message, $logFileHandle);

/** Danach update vorhandener Produkte */
//if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
//    while (($data = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
//        $fileRowCount++;
//        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//
//        if ($data[24] == "0.0000") { $data[24] = ""; }
//        if ($data[25] === "30.12.1899") { $von = "02.01.1970"; } else { $von = $data[25]; }
//        if ($data[26] === "30.12.1899") { $bis = "16.01.1970"; } else { $bis = $data[26]; }
//
//        $fileData = array(
//            'sku' => $data[0],
//            'blocked' => $data[1] == 1 ? $data[1] = "2" : $data[1] = "1",
//            'name_de' => $data[2],
//            'shortdesc_de' => $data[3],
//            'description_de' => $data[4],
//            'bild1' => $class->extrudeImage($data[5]),
//            'bild2' => $class->extrudeImage($data[6]),
//            'bild3' => $class->extrudeImage($data[7]),
//            'bild4' => $class->extrudeImage($data[8]),
//            'barcode' => $data[9],
//            'erw_suche' => $data[10],
//            'verf_menge' => $data[21],
//            'gewicht' => $data[22],
//            'preis' => $data[23],
//            'sonderpreis' => $data[24] / 1.19,
//            'sp_von' => date_format(date_create($von), 'Y-m-d H:i:s'),
//            'sp_bis' => date_format(date_create($bis), 'Y-m-d H:i:s'),
//            'sichtbar' => $data[27],
//            'art_gruppe' => $data[28],
//            'art_kat' => $data[29],
//            'art_kennz' => $data[30],
//            'kettenglieder' => $data[31],
//            'kettenteilung' => $data[32],
//            'mkz_hinten' => $data[33],
//            'mkz_vorn' => $data[34],
//            'modellkennzahl' => $data[35],
//            'zaehnezahl' => $data[36],
//            'ausfuehrung' => $data[37],
//            'fahrzeughersteller' => $data[38],
//            'farbe' => $data[39],
//            'gutachten' => $data[40],
//            'material' => $data[41],
//            'produktmarke' => $data[42],
//            'xref' => $data[43]
//
//        );
//
//        $_updatedSaved = 0;
//        $_attributeUpdatedSaved = 0;
//        $_languageUpdatedSaved = 0;
//
//        $_categories = array(
//            $data[11], $data[12], $data[13], $data[14], $data[15], $data[16], $data[17], $data[18], $data[19], $data[20]
//        );
//
//        $_gallery = $fileData['bild2'] . ", " . $fileData['bild3'] . ", " . $fileData['bild4'];
//        $fileData['auf_Lager'] == "Nicht auf Lager" ? $isInStock = 0 : $isInStock = 1;
//
//        if ($fileData['sichtbar'] == "Katalog / Suche") { $sichtbar = 4; }
//        if ($fileData['sichtbar'] == "Katalog") { $sichtbar = 2; }
//        if ($fileData['sichtbar'] == "Suche") { $sichtbar = 3; }
//        if ($fileData['sichtbar'] == "Einzeln nicht sichtbar") { $sichtbar = 1; }
//
//        $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($fileData['sku']));
//        if ($theProductObj->getSku() == $fileData['sku']) {
//
//            /** Globale Daten */
//            if ($theProductObj->getStatus() != $fileData['blocked']) {
//                Mage::getModel('catalog/product_status')->updateProductStatus($theProductObj->getId(), 0, $fileData['blocked']);
//                $_updatedSaved++;
//                $_attributeName = "blocked";
//
//            }
//
//            /** Prüfung ob externe Images genutzt werden sollen */
//            if ($theProductObj->getUseExternalImages() == 0 OR $theProductObj->getUseExternalImages() == NULL) :
//                $theProductObj
//                    ->setUseExternalImages(1);
//                $_updatedSaved++;
//                $_attributeName = "use external";
//
//            endif;
//
//            /** Hier der Bilder vergleich (muss für pwonline noch geändert werden */
//            if ($theProductObj->getImageExternalUrl() != $fileData['bild1']) :
//                $theProductObj
//                    ->setImageExternalUrl($fileData['bild1'])
//                    ->setSmallImageExternalUrl($fileData['bild1'])
//                    ->setThumbnailExternalUrl($fileData['bild1']);
//                $_updatedSaved++;
//                $_attributeName = "bild1";
//
//            endif;
//
//            if ($theProductObj->getExternalGallery() != $_gallery) {
//                $theProductObj->setExternalGallery($_gallery);
//                $_updatedSaved++;
//                $_attributeName = "Gallerie";
//
//            }
//
//            if ($theProductObj->getBarcode() != $fileData['barcode']) {
//                $theProductObj->setBarcode($fileData['barcode']);
//                $_updatedSaved++;
//                $_attributeName = "barcode";
//
//            }
//
//            if ($theProductObj->getErweiterteSuche() != $fileData['erw_suche']) {
//                $theProductObj->setErweiterteSuche($fileData['erw_suche']);
//                $_updatedSaved++;
//                $_attributeName = "erw_suche";
//
//            }
//
//            if ($theProductObj->getXref() != $fileData['xref']) {
//                $theProductObj->setXref($fileData['xref']);
//                $_updatedSaved++;
//                $_attributeName = "Cross-REf";
//
//            }
//
//            /** Prüfen ob exportierte Kategorie vorhanden und gesettz */
//            foreach ($_categories as $_categoryId) {
//                if ($_categoryId != 0) :
//                    $categoryIds = $theProductObj->getCategoryIds();
//                    if (!in_array($_categoryId, $categoryIds)) :
//                        if (in_array($_categoryId, $catIds)):
//                            $theProductObj->setCategoryIds(array($_categories));
//                            $_updatedSaved++;
//                            $_attributeName = "category1";
//
//                        endif;
//                    endif;
//                endif;
//            }
//
//            /** Prüfen ob gesetzte Kategorien noch gültig */
//            $categoryIds = $theProductObj->getCategoryIds();
//            foreach ($categoryIds as $_categoryId) {
//                if (!in_array($_categoryId, $_categories)) :
//                    $theProductObj->setCategoryIds(array($_categories));
//                    $_updatedSaved++;
//                    $_attributeName = "category";
//                endif;
//            }
//
//            if ($theProductObj->getWeight() != $fileData['gewicht']) {
//                $theProductObj->setWeight($fileData['gewicht']);
//                $_updatedSaved++;
//                $_attributeName = "gewicht";
//            }
//
//            if ($theProductObj->getVisibility() != $sichtbar) {
//                $theProductObj->setVisibility($sichtbar);
//                $_updatedSaved++;
//                $_attributeName = "sichtbar";
//            }
//
//            if ($theProductObj->getPrice() != $fileData['preis']) {
//                $theProductObj->setPrice($fileData['preis']);
//                $_updatedSaved++;
//                $_attributeName = "preis";
//            }
//
//            if ($theProductObj->getSpecialPrice() != $fileData['sonderpreis']) {
//                $theProductObj->setSpecialPrice($fileData['sonderpreis']);
//                $_updatedSaved++;
//                $_attributeName = "sonderpreis";
//            }
//
//            $message = $fileData['sku'] . " => " . $fileData['sp_bis'] . " <= " . $theProductObj->getSpecialToDate();
//            $class->printMessage($message, $logFileHandle);
//            $message = $fileData['sku'] . " => " . $fileData['sp_von'] . " <= " . $theProductObj->getSpecialFromDate();
//            $class->printMessage($message, $logFileHandle);
//
//            if ($theProductObj->getSpecialToDate() != $fileData['sp_bis']) {
//                $theProductObj
//                    ->setSpecialFromDate($fileData['sp_von'])
//                    ->setSpecialToDate($fileData['sp_bis']);
//                $_updatedSaved++;
//                $_attributeName = "Specioal from or special to";
//            }
//
//            if ($theProductObj->getSmsArtikelgruppe() != $fileData['art_gruppe']) {
//                $theProductObj->setSmsArtikelgruppe($fileData['art_gruppe']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsArtikelkategorie() != $fileData['art_kat']) {
//                $theProductObj->setSmsArtikelkategorie($fileData['art_kat']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsArtikelkennzeichnung() != $fileData['art_kennz']) {
//                $theProductObj->setSmsArtikelkennzeichnung($fileData['art_kennz']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsKettenglieder() != $fileData['kettenglieder']) {
//                $theProductObj->setSmsKettenglieder($fileData['kettenglieder']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsKettenteilung() != $fileData['kettenteilung']) {
//                $theProductObj->setSmsKettenteilung($fileData['kettenteilung']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsMkzHinten() != $fileData['mkz_hinten']) {
//                $theProductObj->setSmsMkzHinten($fileData['mkz_hinten']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsMkzVorn() != $fileData['mkz_vorn']) {
//                $theProductObj->setSmsMkzVorn($fileData['mkz_vorn']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsModellkennzahl() != $fileData['modellkennzahl']) {
//                $theProductObj->setSmsModellkennzahl($fileData['modellkennzahl']);
//                $_updatedSaved++;
//            }
//
//            if ($theProductObj->getSmsZaehneanzahl() != $fileData['zaehnezahl']) {
//                $theProductObj->setSmsZaehneanzahl($fileData['zaehnezahl']);
//                $_updatedSaved++;
//            }
//
//            if ($_updatedSaved > 0) {
//                $theProductObj->save();
//                $message = $_updatedSaved . " Allgemeine Änderungen der Daten bei " . $fileData['sku'];
//                $class->printMessage($message, $logFileHandle);
//            } else {
//                $message = "Keine allgemeinen Änderungen an " . $fileData['sku'];
//                $class->printMessage($message, $logFileHandle);
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_ausfuehrung') != $fileData['ausfuehrung']) {
//                $class->setAttributeOption('shopsync_filter_ausfuehrung', $fileData['ausfuehrung'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "ausfuehrung";
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_fahrzeugmarke') != $fileData['fahrzeughersteller']) {
//                $class->setAttributeOption('shopsync_filter_fahrzeugmarke', $fileData['fahrzeughersteller'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "fahrzeugmarke";
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_farbe') != $fileData['farbe']) {
//                $class->setAttributeOption('shopsync_filter_farbe', $fileData['farbe'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "farbe";
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_gutachten') != $fileData['gutachten']) {
//                $class->setAttributeOption('shopsync_filter_gutachten', $fileData['gutachten'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "gutachten";
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_material') != $fileData['material']) {
//                $class->setAttributeOption('shopsync_filter_material', $fileData['material'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "material";
//            }
//
//            if ($theProductObj->getAttributeText('shopsync_filter_produktmarke') != $fileData['produktmarke']) {
//                $class->setAttributeOption('shopsync_filter_produktmarke', $fileData['produktmarke'], $theProductObj);
//                $_attributeUpdatedSaved++;
//                $_attributeName = "produktmarke";
//            }
//
//            if ($_attributeUpdatedSaved > 0) {
//                $message = $fileData['sku'] . " " . $_attributeUpdatedSaved . " Attribute geändert";
//                $class->printMessage($message, $logFileHandle);
//                $message = "Das allgemein geänderte Attribut ist " . $_attributeName;
//                $class->printMessage($message, $logFileHandle);
//            } else {
//                $message = $fileData['sku'] . " ohne Attributänderungen";
//                $class->printMessage($message, $logFileHandle);
//            }
//            /** Storeabhängige Daten /*
//
//            /** Ab hier die sprachabhängige Artikeltext-Synchronisation */
//            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//            $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($fileData['sku']));
//
//            if ($theProductObj->getName() != $fileData['name_de'] OR $theProductObj->getShortDescription() != $fileData['shortdesc_de'] OR $theProductObj->getDescription() != $fileData['description_de']) :
//                $theProductObj
//                    ->setStoreId(0)
//                    ->setName($fileData['name_de'])
//                    ->setShortDescription($fileData['shortdesc_de'])
//                    ->setDescription($fileData['description_de'])
//                    ->setUpdatedAt(strtotime('now'))
//                    ->save();
//                $theProductObj
//                    ->setStoreId($de)
//                    ->setName($fileData['name_de'])
//                    ->setShortDescription($fileData['shortdesc_de'])
//                    ->setDescription($fileData['description_de'])
//                    ->save();
//                $_languageUpdatedSaved++;
//            endif;
//
//            if ($_languageUpdatedSaved > 0) {
//                $message = $fileData['sku'] . " Sprachen aktualisiert";
//                $class->printMessage($message, $logFileHandle);
//            }
//            else {
//                $message = $fileData['sku'] . " ohne Sprachänderungen";
//                $class->printMessage($message, $logFileHandle);
//            }
//        } else {
//            /**
//             * Neues Produkt anlegen
//             */
//            $message = $fileData['sku'] . " als neues Produkt anlegen";
//            $class->printMessage($message, $logFileHandle);
//            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
//            $product = Mage::getModel('catalog/product');
//
//            $product
//                ->setWebsiteIds(array(1))                       //website ID the product is assigned to, as an array
//                ->setAttributeSetId(4)                          //ID of a attribute set named 'default'
//                ->setTypeId('simple')                           //product type
//                ->setCreatedAt(strtotime('now'))                //product creation time
//                ->setSku($fileData['sku'])                      //SKU
//                ->setName($fileData['name_de'])                 //product name
//                ->setWeight($fileData['gewicht'])
//                ->setStatus($fileData['blocked'])               //product status (1 - enabled, 2 - disabled)
//                ->setTaxClassId(2)                              //tax class (0 - none, 1 - default, 2 - taxable, 4 - shipping)
//                ->setVisibility($sichtbar)                      //catalog and search visibility
//                ->setPrice($fileData['preis'])                  //price in form 11.22
//                ->setSpecialPrice($fileData['sonderpreis'])     //special price in form 11.22
//                ->setSpecialFromDate($fileData['sp_von'])       //special price from (MM-DD-YYYY)
//                ->setSpecialToDate($fileData['sp_bis'])         //special price to (MM-DD-YYYY)
//                ->setDescription($fileData['description_de'])
//                ->setShortDescription($fileData['shortdesc_de'])
//                ->setStockData(array(
//                        'use_config_manage_stock' => 0,         //'Use config settings' checkbox
//                        'manage_stock' => 1,                    //manage stock
//                        'is_in_stock' => $isInStock,            //Stock Availability
//                        'qty' => $fileData['verf_menge']        //qty
//                    )
//                )
//                ->setCategoryIds(array($_categories))           //assign product to categories
//                ->setUseExternalImages(1)
//                ->setImageExternalUrl($fileData['bild1'])
//                ->setSmallImageExternalUrl($fileData['bild1'])
//                ->setThumbnailExternalUrl($fileData['bild1'])
//                ->setExternalGallery($_gallery)
//                ->setSmsArtikelgruppe($fileData['art_gruppe'])
//                ->setSmsArtikelkategorie($fileData['art_kat'])
//                ->setSmsArtikelkennzeichnung($fileData['art_kennz'])
//                ->setSmsKettenglieder($fileData['kettenglieder'])
//                ->setSmsKettenteilung($fileData['kettenteilung'])
//                ->setSmsMkzHinten($fileData['mkz_hinten'])
//                ->setSmsMkzVorn($fileData['mkz_vorn'])
//                ->setSmsModellkennzahl($fileData['modellkennzahl'])
//                ->setSmsZaehneanzahl($fileData['zaehnezahl'])
//                ->setErweiterteSuche($fileData['erw_suche'])
//                ->setXref($fileData['xref']);
//            $product->save();
//
//
//            if (!empty($fileData['ausfuehrung'])) {
//                $class->setAttributeOption('shopsync_filter_ausfuehrung', $fileData['ausfuehrung'], $product);
//            }
//            if (!empty($fileData['fahrzeughersteller'])) {
//                $class->setAttributeOption('shopsync_filter_fahrzeugmarke', $fileData['fahrzeughersteller'], $product);
//            }
//            if (!empty($fileData['farbe'])) {
//                $class->setAttributeOption('shopsync_filter_farbe', $fileData['farbe'], $product);
//            }
//            if (!empty($fileData['gutachten'])) {
//                $class->setAttributeOption('shopsync_filter_gutachten', $fileData['gutachten'], $product);
//            }
//            if (!empty($fileData['material'])) {
//                $class->setAttributeOption('shopsync_filter_material', $fileData['material'], $product);
//            }
//            if (!empty($fileData['produktmarke'])) {
//                $class->setAttributeOption('shopsync_filter_produktmarke', $fileData['produktmarke'], $product);
//            }
//        }
//    }
//    $end2 = microtime(true);
//    $message2 = "Laufzeit gesamt inklusive Bereinigung [" . number_format($end2 - $start, 2) . "] Sekunden. Es wurden " . $fileRowCount . " Zeilen eingelesen und " . $actionCount . " Aenderungen vorgenommen.\n";
//    $class->printMessage($message2);
//    $class->closeFile($fileHandle);
//
//    /** Reindiziere Attribute */
//    $message = "Reindiziere Attribute";
//    $class->printMessage($message, $logFileHandle);
//    $reindexStart = microtime(true);
//    $processAttribute = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_attribute');
//    $processAttribute->reindexAll();
//    $reindexEnd = microtime(true);
//    $message = "Attributindex neu aufgebaut in [ " . number_format($reindexEnd - $reindexStart, 2) . " ] Sekunden";
//    $class->printMessage($message, $logFileHandle);
//
//    /** Reindiziere Preise */
//    $message = "Reindiziere Artikelpreise";
//    $class->printMessage($message, $logFileHandle);
//    $reindexStart = microtime(true);
//    $processPrice = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_price');
//    $processPrice->reindexAll();
//    $reindexEnd = microtime(true);
//    $message = "Preisindex neu aufgebaut in [ " . number_format($reindexEnd - $reindexStart, 2) . " ] Sekunden";
//    $class->printMessage($message, $logFileHandle);
//
//    /** Reindex Produkt->Kategorie */
//    $message = "Reindiziere Produkt zu Kategorie";
//    $class->printMessage($message, $logFileHandle);
//    $reindexStart = microtime(true);
//    $processCategory = Mage::getModel('index/indexer')->getProcessByCode('catalog_category_product');
//    $processCategory->reindexAll();
//    $reindexEnd = microtime(true);
//    $message = "Produkt zur Kategorie neu aufgebaut in [ " . number_format($reindexEnd - $reindexStart, 2) . " ] Sekunden";
//    $class->printMessage($message, $logFileHandle);
//
//    /** Elasticsearch wieder anschalten */
//    $processElastic->reindexAll();
//    $processElastic->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
//
//    $class->closeFile($logFileHandle);
//
//} else {
//    $message = "Input-Datei [" . IMPORT_PATH . IMPORT_FILE . "] nicht gefunden!!!";
//    $class->printMessage($message, $logFileHandle);
//    exit;
//}
