<?php
$installer = $this;

$installer->startSetup();

$installer->addAttribute('catalog_product', 'shopsync_active', array(
    'type'                => 'int',
    'backend'            => '',
    'frontend'            => '',
    'label'                => 'Shopsync Active',
    'input'                => 'select',
    'class'                => '',
    'source'            => 'eav/entity_attribute_source_boolean',
    'global'            => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'            => false,
    'required'            => false,
    'user_defined'        => false,
    'default'            => 0,
    'searchable'        => false,
    'filterable'        => false,
    'comparable'        => false,
    'visible_on_front'    => false,
    'unique'            => false,
    'is_configurable'    => false
));

$installer->endSetup();
