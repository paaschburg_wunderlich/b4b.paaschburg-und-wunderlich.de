<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Tabellen-Informationen
 * 
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Tabellen
{
    /**
    * Cateno_ShopSync_Model_Types_Tabellen
    * @return array
    */
    public function getTabellen()
    {
        $theTabellen = array();
        /* customer attribute not filter visible */
        $theCustomerAttributeArray = array();
        $theCustomerAttributes = Mage::getModel('customer/customer')->getAttributes();
        $theAddressAttributes = Mage::getModel('customer/address')->getAttributes();
        $theProductAttributes = Mage::getModel('catalog/product')->getAttributes();

        if(Mage::helper('catalog/category_flat')->isEnabled())
        {
			$theCategoryAttributes = Mage::getModel('eav/entity_attribute')->getCollection()->setEntityTypeFilter(
					Mage::getModel('eav/entity_type')->loadByCode('catalog_category')->getEntityTypeId()
			);
        }
		else
        {
			$theCategoryAttributes = Mage::getModel('catalog/category')->getAttributes();
        }

		/* $theCategoryAttributes = Mage::getModel('eav/entity_attribute')->getCollection()->setEntityTypeFilter(
        Mage::getModel('eav/entity_type')->loadByCode('catalog_category')->getEntityTypeId()
        ); */

		$fieldInfoArray  = array();
		$theCustomerAttributeArray = array();
		foreach ($theCustomerAttributes as $theItem)
        {
            $fieldInfo = $this->GetFeldInfo($theItem);
            $fieldInfoArray[] = $fieldInfo;
            $theCustomerAttributeArray[] = $fieldInfo->Name;
        }

        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('customer/customer', $theCustomerAttributeArray,$fieldInfoArray);

        $readConnection = Mage::getSingleton('core/resource')->getConnection('core_read');
        /* FreieFelder orders */
        $theOrderAttributeArray = array_keys($readConnection->describeTable(Mage::getModel('sales/order')->getResource()->getMainTable()));
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('sales/order', $theOrderAttributeArray);

        /* FreieFelder order_item */
        $theOrderItemAttributeArray = array_keys($readConnection->describeTable(Mage::getModel('sales/order_item')->getResource()->getMainTable()));
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('sales/order_item', $theOrderItemAttributeArray);


        /* customer address attribute not filter visible */
        $theAddressAttributeArray = array();
        $fieldInfoArray  = array();
        foreach ($theAddressAttributes as $theItem)
        {
			$fieldInfo = $this->GetFeldInfo($theItem);
			$fieldInfoArray[] = $fieldInfo;
			$theAddressAttributeArray[] = $fieldInfo->Name;
        }

        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('customer/address', $theAddressAttributeArray, $fieldInfoArray);

        /* catalog category attribute */
            
        $theCategoryAttributeGlobalArray = array();
        $theCategoryAttributeWebsiteArray = array();
        $theCategoryAttributeStoreviewArray = array();
        $fieldInfoCategoryAttributeGlobalArray = array();
        $fieldInfoCategoryAttributeWebsiteArray = array();
        $fieldInfoCategoryAttributeStoreviewArray = array();

        $isGlobal = $isWebsite = $isStoreview = false;
        foreach ($theCategoryAttributes as $theAttribute)
        {
            if($theAttribute->getAttributeCode() == 'increment_id')
                continue;
            switch ($theAttribute->getIsGlobal())
                {
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL:
                        $isGlobal = true;
                        $theCategoryAttributeGlobalArray[] = $theAttribute->getAttributeCode();
                        $fieldInfoCategoryAttributeGlobalArray[] = $this->GetFeldInfo($theAttribute);
                        break;
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE:
                        $isWebsite = true;
                        $theCategoryAttributeWebsiteArray[] = $theAttribute->getAttributeCode();
                        $fieldInfoCategoryAttributeWebsiteArray[] = $this->GetFeldInfo($theAttribute);
                        break;
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE:
                        $isStoreview = true;
                        $theCategoryAttributeStoreviewArray[] = $theAttribute->getAttributeCode();
                        $fieldInfoCategoryAttributeStoreviewArray[] = $this->GetFeldInfo($theAttribute);
                        break;
                }
            }

        if(Mage::helper('catalog/category_flat')->isEnabled())
        {
			$theCategoryAttributeGlobalArray[] = 'entity_id';
			$theCategoryAttributeGlobalArray[] = 'entity_type_id';
			$theCategoryAttributeGlobalArray[] = 'attribute_set_id';
			$theCategoryAttributeGlobalArray[] = 'parent_id';
			$theCategoryAttributeGlobalArray[] = 'updated_at';
			$theCategoryAttributeGlobalArray[] = 'created_at';
        }

        $theCategoryAttributeStoreviewArray[] = 'store_id';
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('catalog/category_global', $theCategoryAttributeGlobalArray,$fieldInfoCategoryAttributeGlobalArray);
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('catalog/category_website', $theCategoryAttributeWebsiteArray,$fieldInfoCategoryAttributeWebsiteArray);
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('catalog/category_storeview', $theCategoryAttributeStoreviewArray,$fieldInfoCategoryAttributeStoreviewArray);

        /* catalog product attribute */
        $theProductAttributeGlobalArray = array();
        $theProductAttributeWebsiteArray = array();
        $theProductAttributeStoreviewArray = array();

        foreach ($theProductAttributes as $theAttribute)
        {
            switch ($theAttribute->getIsGlobal())
            {
                /* 0 */
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL:

                        $theProductAttributeGlobalArray[] = $theAttribute->getAttributeCode();
                        break;
                /* 1 */
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE:

                        $theProductAttributeWebsiteArray[] = $theAttribute->getAttributeCode();
                        break;
                /* 2 */
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE:

                        $theProductAttributeStoreviewArray[] = $theAttribute->getAttributeCode();
                        break;
            }
        }
        $theProductAttribute = Mage::getModel('eav/entity_attribute')->getCollection()->setEntityTypeFilter(
                Mage::getModel('eav/entity_type')->loadByCode('catalog_product')->getEntityTypeId()
        );
        foreach ($theProductAttribute as $theAttribute)
        {
            switch ($theAttribute->getIsGlobal())
            {
                /* 0 */
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL:

                        $theProductAttributeGlobalArray[] = $theAttribute->getAttributeCode();
                        break;
                /* 1 */
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_WEBSITE:

                        $theProductAttributeWebsiteArray[] = $theAttribute->getAttributeCode();
                        break;
                /* 2 */
                case Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE:

                        $theProductAttributeStoreviewArray[] = $theAttribute->getAttributeCode();
                        break;
            }
        }

        // since 1.4.2 the category_ids are no longer available as an attribute
        $theProductAttributeGlobalArray[] = 'category_ids';

        $theProductAttributeStoreviewArray[] = 'store_id';
        $theProductAttributeStoreviewArray = array_unique($theProductAttributeStoreviewArray);
        $theProductAttributeWebsiteArray = array_unique($theProductAttributeWebsiteArray);
        $theProductAttributeGlobalArray = array_unique($theProductAttributeGlobalArray);

        //stock fields
        $theProductAttributeGlobalArray[] = 'manage_stock';
        $theProductAttributeGlobalArray[] = 'use_config_manage_stock';
        $theProductAttributeGlobalArray[] = 'original_inventory_qty';
        $theProductAttributeGlobalArray[] = 'qty';
        $theProductAttributeGlobalArray[] = 'min_qty';
        $theProductAttributeGlobalArray[] = 'use_config_min_qty';
        $theProductAttributeGlobalArray[] = 'min_sale_qty';
        $theProductAttributeGlobalArray[] = 'use_config_min_sale_qty';
        $theProductAttributeGlobalArray[] = 'max_sale_qty';
        $theProductAttributeGlobalArray[] = 'use_config_max_sale_qty';
        $theProductAttributeGlobalArray[] = 'is_qty_decimal';
        $theProductAttributeGlobalArray[] = 'backorders';
        $theProductAttributeGlobalArray[] = 'use_config_backorders';
        $theProductAttributeGlobalArray[] = 'notify_stock_qty';
        $theProductAttributeGlobalArray[] = 'use_config_notify_stock_qty';
        $theProductAttributeGlobalArray[] = 'enable_qty_increments';
        $theProductAttributeGlobalArray[] = 'use_config_enable_qty_increments';
        $theProductAttributeGlobalArray[] = 'qty_increments';
        $theProductAttributeGlobalArray[] = 'use_config_qty_increments';
        $theProductAttributeGlobalArray[] = 'is_in_stock';

        $theTabellen[] = $this->GetProductTabelle('catalog/product_global', $theProductAttributeGlobalArray);
        $theTabellen[] =  $this->GetProductTabelle('catalog/product_website', $theProductAttributeWebsiteArray);
        $theTabellen[] = $this->GetProductTabelle('catalog/product_storeview', $theProductAttributeStoreviewArray);
            
		/*
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('catalog/product_global', $theProductAttributeGlobalArray);
        $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('catalog/product_website', $theProductAttributeWebsiteArray);
        * $theTabellen[] = new Cateno_ShopSync_Model_Types_Tabellen('catalog/product_storeview', $theProductAttributeStoreviewArray);
        */            

		return $theTabellen;
    }

    /**
    * 
    * @param Cateno_ShopSync_Model_Types_Freies_Feld $inFreieFelder
    * @param array $inData
    * @return array
    */
	public function mergeFreiesFeldData($inFreieFelder, $inData)
		{
			if(is_array($inFreieFelder))
			{
				foreach ($inFreieFelder as $theFeld)
				{
					$inData = array_merge($inData, array(
						$theFeld->FeldShop => $theFeld->Wert
					));
				}
			}
			return $inData;
		}

    /**
    * 
    * @param Cateno_ShopSync_Model_Types_Freies_Feld $inFreieFelder
    * @param array $inData
    * @return array
    */
    public function mergeFelderFeldData($inBezeichnungen, $inData)
    {
		if(is_array($inBezeichnungen))
        {
        foreach ($inBezeichnungen as $theFelder)
            {
				foreach ($theFelder->Felder as $theItem)
				{
					$inData = array_merge($inData, array(
						$theItem->FeldShop => $theItem->Wert
					));
				}
            }
        }
		return $inData;
    }

    /**
    * 
    * @param Cateno_ShopSync_Model_Types_Freies_Feld $inFreieFelder
    * @param array $inData
    * @return array
    */
    public function mergeFelderFeldTierPriceData($inBezeichnungen)
    {
		$inData = array();
		$theWebsiteId = Mage::app()->getWebsite()->getId();
		if(is_array($inBezeichnungen))
		{
			foreach ($inBezeichnungen as $theItem)
			{
				$inData[] = array(
					'website_id' => $theWebsiteId,
					'cust_group' => $theItem->kundengruppe,
					'price' => $theItem->preis,
					'price_qty' => $theItem->menge,
				);
			}
		}
		return $inData;
    }

    private function GetFeldInfo($item)
    {
		$name = $item->getAttributeCode();
		$typ = $item->getBackendType();
		$art = $item->getFrontendInput();
		return new Cateno_ShopSync_Model_Types_FeldInfo($name,$typ,$art);
    }
                    
	private function GetProductTabelle($tabellenName,$attributes)
	{
		$tabelle = array();
		$fieldInfo = array();
		foreach ($attributes as $attributeCode)
			{
                                
			$attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeCode); 
			$fieldinfo[] = $this->GetFeldInfo($attribute);
                                
                             
			$d=0;
			}
		return new Cateno_ShopSync_Model_Types_Tabellen($tabellenName, $attributes,$fieldinfo);
	}
}
