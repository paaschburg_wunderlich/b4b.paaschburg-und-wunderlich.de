<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Preisgruppen
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_Model_Resource_Artikel_Preis_Gruppen
{
	/**
	 * Cateno_ShopSync_Model_Types_Artikel_Preis_Groups
	 * @return array
	 */
	public function getArtikelPreisGroups()
	{
		$theGroups = array();
		foreach(Mage::getModel('customer/group')->getCollection() as $theGroup) {
			$theGroups[] = new Cateno_ShopSync_Model_Types_Artikel_Preis_Gruppen(
				$theGroup->getCustomerGroupId(),
				$theGroup->getCustomerGroupCode()
			);
		}
		return $theGroups;
	}
}
