<?php

/**
 * Created with PhpStorm 10.0
 * Autor: Schmidt Medienservice, Wolf Schmidt | Foto-Stendel, Stephan Stendel
 * für Paaschburg & Wunderlich
 * 2016-02-25
 * alle Rechte liegen beim Autor
 */

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

require_once __DIR__ . '/inc/functions.inc.php';
$class = NEW B4b_Abfragen();

$start = microtime(true);

$rows = 0;
$changes = 0;

ini_set('memory_limit','4G');
ini_set('display_errors', '1');
error_reporting(1);

define ('IMPORT_PATH', Mage::getBaseDir('media') . '/erp-tool/');
define ('IMPORT_FILE', 'update_lagermengen.sdf');

define ('LOG_PATH', Mage::getBaseDir() . '/var/log/');
define ('LOG_FILE', 'update_lagermengen.log');
define ('WRITE_LOGFILE', false);

if(WRITE_LOGFILE == true) {
    $logFileHandle = $class->openFile(LOG_PATH, LOG_FILE, 'a');
} else { $logFileHandle = NULL; }

/** Set all Indexes to Manual */
$processCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
foreach ($processCollection as $process) {
    $process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
}

if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {

    while (($data = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {

        $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($data[0]));

        if($theProductObj->getId()) {
            $rows++;
            $data[2] == "Nicht auf Lager" ? $isInStock = 0 : $isInStock = 1;
            if (number_format($data[1], 4, '.', '') != $theProductObj->getStockItem()->getQty() OR $isInStock != $theProductObj->getStockItem()->getIsInStock()) {
                $changes++;
                $stockItem = $theProductObj->getStockItem();
                $data[2] == "Nicht auf Lager" ? $stockItem->setData('is_in_stock', 0) : $stockItem->setData('is_in_stock', 1);
                $stockItem->setData('qty', number_format($data[1], 4, '.', ''));
                $stockItem->save();
                $theProductObj->save();
            }
        }
    }
    $class->closeFile($fileHandle);
} else {
    $message = "Input-Datei [" . IMPORT_PATH . IMPORT_FILE . "] nicht gefunden!!!";
    $class->printMessage($message, $logFileHandle);
    exit;
}

/**  Reindexing Stock Index */
$process = Mage::getModel('index/indexer')->getProcessByCode('cataloginventory_stock');
$process->reindexEverything();

/**  Reindexing Price Index */
$process = Mage::getModel('index/indexer')->getProcessByCode('catalog_product_price');
$process->reindexEverything();

/**Set all Indexes to 'on change' */
$processCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
foreach ($processCollection as $process) {
    $process->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
}

$end = microtime(true);

$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden. Es wurden " . $rows . " Zeilen eingelesen und " . $changes . " Änderungen vorgenommen.";
$class->printMessage($message, $logFileHandle);
$class->closeFile($logFileHandle);
