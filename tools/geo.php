<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 20.10.2015
 * Time: 19:44
 */
?>

<html lang="de">
<head>
    <meta charset="utf-8">
</head>
<?php
require_once __DIR__ . '/../app/Mage.php';
Mage::app();

echo '<pre>';

$test = getAllShoppartnersGeodata(21033);

//var_dump($test);

usort($test, function($a, $b) {
    return $a['distance_air'] - $b['distance_air'];
});

var_dump(array_slice($test, 0 ,5));

//foreach (array_slice($test, 0 ,5) as $shoppartner) {
//    $shoppartnerData[] = array(
//        'entity_id' => $shoppartner['entity_id'],
//        'distance_street' => number_format((getGoogleDistance($shoppartner->getBillingPostcode(), $customerPostcode) / 1000), 2),
//    );
//}
//
//var_dump($shoppartnerData);

echo '</pre>';












function getAllShoppartnersGeodata($customerPostcode)
{
    $collection = Mage::getModel('customer/customer')
        ->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('is_shoppartner', true)
        ->addAttributeToFilter('customer_activated', 1)
        ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
        ->addAttributeToSort('entity_id', 'ASC');

    foreach ($collection as $shoppartner) {

        $geoData = getGeodataToPostcode($shoppartner->getBillingPostcode());
        $customerGeodata = getGeodataToPostcode($customerPostcode);

        $shoppartnerGeodata[] = array(
            'entity_id'         => $shoppartner['entity_id'],
            'postcode'          => $shoppartner->getBillingPostcode(),
            'longitude'         => $geoData['lon'],
            'latitude'          => $geoData['lat'],
            'distance_air'      => number_format(getDistance($customerGeodata['lon'], $customerGeodata['lat'], $geoData['lon'], $geoData['lat']), 2),
//            'distance_street'   => number_format((getGoogleDistance($shoppartner->getBillingPostcode(), $customerPostcode) / 1000), 2),
        );
    }
    return $shoppartnerGeodata;
}


function getDistance($startLon, $startLat, $endLon, $endLat)
{
    $x1 = $startLon;
    $x2 = $endLon;
    $y1 = $startLat;
    $y2 = $endLat;
    return acos(sin($x1 = deg2rad($x1)) * sin($x2 = deg2rad($x2)) + cos($x1) * cos($x2) * cos(deg2rad($y2) - deg2rad($y1))) * (6378.137);
}


function getGoogleDistance($shoppartnerPostcode, $customerPostcode)
{
    $url = "http://maps.googleapis.com/maps/api/distancematrix/json?origins=" . $customerPostcode . "+DE&destinations=" . $shoppartnerPostcode . "+DE&mode=flying&language=de-DE&sensor=false";
    $json = file_get_contents($url);
    $data = json_decode($json, true);

    if($data['status'] == "OK"){

        return $data['rows'][0]['elements'][0]['distance']['value'];
    }
}


function getGeodataToPostcode($postcode)
{
    $resource = Mage::getSingleton('core/resource');
    $readConnection = $resource->getConnection('core_read');
    $stmt = "SELECT 
      t.loc_id,
      t.text_val,
      c.lat,
      c.lon
    FROM 
      geodb_textdata t
    JOIN 
      geodb_coordinates c ON (t.loc_id = c.loc_id)
    WHERE
    	t.text_val = '" . $postcode . "'";
    return $readConnection->fetchRow($stmt);
}
