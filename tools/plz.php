<?php
/*
plz.php
programmiert 06/2004 von Zissis Siantidis

Diese Datei stellt einfache Funktionen bereit, um mit der PLZ-Datenbank openGeoDB zu kommunizieren, erhaeltlich unter:
http://opengeodb.de/              
Diese Datenbank enthaelt alle PLZen Deutschlands mitsamt Koordinaten.

Voraussetzung:
o.g. Datenbank muss in einer MySQL-Datenbank vorliegen.

Benutzung:
- Im Configuration-Teil dieser Datei Angaben zum Datenbankzugriff aendern (Datenbankname, Benutzername, Passwort, Host) 
- Diese Datei mit require("plz.php"); includen
- Funktionen benutzen (Funktionsbeschreibung bei der jeweiligen Funktion)

*/

/*************************
** CONFIGURATION            
** 
** Changes needed
**************************/


// Zugang zur Datenbank mit der openGeoDB Datenbank
$plz_dbhost = "localhost";
$plz_dbname = "geo";
$plz_dbuser = "ww6211";
$plz_dbpass = "bananas";

$plz_erdradius = 6371.0;



/*************************
** IMPLEMENTATION
** 
** Do not change!
**************************/


// wird intern im Falle eines Datenbankfehlers benutzt
function mysql_die($error = "") {
    $mysqlError = mysql_error();
    if (!empty($mysqlError)) echo "SQL server reply: ".$mysqlError;
    echo "<br><a href=\"javascript:history.go(-1)\">BACK</a>";
    exit;
}

// wird intern zum Verbindungsaufbau mit der Datenbank verwendet
function connect_db() {          
global $plz_dbhost,$plz_dbuser,$plz_dbpass,$plz_dbname;
    $db = mysql_connect($plz_dbhost,$plz_dbuser,$plz_dbpass) or mysql_die();
    mysql_select_db($plz_dbname);
    return $db;
}


// liefert alle Postleitzahlen zu einem Ort in einem array zurueck
// Parameter: String $ort
// Returns: array
function plzGetPLZ($ort) {
    $db = connect_db();
    $query = 'SELECT * FROM geodb_locations WHERE name LIKE "%'.$ort.'%"';
    $results = mysql_query ($query,$db) or mysql_die();   
    $erg = array();
    while ($item = mysql_fetch_object($results)) {
        foreach(explode(",",$item->plz) as $p) array_push($erg,$p);
    }
    return $erg;
}

// liefert alle Ortsnamen zu einer PLZ in einem array zurueck (z.B. auch Ortsname:Stadtteilname)
// Parameter: Int $plz
// Returns: array
function plzGetOrt($plz) {
    $db = connect_db();
    $query = 'SELECT * FROM geodb_locations WHERE plz LIKE "%'.$plz.'%"';
    $results = mysql_query ($query,$db) or mysql_die();   
    $erg = array();
    while ($item = mysql_fetch_object($results)) {
        if ($item->typ>6) array_push($erg,$item->ort.":".$item->name);
            else array_push($erg,$item->name);
    }
    return $erg;
}

// liefert die Distanz zwischen zwei PLZ in km zurueck. 
// Vorsicht: Es ist die Luftlinie gemeint, nicht die Strassenentfernung!
// Parameter: Int $plz1, Int $plz2
// Returns: float
function plzGetDistanceBetween($plz1, $plz2) {
global $plz_erdradius;
    $db = connect_db();
    $query = 'SELECT * FROM geodb_locations WHERE plz LIKE "%'.$plz1.'%" AND typ=6';
    $results = mysql_query ($query,$db) or mysql_die();   
    $item1 = mysql_fetch_object($results);
    $latitudeRad = deg2rad($item1->breite);
    $longitudeRad = deg2rad($item1->laenge);
    $formula = "IFNULL((ACOS((SIN($latitudeRad)*SIN(RADIANS(breite))) + (COS($latitudeRad)*COS(RADIANS(breite))*COS(RADIANS(laenge)-$longitudeRad))) * $plz_erdradius),0)";
    $query = 'SELECT *, '.$formula.' AS e FROM geodb_locations WHERE plz LIKE "%'.$plz2.'%" AND typ=6 ORDER BY e ASC';
    $results2 = mysql_query ($query,$db) or mysql_die();   
    $item2 = mysql_fetch_object($results2);
    return $item2->e;    
}

// liefert alle PLZ in definierbarem Umkreis um eine PLZ in einem array zurueck. Dieses hat als Index immer die PLZ und 
// als Wert die Entfernung zum Zentrum der Betrachtung
// Parameter: Int $plz (Zentrum der Betrachtung), float $distance (Umkreisradius in km) 
// Returns: array
function plzGetAllPLZNear($plz, $distance) { 
global $plz_erdradius;
    $db = connect_db();
    $query = 'SELECT * FROM geodb_locations WHERE plz LIKE "%'.$plz.'%" AND typ=6';
    $results = mysql_query ($query,$db) or mysql_die();   
    $erg = array();
    while ($item = mysql_fetch_object($results)) {
        $latitudeRad = deg2rad($item->breite);
        $longitudeRad = deg2rad($item->laenge);
        $formula = "IFNULL((ACOS((SIN($latitudeRad)*SIN(RADIANS(breite))) + (COS($latitudeRad)*COS(RADIANS(breite))*COS(RADIANS(laenge)-$longitudeRad))) * $plz_erdradius),0)";
        $query = "SELECT *, $formula AS e FROM geodb_locations WHERE $formula < $distance ORDER BY e ASC";
        $results2 = mysql_query ($query,$db) or mysql_die();   
        while ($item2 = mysql_fetch_object($results2)) {
           //foreach(explode(",",$item2->plz) as $p) array_push($erg,$p);
           foreach(explode(",",$item2->plz) as $p) $erg[$p]=$item2->e;
        }
    }      
    $erg = array_unique($erg);
    asort($erg);
    return $erg;
}


?>
