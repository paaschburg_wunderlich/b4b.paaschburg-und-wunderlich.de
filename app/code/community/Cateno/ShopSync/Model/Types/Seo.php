<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von SEO-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Seo extends Varien_Object
{
	/**
	 * @var string
	 */
	public $UrlText;

	/**
	 * @var string
	 */
	public $LanguageCode;

	/**
	 * @var array
	 */
	public $Felder;
}
