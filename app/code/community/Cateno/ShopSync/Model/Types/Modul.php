<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Modul-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Modul extends Varien_Object
{
	/**
	 * @var string
	 */
	public $Name;

	/**
	 * @var string
	 */
	public $Bezeichnung;

	public function __construct($inName = null, $inBezeichnung = null)
	{
		$this->Name = $inName;
		$this->Bezeichnung = $inBezeichnung;
	}
}
