<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Kategorie-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Kategorie extends Varien_Object
{
	/**
	 * @var int
	 */
	public $Nr;

	/**
	 * @var string
	 */
	public $KatID;

	/**
	 * @var int
	 */
	public $Index;

	/**
	 * Parent Id
	 * @var string
	 */
	public $ParentKatId;

	/**
	 * Category Image
	 * @var string
	 */
	public $Bild;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Daten>)
	 */
	public $Bezeichnungen;

	/**
	 * @var array
	 */
	public $FreieFelder;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Seo>)
	 */
	public $SEO;

	/**
	 * @var array(<int>)
	 */
	public $MultiShops;
}
