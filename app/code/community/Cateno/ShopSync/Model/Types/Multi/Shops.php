<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Mulit-Shop Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Multi_Shops extends Varien_Object
{
	/**
	 * @var int
	 */
	public $ID;

	/**
	 * @var string
	 */
	public $Name;

	public function __construct($inId = 0, $inName = null)
	{
		$this->ID = $inId;
		$this->Name = $inName;
	}
}
