<?php

$installer = $this;

$installer->startSetup();

$installer->addAttribute('customer', 'shopsync_last_updated', array(
    'type'                    => 'datetime',
    'label'                 => 'Shopsync Last Updated',
    'input'                 => 'date',
    'global'                => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_GLOBAL,
    'visible'               => false,
    'required'          => false,
    'user_defined'    => false,
    'default'               => 0
));

$installer->endSetup();
