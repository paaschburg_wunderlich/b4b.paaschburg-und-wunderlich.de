<?php
/**
 * Magento
 *
 * NOTICE OF LICENSE
 *
 * This source file is subject to the Open Software License (OSL 3.0)
 * that is bundled with this package in the file LICENSE.txt.
 * It is also available through the world-wide-web at this URL:
 * http://opensource.org/licenses/osl-3.0.php
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 * to license@magentocommerce.com so we can send you a copy immediately.
 *
 * DISCLAIMER
 *
 * Do not edit or add to this file if you wish to upgrade Magento to newer
 * versions in the future. If you wish to customize Magento for your
 * needs please refer to http://www.magentocommerce.com for more information.
 *
 * @category    Mage
 * @package     Mage_Catalog
 * @copyright   Copyright (c) 2010 Magento Inc. (http://www.magentocommerce.com)
 * @license     http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
 */

/**
 * Catalog image helper
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */

class Toybanana_ExtImages_Helper_Image
{
	 
	/**
    * Product name needed for image name
    */
	protected $_objName;
	
	/**
    * Image URL to pass to cURL
    */
	protected $_imageUrl;
	
	/**
    * System path to media directory
    */
	protected $_mediaRoot;
	
	/**
    * Path to image dir from $_mediaRoot
    * i.e. /c/o/
    */
	protected $_imageRoot;
	
	/**
    * Image name
    */
	protected $_imageName;
	
	protected $_curlTimeout;
	
	protected $_curlMaxRedir;
	
	protected $_curlConnectTimeout;
	
	/**
    * Magento URL path to media
    */
	protected $_baseUrl;
	
	/**
    * path to image from $_mediaRoot
    * i.e. /c/o/imagename.jpg
    */
	protected $_imagePath;
	
	
	
	protected function _reset()
	{
		$this->_objName = null;
		$this->_imageUrl = null;
		$this->_mediaRoot = null;
		$this->_imageRoot = null;
		$this->_imageName = null;
		$this->_curlTimeout = null;
		$this->_curlMaxRedir = null;
		$this->_baseUrl = null;
		$this->imagePath = null;
		return $this;
	}
	
	public function initProd($productName, $imageUrl)
	{
		$this->_reset();
		$this->setObjName($productName);
		$this->setImageUrl($imageUrl);
		$this->setMediaRoot(Mage::getBaseDir('media').'/catalog/product');
		$this->setImageName($this->getObjName(), $this->getImageUrl());
		$this->setImageRoot(DS . substr(str_replace(' ','',strtolower($this->getImageName())),0,1) . DS . substr(str_replace(' ','',strtolower($this->getImageName())),1,1) . DS);
		$this->setCurlConnectTimeout((Mage::getStoreConfig('ExtImages/curl/curlconntimeout')) ? Mage::getStoreConfig('ExtImages/curl/curlconntimeout') : '1');
		$this->setCurlTimeout((Mage::getStoreConfig('ExtImages/curl/curltimeout')) ? Mage::getStoreConfig('ExtImages/curl/curltimeout') : '3');
		$this->setCurlMaxRedir((Mage::getStoreConfig('ExtImages/curl/curlmaxredir')) ? Mage::getStoreConfig('ExtImages/curl/curlmaxredir') : '5');
		$this->setBaseUrl(Mage::getStoreConfig('ExtImages/general/baseurl'));
		return $this;
		
	}
	
	public function initCat($categoryName, $imageUrl)
	{
		$this->_reset();
		$this->setObjName($categoryName);
		$this->setImageUrl($imageUrl);
		$this->setMediaRoot(Mage::getBaseDir('media').'/catalog/category/');
		$this->setImageName($this->getObjName(), $this->getImageUrl());
		//$this->setImageRoot(DS . substr(str_replace(' ','',strtolower($this->getImageName())),0,1) . DS . substr(str_replace(' ','',strtolower($this->getImageName())),1,1) . DS);
		$this->setCurlConnectTimeout((Mage::getStoreConfig('ExtImages/curl/curlconntimeout')) ? Mage::getStoreConfig('ExtImages/curl/curlconntimeout') : '1');
		$this->setCurlTimeout((Mage::getStoreConfig('ExtImages/curl/curltimeout')) ? Mage::getStoreConfig('ExtImages/curl/curltimeout') : '3');
		$this->setCurlMaxRedir((Mage::getStoreConfig('ExtImages/curl/curlmaxredir')) ? Mage::getStoreConfig('ExtImages/curl/curlmaxredir') : '5');
		$this->setBaseUrl(Mage::getStoreConfig('ExtImages/general/baseurl'));
		return $this;
		
	}
	
	public function curl_get_image()
	{
		$image = null;
		try
		{
			if (!is_null($this->getImageUrl()) && !is_null($this->getObjName())) :
				if ($this->getBaseUrl()):
					$this->setImageUrl($this->getBaseUrl() . $this->getImageUrl());
				endif;
				# fetch remote image.
                                $local_filepath = $this->getMediaRoot().$this->getImageRoot().$this->getImageName();
                                $download_image = true;
					$image = $this->getImageRoot().$this->getImageName();
                                if(file_exists($local_filepath)):
                                       $download_image = false;
                                endif;
                                if($download_image == true):

				$this->setImagePath($local_filepath);
				$purl = parse_url($this->getImageUrl());
				if(isset($purl['scheme']) && ($purl['scheme'] == 'http' || $purl['scheme'] == 'https')):
					
					$data = @file_get_contents($this->getImageUrl());
					if ($data !== null && $data !== false) {
						mkdir(dirname($this->getImagePath()), 0755, true);
						file_put_contents($this->getImagePath(), $data);
					}

					if(file_exists($this->getImagePath()) == false):
						Mage::log('External Image Download Failed for URL: ' . $this->getImageUrl() . ' to location: '. $this->getImagePath());
						return null;
					endif;
				endif;
				endif;
			endif;
		}
		catch (Exception $e)
		{
			Mage::log($e);
		}
		return $image;
	}
	
	protected function setObjName($objName)
	{
		$this->_objName = $objName;
		return $this;
	}
	
	protected function setImageUrl($imageUrl)
	{
		$this->_imageUrl = $imageUrl;
		return $this;
	}
	
	protected function setMediaRoot($mediaRoot)
	{
		$this->_mediaRoot = $mediaRoot;
		return $this;
	}
	
	protected function setImageRoot($imageRoot)
	{
		$this->_imageRoot = $imageRoot;
		return $this;
	}
	
	protected function setCurlTimeout($curlTimeout)
	{
		$this->_curlTimeout = $curlTimeout;
		return $this;
	}
	
	protected function setCurlConnectTimeout($curlConnectTimeout)
	{
		$this->_curlConnectTimeout = $curlConnectTimeout;
		return $this;
	}
	
	protected function setCurlMaxRedir($curlMaxRedir)
	{
		$this->_curlMaxRedir = $curlMaxRedir;
		return $this;
	}
	
	protected function setBaseUrl($baseUrl)
	{
		$this->_baseUrl = $baseUrl;
		return $this;
	}
	
	protected function setImageName($objName, $imageUrl)
	{
		$cleanName = iconv('UTF-8', 'ASCII//TRANSLIT', utf8_encode($objName)); //converts utf-8 characters to ?s so we can remove them
		$preImageName = strtolower(str_replace(' ','-',$cleanName));
		$replace=""; //what you want to replace the bad characters with
		$pattern="/([[:alnum:]_\.-]*)/"; //basically all the filename-safe characters
		$bad_chars=preg_replace($pattern,$replace,$preImageName); //leaves only the "bad" characters
		$bad_arr=str_split($bad_chars); //split them up into an array for the str_replace() func.
		$goodImageName = substr(str_replace($bad_arr,$replace,$preImageName),0,50);
		$this->_imageName = $goodImageName . '-' . md5($imageUrl) . '.jpg';
		return $this;
	}
	
	protected function setImagePath($imagePath)
	{
		$this->_imagePath = $imagePath;
		return $this;
	}
	
	protected function getObjName()
	{
		return $this->_objName;
	}
	
	protected function getImageUrl()
	{
		return $this->_imageUrl;
	}
	
	protected function getMediaRoot()
	{
		return $this->_mediaRoot;
	}
	
	protected function getImageRoot()
	{
		return $this->_imageRoot;
	}
	
	protected function getCurlTimeout()
	{
		return $this->_curlTimeout;
	}
	
	protected function getCurlConnectTimeout()
	{
		return $this->_curlConnectTimeout;
	}
	
	protected function getCurlMaxRedir()
	{
		return $this->_curlMaxRedir;
	}
	
	protected function getBaseUrl()
	{
		return $this->_baseUrl;
	}
	
	protected function getImageName()
	{
		return $this->_imageName;
	}
	
	protected function getImagePath()
	{
		return $this->_imagePath;
	}

}
