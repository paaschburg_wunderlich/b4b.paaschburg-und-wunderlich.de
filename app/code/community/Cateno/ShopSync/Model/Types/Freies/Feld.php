<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung freier Felder in den Shop
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Freies_Feld extends Varien_Object
{
	/**
	 * @var string
	 */
	public $FeldWarenwirtschaft;

	/**
	 * @var string
	 */
	public $FeldShop;

	/**
	 * @var string
	 */
	public $Wert;

	/**
	 * @var string
	 */
	public $Sprache;

	/**
	 * @var string
	 */
	public $Name;
    
	/**
	 * @var string
	 */
	public $FeldArt;

	public function __construct($inFeldShop = '', $inWert = '')
	{
		$this->FeldShop = $inFeldShop;
		$this->FeldWarewirtschaft = '';
		$this->Name = '';
		$this->Sprache = '';
                                    $this->FeldArt= '';
		$this->Wert = $inWert;
	}
}
