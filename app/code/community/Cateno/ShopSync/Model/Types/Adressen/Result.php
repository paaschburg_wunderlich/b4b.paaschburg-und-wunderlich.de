<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Adressen-Rückgabewerten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Adressen_Result
{
	/**
	 * @var string
	 */
	public $AdrNr;

	/**
	 * Magento Customer shopsync_customer_id
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var Cateno_ShopSync_Model_Types_Anschriften_Result
	 */
	public $Anschriften;

	public function __construct($inWShopID = '', $inAdrNr = '', $inAnschriften)
	{
		$this->WShopID = $inWShopID;
		$this->AdrNr = $inAdrNr;
		$this->Anschriften = $inAnschriften;
	}
}
