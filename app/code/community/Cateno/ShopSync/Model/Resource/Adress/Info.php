<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Adress-Infos
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Adress_Info
{
	/**
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var string
	 */
	public $LetzteAenderung;
}
