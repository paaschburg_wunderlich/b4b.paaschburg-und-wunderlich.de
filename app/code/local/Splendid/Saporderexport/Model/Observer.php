<?php
class Splendid_Saporderexport_Model_Observer {

	/**
	 * Bestellung für SAP in Textfile exportieren
	 */

	public function exportOrderToSAP($event) {
		$order = $event->getOrder();

		$path = Mage::getBaseDir('media').'/erp-tool/saporderexport/export/';
		$ourFileName = $path.'order_'.$order->getIncrementId().'.txt';
		$ourFileHandle = fopen($ourFileName, 'w');

		$logPath = Mage::getBaseDir('media').'/saporderexport/log/';
		$logFileName = $logPath.'orderlog.txt';
		$logFileHandle = fopen($logFileName, 'a+');

		$helper = Mage::Helper('splendid_saporderexport');

		if($ourFileHandle) {
			$customer = Mage::getModel('customer/customer');
			$customer->setWebsiteId(Mage::app()->getStore()->getWebsiteId());
			$customer->loadByEmail($order->getCustomer_email());

			$billingAddress = $order->getBillingAddress();
			$shippingAddress = $order->getShippingAddress();

			$items = $order->getAllVisibleItems();
			$counter = 0;
			$outItems = '"DocumentLines": [';
			foreach($items as $item) {
				$outItems.= '{';
				$outItems.= '"LineNum": '.$counter.',';
				$outItems.= '"ItemCode": "'.$item->getSku().'",';
				$outItems.= '"ItemDescription": "'.$item->getName().'",';
				$outItems.= '"Quantity": '.$item->getQty_ordered().',';
				$outItems.= '"Text": "'.$item->getStatus().'",';
				//$outItems.= '"Price": '.$item->getPrice().',';
				//$outItems.= '"PriceAfterVAT": '.$item->getPrice_incl_tax().',';
				$outItems.= '"Currency": "'.$order->getBase_currency_code().'",';
				$outItems.= '"ShippingMethod": "'.$order->getShipping_description().'",';
				$outItems.= '"DiscountPercent": '.$item->getDiscount_percent();
				$outItems.= '},';
				$counter++;
			}
			$outItems.= ']';
			$outItems = str_replace('},]', '}],', $outItems);

			$outCustomer = '"customer": [{';
			$outCustomer.= '"group_id":"'.Mage::getModel('customer/group')->load($customer->getGroup_id())->getCustomerGroupCode().'",';
			$outCustomer.= '"prefix":"'.$customer->getPrefix().'",';
			$outCustomer.= '"firstname":"'.$customer->getFirstname().'",';
			$outCustomer.= '"lastname":"'.$customer->getLastname().'",';
			$outCustomer.= '"suffix":"'.$customer->getSuffix().'",';
			$outCustomer.= '"dob":"'.$customer->getDob().'",';
			$outCustomer.= '"steuernummer":"'.$customer->getTaxvat().'",';
			$outCustomer.= '"gender":"'.$helper->getGender($customer->getGender()).'",';
			$outCustomer.= '"customer_activated":"'.$helper->getActiv($customer->getCustomer_activated()).'",';
			$outCustomer.= '"is_shoppartner":"'.$helper->getActiv($customer->getIs_shoppartner()).'",';
			$outCustomer.= '"erp_customernumber":"'.$customer->getErp_customernumber().'",';
			$outCustomer.= '"store_lat":"'.$customer->getStore_lat().'",';
			$outCustomer.= '"store_lng":"'.$customer->getStore_lng().'",';
			$outCustomer.= '"store_web":"'.$customer->getStore_web().'",';
			$outCustomer.= '"store_description":"'.$customer->getStore_description().'",';
			$outCustomer.= '"sp_invoicenumber":"'.$customer->getSp_invoicenumber().'",';
			$outCustomer.= '"sms_fahrzeug_id":"'.$customer->getSms_fahrzeug_id().'",';

			$billingId = $customer->getDefaultBilling();
			$billing = Mage::getModel('customer/address')->load($billingId);
			$billingStreet = $billing->getStreet();

			$outCustomer.= '"billing":[{';
			$outCustomer.= '"city":"'.$billing->getCity().'",';
			$outCustomer.= '"company":"'.$billing->getCompany().'",';
			$outCustomer.= '"country_id":"'.$billing->getCountry_id().'",';
			$outCustomer.= '"fax":"'.$billing->getFax().'",';
			$outCustomer.= '"firstname":"'.$billing->getFirstname().'",';
			$outCustomer.= '"lastname":"'.$billing->getLastname().'",';
			$outCustomer.= '"middlename":"'.$billing->getMiddlename().'",';
			$outCustomer.= '"postcode":"'.$billing->getPostcode().'",';
			$outCustomer.= '"prefix":"'.$billing->getPrefix().'",';
			$outCustomer.= '"street":"'.$billingStreet[0].' '.$billingStreet[1].'",';
			$outCustomer.= '"suffix":"'.$billing->getsuffix().'",';
			$outCustomer.= '"telephone":"'.$billing->getTelephone().'",';
			$outCustomer.= '"vat_id":"'.$billing->getVat_id().'",';
			$outCustomer.= '"default_billing":"1"';
			$outCustomer.= '}],';

			$shippingId = $customer->getDefaultShipping();
			$shipping = Mage::getModel('customer/address')->load($shippingId);
			$shippingStreet = $billing->getStreet();

			$outCustomer.= '"shipping":[{';
			$outCustomer.= '"city":"'.$shipping->getCity().'",';
			$outCustomer.= '"company":"'.$shipping->getCompany().'",';
			$outCustomer.= '"country_id":"'.$shipping->getCountry_id().'",';
			$outCustomer.= '"fax":"'.$shipping->getFax().'",';
			$outCustomer.= '"firstname":"'.$shipping->getFirstname().'",';
			$outCustomer.= '"lastname":"'.$shipping->getLastname().'",';
			$outCustomer.= '"middlename":"'.$shipping->getMiddlename().'",';
			$outCustomer.= '"postcode":"'.$shipping->getPostcode().'",';
			$outCustomer.= '"prefix":"'.$shipping->getPrefix().'",';
			$outCustomer.= '"street":"'.$shippingStreet[0].' '.$shippingStreet[1].'",';
			$outCustomer.= '"suffix":"'.$shipping->getSuffix().'",';
			$outCustomer.= '"telephone":"'.$shipping->getTelephone().'",';
			$outCustomer.= '"vat_id":"'.$shipping->getVat_id().'",';
			$outCustomer.= '"default_shipping":"1"';
			$outCustomer.= '}]';

			$outCustomer.= '}]';

			$out = '{';
			$out.= '"DocNum": "'.$order->getIncrementId().'",';
			$out.= '"DocDate": "'.date('Y-m-d').'",';
			$out.= '"DocDueDate": "'.date('Y-m-d').'",';
			$out.= '"CardCode": "'.$customer->getKundennummer().'",';
			$out.= '"ShoppartnerId": "'.$order->getShoppartner_id().'",';
			$out.= '"ShoppartnerKdnr": "'.$order->getShoppartner_kdnr().'",';
			$out.= '"CardName": "'.$customer->getLastname().'",';
			$out.= '"Address": "'.$billingAddress->getStreet1().' '.$billingAddress->getPostcode().' '.$billingAddress->getCity().'",';
			//$out.= '"DocTotal": '.$order->getBase_grand_total().',';
			$out.= '"DocCurrency": "'.$order->getBase_currency_code().'",';
			$out.= '"Comments": "'.$order->getOrder_kommentar().'",';
			$out.= '"PaymentMethod": "'.$order->getPayment()->getMethodInstance()->getTitle().'",';
			$out.= '"DocTime": "'.date('H:i:s').'",';
			$out.= $outItems;
			$out.= $outCustomer;
			$out.= '}';
			// txt-Datei mit Bestellung erstellen
			fwrite($ourFileHandle, $out);
			fclose($ourFileHandle);

			// Log schreiben
			if($logFileHandle) {
				$logOut = date('Y-m-d H:i:s').' -- '.$order->getIncrementId().chr(10);
				fwrite($logFileHandle, $logOut);
				fclose($logFileHandle);
			} else {
				// Fehlerbehandlung wenn Log nicht geschrieben werden kann
				$recipients = 'michael.kahl@splendid-internet.de';
				$subject = date('Y-m-d H:i:s').' - SAP Orderlog könnte nicht geschrieben werden';
				$text = date('Y-m-d H:i:s').' - SAP Orderlog könnte nicht für Bestellung '.$order->getIncrementId().' geschrieben werden';
				mail($recipients, $subject, $text, 'From: SAP Orderlog <info@pwonline.de>');
			}
		} else {
			// Fehlerbehandlung wenn Datei nicht geschrieben werden kann
			$recipients = 'michael.kahl@splendid-internet.de';
			$subject = date('Y-m-d H:i:s').' - SAP Orderexport fehlgeschlagen';
			$text = date('Y-m-d H:i:s').' - Der SAP-Export für die Bestellung '.$order->getIncrementId().' ist fehlgeschlagen';
			mail($recipients, $subject, $text, 'From: SAP Orderexport <info@pwonline.de>');
		}
	}
}
