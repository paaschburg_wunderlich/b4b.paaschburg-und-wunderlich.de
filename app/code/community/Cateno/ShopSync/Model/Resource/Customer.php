<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Kunden-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Customer
{
	/**
	 * getCustomer by shopsync_customer_id
	 * @param int $inShopsyncCustomerId
	 */
	public function getCustomerByShopsyncCustomerId($inShopsyncCustomerId)
	{
         $theCustomerCollection = Mage::getModel('customer/customer')
         	->getCollection()
         	->addAttributeToSelect('shopsync_customer_id')
         	->addAttributeToSelect('shopsync_last_updated')
         	->addFieldToFilter('shopsync_customer_id', array('eq' => $inShopsyncCustomerId));

         if(count($theCustomerCollection)) {
         	foreach($theCustomerCollection as $theCustomer) {
         		return Mage::getModel('customer/customer')->load($theCustomer->getId());
         	}
         }
         return 0;
	}
}
