<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 22.04.2016
 * Time: 08:25
 */
$start = microtime(true);

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

$class = NEW StendelRebuildImages();

error_reporting(1);

define ('OUTPUT_MESSAGES', true);
define ('TABLE_BASE_IMAGES', 'stendel_images_base');
define ('TABLE_DIV_IMAGES', 'stendel_images_div');
define ('TABLE_CHANGED_IMAGES', 'stendel_images_changed');
$checkSumFile = Mage::getBaseDir() . '/abfragen/refresh_images_checksum.txt';

/** Including the Helper for External Images Extension */
$externalImageHelper = Mage::helper('ExtImages/Image');

/** Checking runtime environment to select source folder */
if($_SERVER['SERVER_ADDR'] == '127.0.0.1') {
    $imageFolder = 'D:/xampp/htdocs/brands4bikes_cz.local/media/teaser/';
    echo '<pre>';
} else {
    $imageFolder = '/home/brands4bikeseu/www/images/';
}

/** Create DB Resources */
$coreResource = Mage::getSingleton('core/resource');
$dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
$dbWrite = Mage::getSingleton('core/resource')->getConnection('core_write');

/** Prepare necessary tables */
$class->createTable($dbWrite, TABLE_BASE_IMAGES);
$message = "Table [" . TABLE_BASE_IMAGES. "] prepared.";
$class->printMessage($message);

$class->createTable($dbWrite, TABLE_DIV_IMAGES);
$message = "Table [" . TABLE_DIV_IMAGES. "] prepared.";
$class->printMessage($message);

/** If table empty, do a initial filling */
$result = $dbRead->query("SELECT * FROM `" . TABLE_BASE_IMAGES . "` LIMIT 1;");
if ($result->rowCount() == 0) {
    $message = "Table [" . TABLE_BASE_IMAGES . "] is empty. Initalizing Folder Content and write to Database.";
    $class->printMessage($message);

    $elements = $class->getDirectoryContent($imageFolder);

    /** Print a message */
    $message = "Filling Table [" . TABLE_BASE_IMAGES . "] with [" . count($elements) . "] rows.";
    $class->printMessage($message);

    /** Write all file info to table 'base' */
    foreach ($elements as $element) {
        $dbWrite->insert(
            $coreResource->getTableName(TABLE_BASE_IMAGES),
            array(
                'image_name'    => $element['name'],
                'image_size'    => $element['size'],
                'image_date'    => $element['date'],
                'image_hash'    => sha1($element['name'] . $element['size'] . $element['date'])
            )
        );
    }
    /** Print a message */
    $message = "*** Script must restart! ***";
    $class->printMessage($message);

    /** Write total folder hash to checksum.txt */
    $class->writeChecksumFile($checkSumFile, sha1(serialize($class->checkFolder($imageFolder, true))));

    /** Get runtime and print it */
    $end = microtime(true);
    $runtime = number_format($end - $start, 2);
    $message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden.\n";
    $class->printMessage($message);

    exit;
}

if($class->readChecksumFile($checkSumFile) != sha1(serialize($class->checkFolder($imageFolder, true)))) {

    /** Print a message */
    $message = "Changes in folder [" . $imageFolder . "] detected.";
    $class->printMessage($message);

    /** Truncate table stendel_check_images */
    $dbWrite->truncate(
        $coreResource->getTableName(TABLE_DIV_IMAGES)
    );

    $elements = $class->getDirectoryContent($imageFolder);

    /** Print a message */
    $message = "Filling Table [" . TABLE_DIV_IMAGES . "] with [" . count($elements) . "] rows.";
    $class->printMessage($message);

    /** Write all file info to table 'checking' */
    foreach ($elements as $element) {
        $dbWrite->insert(
            $coreResource->getTableName(TABLE_DIV_IMAGES),
            array(
                'image_name'    => $element['name'],
                'image_size'    => $element['size'],
                'image_date'    => $element['date'],
                'image_hash'    => sha1($element['name'] . $element['size'] . $element['date'])
            )
        );
    }

    /** Checking div between the two tables and filling arrays */
    $result = $dbRead->query("SELECT * FROM " . TABLE_DIV_IMAGES . " WHERE (image_hash) NOT IN (SELECT image_hash FROM " . TABLE_BASE_IMAGES . ");");

    while ($row = $result->fetch()) {
        $changedImages[] = array(
            'id'            => $row['id'],
            'image_name'    => $row['image_name'],
            'image_size'    => $row['image_size'],
            'image_date'    => $row['image_date'],
            'image_hash'    => $row['image_hash'],
        );
        $collectionGetter[] = array('image_name' => $row['image_name']);
    }

    /** Get Product collection by using image_names */
    $collection = Mage::getModel('catalog/product')->getCollection()
        ->addAttributeToSelect('*')
        ->addAttributeToFilter('image_external_url', array('in' => array($collectionGetter)))
        ->addAttributeToFilter('status', array('eq' => Mage_Catalog_Model_Product_Status::STATUS_ENABLED));

    /** If collection has data */
    if(count($collection->getData()) > 0) {

        /** Walk through all collected Products */
        foreach ($collection as $theProduct) {

            /** Load the Product Object */
            $theProduct->load();

            /** Print a message */
            $message = '[' . $theProduct->getSku() . ' - ' . $theProduct->getName() . '] need a refreshing to get changed images.';
            $class->printMessage($message);

            /** Delete various Image of the Product */
            $theImageRootPath = Mage::getBaseDir('media') . '/catalog/product/';
            $theImageName = $theProduct->getImage();
            $class->deleteImages($theImageRootPath, $theImageName);

            /** Set Product Position in Category to 0 for refreshing the first Category Page */
            $theCategory = $theProduct->getCategoryIds();
            $class->setPosition($theCategory[0], $theProduct->getId(), 0);

            /** Load the Category via Url with curl to initiate Image refresh */
            $class->getCurlUrl(Mage::getModel("catalog/category")->load($theCategory[0])->getUrl());

            /** Load the Category via Url with curl to initiate Image refresh */
            $class->getCurlUrl(Mage::getModel("catalog/category")->load($theCategory[0])->getUrl());

            /** Set Product Position in Category back to 1 */
            $class->setPosition($theCategory[0], $theProduct->getId(), 1);

        }
    }

    /** Write total folder hash to checksum.txt */
    $class->writeChecksumFile($checkSumFile, sha1(serialize($class->checkFolder($imageFolder, true))));

    /** Drop table 'base' */
    $dbWrite->query("DROP TABLE `". TABLE_BASE_IMAGES. "`;");

    /** Rename table 'div' to 'base' for the next run */
    $dbWrite->query("RENAME TABLE `". TABLE_DIV_IMAGES . "` TO `" . TABLE_BASE_IMAGES . "`");

    /** Recreate table 'div' for the next run */
    $class->createTable($dbWrite, TABLE_DIV_IMAGES);

    /** Get runtime and print it */
    $end = microtime(true);
    $runtime = number_format($end - $start, 2);
    $message = "Runtime [" . number_format($end - $start, 2) . "] Sec.\n";
    $class->printMessage($message);

} else {
    /** Print a message */
    $message = "nothing to do...";
    $class->printMessage($message);
}


class StendelRebuildImages
{

    /**
     * @param $dbWrite
     * @param $tableName
     * @return bool
     */
    public function createTable($dbWrite, $tableName)
    {
        $dbWrite->query("CREATE TABLE IF NOT EXISTS `" . $tableName . "` (
              `id` int(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
              `image_name` varchar(255) NOT NULL,
              `image_size` int(12) NOT NULL,
              `image_date` varchar(24) NOT NULL,
              `image_hash` varchar(64) NOT NULL
            ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
        return true;
    }


    /**
     * @param $folder
     * @return array
     */
    public function getDirectoryContent($folder)
    {
        $dirContent = scandir($folder);
        foreach($dirContent as $fileName){
            if($fileName != "." && $fileName != "..") {
                if(is_file($folder . $fileName)) {
                    $elements[] = array (
                        'name'   => $fileName,
                        'size'   => $this->getFileSize($folder . $fileName),
                        'date'   => $this->getFileDate($folder . $fileName)
                    );
                }
            }
        }
        return $elements;
    }


    /**
     * @param $fileName
     * @return int
     */
    public function getFileSize($fileName)
    {
        return filesize($fileName);
    }


    /**
     * @param $fileName
     * @return int
     */
    public function getFileDate($fileName)
    {
        return filectime($fileName);
    }


    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        if(OUTPUT_MESSAGES == true) {
            echo "[" . date('Y-m-d H:i:s') . "] " . $message . "\n";
        }
    }


    /**
     * @param $path
     */
    public function writeChecksumFile($path, $checksum)
    {
        $fp = fopen($path, 'w');
        fwrite($fp, $checksum);
        fclose($fp);
    }


    /**
     * @param $path
     * @return string
     */
    public function readChecksumFile($path)
    {
        $fp = fopen($path, 'r');
        $content = fread($fp, 256);
        fclose($fp);
        return $content;
    }


    /**
     * @param $path
     * @param bool $recursive
     * @return array
     */
    function checkFolder($path, $recursive = false)
    {
        $result = array();
        if (is_dir($path) === true) {
            $path = $this->getPath($path);
            $files = array_diff(scandir($path), array('.', '..'));

            foreach ($files as $file) {
                if (is_dir($path . $file) === true) {
                    $result[$file] = ($recursive === true) ? $this->checkFolder($path . $file, $recursive) : $this->checkSize($path . $file, true);
                } else if (is_file($path . $file) === true) {
                    $result[$file] = $this->checkSize($path . $file);
                }
            }
        } else if (is_file($path) === true) {
            $result[basename($path)] = $this->checkSize($path);
        }
        return $result;
    }


    /**
     * @param $path
     * @param bool $recursive
     * @return int|string
     */
    function checkSize($path, $recursive = true)
    {
        $result = 0;

        if (is_dir($path) === true) {
            $path = Path($path);
            $files = array_diff(scandir($path), array('.', '..'));

            foreach ($files as $file) {
                if (is_dir($path . $file) === true) {
                    $result += ($recursive === true) ? Size($path . $file, $recursive) : 0;
                } else if (is_file() === true) {
                    $result += sprintf('%u', filesize($path . $file));
                }
            }
        }
        else if (is_file($path) === true) {
            $result += sprintf('%u', filesize($path));
        }
        return $result;
    }


    /**
     * @param $path
     * @return bool|string
     */
    function getPath($path)
    {
        if (file_exists($path) === true) {
            $path = rtrim(str_replace('\\', '/', realpath($path)), '/');
            if (is_dir($path) === true) {
                $path .= '/';
            }
            return $path;
        }
        return false;
    }


    /**
     * @param $url
     * @return mixed
     */
    public function getCurlUrl($url)
    {
        $allStores = Mage::app()->getStores();
        $agent = 'User-Agent: Mozilla/5.0 (Windows NT 5.1; rv:23.0) Gecko/20100101 Firefox/45.0';

        $scriptName = $_SERVER['SCRIPT_NAME'];
        $url = preg_replace("[$scriptName/]", "", $url);

        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $message = 'Refreshing Category Url [' . Mage::app()->getStore()->getCode() . ' - ' . $url . ']';
        $this->printMessage($message);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_USERAGENT, $agent);
        curl_setopt($ch, CURLOPT_MUTE, false);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_exec($ch);

        foreach ($allStores as $eachStoreId => $val) {
            Mage::app()->setCurrentStore($eachStoreId);
            $message = 'Refreshing Category Url [' . Mage::app()->getStore()->getCode() . ' - ' . $url . ']';
            $this->printMessage($message);

            $ch = curl_init();
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_USERAGENT, $agent);
            curl_setopt($ch, CURLOPT_MUTE, false);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_exec($ch);
        }
    }


    /**
     * @param $categoryId
     * @param $productId
     * @param $newPosition
     */
    public function setPosition($categoryId, $productId, $newPosition)
    {
        $category = Mage::getModel('catalog/category')->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)->load($categoryId);
        $myProduct = $category->getProductsPosition();
        $myProduct[$productId] = $newPosition;
        $category->setPostedProducts($myProduct);
        $category->save();
    }


    /**
     * @param $theImageRootPath
     * @param $theImageName
     */
    public function deleteImages($theImageRootPath, $theImageName)
    {
        array_map('unlink', glob($theImageRootPath . $theImageName));
        $message = 'Image [' . $theImageName . '] successful deleted';
        $this->printMessage($message);
        $thePlaceholder = '*';
        for ($i = 0; $i <= 4; $i++) {
            array_map('unlink', glob($theImageRootPath . $thePlaceholder . $theImageName));
            $message = 'Image [' . $thePlaceholder . $theImageName . '] successful deleted';
            $this->printMessage($message);

            $thePlaceholder .= '/*';
        }
    }

}





