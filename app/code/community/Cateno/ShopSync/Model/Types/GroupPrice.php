<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von kundengruppenspezifischen Preisinformationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_GroupPrice extends Varien_Object
{
	/**
	 * @var int
	 */
	public $WebSiteID;
    /**
	 * @var int
	 */
	public $Nr;

    /**
	 * @var string
	 */
	public $GroupName;

	/**
	 * @var float
	 */
	public $Price;

	public function __construct($inWebSiteID, $inNr, $inPrice, $inGroupName)
	{
        $this->WebSiteID = $inWebSiteID;
        $this->Nr = $inNr;
        $this->GroupName = $inGroupName;
        $this->Price = $inPrice;
    }
}
