jQuery(document).ready(function(){

	/**
	 * smoothScroll
	 */
	
	if (jQuery("a[rel='jumper']").size() != 0) {
		jQuery("a[rel='jumper']").each(function() {
			jQuery(this).click(function() {
				var sTarget = jQuery(this).attr('href');
				var sPos = sTarget.indexOf('#');
				sTarget = sTarget.substring(sPos);
				jQuery('html,body').animate({scrollTop:jQuery(sTarget).offset().top},500);
				return false;
			});
		})
	}
	
	/**
	 * show-cart 
	 */
	
	if (jQuery('.header .block-cart').size() != 0) {
		jQuery('.header .block-cart').mouseenter(function(){
			jQuery(this).children('.cart-content').css({display:'block'}).parent().addClass('hover');
		}).mouseleave(function(){
			jQuery(this).children('.cart-content').css({display:'none'}).parent().removeClass('hover');
		});
	}
	
	/**
	 * nav-language
	 */
	
	if (jQuery('.nav-language').size() != 0) {
		jQuery('.nav-language').mouseenter(function(){
			jQuery(this).addClass('nav-language-hover');
		}).mouseleave(function(){
			jQuery(this).removeClass('nav-language-hover');
		});
	}
	
	/**
	 * external-link
	 */
	
	if (jQuery('a.external-link').size() != 0) {
		jQuery('a.external-link').attr('target','_blank');
	}
	
	/**
	 * start jcarousel
	 */
	
	if (jQuery('#brands').size() != 0) {
		jQuery('#brands').jcarousel({scroll:1});
	}
	
	jqSlide('.slide-head', '.slide-content', true);
	
	/**
	 * start jqAccordion
	 */
	
	if (jQuery('#narrow-by-list').size() != 0) {
		jqAccordion('#narrow-by-list');
	}
	
});

/**
 * jqAccordion
 * @version 1.0
 * @author AW
 */

function jqAccordion(accEl) {
	var $accContainer = jQuery(accEl);
	var $accHeader = jQuery(accEl+' dt');
	var $accContent = jQuery(accEl+' dd');
	
	// set function class for css
	$accContainer.addClass('jqAccordion');
	
	$accHeader.each(function(index) {
		var $thisEl = jQuery(this);
		// hide all content elements on reload
		$thisEl.next().hide();
		// click Action
		$thisEl.click(function() {
			// hide content elements
			if($thisEl.hasClass('open')) {
				$thisEl.removeClass('open');
				$thisEl.next().slideUp('fast');
			} else {
				// show content elements
				$thisEl.addClass('open');
				$thisEl.next().slideDown('fast');
			}
		});
	});
}

/**
 * jqSlide
 * @version 1.0
 * @author AW
 */

function jqSlide(slideHead, slideContent, useCookie){
	var $headerSet = jQuery(slideHead);
	var $contentSet = jQuery(slideContent);
	
	var cookieName = 'slideCookie';
	var setCookie = function(val) {
		jQuery.cookie(cookieName, val);
	}
	var getCookie = function() {
		return (jQuery.cookie(cookieName) !== undefined) 
			? jQuery.cookie(cookieName) 
			: false;
	}
	var deleteCookie = function() {
		jQuery.cookie(cookieName, false);
	}

	if ($headerSet.size() != 0 && $contentSet.size() != 0) {
		$headerSet.click(function() {
			var $curEl = jQuery(this);
			var $curContent = jQuery(this).next();
			var curIndex = $headerSet.index($curEl);
			
			setCookie(curIndex);
			
			// Hide all active elements
			$headerSet.removeClass('slider-on');
			$contentSet.slideUp('fast');
			
			// Just display the clicked element
			if($curContent.is(':hidden') == true) {
				$curEl.addClass('slider-on');
				$curContent.slideDown('fast');
			} else if ($curContent.is(':visible') == true) {
				deleteCookie();
			}
		});
		
		// Hide all elementens (initial)
		$contentSet.hide();
		// Set element to active, if available via cookie
		if (curIndex = getCookie()) {
			$headerSet.eq(curIndex).addClass('slider-on');
			$contentSet.eq(curIndex).show();
		}
	}
}

/**
 * jQuery Cookie plugin
 *
 * Copyright (c) 2010 Klaus Hartl (stilbuero.de)
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 */

jQuery.cookie = function (key, value, options) {

    // key and at least value given, set cookie...
    if (arguments.length > 1 && String(value) !== "[object Object]") {
        options = jQuery.extend({}, options);

        if (value === null || value === undefined) {
            options.expires = -1;
        }

        if (typeof options.expires === 'number') {
            var days = options.expires, t = options.expires = new Date();
            t.setDate(t.getDate() + days);
        }

        value = String(value);

        return (document.cookie = [
            encodeURIComponent(key), '=',
            options.raw ? value : encodeURIComponent(value),
            options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
            options.path ? '; path=' + options.path : '',
            options.domain ? '; domain=' + options.domain : '',
            options.secure ? '; secure' : ''
        ].join(''));
    }

    // key and possibly options given, get cookie...
    options = value || {};
    var result, decode = options.raw ? function (s) { return s; } : decodeURIComponent;
    return (result = new RegExp('(?:^|; )' + encodeURIComponent(key) + '=([^;]*)').exec(document.cookie)) ? decode(result[1]) : null;
};
