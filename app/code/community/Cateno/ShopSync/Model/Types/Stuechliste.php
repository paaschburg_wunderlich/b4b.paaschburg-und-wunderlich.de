<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Stücklisten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Stueckliste extends Varien_Object
{
	/**
	 * @var string
	 */
	public $ArtNr;

	/**
	 * @var int
	 */
	public $ZeilenNr;

	/**
	 * @var int
	 */
	public $Mge;

	/**
	 * @var float
	 */
	public $EprNt;

	/**
	 * @var float
	 */
	public $EVkOrg;
}
