<?php
class Sms_Neuheiten_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
            $this->loadLayout();
            $this->getLayout()
                ->getBlock('root')
                ->setTemplate('page/2columns-left.phtml');
            $this->renderLayout();
    }
}