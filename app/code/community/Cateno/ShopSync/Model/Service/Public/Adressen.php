<?php
/**
 * Adressen Events for User-Functions
 * Aufruf für Kundenskript
 * @category Single
 * @package Cateno_ShopSync
 *
 * @version 0.1.0
*/
class Cateno_ShopSync_Model_Service_Public_Adressen extends Cateno_ShopSync_Model_Types_Abstract
{
	public function NachAdressenEnde($inZaehler)
	{
	    Mage::dispatchEvent('NachAdressenEnde', array('counter' => $inZaehler));
        return false;
	}
}
