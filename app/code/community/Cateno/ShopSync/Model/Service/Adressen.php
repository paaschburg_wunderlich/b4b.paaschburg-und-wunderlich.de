<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Einstiegspunkt für Adress-Sync
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Service_Adressen
{
	/**
	 * @param array(string) $inWShopIDs(Magento shopsync_customer_id)s
	 * @param Cateno_ShopSync_Model_Types_Freies_Feld $inFreieFelder
	 * @param Cateno_ShopSync_Model_Types_Freies_Feld_Anschriften $inFreieFelderAnschriften
	 * @return array(Cateno_ShopSync_Model_Types_Adresse)
	 */
	public function getAdressen($inWShopIDs, $inFreieFelder, $inFreieFelderAnschriften)
	{
		$theAdresseArray = array();
		foreach($inWShopIDs as $theCustomerId) {
		 $this->RebuildCusomerData($theCustomerId);
			$theAdresse = new Cateno_ShopSync_Model_Types_Adresse();
			$theCustomer = Mage::getModel('customer/customer')->load($theCustomerId);
			if($theCustomer->getId()) {
				$theAdresse->WShopID = $theCustomerId;
                $theAdresse->FreieFelder = Mage::getModel('shopsync/freies_feld')->setFreieFelder($theCustomer, $inFreieFelder);
		        $theAdresse->Anschriften = Mage::getModel('shopsync/anschriften')->setAnschriften($theCustomer, $inFreieFelderAnschriften);
				$theAdresseArray[] = $theAdresse;
			} else {
			    throw new SoapFault('customer fault (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')','customer id: '.$theCustomerId.' not exist!');
			}
		}
		return $theAdresseArray;
	}

	/**
	 * @param $nurMitBestellung true = nur Adressen von Kunden, die schon einmal bestellt haben
	 * @return array
	 */
	public function getAdressenInfo($nurMitBestellung)
	{
		// die Kunden
        $theCustomerCollection = Mage::getModel('customer/customer')->getCollection()
			->joinAttribute('shopsync_last_updated', 'customer/shopsync_last_updated', 'entity_id', null, 'left');

		if ($nurMitBestellung) {
			// mit der Bestell-Tabelle verknüpfen und Kunden ohne Bestellungen ausschließen
            $theCustomerCollection->getSelect()->joinLeft(
				array('orderTable' => Mage::getSingleton('core/resource')->getTableName('sales/order')),
				'e.entity_id = orderTable.customer_id',
				'entity_id AS order_id')
				->group('e.entity_id')
				->where('orderTable.entity_id > 0');
		}

		foreach ($theCustomerCollection as $theCustomer) {
			if ($theCustomer->getShopsyncLastUpdated() == '') {
				$theAddressInfo[] = array(
					'LetzteAenderung' => $theCustomer->getCreatedAt(),
					'WShopId'         => $theCustomer->getId()
				);
			} elseif ($theCustomer->getShopsyncLastUpdated() < $theCustomer->getUpdatedAt()) {
				$theAddressInfo[] = array(
					'LetzteAenderung' => $theCustomer->getUpdatedAt(),
					'WShopId'         => $theCustomer->getId()
				);
			}
		}

		// als Objekt gekapselt übergeben, um es als Referenz im Observer verändern zu können
        $object = new Varien_Object($theAddressInfo);
		Mage::dispatchEvent('shopsync_after_get_adressen_info', array('addressInfo' => $object));
		$theAddressInfo = $object->getData();

		return $theAddressInfo;
	}

	/**
	 * @param Cateno_ShopSync_Model_Types_Adresse $inAdressen
	 * @param boolean $inNeveShopAdressenEMail
	 * @return array
	 */
	public function setAdressen($inAdressen, $inNeueShopAdressenEMail)
	{
		$theAdressenResult = array();
		foreach($inAdressen as $theAddress) {
			$theAdressenResult[] = $this->setAdresse($theAddress, $inNeueShopAdressenEMail);
		}
		$theCustomerIds = array();
		foreach($theAdressenResult as $theAdresseResult) {
			$theCustomerIds[] = $theAdresseResult->WShopID;
		}
		Mage::dispatchEvent('NachAdressenListe', array('customer_ids' => $theCustomerIds));
		return $theAdressenResult;
	}

	protected function setAdresse($inAdresse, $inNeueShopAdressenEMail)
    {
        //save customer
        $theCustomerID = $inAdresse->WShopID;
        $theCustomerData = array();

        //create or update
        $customerNew = true;
		if(!empty($theCustomerID)) {
			$theCustomer = Mage::getModel('customer/customer')->load($theCustomerID);
			$customerNew = false;
			if (!$theCustomer->getId()) {
				throw new SoapFault('Customer not found (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'Customer ID: '.$theCustomerID.' does not exist');
			} else {
				$theCustomerID = $theCustomer->getId();
			}
		}

		//get freiefelder value
        $theCustomerData = Mage::getSingleton('shopsync/tabellen')->mergeFreiesFeldData($inAdresse->FreieFelder, $theCustomerData);
		$theCustomerData['shopsync_last_updated'] = date("Y-m-d H:i:s",Mage::getModel('core/date')->gmtTimestamp() + 60 );

        $theCustomerApi = Mage::getSingleton('customer/customer_api');
        try {
            if ($customerNew) {
                $theCustomerID = $theCustomerApi->create($theCustomerData);
            } else {
                $theCustomerApi->update($theCustomerID, $theCustomerData);
            }
        } catch(Mage_Api_Exception $apiE) {
			throw new SoapFault('Customer could not be saved (Create = ' . var_export($customerNew, 1). ') (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $apiE->getCustomMessage());
        } catch(Exception $e) {
			throw new SoapFault('Customer could not be saved (Create = ' . var_export($customerNew, 1). ') (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $e->getMessage());
        }

        //save address
        $theAddressResult = array();
        foreach ($inAdresse->Anschriften as $anschrift) {
            $addressNew = true;
            $theAddressData = array();
            $theAddressData = Mage::getSingleton('shopsync/tabellen')->mergeFreiesFeldData($anschrift->FreieFelder ,$theAddressData);
			$theAddressData['shopsync_last_updated'] = date("Y-m-d H:i:s",Mage::getModel('core/date')->gmtTimestamp() + 60 );

            $theAddressApi = Mage::getSingleton('customer/address_api');


            //delete address
            $magentoAddressId = $anschrift->WShopID;

			// addressId='0' is still ok and shouldn't be counted as 'empty'
            if (!empty($magentoAddressId) || $magentoAddressId === 0) {
	        	$theAddressObj = Mage::getModel('customer/address')->load($magentoAddressId);
    			$addressNew = false;
    			if(!$theAddressObj->getId()){
    				throw new SoapFault('Address ID not found (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'Address ID (WShopID): '.$magentoAddressId.' does not exist');
	        	}
    		}

            try{
                if ($addressNew) {
                    $magentoAddressId = $theAddressApi->create($theCustomerID, $theAddressData);
                } else {
                    $theAddressApi->update($magentoAddressId, $theAddressData);
                }
            } catch (Mage_Api_Exception $apiE) {
                throw new SoapFault('Address save error (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')','Customer ID:'.$theCustomerID.' '.$apiE->getCustomMessage());
            } catch(Exception $e) {
                throw new SoapFault('Address save error (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')','Customer ID:'.$theCustomerID.' '.$e->getMessage());
            }

            $theAddressResult[] = new Cateno_ShopSync_Model_Types_Anschriften_Result($magentoAddressId, $anschrift->AnsNr);

            //save billing address and shipping address
            if ($anschrift->StandardReAns) {
                $theCustomerData['default_billing'] = $magentoAddressId;
            }
            if ($anschrift->StandardLiAns) {
                $theCustomerData['default_shipping'] = $magentoAddressId;
            }

            try {
                $theCustomerApi->update($theCustomerID, $theCustomerData);
            } catch (Mage_Api_Exception $apiE) {
                throw new SoapFault('Customer save error (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')','Customer ID:'.$theCustomerID.' '.$apiE->getCustomMessage());
            } catch(Exception $e) {
                throw new SoapFault('Customer save error (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')','Customer ID:'.$theCustomerID.' '.$e->getMessage());
            }

            unset($theAddressData);
        }

        if ($inNeueShopAdressenEMail && $customerNew) {
            $theCustomer = Mage::getModel('customer/customer')->load($theCustomerID);
            $theStoreId = Mage::app()->getStore()->getId();
            if (array_key_exists('password', $theCustomerData) && $theCustomerData['password'] != '') {
            	$theCustomer->setPassword($theCustomerData['password']);
            }

			$theCustomer->sendNewAccountEmail('registered', '', $theStoreId);
        }

        Mage::dispatchEvent('NachAdresse', array('customer_id' => $theCustomerID));
        return new Cateno_ShopSync_Model_Types_Adressen_Result($theCustomerID, $inAdresse->AdrNr, $theAddressResult);

	}

	/**
	 *
	 * @param Cateno_ShopSync_Model_Types_Anschriften_Result $inAdressenResult
	 * @return boolean
	 */
	public function setAdressenAdrNrDatum($inAdressenResult)
	{
		foreach($inAdressenResult as $theAdresseResult) {
			$theCustomer = Mage::getModel('customer/customer')->load($theAdresseResult->WShopID);
			if ($theCustomer->getId()){
				$theCustomer->setData('shopsync_last_updated', Mage::getModel('core/date')->gmtDate());
				$theCustomer->save();
			} else {
				throw new SoapFault('Customer not found (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'Customer with ID (WShopID):'.$theAdresseResult->WShopID.' does not exist');
			}
		}
		return true;
	}

	private function RebuildCusomerData($customerId)
                {
              
              $customer = Mage::getModel('customer/customer')->load($customerId);
              $addresses = $customer->getAddressesCollection();
              foreach ($addresses as $address)
                    {
                    $this->MergeStreets($address);
                    $address->save();
                    
                    }
                    
                    }
                
                 private function MergeStreets($address)
                         {
                       if(strpos($address['street'], "\n") !== FALSE) 
                {
                             $streets = explode("\n", $address['street']);
                             $address['street'] = $streets[0] . " " . $streets[1];
                }

                         }

    /**
     * @param array $inZaehler
     * @return array
     */
    public function NachAdressenEnde($inZaehler)
    {
        return Mage::getSingleton('shopsync_service/public_adressen')->NachAdressenEnde($inZaehler);
    }
}
