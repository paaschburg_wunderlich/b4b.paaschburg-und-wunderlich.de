<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 22.04.2016
 * Time: 08:25
 */
$start = microtime(true);

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

require_once __DIR__ . '/inc/functions.inc.php';
$class = NEW B4b_Abfragen();

$message = "*** START ***";
$class->printMessage($message, $logFileHandle);

$fileRowCount = 0;

ini_set('memory_limit','2G');
ini_set('display_errors', '1');
error_reporting(1);

define ('IMPORT_PATH', Mage::getBaseDir('media') . '/erp-tool/');
define ('IMPORT_FILE', 'update_lagermengen.sdf');

define ('LOG_PATH', Mage::getBaseDir() . '/var/log/');
define ('LOG_FILE', 'test.log');
define ('WRITE_LOGFILE', false);

define ('TABLE_ACTIVE', 'stendel_active_products');
define ('TABLE_EXPORT', 'stendel_export_products');

if(WRITE_LOGFILE == true) {
    $logFileHandle = $class->openFile(LOG_PATH, LOG_FILE, 'a');
} else { $logFileHandle = NULL; }

/** Create DB Resources */
$coreResource = Mage::getSingleton('core/resource');
$dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
$dbWrite = Mage::getSingleton('core/resource')->getConnection('core_write');

createTable($dbWrite, TABLE_ACTIVE);
createTable($dbWrite, TABLE_EXPORT);

$dbWrite->truncate(
    $coreResource->getTableName(TABLE_ACTIVE)
);

$dbWrite->truncate(
    $coreResource->getTableName(TABLE_EXPORT)
);

$productsCollection = Mage::getModel('catalog/product')->getCollection()
//    ->setPageSize(150)
    ->setOrder('sku', 'ASC')
    ->addAttributeToFilter('status', array('eq' => 1));

foreach ($productsCollection as $myProduct) {
    $dbWrite->insert(
        $coreResource->getTableName(TABLE_ACTIVE),
        array(
            'sku'    => $myProduct->getSku(),
    'qty'    => $myProduct->getStockItem()->getQty()
        )
    );
}
$message = "Es wurden [" . count($productsCollection) . "] SKU's in die DB [" . TABLE_ACTIVE . "] geschrieben.";
$class->printMessage($message, $logFileHandle);


if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($data = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {
        $fileRowCount++;
        $dbWrite->insert(
            $coreResource->getTableName(TABLE_EXPORT),
            array(
                'sku'    => $data[0],
        'qty'    => $data[1]
            )
        );
    }
    $message = "Es wurden [" . $fileRowCount . "] SKU's in die DB [" . TABLE_EXPORT . "] geschrieben.";
    $class->printMessage($message, $logFileHandle);
} else {
    $message = "Input-Datei [" . IMPORT_PATH . IMPORT_FILE . "] nicht gefunden!!!";
    $class->printMessage($message, $logFileHandle);
    exit;
}



$end = microtime(true);
$message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden.";
$class->printMessage($message, $logFileHandle);












/**
 * @param $dbWrite
 * @param $tableName
 * @return bool
 */
function createTable($dbWrite, $tableName)
{
    $dbWrite->query("CREATE TABLE IF NOT EXISTS `" . $tableName . "` (
          `id` int(6) NOT NULL AUTO_INCREMENT PRIMARY KEY,
          `sku` varchar(24) NOT NULL,
          `qty` int(6)
        ) ENGINE=InnoDB DEFAULT CHARSET=latin1;");
    return true;
}