<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für Umsatzsteuer-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Ust
{
	/**
	 * @param array $inItem
	 * @return Cateno_ShopSync_Model_Types_Ust
	 */
	public function getUst($inItem)
	{
		$theUst = new Cateno_ShopSync_Model_Types_Ust();
		$theUst->Schluessel = $inItem['Schluessel'];
		$theUst->Satz = $inItem['Satz'];
		$theUst->Bezeichnung = $inItem['Bezeichnung'];
		return $theUst;
	}
}
