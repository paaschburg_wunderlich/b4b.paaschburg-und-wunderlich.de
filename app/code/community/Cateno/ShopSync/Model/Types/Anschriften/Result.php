<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Anschriften-Rückgabewerten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Anschriften_Result
{
	/**
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var int
	 */
	public $AnsNr;

	public function __construct($inWShopID = '', $inAnsNr = 0)
	{
		$this->WShopID = $inWShopID;
		$this->AnsNr = $inAnsNr;
	}
}
