<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Vorgangspositionen-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Vorgang_Position_Var
{
	/**
	 * @var string
	 */
	public $ArtNr;

	/**
	 * @var string
	 */
	public $EPrNt;

	/**
	 * @var string
	 */
	public $OptionsName;

	/**
	 * @var string
	 */
	public $OptionsValue;

	public function __construct($ArtNr = '', $EPrNt = '', $OptionsName = '', $OptionsValue = '')
	{
		$this->ArtNr = $ArtNr;
		$this->EPrNt = $EPrNt;
		$this->OptionsName = $OptionsName;
		$this->OptionsValue = $OptionsValue;
	}
}
