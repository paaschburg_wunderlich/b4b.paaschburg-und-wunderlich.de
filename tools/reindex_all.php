<?php
/**
 * Created by PhpStorm.
 * User: stendel
 * Date: 04.11.2016
 * Time: 09:12
 */

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

$indexCollection = Mage::getModel('index/process')->getCollection();
foreach ($indexCollection as $index) {
    $index->reindexAll();
}