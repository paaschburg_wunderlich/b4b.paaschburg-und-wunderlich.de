<?php
/**
 * Artikel
 *
 * @category Single
 * @package Cateno_ShopSync
 * Einstiegspunkt für Artikel-Sync
 * 
 * @version 0.1.0
 */
class Cateno_ShopSync_Model_Service_Artikel extends Cateno_ShopSync_Model_Types_Abstract
{
	const XML_PATH_CATEGORY_AFFECTED_ALL = 'shopsync/global/category_affected_all';
	const XML_PATH_SHOPBEENDEN_AFFECTAED_ALL = 'shopsync/global/shopbeenden_affected_all';
	//const ATTRIBUTE_CODE_PREFIX = 'shopsync_';
	const PRODUCT_FLAT_DATA = 4;
	const CATALOG_SEARCH_INDEX  = 7;
	const CATALOG_PRODUCT_ASSOCIATION = 6; 
	
	const XML_PATH_QUICK_SEARCH    = 'shopsync/setattribute/quick_search';
	const XML_PATH_ADVANCED_SEARCH = 'shopsync/setattribute/advanced_search';
	const XML_PATH_FRONT_END    = 'shopsync/setattribute/front_end';
	const XML_PATH_NAVIGATION   = 'shopsync/setattribute/navigation';
	const XML_PATH_LAYERD_NAVIGATION = 'shopsync/setattribute/layered_navigation';
	const XML_PATH_CONDITIONS   = 'shopsync/setattribute/conditions';
	const XML_PATH_IS_VISIBLE   = 'shopsync/setattribute/isvisible';
	const XML_PATH_PRODUCT_LIST = 'shopsync/setattribute/product_list';
	const XML_PATH_SORT_PRODUCT_LIST = 'shopsync/setattribute/sort_product_list';
	const XML_PATH_PRO_SET  = 'shopsync/artikel/artikel_set';
	
	/**
	 * Add Attribute with options
	 * 
	 * @param string	$attributeCode	 		Magento attribute code 'shopsync_XXX'
	 * @param string	$attributeType	 		Magento attribute type ['text'|'textarea'|'date'|'boolean'|'multiselect'|'select'|'price']
	 * @param string	$attributeLabel	 		Magento attribute label 
	 * @param array		$attributeOptionArray	optional	Attribute options 
	 *  
	 * @return void
	 */
	public function setAttribute($attributeCode, $attributeType, $attributeLabel, $attributeOptionArray = null)
	{
		// Allowed attribute types
		$allowedType = array (
			'text',            // Einzeiliges Textfeld
            'textarea',        // Mehrzeiliger Textbereich
            'date',            // Datum
            'boolean',        // Ja/Nein
            'multiselect',    // Mehrfach Auswahl
            'select',        // Drop-Down
            'price',        // Preis
			//'media_image',	// Bild
			//'weee',			// Feste Produktsteuer (FPT)
		);

		// check if attributeType is allowed, if not exit
		if(!in_array($attributeType, $allowedType)) {
			throw new SoapFault('Invalid Attribute Type', 'Type "' . $attributeType . '" is not allowed!');
		}

		// set the frontend name for all stores
		$storeCollection = Mage::getModel('core/store')->getCollection();
		$frontendNameArray = array(0 => $attributeLabel);
		foreach ($storeCollection as $store) {
			$frontendNameArray[$store->getStoreId()] = $attributeLabel;
		}
		
		// get the product attribute entity type
		$entityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();

		// try to load the attribute
		$attributeObject = Mage::getModel('catalog/resource_eav_attribute')
			//->loadByCode($entityTypeId, self::ATTRIBUTE_CODE_PREFIX . $attributeCode)
			->loadByCode($entityTypeId, $attributeCode)
			->setEntityTypeId($entityTypeId);
			
		// attribute does not exist, create new one
		if (!$attributeObject->getId()) {
			$attributeObject->setIsUserDefined(1);
			//$attributeObject->setAttributeCode(self::ATTRIBUTE_CODE_PREFIX . $attributeCode);
			$attributeObject->setAttributeCode($attributeCode);
			$attributeObject->setFrontendInput($attributeType);
			
			// set attribute options (defined in system config)
			$attributeObject->setIsSearchable(Mage::getStoreConfig(self::XML_PATH_QUICK_SEARCH));
			$attributeObject->setIsVisibleInAdvancedSearch(Mage::getStoreConfig(self::XML_PATH_ADVANCED_SEARCH));
			$attributeObject->setIsComparable(Mage::getStoreConfig(self::XML_PATH_FRONT_END));
			$attributeObject->setIsFilterable(Mage::getStoreConfig(self::XML_PATH_NAVIGATION));
			$attributeObject->setIsFilterableInSearch(Mage::getStoreConfig(self::XML_PATH_LAYERD_NAVIGATION));
			$attributeObject->setIsUsedForPromoRules(Mage::getStoreConfig(self::XML_PATH_CONDITIONS));
			$attributeObject->setIsVisibleOnFront(Mage::getStoreConfig(self::XML_PATH_IS_VISIBLE));
			$attributeObject->setUsedInProductListing(Mage::getStoreConfig(self::XML_PATH_PRODUCT_LIST));
			$attributeObject->setUsedForSortBy(Mage::getStoreConfig(self::XML_PATH_SORT_PRODUCT_LIST));
		}
		// check if the given type is the same as the actual type, if not exit
		elseif($attributeType != $attributeObject->getFrontendInput()) {
			throw new SoapFault('Wrong Attribute Type', 'Attribute exists but type "' . $attributeType . '" is not correct (Current type is "' . $attributeObject->getFrontendInput() . '")!');
		}
		
		// set the attribute backend type
		$attributeBackendType = $attributeObject->getBackendTypeByInput($attributeType);
		$attributeObject->setBackendType($attributeBackendType);
		
		// set the frontend labels, or exit if none is given
		if (!empty($attributeLabel)) {
			$attributeObject->setFrontendLabel($frontendNameArray);
		} else {
			throw new SoapFault('No frontend label given', 'Frontend label is required');
		}
		
		// set attribute options if we have a select or multiselect attribute
		if (($attributeType == 'select' || $attributeType == 'multiselect') && is_array($attributeOptionArray)) {
			// final option array
			$finalOptionArray = array();

			// load current option collection
			$attributeOptionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
				->setAttributeFilter($attributeObject->getId())
				->setStoreFilter(0, false)
				->load();

			// add/update options
			foreach ($attributeOptionArray as $key => $item) {
				// create $storeOptionArray
				$storeOptionArray = array();
				$isAdminValue = true;
				foreach ($storeCollection as $store) {
					// first value is the admin value
					if ($isAdminValue) {
					
						$storeOptionArray[] = $item->FeldShop;
						$storeOptionArray[$store->getStoreId()] = $item->Wert;
						$isAdminValue = false;
					}
					// other values are for the frontend
					else {
						$storeOptionArray[$store->getStoreId()] = $item->Wert;
					}
				}
				
				// assume we have a new option
				$isNew = true;
				
				// check if we really have a new option, if not update the old one
				foreach ($attributeOptionCollection as $attributeOption) {
					if ($attributeOption->getValue() == $item->FeldShop) {
						$isNew = false;
						$finalOptionArray['order'][$attributeOption->getId()] = $key;
						$finalOptionArray['value'][$attributeOption->getId()] = $storeOptionArray;
					}
				}
				
				// add new option
				if ($isNew) {
					$currentOptionCount = count($finalOptionArray['value']);
					$finalOptionArray['order']['option_' . $currentOptionCount] = $key;
					$finalOptionArray['value']['option_' . $currentOptionCount] = $storeOptionArray;
				}
			}

			// add options to the attribute object
			$attributeObject->setOption($finalOptionArray);
		}
			
		// try to save the attribute and add it ot the default attribute set
		try {
			// save
			$attributeObject->save();
			$setId = Mage::getStoreConfig(self::XML_PATH_PRO_SET);
			// add to default attribute set
			$installerObject = new Mage_Eav_Model_Entity_Setup('core_setup');
			$attributeSetObject = Mage::getModel('eav/entity_attribute_set')
				->setEntityTypeId($entityTypeId);
			
			$installerObject->addAttributeToGroup('catalog_product',$setId, 'Shopsync', $attributeObject->getAttributeCode());
			
			// build the reuqestObject for the event
			$requestObject = array(
				'code' => $attributeCode,
				'type'    => $attributeType,
				'name'    => $attributeLabel,
				'array' => $attributeOptionArray,
			);
			$requestObject = (object)$requestObject;
						
			/*	
			Mage::dispatchEvent('shopsync_set_attribute_after', array(
				'attribute' 	=> $attributeObject, 
				'request'	 	=> $requestObject,
			));
			*/
			
			Mage::dispatchEvent('NachAttribute', array(
				'attribute'    => $attributeObject,
				'request'        => $requestObject,
			));
			
		} catch(Exception $e) {
			throw new SoapFault('setAttribute failed', $e->getMessage());
		}
	}

	/**
	 * No. 1
	 * @param Cateno_ShopSync_Model_Types_Artikel_Liste $inName
	 * @return int
	 */
	public function AddProductsOptions($inName)
	{
		$theEntityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
		$theModel = Mage::getModel('catalog/resource_eav_attribute')->setEntityTypeId($theEntityTypeId);
		
		$theAttributeCode = 'defaultattributecode';
		
		$theNewData = array(
			'attribute_code' => $theAttributeCode,
			'frontend_label' => array($inName),
		);
		
		$theModel->addData($theNewData);
		$theModel->save();
		
		//update code
		//$theModel->setAttributeCode(self::ATTRIBUTE_CODE_PREFIX.$theModel->getId());
		$theModel->setAttributeCode($theModel->getId());
		$theModel->save();
		// TODO
		return $theModel->getId();
	}
	
	/**
	 * No. 2
	 * Enter description here ...
	 */
	public function setProductsOptionsValues()
	{
		// TODO
	}
	
	/**
	 * No. 3
	 * @param Cateno_ShopSync_Model_Types_Artikel_Liste $inArtikelListe
	 * @param $inTabellenLoeschen
	 * @return boolean
	 */
	public function setArtikelListe($inArtikelListe, $inTabellenLoeschen)
	{
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$theHelper = Mage::helper('shopsync/debug');
		$theHelper->theDebugTimeArray = array();
		
		$idx = 0;
		$cnt = count($inArtikelListe);
		$theHelper->addDebugTime('setArtikelListe Begin Time');
		$shopsyncArtikel = Mage::getModel('shopsync/artikel');
		foreach ($inArtikelListe as $theArtikel) {
			$theHelper->addDebugTime(($idx +1).' setArtikel Begin Time');
			$productModel = $shopsyncArtikel->setArtikel($theArtikel,$theHelper);
			# Mage::dispatchEvent('shopsync_set_artikel_after', array('product' => $productModel, 'request_object' => $theArtikel));
			Mage::dispatchEvent('NachArtikel', array('Artikel' => $theArtikel));
			$productCollection[$productModel->getId()] = $productModel;
			$theHelper->addDebugTime(($idx +1).'setArtikel End Time');
			$idx ++;
		}
		# Mage::dispatchEvent('shopsync_set_artikel_liste_after', array('productCollection' => $productCollection, 'request_array' => $inArtikelListe));
		Mage::dispatchEvent('NachArtikelListe', array('productCollection' => $productCollection, 'request_array' => $inArtikelListe));
		
		$theHelper->addDebugTime('setArtikelListe End Time');
		$theHelper->debugeTimeOutput();
	}
	
	public function setArtikelBilder($inPictureList)
	{
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		if (is_array($inPictureList) && !is_object($inPictureList)) {
			foreach ($inPictureList as $pictureObject) {
				$artikelModel = Mage::getModel('shopsync/artikel')->setBilder($pictureObject);
			}
		}
	}

	public function setKategorieBilder($inPictureList)
	{
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		if (is_array($inPictureList) && !is_object($inPictureList)) {
			foreach ($inPictureList as $pictureObject) {
				$kategorieModel = Mage::getModel('shopsync/kategorie')->setBilder($pictureObject);
			}
		}
	}

	/**
	 * No. 4
	 * @param string $inArtNr the Artikel SKU
	 * @param array $inArtikelZusaetze
	 * @return boolean
	 */
	public function setArtikelZusaetze($inArtNr, $inArtikelZusaetze)
	{
		$_typeMap = array(
			'related'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED,
			'up_sell'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_UPSELL,
			'cross_sell'    => Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL,
			'grouped'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED
		);
		
		$typeId = $_typeMap['related'];
		
		$productModel = Mage::getModel('catalog/product');
		$productModel->load($productModel->getIdBySku($inArtNr));
		if (!$productModel->getId()) {
			throw new SoapFault('Incoming product does not exist (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'SKU: ' . var_export($inArtNr, 1));
		}
		
		$link = $productModel->getLinkInstance()->setLinkTypeId($typeId);
		foreach ($inArtikelZusaetze as $inLinkSku) {
			$links[$productModel->getIdBySku($inLinkSku->ArtNr)] = array('position' => 0);
		}

		try {
			$link->getResource()->saveProductLinks($productModel, $links, $typeId);
			return true;
		} catch (Exception $e) {
			throw new SoapFault('Product links could not be saved (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', print_r($e,1));
			return false;
		}
	}
	
	/**
	 * No. 5 
	 * @param array $inKategorien
	 * @return array
	 */
	public function setKategorieListe($inKategorien)
	{
		$theKategorieIds = array();
		foreach ($inKategorien as $theKategorie) {
			$theKategorieObj = Mage::getModel('shopsync/kategorie')->setKategorie($theKategorie);
			$data[$theKategorieObj->getId()] = $theKategorie->Index + 1;
			if (!$theKategorieObj) {
				continue;
			}
			$theKategorieIdObj[] = new Cateno_ShopSync_Model_Types_Kategorie_Id($theKategorie->Nr,$theKategorieObj->getId());
			$theKategorieIds[] = $theKategorieObj->getId();
			# Mage::dispatchEvent('shopsync_set_artikel_kategorie_after', array('category_id' => $theKategorieObj->getId(), 'request_object' => $theKategorie));
			Mage::dispatchEvent('NachArtikelKategorie', array('category_id' => $theKategorieObj->getId(), 'request_object' => $theKategorie));
		}

		Mage::dispatchEvent('NachArtikelKategorieListe', array('category_id_array' => $theKategorieIds, 'request_array' => $inKategorien));
		return $theKategorieIdObj;
	}
	
	/**
	 * No. 6
	 * setProduct Stock by Sku => array
	 */
	public function setLager($inLagerListe)
	{
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		$theObj = Mage::getModel('shopsync/lager');
		foreach ($inLagerListe as $theLager) {
			$theArtikelObj = $theObj->setLager($theLager);
			Mage::dispatchEvent('NachLager', array('Lager' => $theLager));
		}
	}
	
	/**
	 * No. 7
	 * to every product a attribtue 'shopsync_active' default 0;
	 * The following methods are used to clean up the product catalog and can be accessed via the Web:
	 * @return boolean
	 */
	public function ShopBereinigenStarten()
	{
		// Admin-Store setzen
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		
		$theProductIds = Mage::getModel('catalog/product')->getCollection()->getAllIds();
		Mage::getSingleton('catalog/product_action')
			->updateAttributes($theProductIds, array('shopsync_active' => 0), Mage_Core_Model_App::ADMIN_STORE_ID);
		return true;
	}
	
	/**
	 * No. 8
	 * @param string ArtikelListe
	 * @return boolean
	 */
	public function ShopBereinigenArtikelAktuell($inArtikelListe)
	{
		// Admin-Store setzen
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		
		$theProductCollection = Mage::getModel('catalog/product')
			->getCollection()
			->addAttributeToSelect('sku')
			->addAttributeToSelect('shopsync_active')
			->addFieldToFilter('sku', array('in' => $inArtikelListe));

		Mage::getSingleton('catalog/product_action')
			->updateAttributes($theProductCollection->getAllIds(), array('shopsync_active' => 1,'status' => 1), Mage_Core_Model_App::ADMIN_STORE_ID);
		return true;
	}
	
	/**
	 * No. 9
	 * @param boolean $inArtikelSperren
	 * @return boolean
	 */
	public function ShopBereinigenBeenden($inArtikelSperren)
	{
		// Admin-Store setzen
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		
		// holt alle Artikel mit shopsync_active = 0
		$theProductCollection = Mage::getModel('catalog/product')
									->getCollection()
									->addAttributeToSelect('shopsync_active')
									->addAttributeToFilter('shopsync_active', 0);

		$cnt = $theProductCollection->count();

		// alle IDs mit Produkten shopsync_active == 0
		$theProductIds = $theProductCollection->getAllIds();
		
		if ($inArtikelSperren) {
			Mage::getSingleton('catalog/product_action')->updateAttributes($theProductIds, array('status' => 2), Mage_Core_Model_App::ADMIN_STORE_ID);
		} else {
			foreach ($theProductIds as $theproductId) {
				$theDeleteProdcut = Mage::getSingleton('catalog/product')->load($theproductId);
				$theDeleteProdcut->delete();
			}
		}
		return $cnt;
	}
	
	/**
	 * No. 10
	 * @param boolean $inArtikelKategorienSperren
	 * @return boolean
	 */
	public function ArtikelKategorienBereinigen($inArtikelKategorienSperren)
	{
		$theCategoryIds = array();
		$theCategoriesCollection = Mage::getModel('catalog/category')
			->getCollection()
			->addFieldToFilter('entity_id', array('gt' => Mage::app()->getStore()->getRootCategoryId()))
			->addFieldToFilter('parent_id', array('neq' => 1));

		$theResourceModel = Mage::getResourceModel('catalog/category');
		foreach ($theCategoriesCollection as $theCategory) {
			if (!$theResourceModel->getProductCount($theCategory)) { 
//				if($theResourceModel->getChildrenAmount($theCategory)) continue;
				$theCategoryIds[$theCategory->getId()] = (int)$theCategory->getIsActive();
			}
		}
		$isAffectedAll = Mage::getStoreConfig(self::XML_PATH_CATEGORY_AFFECTED_ALL);
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
		switch (Mage::helper('shopsync')->getKategorienBereinigen()) {
			case Cateno_ShopSync_Model_Resource_System_Config_Source_Category::BEREINIGEN_OPTIONS_NO_DECIDE:
				if ($inArtikelKategorienSperren == 1) {
					
					if ($isAffectedAll){
						foreach ($theCategoriesCollection as $theCategory) {
							$theCategory->setIsActive(0);
							$theCategory->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
							$theCategory->save();
						}
					} else {
						foreach ($theCategoryIds as $theCategoryId => $theIsActive) {
							$theCY = Mage::getModel('catalog/category')->load($theCategoryId);
							$theCY->setIsActive(0);
							$theCY->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
							$theCY->save();
						}
					}
				} else {
					try {
						if ($isAffectedAll) {
							foreach ($theCategoriesCollection as $theCategory) {
								Mage::getSingleton('admin/session')->setDeletedPath($theCategory->getPath());
								$theCategory->delete();
							}
						} else {
							foreach ($theCategoryIds as $theCategoryId => $theIsActive) {
								$theDeleteCategory = Mage::getSingleton('catalog/category')->load($theCategoryId);
								Mage::getSingleton('admin/session')->setDeletedPath($theDeleteCategory->getPath());
								$theDeleteCategory->delete();
							}
						}
					} catch(Exception $e ) {
						Mage::log($e->getMessage());
					}
				}
				break;
			case Cateno_ShopSync_Model_Resource_System_Config_Source_Category::BEREINIGEN_OPTIONS_INACTIVE:
				if ($isAffectedAll) {
					foreach($theCategoriesCollection as $theCategory){
						$theCategory->setIsActive(0);
						$theCategory->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
						$theCategory->save();
					}
				} else {
					foreach ($theCategoryIds as $theCategoryId => $theIsActive) {
						$theCY = Mage::getModel('catalog/category')->load($theCategoryId);
						$theCY->setIsActive(0);
						$theCY->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
						$theCY->save();
					}
				}
				break;
			case Cateno_ShopSync_Model_Resource_System_Config_Source_Category::BEREINIGEN_OPTIONS_DELETE:
				if ($isAffectedAll) {
					foreach ($theCategoriesCollection as $theCategory) {
						Mage::getSingleton('admin/session')->setDeletedPath($theCategory->getPath());
						$theCategory->delete();
					}
				} else {
					foreach ($theCategoryIds as $theCategoryId => $theIsActive) {
						$theDeleteCategory = Mage::getSingleton('catalog/category')->load($theCategoryId);
						$theDeleteCategory->delete(Mage_Core_Model_App::ADMIN_STORE_ID);
					}
				}
				break;
		}
		
		return true;
	}
	
	/**
	 * No. 11
	 * @param array $inZaehler
	 * @return array
	 */
	public function NachArtikelEnde($inZaehler)
	{
		return Mage::getSingleton('shopsync_service/public_artikel')->NachArtikelEnde($inZaehler);
	}
    
	/**
	 * No. 12
	 * @param array $inZaehler
	 * @return array
	 */
	public function NachArtikelKategorieEnde($inZaehler)
	{
		return Mage::getSingleton('shopsync_service/public_artikel')->NachArtikelKategorieEnde($inZaehler);
	}
	
	/**
	 * No. 13
	 * @param array $inZaehler
	 * @return array
	 */
	public function NachLagerEnde($inZaehler)
	{
		return Mage::getSingleton('shopsync_service/public_artikel')->NachLagerEnde($inZaehler);
	}

    public function KategorieBereinigenStart()
    {
        $storids = Mage::app()->getStores();
        $rootCategoryIds = array();
        foreach ($storids as $storid) {
            $rootCategoryId = $storid->getRootCategoryId();
                
            if(!in_array($rootCategoryId, $rootCategoryIds))
                    $rootCategoryIds[] = $rootCategoryId;
                
        }
            
        $Categorys =  Mage::getModel('catalog/category')->getCollection()->addAttributeToSelect('*');
            
        $theKategorieIdObj = array();
            
            
        foreach ($Categorys as $Category) {
            $Category->load();
            $id=$Category->getEntityId();
                
            if($id!=1 && !in_array($id,$CategoryNrs) && !in_array($id, $rootCategoryIds) ){
                $CategoryNrs[] = $id;
                $theKategorieIdObj[] = new Cateno_ShopSync_Model_Types_Kategorie_Id(null,$id);
            }
                        
                
        }
            
        return $theKategorieIdObj;
    }
        
    public function KategorieBereinigenEnde($kategorieListe,$disabale)
    {
        Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
        $Categorys =  Mage::getModel('catalog/category')->getCollection()->addFieldToFilter('entity_id',$kategorieListe )->addAttributeToSelect('name');
            
        foreach ($Categorys as $Category) {
            $Category->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);
            $Category->load();
            $name = $Category->getName();
            $n = $Category->getProductCount();
            if($disabale){
                $Category->setIsActive(0);
                $Category->save();
                    
            } else{
                $Category->delete();
                    
            }
                
            }
        return true;
    }

}
