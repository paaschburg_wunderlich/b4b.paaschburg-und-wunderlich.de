<?php
/**
 * Cateno Debug
 *
 * @category Helper
 * @package Cateno_ShopSync
 * Lädt Debug Modul
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_Helper_Debug extends Cateno_ShopSync_Helper_Data
{
	const XML_DEBUG_ENABLE = 'shopsync/debug/enable';
	const XML_DEBUG_REQUEST = 'shopsync/debug/request';
	const XML_DEBUG_RESPONSE = 'shopsync/debug/response';

	public $theDebugTimeArray = array();

	/**
	 * check debug module is enable
	 */
	public function isDebugEnable()
	{
		return Mage::getStoreConfig(self::XML_DEBUG_ENABLE);
	}

	/**
	 * get request file path
	 */
	public function getRequestPath()
	{
		return Mage::getStoreConfig(self::XML_DEBUG_REQUEST);
	}

	/**
	 * get response file path
	 */
	public function getResponsePath()
	{
		return Mage::getStoreConfig(self::XML_DEBUG_RESPONSE);
	}

	public function createRequestLog($inRequest)
	{
		if(!is_dir($this->getRequestPath())) {
			mkdir($this->getRequestPath(), 0755, true);
		}
		$theFileName = $this->getRequestPath() . DS . Mage::getModel('core/date')->gmtDate('Ymd').'.txt';
		$theContents = array();
		$theContents[0] = Mage::getModel('core/date')->gmtDate() . " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+";
		$theContents[1] = $inRequest;
		$theContents[2] = " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+";
		if(!file_exists($theFileName)) {
			file_put_contents($theFileName, print_r(implode("\n", $theContents), true));
		} else {
			$theFP = fopen($theFileName, 'a+');
			fwrite($theFP, "\n");
			fwrite($theFP, "\n");
			foreach($theContents as $theLine) {
				fwrite($theFP, $theLine."\n");
			}
			fclose($theFP);
		}

	}

	public function createResponseLog($inResponse)
	{
		if(!is_dir($this->getResponsePath())) {
			mkdir($this->getResponsePath(), 0755, true);
		}
		$theFileName = $this->getResponsePath() . DS . Mage::getModel('core/date')->gmtDate('Ymd').'.txt';
		$theContents = array();
		$theContents[0] = Mage::getModel('core/date')->gmtDate() . " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+";
		$theContents[1] = $inResponse;
		$theContents[2] = " +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-++-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+";
		if(!file_exists($theFileName)) {
			file_put_contents($theFileName, print_r(implode("\n", $theContents), true));
		} else {
			$theFP = fopen($theFileName, 'a+');
			fwrite($theFP, "\n");
			fwrite($theFP, "\n");
			foreach($theContents as $theLine) {
				fwrite($theFP, $theLine."\n");
			}
			fclose($theFP);
		}
	}

    public function addDebugTime($desc)
    {
        $this->theDebugTimeArray[] = array(
                            'time' => microtime(true),
                            'desc' => $desc
                            );
    }

    public function debugeTimeOutput()
    {
        $cnt = count($this->theDebugTimeArray);
        $theRunTime = 0;
        for($idx = 1; $idx < $cnt; $idx ++){
            $theDiffTime = $this->theDebugTimeArray[$idx]['time'] - $this->theDebugTimeArray[$idx - 1]['time'];
            $theRunTime += $theDiffTime;
            Mage::log($theRunTime.' sec '.$this->theDebugTimeArray[$idx]['desc']);
        }
    }
}
