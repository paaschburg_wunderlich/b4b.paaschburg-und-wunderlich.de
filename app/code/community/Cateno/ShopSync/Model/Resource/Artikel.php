<?php
/**
* Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Artikeln
 *
 * @version 0.5.3	Bugfix setArtikel Zeile 180ff - Filter-Attribute werden nun gelöscht, wenn NULL übergeben wird
 * @version 0.5.5	Bugfix Cateno_ShopSync_Model_Resource_Artikel::setBilder // Media-API mit Produkt-ID statt SKU ansteuern
 */
class Cateno_ShopSync_Model_Resource_Artikel
{
	const XML_PATH_PRO_SET = 'shopsync/artikel/artikel_set';
	const XML_PATH_PRO_TYPE = 'shopsync/artikel/artikel_type';
	const XML_PATH_PIC_DIR = 'shopsync/artikel/pic_dir';
	const XML_PATH_WEBSITE = 'shopsync/artikel/website';
	const XML_PATH_VISIBILITY = 'shopsync/artikel/visibility';
	const XML_PATH_TAX = 'shopsync/artikel/tax';
	const ATTRIBUTE_CODE_PREFIX = 'shopsync_';
	const ATTRIBUTE_FILTER_PREFIX = 'shopsync_filter_';

	public $attribute_array = array();
	protected $defaultImageExt = array(
		'gif' => 'image/gif',
		'jpg' => 'image/jpeg',
		'jpeg' => 'image/jpeg',
		'jpe' => 'image/jpeg',
		'png' => 'image/png'
	);
	protected $largeImage = '';
	protected $smallImage = '';
	protected $thumbImage = '';

	/**
	 * check image type
	 * @param mine
	 * @return boolean
	 */
	protected function _checkExt($inMine)
	{
            foreach ($this->defaultImageExt as $theExt)
                  {
                  if($theExt == $inMine)
                        {
				return true;
				break;
			}
		}
		return false;
	}

	private function isConfigurable($inArtikel, Mage_Catalog_Model_Product $product)
	{
		$isHasAttributeListe = false;
		if(isset($inArtikel->Attributeliste))
			$isHasAttributeListe = @count($inArtikel->Attributeliste) > 0 ? true : false;

		if(!$product->getId())
		{
			return $isHasAttributeListe;
		}
		else
		{
			return $product->isConfigurable() || $isHasAttributeListe;
		}
	}

	/**
	 * set lager
	 * @param $data
	 * @return boolean
	 */
	public function setLager($inLager)
	{
		//get sku
        $theSku = $inLager->ArtNr;
		//get product id
        $theProductId = Mage::getModel('catalog/product')->getIdBySku($theSku);
		//get product
        $theProductObj = Mage::getModel('catalog/product')->load($theProductId);
        if($theProductObj->getId())
		{
			$theLagerData = array();
			$theProductApi = Mage::getSingleton('catalog/product_api');
			$theLagerData = Mage::getModel('shopsync/tabellen')->mergeFreiesFeldData($inLager->Felder, $theArtikelData);
			try
			{
				$theProductApi->update($theSku, $theLagerData);
			} catch (Exception $e)
			{
				throw new SoapFault('Stock could not be saved (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $e->getMessage());
			}
		}
        else
        {
            throw new SoapFault('Product SKU not found (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'Product SKU "' . $theSku . '" doesn\'t exist.');
		}
    }

	/**
	 * set product
	 * @param $data
	 * @param $isLast
	 * @return boolean
	 */
	public function setArtikel($inArtikel, $theHelper)
	{
        $theEntityTypeId = Mage::getModel('eav/entity')->setType('catalog_product')->getTypeId();
		//Multiselect
        foreach ($inArtikel->Felder as $artikelFelder)
        {
			if (isset($artikelFelder->FeldArt)) {
				if($artikelFelder->FeldArt == "multiselect")
				{
					$artikelFelder->Wert = $this->SetMultiselect($artikelFelder->FeldShop, $artikelFelder->Wert);
				}
				if($artikelFelder->FeldArt == "select")
				{
					$artikelFelder->Wert = $this->SetSelect($artikelFelder->FeldShop, $artikelFelder->Wert,$theEntityTypeId);              
				}
			}
        }  
		//set default store id


		$theStoreCollection = Mage::getResourceModel('core/store_collection')->getData();
		$theStoreArr = $theStoreArr = $this->GetStoreIds();

		$theArtikelData = array();

		//init parameter
        //set product set
        $theArtikelData['attribute_set_id'] = Mage::getStoreConfig(self::XML_PATH_PRO_SET);
		$theSku = $inArtikel->ArtNr;

		$product = Mage::getModel('catalog/product');

		$theProductId = $product->getIdBySku($theSku);

		//get product object
        $product = $product->load($theProductId);

		//is configurable product
		$isConfigurable = $this->isConfigurable($inArtikel, $product);

		//set product type
        if($isConfigurable)
		{
			$theType = Mage_Catalog_Model_Product_Type::TYPE_CONFIGURABLE;
        }
		else
        {
			$theType = Mage::getStoreConfig(self::XML_PATH_PRO_TYPE);
		}

		$theArtikelData['sku'] = $theSku;
		$theArtikelData['type_id'] = $theType;
		$theArtikelData['tax_class_id'] = Mage::getStoreConfig(self::XML_PATH_TAX);
		$theArtikelData['visibility'] = Mage::getStoreConfig(self::XML_PATH_VISIBILITY);
		$theArtikelData = Mage::getModel('shopsync/tabellen')->mergeFreiesFeldData($inArtikel->Felder, $theArtikelData);
		$theArtikelData['categories'] = $theArtikelData['category_ids'];

		if(isset($theArtikelData['website_ids']) && trim($theArtikelData['website_ids'] != ''))
		{
			$theArtikelData['website_ids'] = explode(',', $theArtikelData['website_ids']);
		}
		else
		{
			$theArtikelData['website_ids'] = array(Mage::getStoreConfig(self::XML_PATH_WEBSITE));
		}

		$theArtikelData['is_massupdate'] = true;
		$theArtikelData['exclude_url_rewrite'] = true;

		//set configurable product value
		if($isConfigurable)
		{
			$this->setConfigurableData($inArtikel, $product, $theArtikelData, $theEntityTypeId);
		}

		//get attribute
		foreach ($theArtikelData as $theDataKey => $theDataValue)
		{
			$isShopsyncCode = preg_match('/' . self::ATTRIBUTE_CODE_PREFIX . '\d+/', trim($theDataKey));
			if(is_numeric($theDataKey) || $isShopsyncCode)
			{
				$isArributeExist = true;
				$theAttributeCode = $theDataKey;
				if(!$isShopsyncCode)
				{
					$theAttributeCode = self::ATTRIBUTE_CODE_PREFIX . $theDataKey;
				}

				$theAttributeObject = Mage::getModel('catalog/resource_eav_attribute')
				->loadByCode($theEntityTypeId, $theAttributeCode)
				->setEntityTypeId($theEntityTypeId);
				try
				{
					if($theAttributeObject->getId())
					{
						$theAttributeValue = $this->getOptionId($theAttributeObject->getId(), $theDataValue);
						$theArtikelData[$theAttributeCode] = $theAttributeValue;
					}
				} catch (Exception $e)
				{
					throw new SoapFault('Attribute Code Fault', 'Attribute code doest not exist (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')');
				}
			}
		}

							
		foreach ($inArtikel->Tabellen as $theItem)
		{
			if(strcasecmp('catalog/product_website', trim($theItem->TabellenName)) == 0)
			{
				$theArtikelData = Mage::getModel('shopsync/tabellen')->mergeFelderFeldData($theItem->Daten, $theArtikelData);
			}
		}
		//tax class id
        /* old code - static in config.xml -> changes got overwritten by updates
		$param = 'tax_class_'.$theArtikelData['tax_class_id'];
		$theTaxValue = Mage::getConfig()->getNode('default/shopsync/tax')->$param;
		if (count($theTaxValue)) {
			$theArtikelData['tax_class_id'] = $theTaxValue[0];
		}
		*/

		// get the current tax class id from the article
        $shopySyncTaxId = $theArtikelData['tax_class_id'];
		// load the mapping from store config
        $taxClassMapping = Mage::getStoreConfig('shopsync/tax_class_mapping');

		// set the index to null
        $taxIndex = null;

		// try to find a matching tax class id in the mapping
        // $taxKey is normal_shopsync or reduced_shopsync
        // $taxValue is the tax class id
            foreach ($taxClassMapping as $taxKey => $taxValue)
                  {
                  if(strpos($taxKey, '_shopsync') !== FALSE && $taxValue == $shopySyncTaxId)
                        {
				$taxIndex = str_replace('_shopsync', '_magento', $taxKey);
			}
		}

		// check if $taxIndex is set and has an existing mapping
        // -> set the new tax class id
		if($taxIndex && $taxClassMapping[$taxIndex])
		{
			$theArtikelData['tax_class_id'] = $taxClassMapping[$taxIndex];
		}
		/* DEBUG START * /
		echo 'param		   :: ';	print_r($param);			echo "\n";
		echo 'taxIndex		:: ';	print_r($taxIndex);			echo "\n";
		echo 'mageTaxId	   :: ';	print_r($mageTaxId);		echo "\n";
		echo 'theArtikelData  :: ';	print_r($theArtikelData);	echo "\n";
		echo 'theTaxValue	 :: ';	print_r($theTaxValue);		echo "\n";
		echo 'taxClassMapping :: ';	print_r($taxClassMapping);	echo "\n";
		die(__METHOD__ . __LINE__);
		#/ * DEBUG END */

		//tier price
        $theTierPrice = array();
		$theTierPrice = Mage::getModel('shopsync/tabellen')->mergeFelderFeldTierPriceData($inArtikel->Staffelpreise);
		$theArtikelData['tier_price'] = $theTierPrice;

		$prices = array();
		foreach ($inArtikel->GroupPrices as $groupprices)
		{
			$prices[] = array('website_id' => $groupprices->WebSiteID, 'cust_group' => $groupprices->Nr, 'price' => $groupprices->Price);
		}
		$theArtikelData['group_price'] = $prices;
		$theHelper->addDebugTime('Init Parameter');

		// Produkt speichern
		$product = $this->saveProduct($theArtikelData);

		if($inArtikel->ZusaetzeEntfernen)
		{
			$this->DeleteArtikelZusaetze($product);
		}

		//Kategoriepositionen
		if (is_array($inArtikel->KategoriePositionen)) {
			if(isset($inArtikel->KategoriePositionen[0])) {
				if((isset($inArtikel->KategoriePositionen[0]->SKU) && trim($inArtikel->KategoriePositionen[0]->SKU)!=='')) {
					$theSku = $inArtikel->KategoriePositionen[0]->SKU;
					$theProductId = Mage::getModel('Catalog/Product')->getIdBySku($theSku);
					$theCategoryPosition = new Cateno_ShopSync_Model_Types_KategoriePositionen();
					foreach ($inArtikel->KategoriePositionen as $theCategoryPosition) {
						Mage::app()->setCurrentStore(Mage::getModel('core/store')->load(Mage_Core_Model_App::ADMIN_STORE_ID));
						$category = Mage::getModel('catalog/category')->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID)->load($theCategoryPosition->KategID);
						$products = $category->getProductsPosition();
						if ($products[$theProductId] != $theCategoryPosition->KategPos) {
							$products[$theProductId] = $theCategoryPosition->KategPos;
							$category->setPostedProducts($products);
							$category->save();
						}
					}
				}
			}
		}

		//update attribute
        foreach ($inArtikel->Tabellen as $theItem)
        {
            if(strcasecmp(trim($theItem->TabellenName), 'catalog/product_storeview') == 0)
            {
                foreach ($theItem->Daten as $theKey => $theData)
                {
					$inData = array();
                    foreach ($theData->Felder as $theFeld)
                    {
                        $isShopsyncCode = preg_match('/' . self::ATTRIBUTE_CODE_PREFIX . '\d+/', trim($theFeld->FeldShop));
                        if(is_numeric($theFeld->FeldShop) || $isShopsyncCode)
                        {
							$isArributeExist = true;
							$theAttributeCode = $theFeld->FeldShop;
                            if(!$isShopsyncCode)
	                        {
                                $theAttributeCode = self::ATTRIBUTE_CODE_PREFIX . $theFeld->FeldShop;
							}

							$theAttributeObject = Mage::getModel('catalog/resource_eav_attribute')
								->loadByCode($theEntityTypeId, $theAttributeCode)
								->setEntityTypeId($theEntityTypeId);
							try
                            {
					            if($theAttributeObject->getId())
                                {
									$theAttributeValue = $this->getOptionId($theAttributeObject->getId(), $theFeld->Wert);
									$inData = array_merge($inData, array(
										$theAttributeCode => $theAttributeValue
									));
								}
                            } catch (Exception $e)
                            {
								throw new SoapFault('Attribute Code Fault', 'Attribute code doest not exist (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')');
							}
                        }
                        else
						{
							if(isset($theFeld->Wert)) {
								$inData = array_merge($inData, array(
									$theFeld->FeldShop => $theFeld->Wert
								));
							}
						}
					}

                    if(isset($inData['store_id']) && $inData['store_id'] != '')
                    {
						$theStoreId = $inData['store_id'];

                        foreach ($inData as $theCode => $theValue)
                        {
							if(in_array($theStoreId, $theStoreArr) && $theCode != 'store_id')
							{
								$product->addAttributeUpdate($theCode, str_replace('\n', "\n", $theValue), $theStoreId);
							}
						}
                    }
					else
                    {
						throw new SoapFault('Store Id Fault (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'Store id required');
					}
				}
			}
		}
		$theHelper->addDebugTime('update product attribute');
		return $product;
	}

	/**
	 * Eigenschaften des Produkts setzen und speichern.
	 * @param array die Produkteigenschaften
	 * @return object catalog/product
	 */
	public function saveProduct($data)
	{
		$product = Mage::getModel('catalog/product');

		$productId = $product->getIdBySku($data['sku']);

		// Produkt vorhanden?
		if($productId)
		{
			$product->load($productId);
            //Delete Special Price
            $product->setSpecialPrice(null)->setSpecialFromDate(null)->setSpecialToDate(null);
        }

		// Attribute durchlaufen
        foreach ($data as $key => $value)
        {
        if($key == 'category_ids')
        {
			// muss ein Array sein, sonst wird nur die erste ID berücksichtigt
			$value = explode(',', str_replace(' ', '', $data['category_ids']));
			$product->setCategoryIds($value);
			}
			else
            {
				$product->setData($key, $value);
			}
		}

		//$product->setStockData(array('use_config_manage_stock' => 1, 'is_in_stock' => $data['is_in_stock']));
		// use_config_manage_stock wurde immer auf 1 gesetzt (siehe oben). Nun wird er nur noch auf 1 gesetzt wenn es nicht definiert wurde
		$stockData = $product->getStockData();

		key_exists('use_config_manage_stock', $data) ? $stockData['use_config_manage_stock'] = $data['use_config_manage_stock'] : $stockData['use_config_manage_stock'] = 1;
		//Wenn use_config_manage_stock auf 0 gesetzt wird, muss / kann auch manage_stock definiert werden.
		key_exists('manage_stock', $data) ? $stockData['manage_stock'] = $data['manage_stock'] : null;
		key_exists('is_in_stock', $data) ? $stockData['is_in_stock'] = $data['is_in_stock'] : null;

		//VPE

		key_exists('use_config_qty_increments', $data) ? $stockData['use_config_qty_increments'] = $data['use_config_qty_increments'] : null;
		key_exists('enable_qty_increments', $data) ? $stockData['enable_qty_increments'] = $data['enable_qty_increments'] : null;
		key_exists('use_config_enable_qty_increments', $data) ? $stockData['use_config_enable_qty_increments'] = $data['use_config_enable_qty_increments'] : null;
		key_exists('qty_increments', $data) ? $stockData['qty_increments'] = $data['qty_increments'] : null;
		key_exists('is_qty_decimal', $data) ? $stockData['is_qty_decimal'] = $data['is_qty_decimal'] : null;

		key_exists('use_config_min_qty', $data) ? $stockData['use_config_min_qty'] = $data['use_config_min_qty'] : null;
        key_exists('min_qty', $data) ? $stockData['min_qty'] = $data['min_qty'] : null;
        key_exists('original_inventory_qty', $data) ? $stockData['original_inventory_qty'] = $data['original_inventory_qty'] : null;

        key_exists('use_config_min_sale_qty', $data) ? $stockData['use_config_min_sale_qty'] = $data['use_config_min_sale_qty'] : null;
        key_exists('min_sale_qty', $data) ? $stockData['min_sale_qty'] = $data['min_sale_qty'] : null;
        key_exists('use_config_max_sale_qty', $data) ? $stockData['use_config_max_sale_qty'] = $data['use_config_max_sale_qty'] : null;
        key_exists('max_sale_qty', $data) ? $stockData['max_sale_qty'] = $data['max_sale_qty'] : null;

        key_exists('use_config_backorders', $data) ? $stockData['use_config_backorders'] = $data['use_config_backorders'] : null;
        key_exists('backorders', $data) ? $stockData['backorders'] = $data['backorders'] : null;

        key_exists('use_config_notify_stock_qty', $data) ? $stockData['use_config_notify_stock_qty'] = $data['use_config_notify_stock_qty'] : null;
        key_exists('notify_stock_qty', $data) ? $stockData['notify_stock_qty'] = $data['notify_stock_qty'] : null;


		$product->setStockData($stockData);


		unset($data);

		try
		{
			$product->save();
		} catch (Exception $e)
		{
			Mage::logException($e);
		}

		return $product;
	}

	public function setBilder($pictureObject)
	{
		// get SKU from pictureObject
        $productSku = $pictureObject->ArtNr;

		// try to load a product by the given SKU and check if we get a valid product id, if not exit
        $product = Mage::getModel('catalog/product');
		$productId = $product->getIdBySku($productSku);

		// exit, if we have no valid product id
        if(!$productId)
        {
            throw new SoapFault('Invalid SKU', 'SKU: ' . $productSku . ' does not exist. (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')');
		}

		// product found, get picture informations
        // $newPictures array, intentionally NOT:
        // -> $newPictures[idx] = array('file' => 'fileName', 'update' = boolean)
        // -> less foreach looping trough all elements of the array
        $newPictures = array(
			'files' => array(),
			'update' => array(),
		);

		// multiple pictures
        if($pictureObject->ArtikelBilderArray && is_array($pictureObject->ArtikelBilderArray))
		{
            foreach ($pictureObject->ArtikelBilderArray as $position => $picture)
			{
                if (isset($picture) && trim($picture)!=='') {
                    $newPictures['files'][$position] = trim($picture);
                    $newPictures['update'][$position] = false;
                }
            }
		}

		// get the picture import path
        $pictureImportPath = Mage::getBaseDir() . DS . str_replace('/', DS, Mage::getStoreConfig(self::XML_PATH_PIC_DIR));

		// scan for updated images
        $updatePictures = scandir($pictureImportPath);
        foreach ($newPictures['files'] as $position => $newPicture)
        {
            if(in_array($newPicture, $updatePictures))
            {
				$newPictures['update'][$position] = true;
			}
		}

		// get media api and media config objects
		Mage::getSingleton('api/session')->start();
        $mediaApi = Mage::getSingleton('catalog/product_attribute_media_api_v2');
		$mediaConfig = Mage::getSingleton('catalog/product_media_config');

		// get the current product pictures
        $currentPictures = $mediaApi->items($productId);

		// oldPictures array, for updating picture/position or creating new pictures
        $oldPictures = array(
			'shortFileName' => array(),
			'fullFileName' => array(),
		);

		// delete all current pictures which are not in the new pictures array
        if(is_array($currentPictures) && count($currentPictures))
        {
            foreach ($currentPictures as $currentPicture)
            {
				// get the shortFileName from the magento picture path (/a/b/abc.jpg -> abc.jpg)
                $fileName = explode('/', $currentPicture['file']);
				$fileName = end($fileName);
				// add to the oldPictures array
                $oldPictures['shortFileName'][] = $fileName;
				$oldPictures['fullFileName'][] = $currentPicture['file'];

				// check if the picture is NOT in the list of new pictures
                if (!in_array($fileName, $newPictures['files'])) {
                    $oldFilePath = Mage::getBaseDir('media') . '/catalog/product' . $currentPicture['file'];
                    //cheick if the picture not int the list was just renamed
                    if ($newPictures['update'][$index]) {
                        $md5NewImage = (md5_file($pictureImportPath . DS . $newPictures['files'][$index]));
                        $md5OldImage = md5_file($oldFilePath);
                        if ($md5NewImage == $md5OldImage) {
                            //different name, but same content: remove image from list of new pictures & re-normalize array indices
                            unset($newPictures['files'][$index]);
                            unset($newPictures['update'][$index]);
                            $newPictures['files'] = array_values($newPictures['files']);
                            $newPictures['update'] = array_values($newPictures['update']);
                            continue;
                        }
                    }
					// try to remove the image from the product
                    if ($mediaApi->remove($productId, $currentPicture['file'])) {
						// delete the image file
                        @unlink(Mage::getBaseDir('media') . '/catalog/product' . $currentPicture['file']);
                        if(file_exists($pictureImportPath . DS . $fileName))
                            @unlink($pictureImportPath . DS . $fileName); // only images which the original file name can be deleted that way   
					}
				}
			}
		}

		// update pictures with new files in import directory
        foreach ($newPictures['files'] as $position => $newPicture)
		{
			// check if there is a new/updated picture
			if($newPictures['update'][$position])
			{
				// get picure mime info
				$pictureDetails = getimagesize($pictureImportPath . DS . $newPicture);

				// prepare the newPictureName
				$newPictureName = '';
				$newPictureNameTmp = explode('.', $newPicture);
				for ($i = 0; $i < (count($newPictureNameTmp) - 1); $i++)
				{
					$newPictureName .= $newPictureNameTmp[$i];
				}

				// prepare the newPictureFile object
                $newPictureFileObject = new stdClass;
				$newPictureFileObject->content = base64_encode(file_get_contents($pictureImportPath . DS . $newPicture));
				$newPictureFileObject->mime = $pictureDetails['mime'];
				$newPictureFileObject->name = $newPictureName;

				// prepare the types array (not implemented yet)
                $types = array();
				// set the first image as default
				if($position === 0)
				{
					$types = array(
						'image', // Base Image
                        'small_image', // Small Image
                        'thumbnail', // Thumbnail
                    );
				}

				// prepare the newPicture object
                $newPictureObject = new stdClass;
				$newPictureObject->types = $types;
				$newPictureObject->position = $position + 1;
				$newPictureObject->exclude = 0;
				$newPictureObject->file = $newPictureFileObject;

				// try to add/update the picture
				try
				{
					// old file exists, overwrite
					if(in_array($newPicture, $oldPictures['shortFileName']))
					{
						// get the old picture fullFileName (/a/b/abc.jpg)
                        $oldPictureIndex = array_keys($oldPictures['shortFileName'], $newPicture);
						$oldPictureIndex = $oldPictureIndex[0];
						$oldPictureFullFileName = $oldPictures['fullFileName'][$oldPictureIndex];
						$mediaApi->update($productId, $oldPictureFullFileName, $newPictureObject);
					}
					// no old file exists, add new picture
					else
					{
						$mediaApi->create($productId, $newPictureObject);
					}
					//@unlink($pictureImportPath . DS . $newPicture);
                } catch (Mage_Api_Exception $apiE) {
                    throw new SoapFault('Mage_Api_Exception: Picture could not be saved ', $apiE->getMessage());
                } catch (Mage_Core_Exception $e) {
                    throw new SoapFault('Mage_Core_Exception: Picture could not be saved ', $e->getMessage());
                } catch (Exception $e) {
                    throw new SoapFault('Exception: Picture could not be saved ', $e->getMessage());
                }
			}
			// picture exists and has no new file given, but we may need to update the picture position
			else
			{
				// prepare the types array (not implemented yet)
                $types = array();
				// set the first image as default
				if($position === 0)
				{
					$types = array(
						'image', // Base Image
                        'small_image', // Small Image
                        'thumbnail', // Thumbnail
                    );
				}
				// prepare the newPicture object
                $newPictureObject = new stdClass;
				$newPictureObject->types = $types;
				$newPictureObject->position = $position + 1;
				$newPictureObject->exclude = 0;
				// using empty fileObject, only update the position
                $newPictureObject->file = new stdClass;

				// try to update the position
				try
				{
					// old file exists, update position
					if(in_array($newPicture, $oldPictures['shortFileName']))
					{
						// get the old picture fullFileName (/a/b/abc.jpg)
                        $oldPictureIndex = array_keys($oldPictures['shortFileName'], $newPicture);
						$oldPictureIndex = $oldPictureIndex[0];
						$oldPictureFullFileName = $oldPictures['fullFileName'][$oldPictureIndex];
						$mediaApi->update($productId, $oldPictureFullFileName, $newPictureObject);
					}
				} catch (Mage_Api_Exception $apiE)
				{
					throw new SoapFault('Picture could not be updated (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $apiE->getCustomMessage());
				} catch (Mage_Core_Exception $e)
				{
					throw new SoapFault('Picture could not be updated (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $e->getMessage());
				} catch (Exception $e)
				{
					throw new SoapFault('Picture could not be updated (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $e->getMessage());
				}
			}
		}
        //Mage::getModel('catalog/product_image')->clearCache();
        //Mage::dispatchEvent('clean_catalog_images_cache_after');
    }

	protected function setConfigurableData($inArtikel, $theProductObj, &$theArtikelData, $theEntityTypeId)
	{
		$theArtikelData['configurable_products_data'] = array();
		$idx = 0;

        foreach ($inArtikel->Attributeliste as $attribute)
		{
			$attributeModel = Mage::getModel('catalog/resource_eav_attribute')
				->loadByCode($theEntityTypeId, $attribute->AttributeCode)
				->setEntityTypeId($theEntityTypeId);
			$theArtikelData['attributes'][] = $attributeModel->getId();
			$configurableAttributes = array();
			if($theProductObj->getId())
			{
				if($theProductObj->isConfigurable())
				{
					$configurableAttributes = $theProductObj->getTypeInstance(true)
						->getConfigurableAttributesAsArray($theProductObj);
				}
				else
				{
					throw new SoapFault('configurable product fault (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $theProductObj->getSku() . ' is no configurable product');
				}
			}
			$id = null;

			foreach ($configurableAttributes as $theAttribute)
			{
				if($theAttribute['attribute_id'] == $attributeModel->getId())
				{
					$id = $theAttribute['id'];
					break;
				}
			}
			$values = array();
            foreach ($attribute->valueListe as $valueListe)
            {
				$value_index = $this->getOptionId($attributeModel->getId(), $valueListe->AttributeValue);
				$values[] = array(
					'label' => $valueListe->AttributeValue,
					'attribute_id' => $attributeModel->getId(),
					'value_index' => $value_index,
                    'is_percent' => (int) $valueListe->IsPercent,
					'pricing_value' => $valueListe->AttributePrice,
				);
			}

			$theArtikelData['configurable_attributes_data'][] = array(
                'id' => $id,
				'label' => $attributeModel->getData('frontend_label'),
				'use_default' => $attribute->UseDefault,
				'position' => 0,
				'values' => $values,
				'attribute_id' => $attributeModel->getId(),
				'attribute_code' => $attributeModel->getCode(),
				'frontend_label' => $attributeModel->getData('frontend_label'),
				'store_label' => $attributeModel->getData('frontend_label'),
                'html_id' => 'configurable__attribute_' . $idx,
			);
            $idx++;
		}

        foreach ($inArtikel->SimpleSkuliste as $theSimpleSku)
        {
			//get simple product obj
			$theSimpleProductId = Mage::getModel('catalog/product')->getIdBySku($theSimpleSku);
			$theSimpleProduct = Mage::getModel('catalog/product')->load($theSimpleProductId);
			if($theSimpleProduct->getId())
            {
				//check attribute code exist in the product
				foreach ($inArtikel->Attributeliste as $attribute)
                {
					$theAttributeValue = $theSimpleProduct->getData($attribute->AttributeCode);
					if(!empty($theAttributeValue))
                    {
						//get attribute value in product
						$attributeModel = Mage::getModel('catalog/resource_eav_attribute')
									->loadByCode($theEntityTypeId, $attribute->AttributeCode)
									->setEntityTypeId($theEntityTypeId);
						//set artike data
						$theArtikelData['configurable_products_data'][$theSimpleProductId][] = array(
							'attribute_id' => $attributeModel->getId(),
							'label' => $attributeModel->getData('frontend_label'),
							'value_index' => $theAttributeValue,
						);
                    }
					else
                    {
						throw new SoapFault('AttributeValue wurde im SimpleProduct nicht gefunden (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'simple product sku: ' . $theSimpleSku . '.');
                    }
				}
			}
            else
            {
                throw new SoapFault('simple product sku fault (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', 'simple product sku: ' . $theSimpleSku . ' not exist.');
			}
		}
	}

	private function getOptionId($attributeId, $optionValue)
	{
		$optionCollection = Mage::getResourceModel('eav/entity_attribute_option_collection')
								->setAttributeFilter($attributeId)
								->setStoreFilter(0, false)
								->addFieldToFilter('value', $optionValue)
								->load()
								;
		if(!$optionCollection->count())
		{
			return null;
		}

		return $optionCollection->getFirstItem()->getId();
	}

	private function GetAttributeOptions($attributeCode)
	{
		$attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeCode);              
                 
		$options = $attribute->getSource()->getAllOptions(false);
		unset($attribute);
		return $options;
	}
                 
	private function GetAttributeIds($attributeCode, $attributeValues)
	{       
		$attributeIds = array();
		$options = $this->GetAttributeOptions($attributeCode);
  
		foreach ($options as $option)
		{
			if(in_array($option['label'], $attributeValues))
			{
			$attributeIds[] = $option['value'];
			}
		}            

		return $attributeIds;
	}
      
    private function GetAttributeLabels($attributeCode)
    {       
		$options = $this->GetAttributeOptions($attributeCode);
		$labels = array();
		foreach ($options as $option)
		{
			$labels[] = $option['label'];
		}
		unset($options);
		return $labels;
	}
                 
	private function GetAttributeType($attributeCode)
	{
		$attribute = Mage::getSingleton("eav/config")->getAttribute('catalog_product', $attributeCode); 
                    
		if($attribute == null)
		return;
                    
		$attributeTyp = $attribute->getFrontendInput();
		unset($attribute);
		return $attributeTyp;
	}
              
    private function GetStoreIds()
    {                     
		$storeIds = array();
		$theStoreCollection = Mage::getResourceModel('core/store_collection')->getData();
		foreach ($theStoreCollection as $theStore)
        {
			$storeIds[] = $theStore['store_id'];
        }
                 
        unset($theStoreCollection);
                 
        return $storeIds;
    }
                        
	private function AddValuesToAttribute($attributeCode, $newLabels)
    {
		$storeIds = $this->GetStoreIds();                      
                      
		$attributeModel = Mage::getModel('catalog/resource_eav_attribute');
		$attribute = $attributeModel->loadByCode('catalog_product', $attributeCode);
		$attributeId = $attribute->getAttributeId();
                                
		$option['attribute_id'] = $attributeId;     
        
		foreach( $newLabels as $newLabel) {            
			foreach ($storeIds as $storeId) {                
				if(trim($newLabel)!=='') //do not add empty attributes
					$option['value']["_".$newLabel][$storeId] = (string)$newLabel; // avoid issues with leading numbers in the indexer and the label
			}     
		}
           
        $setup = new Mage_Eav_Model_Entity_Setup('core_setup');
                    $setup->startSetup();
                    $setup->addAttributeOption($option);
                    $setup->applyAllDataUpdates();
                    $setup->applyAllUpdates();
                    $setup->endSetup();                                   
                                   
		unset($storeIds, $attributeModel, $attribute, $attributeId, $option, $setup);
	}
                      
	private function SetMultiselect($attributeCode, $attributeValue)
	{                            
		$attributeValue = str_replace("\"", "", $attributeValue); 
		$attributeValues = split(",", $attributeValue);
      
		$attrubuteLabels = $this->GetAttributeLabels($attributeCode);
      
		$newValues = array_diff($attributeValues, $attrubuteLabels); 
      
		if(count($newValues) > 0)
		{
			$this->AddValuesToAttribute($attributeCode, $newValues);
		}
                            
		$attributeIds = $this->GetAttributeIds($attributeCode, $attributeValues);

		return $attributeIds;
	}

    private function DeleteArtikelZusaetze($product)
    {
        $link = $product->getLinkInstance()->setLinkTypeId(Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED);
        $link->getResource()->saveProductLinks($product, array(), Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED);
    }

    private function SetSelect($feldShop,$feldWert,$theEntityTypeId)
	{
		$attributeCode = $feldShop;
        $optionValue = $feldWert;
                        
        $attributeModel = Mage::getModel('catalog/resource_eav_attribute')
                ->loadByCode($theEntityTypeId, $attributeCode)
                ->setEntityTypeId($theEntityTypeId);
        try
        {
			if($attributeModel->getId())
            {
				if(is_null($feldWert) || empty($feldWert))
                {
			        $optionId = '';
                }
			    else
                {
					$optionId = $this->getOptionId($attributeModel->getId(), $optionValue);
					if(is_null($optionId))
					{
						$installer = Mage::getModel('eav/entity_setup', 'core_setup');
						$optionArray = array(
							'attribute_id' => $attributeModel->getId(),
							'value' => array('option1' => array($optionValue))
						);
						$installer->startSetup();
						$installer->addAttributeOption($optionArray);
						$installer->endSetup();
						$optionId = $this->getOptionId($attributeModel->getId(), $optionValue);
					}
				}
				return $optionId;
            }
        } catch (Exception $e)
        {
			throw new SoapFault('Attribute Code Fault', 'Attribute code doest not exist (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')' . "\n" . $e);
        }                  
    }    
}
