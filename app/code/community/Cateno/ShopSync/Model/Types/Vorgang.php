<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Vorgangsinformationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Vorgang
{
	/**
	 * increment_id
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var string
	 */
	public $WShopIDAdresse;

	/**
	 * @var string
	 */
	public $WShopIDReAns;

	/**
	 * @var string
	 */
	public $WShopIDLiAns;

	/**
	 * @var string
	 */
	public $AdrNr;

	/**
	 * @var string
	 */
	public $BelegNr;

	/**
	 * @var string
	 */
	public $ReFirstName;

	/**
	 * @var string
	 */
	public $ReLastName;

	/**
	 * @var string
	 */
	public $RePrefix;

	/**
     * @var string
     */
    public $ReCompany;

	/**
	 * @var string
	 */
	public $ReStr;

	/**
	 * @var string
	 */
	public $ReLandKennz;

	/**
	 * @var string
	 */
	public $RePLZ;

	/**
	 * @var string
	 */
	public $ReOrt;
        
    /**
	 * @var string
	 */
	public $ReTel;
        
    /**
	 * @var string
	 */
	public $ReFax;
        
    /**
	 * @var string
	 */
	public $ReEmail;

	/**
	 * @var string
	 */
	public $LiFirstName;

	/**
	 * @var string
	 */
	public $LiLastName;

	/**
	 * @var string
	 */
	public $LiPrefix;

	/**
     * @var string
     */
    public $LiCompany;

	/**
	 * @var string
	 */
	public $LiStr;

	/**
	 * @var string
	 */
	public $LiLandKennz;

	/**
	 * @var string
	 */
	public $LiPLZ;

	/**
	 * @var string
	 */
	public $LiOrt;
        
    /**
	 * @var string
	 */
	public $LiTel;
        
    /**
	 * @var string
	 */
	public $LiFax;
        
    /**
	 * @var string
	 */
	public $LiEmail;

	/**
	 * @var string
	 */
	public $Dat;

	/**
	 * @var string
	 */
	public $ZahlBed;

	/**
	 * @var string
	 */
	public $Waehr;

	/**
	 * @var string
	 */
	public $ZahlungsOption;

	/**
	 * @var string
	 */
	public $VersandArt;

	/**
	 * @var string
	 */
	public $Bemerkung;

	/**
	 * @var string
	 */
	public $Kartlnhab;

	/**
	 * @var string
	 */
	public $KartNr;

	/**
	 * @var string
	 */
	public $KartGueltig;

	/**
	 * bankransfer_number
	 * @var string
	 */
	public $KNr;

	/**
	 * bankransfer_blz
	 * @var string
	 */
	public $BLZ;

	/**
	 * bankransfer_bic
	 * @var string
	 */
	public $BIC;

    /**
	 * bankransfer_iban
	 * @var string
	 */
	public $IBAN;

	/**
	 * bankransfer_owner
	 * @var string
	 */
	public $Inhab;

	/**
	 * @var string
	 */
	public $AspAnr;

	/**
	 * @var string
	 */
	public $AspEMail;

	/**
	 * @var Cateno_ShopSync_Model_Types_Vorgang_Position
	 */
	public $VorgangPosition;

	/**
	 * @var Cateno_ShopSync_Model_Types_Freies_Feld
	 */
	public $FreieFelder;

	/**
	 * @var string
	 */
	public $UStld;

	/**
	 * @var int
	 */
	public $ArtPrGrp;

	/**
	 * @var float
	 */
	public $GPreisBt;

	/**
	 * @var float
	 */
	public $GPreisNt;
}
