<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung des Vorgangsstatus
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Vorgang_Status extends Varien_Object
{
	/**
	 * @var string
	 */
	public $Status;

	/**
	 * @var string
	 */
	public $Bezeichnung;

	public function __construct($inStatus = null, $inBezeichnung = null)
	{
		$this->Status = $inStatus;
		$this->Bezeichnung = $inBezeichnung;
	}
}
