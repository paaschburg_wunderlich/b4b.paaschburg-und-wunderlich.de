<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Adress-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Adress_Info
{
	/**
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var string
	 */
	public $LetzteAenderung;

	public function __construct($inWShopID = '', $inLetzteAenderung = '')
	{
		$this->WShopID = $inWShopID;
		$this->LetzteAenderung = $inLetzteAenderung;
	}
}
