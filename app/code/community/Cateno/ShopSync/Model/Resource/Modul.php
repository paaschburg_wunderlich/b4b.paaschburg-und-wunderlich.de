<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Modul-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Modul
{
    /**
    * Cateno_ShopSync_Model_Types_Modul
    * @return array
    */
    public function getShippingMethods()
    {
		$theMethods = array();
		foreach (Mage::getModel('shipping/config')->getAllCarriers() as $theCode => $theMethod) {
            if($theCode == 'googlecheckout')
                continue;
			$theMethods[] = new Cateno_ShopSync_Model_Types_Modul($theCode, Mage::getStoreConfig('carriers/' . $theCode . '/title'));
        }
		return $theMethods;
    }

    /**
    * Cateno_ShopSync_Model_Types_Modul
    * @return array
    */
    public function getPaymentMethods()
    {
		$payments = Mage::getSingleton('payment/config')->getAllMethods();

		foreach ($payments as $paymentCode => $paymentModel) {
            $paymentTitle = Mage::getStoreConfig('payment/' . $paymentCode . '/title');
            $theMethods[] = new Cateno_ShopSync_Model_Types_Modul($paymentCode, $paymentTitle);
        }
		return $theMethods;
    }

	/**
	* Cateno_ShopSync_Model_Types_Modul for upper two methods
	* @return boolean
	*/
	protected function _canUseMethod($inMethod)
    {
		if(!$inMethod->canUseForCurrency(Mage::app()->getStore()->getBaseCurrencyCode())) {
			return false;
		}
		return true;
    }
}
