<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Daten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Daten extends Varien_Object
{
	/**
	 * @param string $inBezeichnungen
	 * @return Cateno_ShopSync_Model_Types_Daten
	 */
	public function getDaten($inBezeichnungen)
	{
		$theDaten = new Cateno_ShopSync_Model_Types_Daten();
		$theDaten->Felder = $inBezeichnungen;
		return $theDaten;
	}
}
