<?php

 $mageFilename = "../../../../../../../../app/Mage.php";
 require_once $mageFilename; 
 umask(0);
 Mage::app();
 $activeprofile = basename(__FILE__, '.css.php');
 $baseconfig = Mage::helper("jmbasetheme")->getactiveprofile($activeprofile);
 header("Content-type: text/css; charset: UTF-8");
?>

/* Base settings */
body#bd {
   background-color: <?php echo $baseconfig["bgolor"]; ?>;
   background-image:url("images/<?php echo $baseconfig["bgimage"]; ?>");
}

#ja-mycart .btn-toggle.active,
#ja-mycart .btn-toggle:hover {
   background-color:<?php echo $baseconfig["color"]; ?>;
}

a,
.shop-access a:hover, /*Quick Access---*/
.shop-access a:active,
.shop-access a:focus,

#nav li.active a,   /* 0 Level */
#nav li.over a,
#nav a:hover,

#ja-pathway ul li a,  /*breadcrums---*/

.ja-spotlight .col-1 .block-content a, /* Spotlight */
.ja-spotlight .block .block-content a:hover,
.ja-spotlight .block .block-content a:focus,
.ja-spotlight .block .block-content a:active,
.socail-footer a:hover,
.ja-topsl_bottom li span.color-text3,
.ja-topsl_bottom li span.color-text2,

#ja-footer li a, /* FOOTER */

.pager .pages .current, /* Pager magento 1.4*/

.ratings .rating-links a:hover, /*RATINGS */
.ratings .rating-links a:focus,
.ratings .rating-links a:active,

.price, /*PRICES */
.regular-price .price,
.special-price .price,

a.minimal-price-link .price, /*Minimal price (as low as)---*/

.price-excluding-tax .price, /*Excluding tax---*/

.price-including-tax .price, /*Including tax---*/

.add-to-links a:focus, /*Add To---*/

.block-cart .subtotal .price, /*Mini cart - top---*/

ol#cart-sidebar .price,

.block-navigation #ja-sidenav li a:active, /*Layered Navigation---*/

.product-name a:hover, /*Product---*/
.product-name a:focus,
.product-name a:active,
.products-grid li.item:hover .product-name a, 

.product-essential .availability.in-stock span,

ul.ja-tab-navigator li a:hover, /*PRODUCT TABS */
ul.ja-tab-navigator li a:focus,
ul.ja-tab-navigator li a:active,

.email-friend a:hover, /*Product Info---*/
.email-friend a:focus,
.add-to-links a:hover,
.add-to-links a:focus,

.giftmessages .gift-header, /*Gift Messages----*/

button.btn-cart:hover,
button.btn-cart:focus,
button.button.btn-cart:hover,
button.button.btn-cart:focus,
.products-grid li.item:hover button.btn-cart,

#multiship-addresses-table button, /*Multiple Addresses checkout---*/
.place-order-box .grand-total .price, 

.block-account li a:hover, /*My Account navigation---*/
.block-account li a:active,
.block-account li a:focus,
.block-account li.current,

.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active,
.block-navigation #ja-sidenav ul.level0 li a:hover,
.block-navigation #ja-sidenav ul.level0 li a:focus,
.block-navigation #ja-sidenav ul.level0 li a:active,
.block-navigation #ja-sidenav ul.level0 li.active a,

.jm-megamenu ul.level1 li.mega a.mega span.menu-title,

.jm-megamenu .megamenu.level2 li.mega a.mega:hover span.menu-title,

.jm-megamenu .category-products .products-list li.item a:hover,

.jmzin-content:hover .product-name a, /* JM Categories List ---*/

.block-navigation #ja-sidenav li.active a,

.jm-product-deals .products-list .deal-infor-wrap .sale-ends, /* JM Daily Deals ---*/
.jm-product-deals .products-grid .deals-info li strong.sale-ends, 
.jm-product-deals .products-grid .deals-info li strong.discount, 
.jm-product-deals .products-grid .deals-info li strong.save,
.jm-product-deals .products-list .add-to-links a:hover,
.jm-product-deals .products-list .add-to-links a:focus,
.jm-product-deals .products-list .add-to-links a:active {
	color:<?php echo $baseconfig["color"]; ?>;
}

.input-text:focus, select:focus, textarea:focus,
#ja-search .input-text:focus,
.block-subscribe .input-box input:focus,

button.button:hover,
button.button:focus,
button:hover,
button:focus,

.truncated a.details,

.block-navigation #ja-sidenav li a:active,

.product-img-box .more-views li a:hover,

.opc .allow .number,
.opc .active .number,

.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active,

.jm-megamenu ul.level0 li.active  {
	border-color:<?php echo $baseconfig["color"]; ?>;
}

.static-top1 .inner1 .widget-cms-link a,
.static-top2 .inner1 .widget-cms-link a,

button.button:hover,
button.button:focus,
button:hover,
button:focus,

button.button.btn-proceed-checkout,
.add-to-cart button.button.btn-cart, /* detail page */
.jm-product-deals .products-list button.btn-cart,

#ja-mycart .active + .inner-toggle, /* My cart ---*/
#ja-mycart .btn-toggle:hover + .inner-toggle,

ul.ja-tab-navigator li.active,

.opc .allow .number,
.opc .active .number,

.checkout-progress li.active,
.shipment-count,
.box-account ol .number,

.jm-megamenu ul.level0 li.active, /* Mega Menu ---*/
.jm-megamenu .jm-product-deals.grid.block,

.jm-mask-desc .readmore a.readon /* slide show */,

#ja-mainnav .btn-toggle.active,
#ja-mainnav .btn-toggle:hover {
	background-color:<?php echo $baseconfig["color"]; ?> ;
}

.quick-access,
#ja-mainnav-inner {
	background-color:<?php echo $baseconfig["headerolor"]; ?>;
	background-image:url("images/<?php echo $baseconfig["headerimage"]; ?>");
}

#ja-botsl .main .inner,
#ja-footer .main .inner {
	background-color:<?php echo $baseconfig["footerolor"]; ?>;
	background-image:url("images//<?php echo $baseconfig["footerimage"]; ?>");
}

.product-img-box .product-image-zoom {
	width: <?php echo $baseconfig["productlimagewidth"]; ?>px;
	height: <?php echo $baseconfig["productlimageheight"]; ?>px;
}

@media only screen and (max-width:719px) {
	#ja-mainnav-inner {
		background-color:<?php echo $baseconfig["color"]; ?> ;
	}
}

/* Profile settings */

h1#logo a {
	background-image: url("images//<?php echo $baseconfig["bluelogo"]; ?>") !important;
}

button.btn-cart, button.button.btn-cart,
button.btn-cart:hover,
button.btn-cart:focus,
button.button.btn-cart:hover,
button.button.btn-cart:focus,
.products-grid li.item:hover button.btn-cart,
#wishlist-table td:nth-child(3n) button.btn-cart {
	background-image: url("images/<?php echo $baseconfig["blueaddtocart"]; ?>");
}

.block .block-title,
.page-title {
	background-image: url("images/<?php echo $baseconfig["bluetitle"]; ?>");
}

.add-to-links a.link-compare {
	background-image: url("images/<?php echo $baseconfig["bluecompare"]; ?>");
}

.add-to-links a.link-wishlist {
	background-image: url("images/<?php echo $baseconfig["bluewishlist"]; ?>");
}

.email-friend a {
	background-image: url("images/<?php echo $baseconfig["blueemail"]; ?>");
}

.socail-footer a {
	background-image: url("images/<?php echo $baseconfig["bluesocial"]; ?>");
}

.jm-lemmon-slider .controls .next-slide,
.jm-product-lemmon .controls .next-slide {
	background-image: url("images/<?php echo $baseconfig["bluenext"]; ?>");
}

.jm-lemmon-slider .controls .prev-slide,
.jm-product-lemmon .controls .prev-slide {
	background-image: url("images/<?php echo $baseconfig["bluepre"]; ?>");
}

a.readon,
a.link-learn {
	background-image: url("images/<?php echo $baseconfig["bluereadmore"]; ?>");
}

.jmzin-content:hover a.readon,
.jmzin-content:hover a.readon:hover,
.jmzin-content:hover a.readon:focus,
.jmzin-content:hover a.readon:active,
a.link-learn:hover,
a.link-learn:focus,
a.link-learn:active {
	color:<?php echo $baseconfig["color"]; ?>;
}

.static-top1 .bg-color {
	background-color:<?php echo $baseconfig["secondcolor"]; ?>;
}

.static-top2 .bg-color {
	background-color:<?php echo $baseconfig["thirdcolor"]; ?>;
}