<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Rabatt-Daten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Abw_Artikel_Daten_Rab
{
	/**
	 * @var float
	 */
	public $Mge;

	/**
	 * @var float
	 */
	public $Sz;

	/**
	 * @var float
	 */
	public $Pr;
}
