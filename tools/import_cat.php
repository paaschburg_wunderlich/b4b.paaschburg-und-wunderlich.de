<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 20.01.2016
 * Time: 09:09
 */

require_once '../app/Mage.php';
Mage::app();

$class = NEW StendelUpdateCategories();

error_reporting(1);

define ('IMPORT_FILE', 'categories.sdf');
define ('IMPORT_PATH', Mage::getBaseDir('media') . '/import/');

define ('LOG_FILE', 'categories_update.log');
define ('LOG_PATH', '');

define ('TEMP_TABLE', 'stendel_category_temp');
$newPosition = 100;

echo "<pre>";

$coreResource = Mage::getSingleton('core/resource');
$dbRead = Mage::getSingleton('core/resource')->getConnection('core_read');
$dbWrite = Mage::getSingleton('core/resource')->getConnection('core_write');

$class->createTempDbTable($dbWrite, TEMP_TABLE);
$message = "Temp Table [" . TEMP_TABLE . "] angelegt.";
$class->printMessage($message);

$allStores = Mage::app()->getStores();
$process = Mage::getModel('index/indexer')->getProcessByCode('elasticsearch_category');
$process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();

if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($fileData = fgetcsv($fileHandle, 1000, ",")) !== FALSE) {

        $dbWrite->insert(
            $coreResource->getTableName(TEMP_TABLE),
            array(
                'cat_id'                 => $fileData[0],
                'parent_id'              => $fileData[1],
                'cat_path'               => $fileData[2],
                'is_anchor'              => $fileData[3],
                'is_navigation'          => $fileData[4],
                'cat_description_de_DE'  => $fileData[5],
                'cat_description_en_GB'  => $fileData[6],
                'cat_description_cs_CZ'  => $fileData[7],
                'cat_description_fr_FR'  => $fileData[8],
                'cat_description_it_IT'  => $fileData[9],
                'cat_description_pl_PL'  => $fileData[10],
                'cat_description_sv_SE'  => $fileData[11]
            )
        );
    }
}

$result = $dbRead->query("SELECT * FROM `" . TEMP_TABLE . "` ORDER BY `cat_path` ASC;");
while($row = $result->fetch()) {

    $parentCategory = Mage::getModel('catalog/category')->load($row['parent_id']);
    $existCategory = Mage::getModel('catalog/category')->load($row['cat_id']);

    if($row['parent_id'] == 0 AND !$existCategory->getId()) {
        $class->createRootCategory($row['cat_id'], $row['cat_path'], $row['cat_description_de_DE']);
        $message = "(ROOT) Kategorie: " . $row['cat_description_de_DE'] . " erstellt.";
        $class->printMessage($message);
    }

    if($row['parent_id'] != 0) {

        $objCategory = Mage::getModel('catalog/category');
        $objCategory->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        if(!$existCategory->getPosition()) {
            $position = $newPosition;
            $newPosition++;
        } else {
            $position = $existCategory->getPosition();
        }

        if(!$existCategory->getPath()) {
            /** todo Die BASE Cat ermitteln und gegen die 1 tauschen */
            $path = '1/' . $row['cat_path'];
        } else {
            $path = $existCategory->getPath();
        }

        $children = $dbRead->query("SELECT count(cat_id) as count FROM " . TEMP_TABLE . " WHERE cat_path like '%" . $row['cat_id'] . "%'");
        $childrenCount = $children->fetch();

        $aryCategory = array(
            'entity_id'         => $row['cat_id'],
            'path'              => $path,
            'name'              => $row['cat_description_de_DE'],
            'is_active'         => 1,
            'include_in_menu'   => $row['is_navigation'],
            'is_anchor'         => $row['is_anchor'],
            'position'          => $position,
            'display_mode'      => 'PRODUCTS',
            'url_key'           => $row['cat_description_de_DE'],
            'created_at'        => date("Y-m-d H:i:s", time()),
            'children_count'    => $childrenCount['count'],
            'level'             => count(explode('/', $row['cat_path']))
        );
        $objCategory->addData($aryCategory);

        try {
            $objCategory->save();
        } catch (Exception $e) {
            print $e->getMessage();
        }

        foreach ($allStores as $_eachStoreId => $val) {

            $storeId = Mage::app()->getStore($_eachStoreId)->getId();
            $storeIsoCode = Mage::getStoreConfig('general/locale/code', $storeId);

            $categoryTrans = $row['cat_description_' . $storeIsoCode];

            if(!$row['cat_description_' . $storeIsoCode]) {
                $categoryTrans = $row['cat_description_de_DE'];
            }

            $objCategory->setStoreId($storeId);
            $aryCategory = array(
                'entity_id' => $row['cat_id'],
                'name' => $categoryTrans
            );
            $objCategory->addData($aryCategory);

            try {
                $objCategory->save();
            } catch (Exception $e) {
                print $e->getMessage();
            }

            $message = "(STORE) ID: " . $storeId . " - Lang. Code: " . $storeIsoCode . " (TRANS) " . $categoryTrans;
            $class->printMessage($message);
        }
    }

    $message = "***";
    $class->printMessage($message);
}

$process->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
$process->reindexAll();

echo "</pre>";


class StendelUpdateCategories
{
    /**
     * @param $cat_id
     * @param $cat_path
     * @param $cat_name
     * @return bool
     */
    public function createRootCategory($cat_id, $cat_path, $cat_name)
    {
        $objCategory = Mage::getModel('catalog/category');
        $objCategory->setStoreId(Mage_Core_Model_App::ADMIN_STORE_ID);

        $aryCategory = array(
            'entity_id' => $cat_id,
            /** todo Die BASE Cat ermitteln und gegen die 1 tauschen */
            'path' => '1/3',
            'name' => $cat_name,
            'url_key' => $cat_name,
            'is_active' => 1,
            'include_in_menu' => 1,
            'is_anchor'  => 1,
            'position'  => 1,
            'display_mode' => 'PRODUCTS',
            'created_at' => date("Y-m-d H:i:s", time()),
            'children_count' => 5,
            'level' => count(explode('/', $cat_path))
        );
        $objCategory->addData($aryCategory);

        try {
            $objCategory->save();
        } catch (Exception $e) {
            print $e->getMessage();
        }
        return true;
    }


    /**
     * @param $dbWrite
     * @return bool
     */
    public function createTempDbTable($dbWrite, $tempTable)
    {
        $dbWrite->query("DROP TABLE IF EXISTS `" . TEMP_TABLE . "`;");
        $dbWrite->query("CREATE TABLE IF NOT EXISTS `" . TEMP_TABLE . "` (
  `cat_id` int(5) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `cat_path` varchar(128) NOT NULL,
  `is_anchor` int(1) DEFAULT NULL,
  `is_navigation` int(1) DEFAULT NULL,
  `cat_description_de_DE` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_description_en_GB` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_description_cs_CZ` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_description_fr_FR` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_description_it_IT` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_description_pl_PL` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cat_description_sv_SE` varchar(255) CHARACTER SET utf8 NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;");

        $dbWrite->query("ALTER TABLE `" . TEMP_TABLE . "`
  ADD PRIMARY KEY (`cat_id`),
  ADD UNIQUE KEY `cat_id` (`cat_id`);
");

        return true;
    }


    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage);
    }


    /**
     * @param $message
     */
    public function printMessage($message)
    {
        echo "[" . date('Y-m-d H:i:s') . "] " . $message . "\n";
    }
}