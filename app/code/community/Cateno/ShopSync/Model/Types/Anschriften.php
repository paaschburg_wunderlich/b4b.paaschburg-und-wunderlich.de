<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Anschriften-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Anschriften
{
	/**
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var int
	 */
	public $AnsNr;

	/**
	 * @var string
	 */
	public $LandKennz;

	/**
	 * @var string
	 */
	public $Geschlecht;

	/**
	 * @var Cateno_ShopSync_Model_Types_Ansprechpartner
	 */
	public $Ansprechpartner;

	/**
	 * @var Cateno_ShopSync_Model_Types_Freies_Feld
	 */
	public $FreieFelder;

	/**
	 * @var boolean
	 */
	public $StdReKz;

	/**
     * @var boolean
     */
    public $StandardReAns;

    /**
     * @var boolean
     */
    public $StandardLiAns;

	public function __construct($inWShopID = 0)
	{
		$this->WShopID = $inWShopID;
	}
}
