<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Artikel-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Artikel extends Varien_Object
{
	/**
	 * @var string
	 */
	public $ArtNr;

	/**
	 * @var array
	 */
	public $Felder;

	/**
	 * @var boolean
	 */
	public $SetStueckliste;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Stueckliste>)
	 */
	public $Stueckliste;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Varianten>)
	 */
	public $Varianten;

	/**
	 * @var boolean
	 */
	public $ZusaetzeEntfernen;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Tabellen>)
	 */
	public $Tabellen;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Seo>)
	 */
	public $SEO;

        /**
	 * @var array(<Cateno_ShopSync_Model_Types_KategoriePosition>)
	 */
	public $KategoriePositionen;
}
