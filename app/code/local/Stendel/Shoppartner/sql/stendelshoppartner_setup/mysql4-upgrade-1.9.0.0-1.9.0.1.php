<?php

/**
 * Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 */

/**
 * Install script
 *
 * Install script for Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 * @author    Stefan Stendel <stefan.stendel@googlemail.com>
 * @version   2015-05-25 10:32:14Z
 */

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->startSetup();

/**
 * Das Attribut 'store_name' hinzufügen
 */
$installer->addAttribute('customer', 'store_name', array(
    'type'              => 'varchar',
    'input'             => 'text',
    'label'             => 'Firma',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Firmenname des Shoppartner.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'store_name', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'store_name');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', 0)
    ->setData('sort_order', 100)
;
$attribute->save();

$installer->endSetup();
