<?php
/**
 * Autor: Schmidt Medienservice, Wolf Schmidt
 * für Paaschburg & Wunderlich
 * 2016-02-10
 * alle Rechte liegen beim Autor
 */
ini_set("memory_limit",'256M');
ini_set('max_execution_time', 900);

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

?>
<head>
    <meta charset="utf-8">
</head>
<?php
$db_read = Mage::getSingleton('core/resource')->getConnection('core_read');

//echo "Magento_ID;Kundennummer;Email;Anrede;Vorname;;Nachname;;Firma;Ort;Land;;PLZ;Telefon;Fax;;Umsatz netto\n";

$collection = mage::getModel('customer/customer')
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('is_shoppartner', true)
    ->addAttributeToFilter('customer_activated', 1)
    ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
    ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
    ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
    ->addAttributeToSort('entity_id', 'ASC');

echo $collection->count() . " aktive Shoppartner\n\n";
echo "Magento ID;Kunden Nr.;Firmenname;Vorname;Name;Strasse;PLZ;Ort;E-Mail;Subtotal\n";
foreach($collection as $shoppartner)
{
//$shoppartner_wahl = "SELECT  ce.entity_id , ce.email
//                    FROM  customer_entity ce
//                    JOIN customer_entity_int cei
//                    ON cei.entity_id = ce.entity_id
//                    WHERE  ce.is_active = 1 AND ce.group_id = 3 AND cei.attribute_id = 204 AND cei.value = 1
//                    ";
//$shoppartner = $db_read->fetchAll($shoppartner_wahl);
//
//foreach($shoppartner as $column => $value) {
//
//    $shoppartner_kdnr = "SELECT value as kundennummer FROM customer_entity_int WHERE attribute_id = 194 AND entity_id = $value[entity_id]";
//    $kunde = $db_read->fetchRow($shoppartner_kdnr);
//    echo $value['entity_id'] . ";" . $kunde['kundennummer'] . ";" . $value['email'] . ";" ;
//
//    $shoppartner_address = "SELECT value, attribute_id
//                            FROM customer_address_entity_varchar
//                            WHERE entity_id = $value[entity_id]";
//    $shoppartner_adresse = $db_read->fetchAll($shoppartner_address);
    $id = $shoppartner->getEntityId();

    $shoppartner_bestellungen = "SELECT sum(subtotal) as summe_subtotal , sum(subtotal_refunded) as summe_refunded , sum(subtotal_canceled) as summe_canceled
                                FROM sales_flat_order
                                WHERE shoppartner_id = $id";
    $shoppartner_umsatz = $db_read->fetchAll($shoppartner_bestellungen);

    foreach($shoppartner_umsatz as $column => $umsatz) {
        $gesamt = $umsatz['summe_subtotal'] - $umsatz['summe_refunded'] - $umsatz['summe_canceled'];
    }

    echo $shoppartner->getEntityId() . ";" .
        $shoppartner->getErpCustomernumber() . ";" .
        $shoppartner->getStoreName() . ";" .
        $shoppartner->getFirstname() . ";" .
        $shoppartner->getName() . ";" .
        $shoppartner->getBillingStreet() . ";" .
        $shoppartner->getBillingPostcode() . ";" .
        $shoppartner->getBillingCity() . ";" .
        $shoppartner->getEmail() . ";" .
        number_format($gesamt, 2, ',', '.') . "\n";

}

//
//    foreach($shoppartner_adresse as $column => $anschrift) {
//        echo $anschrift['value'] . ";";
//    }

//    echo "\n";
//}
