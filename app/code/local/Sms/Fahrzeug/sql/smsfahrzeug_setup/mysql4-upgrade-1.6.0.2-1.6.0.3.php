<?php

/**
 * Sms_Fahrzeug
 *
 * @category  Mage
 * @package   Sms_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 */

/**
 * Install script
 *
 * Install script for Sms_Fahrzeug to extend the table sms_fahrzeug and add the customer attrubute "fahrzeug_id"
 *
 * @category  Mage
 * @package   Sms_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 * @author    Wolf Schmidtk <wolf.schmidt@schmidt-medien.de>
 * @version   2015-05-19 10:32:14Z
 */

$installer = $this;

$this->startSetup();

$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsbelag_vl_trw', 'varchar(25) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsbelag_vr_trw', 'varchar(50) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsbelag_hi_trw', 'year(4) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsscheibe_vl_trw', 'year(4) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsscheibe_vr_trw', 'varchar(17) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsscheibe_hi_trw', 'varchar(25) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_gabelsimmerringsaetze', 'float(4,0) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kupplungen', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kupplungsfedern', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlagersatz_vorn', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlagersatz_hinten', 'varchar(10) NOT NULL ');

$this->endSetup();
