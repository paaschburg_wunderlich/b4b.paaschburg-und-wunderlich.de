<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Sprachen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Sprachen extends Varien_Object
{
	/**
	 * @var string
	 */
	public $ISOCode;

	/**
	 * @var string
	 */
	public $Bez;

	/**
	 * @var int
	 */
	public $LanguageId;

	public function __construct($inISOCode = null, $inBez = null, $inLanguageId = 0)
	{
		$this->ISOCode = $inISOCode;
		$this->Bez = $inBez;
		$this->LanguageId = $inLanguageId;
	}
}
