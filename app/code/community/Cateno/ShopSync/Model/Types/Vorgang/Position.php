<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Vorgangspositionen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Vorgang_Position
{
	/**
	 * products_model
	 * @var string
	 */
	public $ArtNr;

	/**
	 * products_price
	 * @var float
	 */
	public $EPrNt;

	/**
	 * products_price + tax
	 * @var float
	 */
	public $EPrBt;

	/**
	 * products_name
	 * @var string
	 */
	public $Bez;

	/**
	 * products_qty
	 * @var float
	 */
	public $Mge;

	/**
	 * @var float
	 */
	public $LiefMge;

	/**
	 * products_tax
	 * @var float
	 */
	public $UstSz;

	/**
	 * @var boolean
	 */
	public $IstFrachtPosition;

	/**
	 * NULL
	 * @var string
	 */
	public $KlassenName;

	/**
	 * @var Cateno_ShopSync_Model_Types_Vorgang_Position_Var
	 */
	public $ArtVar;

	/**
	 * @var Cateno_ShopSync_Model_Types_Freies_Feld
	 */
	public $FreieFelder;

	public function __construct($ArtNr = '', $EPrNt = 0.00, $EPrBt = 0.00, $Bez = '', $Mge = 0.00, $UstSz = 0.00, $KlassenName = '', Cateno_ShopSync_Model_Types_Vorgang_Position_Var $ArtVar = null, $IstFrachtPosition = false, Cateno_ShopSync_Model_Types_Freies_Feld $FreieFelder = null)
	{
		$this->ArtNr = $ArtNr;
		$this->EPrNt = $EPrNt;
		$this->EPrBt = $EPrBt;
		$this->Bez = $Bez;
		$this->Mge = $Mge;
		$this->UstSz = $UstSz;
		$this->KlassenName = $KlassenName;
		$this->ArtVar = $ArtVar;
		$this->IstFrachtPosition = $IstFrachtPosition;
		$this->FreieFelder = $FreieFelder;
	}
}
