<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung der Kategorie-ID
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Kategorie_Id extends Varien_Object
{
	/**
	 * category number
	 * @var int
	 */
	public $Nr;

	/**
	 * category_id
	 * @var int
	 */
	public $KatID;

	public function __construct($inNr = 0, $inKatID = 0)
	{
		$this->Nr = $inNr;
		$this->KatID = $inKatID;
	}
}
