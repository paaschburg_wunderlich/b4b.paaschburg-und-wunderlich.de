<?php
/**
 * Sms_Fahrzeug
 *
 * @category  Mage
 * @package   SmS_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 */

/**
 * Update script
 *
 * Update script for Sms_Fahrzeug to add some attributes to products.
 *
 * @category  Mage
 * @package   Sms_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 * @author    Wolf Schmidtk <wolf.schmidt@schmidt-medien.de>
 * @version   2015-05-28 10:32:14Z
 */

// Attribute Artikelgruppe, Artikelkategorie, Artikelkennzeichnung, mkz, mkz_vorn und mkz_hinten an Produkte anfügen

/* @var $installer Mage_Catalog_Model_Entity_Setup */
$installer = $this;
$installer->startSetup();
$installer->addAttribute('catalog_product','sms_artikelgruppe',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Artikelgruppe',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 100,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Artikelgruppe', 'sms_artikelgruppe');

$installer->addAttribute('catalog_product','sms_artikelkategorie',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Artikelkategorie',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 101,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Artikelkategorie', 'sms_artikelkategorie');

$installer->addAttribute('catalog_product','sms_artikelkennzeichnung',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Artikelkennzeichnung',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 102,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Artikelkennzeichnung', 'sms_artikelkennzeichnung');

$installer->addAttribute('catalog_product','sms_modellkennzahl',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Modellkennzahl',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 103,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Modellkennzahl', 'sms_modellkennzahl');

$installer->addAttribute('catalog_product','sms_mkz_vorn',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'MKZ vorn',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 104,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'MKZ vorn', 'sms_mkz_vorn');

$installer->addAttribute('catalog_product','sms_mkz_hinten',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'MKZ hinten',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 105,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'MKZ hinten', 'sms_mkz_hinten');

$installer->endSetup();