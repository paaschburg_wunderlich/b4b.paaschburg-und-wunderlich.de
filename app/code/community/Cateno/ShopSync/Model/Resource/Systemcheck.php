<?php
/**
 * Cateno Systemcheck api
 *
 * Cateno
 * @package Cateno_ShopSync
 * Ressource für Systemcheck
 *
 * @version 0.1.0
*/
class Cateno_ShopSync_Model_Resource_Systemcheck
{
	const MODULE_VERSION = '0.1.0';
	const SUPPORT_MAGENTO_VERSION = '1.4.1.0';
	const SUPPORT_PHP_VERSION = '5.1.6';
	const SHOP_SYSTEM = 'Magento';

	/**
	 * get this module version
	 * @return string
	 */
	public function getModuleVersion()
	{
		return self::MODULE_VERSION;
	}

	/**
	 * Retrieve systemcheck comparePhpVersion
	 * @return string
	 */
	public function comparePhpVersion()
	{
		if( version_compare(PHP_VERSION, self::SUPPORT_PHP_VERSION) >= 0) {
			return 'OK';
		} else {
			return 'ERR';
		}
	}

	/**
	 * get System php version
	 * @return string
	 */
	public function getPHPVersion()
	{
		return phpversion();
	}

	/**
	 * Test php support soap mo.
	 * @return string
	 */
	public function isOpenSoap()
	{
		// get_loaded_extensions()
        if(extension_loaded('soap')) {
			return 'OK';
		} else {
			return 'ERR';
		}
	}

	/**
	 * Get Shop System.
	 * @return string
	 */
	public function getShopSystem()
	{
		return self::SHOP_SYSTEM;
	}

	/**
	 * Compare Magento version must lt 1.4.1.0
	 * @return string
	 */
	public function compareMagentoVersion()
	{
		if(version_compare(Mage::getVersion(), self::SUPPORT_MAGENTO_VERSION) >= 0) {
			return 'OK';
		} else {
			return 'ERR';
		}
	}

	/**
	 * get Magento Version
	 * @return string
	 */
	public function getMagentoVersion()
	{
		return Mage::getVersion();
	}
}
