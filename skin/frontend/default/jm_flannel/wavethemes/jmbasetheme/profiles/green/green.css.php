<?php

$slideNavColor = "#00ff00";


 $mageFilename = "../../../../../../../../app/Mage.php";
 require_once $mageFilename;
 umask(0);
 Mage::app();
 $activeprofile = basename(__FILE__, '.css.php');
 $baseconfig = Mage::helper("jmbasetheme")->getactiveprofile($activeprofile);
 header("Content-type: text/css; charset: UTF-8");
?>

/* Base settings */
body#bd {
   background-color: <?php echo $baseconfig["bgolor"]; ?>;
   background-image:url("images/<?php echo $baseconfig["bgimage"]; ?>");
<!--   background-position: center top;-->
<!--   background-repeat: no-repeat;-->
<!--   background-color: #f8f8f8;-->
   background: rgb(224,224,224); /* Old browsers */
   background: -moz-linear-gradient(top,  rgba(224,224,224,1) 0%, rgba(224,224,224,1) 33%, rgba(248,248,248,1) 100%); /* FF3.6+ */
   background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(224,224,224,1)), color-stop(33%,rgba(224,224,224,1)), color-stop(100%,rgba(248,248,248,1))); /* Chrome,Safari4+ */
   background: -webkit-linear-gradient(top,  rgba(224,224,224,1) 0%,rgba(224,224,224,1) 33%,rgba(248,248,248,1) 100%); /* Chrome10+,Safari5.1+ */
   background: -o-linear-gradient(top,  rgba(224,224,224,1) 0%,rgba(224,224,224,1) 33%,rgba(248,248,248,1) 100%); /* Opera 11.10+ */
   background: -ms-linear-gradient(top,  rgba(224,224,224,1) 0%,rgba(224,224,224,1) 33%,rgba(248,248,248,1) 100%); /* IE10+ */
   background: linear-gradient(to bottom,  rgba(224,224,224,1) 0%,rgba(224,224,224,1) 33%,rgba(248,248,248,1) 100%); /* W3C */
   filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e0e0e0', endColorstr='#f8f8f8',GradientType=0 ); /* IE6-9 */
}

#ja-mycart .btn-toggle.active,
#ja-mycart .btn-toggle:hover {
   background-color:<?php echo $baseconfig["color"]; ?>;
}

a,
.shop-access a:hover, /*Quick Access---*/
.shop-access a:active,
.shop-access a:focus,

#nav li.active a,   /* 0 Level */
#nav li.over a,
#nav a:hover,

#ja-pathway ul li strong,  /*breadcrums---*/
#ja-pathway ul li a,

.block-navigation #ja-sidenav li.active a,

.ja-spotlight .col-1 .block-content a, /* Spotlight */
.ja-spotlight .block .block-content a:hover,
.ja-spotlight .block .block-content a:focus,
.ja-spotlight .block .block-content a:active,
.socail-footer a:hover,
.ja-topsl_bottom li span.color-text3,

#ja-footer li a, /* FOOTER */

.pager .pages .current, /* Pager magento 1.4*/

.ratings .rating-links a:hover, /*RATINGS */
.ratings .rating-links a:focus,
.ratings .rating-links a:active,

.price, /*PRICES */
.regular-price .price,
.special-price .price,

a.minimal-price-link .price, /*Minimal price (as low as)---*/

.price-excluding-tax .price, /*Excluding tax---*/

.price-including-tax .price, /*Including tax---*/

.add-to-links a:focus, /*Add To---*/

.block-cart .subtotal .price, /*Mini cart - top---*/

ol#cart-sidebar .price,

.block-navigation #ja-sidenav li a:active, /*Layered Navigation---*/

.product-name a:hover, /*Product---*/
.product-name a:focus,
.product-name a:active,
.products-grid li.item:hover .product-name a,

.product-essential .availability.in-stock span,

ul.ja-tab-navigator li a:hover, /*PRODUCT TABS */
ul.ja-tab-navigator li a:focus,
ul.ja-tab-navigator li a:active,

.email-friend a:hover, /*Product Info---*/
.email-friend a:focus,
.add-to-links a:hover,
.add-to-links a:focus,

.giftmessages .gift-header, /*Gift Messages----*/

button.btn-cart:hover,
button.btn-cart:focus,
button.button.btn-cart:hover,
button.button.btn-cart:focus,
.products-grid li.item:hover button.btn-cart,

#multiship-addresses-table button, /*Multiple Addresses checkout---*/
.place-order-box .grand-total .price,

.block-account li a:hover, /*My Account navigation---*/
.block-account li a:active,
.block-account li a:focus,

.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active,

.jm-megamenu ul.level1 li.mega a.mega span.menu-title,

.jm-megamenu .megamenu.level2 li.mega a.mega:hover span.menu-title,

.jm-megamenu .category-products .products-list li.item a:hover,

.jmzin-content:hover .product-name a
 {
	color:<?php echo $baseconfig["color"]; ?>;
}

.input-text:focus, select:focus, textarea:focus,
#ja-search .input-text:focus,
.block-subscribe .input-box input:focus,

button.button:hover,
button.button:focus,
button:hover,
button:focus,

.truncated a.details,

.block-navigation #ja-sidenav li a:active,

.product-img-box .more-views li a:hover,

.opc .allow .number,
.opc .active .number,

.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active,

.jm-megamenu ul.level0 li.active  {
	border-color:<?php echo $baseconfig["color"]; ?>;
}

.static-top1 .inner1 button,

button.button:hover,
button.button:focus,
button:hover,
button:focus,

.add-to-cart button.button.btn-cart, /* detail page */

#ja-mycart .active + .inner-toggle,

ul.ja-tab-navigator li.active,

.opc .allow .number,
.opc .active .number,

.checkout-progress li.active,
.shipment-count,
.box-account ol .number,

.jm-megamenu ul.level0 li.active,

.jm-mask-desc .readmore a.readon /* slide show */,

.static-top1 .inner1 .widget-cms-link a,

.static-top2 .inner1 .widget-cms-link a,

.jm-megamenu .jm-product-deals.grid.block,

button.button.btn-proceed-checkout

  {
	background-color:<?php echo $baseconfig["color"]; ?>;
}

.quick-access,
#ja-mainnav-inner {
	background-color:<?php echo $baseconfig["headerolor"]; ?>;

	background-image:url("images/<?php echo $baseconfig["headerimage"]; ?>");
}

#ja-botsl .main .inner,
#ja-footer .main .inner {
	background-color:<?php echo $baseconfig["footerolor"]; ?>;
	background-image:url("images/<?php echo $baseconfig["footerimage"]; ?>");
}

.product-img-box .product-image-zoom {
	width: <?php echo $baseconfig["productlimagewidth"]; ?>px;
	height: <?php echo $baseconfig["productlimageheight"]; ?>px;
}

@media only screen and (max-width:719px) {
	#ja-mainnav-inner {
		background-color:<?php echo $baseconfig["color"]; ?> ;
	}
}

/* Profile settings */

h1#logo a {
	background-image: url("images/<?php echo $baseconfig["bluelogo"]; ?>") !important;
}

button.btn-cart, button.button.btn-cart,
button.btn-cart:hover,
button.btn-cart:focus,
button.button.btn-cart:hover,
button.button.btn-cart:focus,
.products-grid li.item:hover button.btn-cart,
#wishlist-table td:nth-child(3n) button.btn-cart {
	background-image: url("images/<?php echo $baseconfig["blueaddtocart"]; ?>");
}

.block .block-title,
.page-title {
	background-image: url("images/<?php echo $baseconfig["bluetitle"]; ?>");
}

.add-to-links a.link-compare {
	background-image: url("images/<?php echo $baseconfig["bluecompare"]; ?>");
}

.add-to-links a.link-wishlist {
	background-image: url("images/<?php echo $baseconfig["bluewishlist"]; ?>");
}

.email-friend a {
	background-image: url("images/<?php echo $baseconfig["blueemail"]; ?>");
}

.socail-footer a {
	background-image: url("images/<?php echo $baseconfig["bluesocial"]; ?>");
}

.jm-lemmon-slider .controls .next-slide,
.jm-product-lemmon .controls .next-slide {
	background-image: url("images/<?php echo $baseconfig["bluenext"]; ?>");
}

.jm-lemmon-slider .controls .prev-slide,
.jm-product-lemmon .controls .prev-slide {
	background-image: url("images/<?php echo $baseconfig["bluepre"]; ?>");
}

a.readon,
a.link-learn {
	background-image: url("images/<?php echo $baseconfig["bluereadmore"]; ?>");
}

.jmzin-content:hover a.readon,
.jmzin-content:hover a.readon:hover,
.jmzin-content:hover a.readon:focus,
.jmzin-content:hover a.readon:active,
a.link-learn:hover,
a.link-learn:focus,
a.link-learn:active {
	color:<?php echo $baseconfig["color"]; ?>;
}

.static-top1 .bg-color {
	background-color:<?php echo $baseconfig["secondcolor"]; ?>;
}

.static-top2 .bg-color {
	background-color:<?php echo $baseconfig["thirdcolor"]; ?>;
}





/** EXTRA OVERRIDES BY STENDEL**/
.block-navigation #ja-sidenav li.active a,
.block-navigation #ja-sidenav li a:active, /*Layered Navigation---*/
.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active
{
   color: <?php echo $slideNavColor; ?>;
}

.block-navigation #ja-sidenav li a:active,
.block-navigation #ja-sidenav li a:hover, /*Left navigation---*/
.block-navigation #ja-sidenav li a:focus,
.block-navigation #ja-sidenav li a:active
{
   border-color: <?php echo $slideNavColor; ?>;
}

#ja-botsl .main .inner {
   background: rgb(0,48,0); /* Old browsers */
   background: -moz-linear-gradient(top,  rgba(0,48,0,1) 0%, rgba(0,0,0,1) 100%); /* FF3.6+ */
   background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,48,0,1)), color-stop(100%,rgba(0,0,0,1))); /* Chrome,Safari4+ */
   background: -webkit-linear-gradient(top,  rgba(0,48,0,1) 0%,rgba(0,0,0,1) 100%); /* Chrome10+,Safari5.1+ */
   background: -o-linear-gradient(top,  rgba(0,48,0,1) 0%,rgba(0,0,0,1) 100%); /* Opera 11.10+ */
   background: -ms-linear-gradient(top,  rgba(0,48,0,1) 0%,rgba(0,0,0,1) 100%); /* IE10+ */
   background: linear-gradient(to bottom,  rgba(0,48,0,1) 0%,rgba(0,0,0,1) 100%); /* W3C */
   filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#003000', endColorstr='#000000',GradientType=0 ); /* IE6-9 */
}

button.button, button {
   background: none repeat scroll 0 0 #005500;
}