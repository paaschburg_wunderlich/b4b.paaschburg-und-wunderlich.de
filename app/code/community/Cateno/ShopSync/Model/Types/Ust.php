<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Umsatzsteuer-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Ust extends Varien_Object
{
	/**
	 * @var int
	 */
	public $Schluessel;

	/**
	 * @var float
	 */
	public $Satz;

	/**
	 * @var string
	 */
	public $Bezeichnung;
}
