<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 25.05.2016
 * Time: 11:47
 */

include_once ('config.inc.php');

class B4b_Abfragen
{

    /**
     * @param $path
     * @param $fileHandle
     * @param $param
     * @return resource
     */
    public function openFile($path, $fileHandle, $param)
    {
        return fopen($path . $fileHandle, $param);
    }


    /**
     * @param $fileHandle
     * @return bool
     */
    public function closeFile($fileHandle)
    {
        return fclose ($fileHandle);
    }


    /**
     * @param $logMessage
     * @param $fileHandle
     * @return int
     */
    public function writeLogEntry($logMessage, $fileHandle)
    {
        return fputs($fileHandle, "[" . date('Y-m-d H:i:s') . "] " . $logMessage . "\n");
    }


    /**
     * @param $message
     * @param $logFileHandle
     * @return int
     */
    public function printMessage($message, $logFileHandle)
    {
        if(WRITE_LOGFILE == true) {
            return $this->writeLogEntry($message, $logFileHandle);
        } else {
            echo "[" . date('Y-m-d H:i:s') . "] " . $message . "\n";
        }
    }


    /**
     * @param $image
     * @return string
     */
    public function extrudeImage($image)
    {
        $needle = "\\";

        if ($image == "(Leer)") {
            $image = "";
        } else {
            $pos = strripos ( $image , $needle);
            if ($pos != 0) {
                $pos = $pos + 1;
            }
            $image = substr($image,$pos);
        }
        return $image;
    }


    /**
     * @param $attribute_code
     * @param $data
     * @param $theProductObj
     */
    public function setAttributeOption($attribute_code, $data, $theProductObj) {
        $attribute = Mage::getModel('catalog/product')->getResource()->getAttribute($attribute_code);
        $attribute_value_id = $attribute->getSource()->getOptionId($data);
        $theProductObj->addAttributeUpdate($attribute_code, $attribute_value_id);
    }


    /**
     * @param $indexMethodName
     * @return mixed
     */
    public function setIndexModeToManual($indexMethodName, $logFileHandle)
    {
        $message = "Index [" . $indexMethodName . "] auf 'MODE_MAUAL' gesetzt";
        $this->printMessage($message, $logFileHandle);
        $processIndex = Mage::getSingleton('index/indexer')->getProcessByCode($indexMethodName);
        return $processIndex->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
    }


    /**
     * @return bool
     */
    public function setAllIndexesToManual()
    {
        $processCollection = Mage::getSingleton('index/indexer')->getProcessesCollection();
        foreach ($processCollection as $process) {
            $process->setMode(Mage_Index_Model_Process::MODE_MANUAL)->save();
        }
        return true;
    }

    /**
     * @param $indexMethodName
     * @return mixed
     */
    public function setIndexModeToRealTime($indexMethodName, $logFileHandle)
    {
        $message = "Index [" . $indexMethodName . "] auf 'MODE_REAL_TIME' gesetzt";
        $this->printMessage($message, $logFileHandle);
        $processIndex = Mage::getSingleton('index/indexer')->getProcessByCode($indexMethodName);
        return $processIndex->setMode(Mage_Index_Model_Process::MODE_REAL_TIME)->save();
    }

    /**
     * @param $indexMethodName
     * @param $logFileHandle
     */
    public function  rebuildIndex($indexMethodName, $logFileHandle)
    {
        $processIndex = Mage::getModel('index/indexer')->getProcessByCode($indexMethodName);
        $message = "Reindiziere [" . $indexMethodName . "]";
        $this->printMessage($message, $logFileHandle);
        $reindexStart = microtime(true);
        $processIndex->reindexAll();
        $reindexEnd = microtime(true);
        $message = "Index [" . $indexMethodName . "] neu aufgebaut in [ " . number_format($reindexEnd - $reindexStart, 2) . " ] Sekunden";
        $this->printMessage($message, $logFileHandle);
    }


    /**
     * @param $myProductData
     * @param $logFileHandle
     */
    public function updateProduct($myProductData, $logFileHandle)
    {
        /** ISO Code des default Store ermitteln */
        $defaulStoreIsoCode = $this->getStoreIsoCode($this->getDefaultStoreId());

        if($myProductData['store_' . $defaulStoreIsoCode] == 1) {

            Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);
            $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($myProductData['sku']));

            /** $externalImageGallery vorbereiten */
            $externalImageGallery = array(
                'bild2' => $myProductData['bild2'],
                'bild3' => $myProductData['bild3'],
                'bild4' => $myProductData['bild4']
            );
            rsort($externalImageGallery);
            $externalImageGallery = join(',', array_filter($externalImageGallery));

            /** Das feld isInStock vorbereiten */
            $myProductData['auf_Lager'] == "Nicht auf Lager" ? $isInStock = 0 : $isInStock = 1;

            /** Sichtbarkei vorbereiten */
            if ($myProductData['sichtbar'] == "Katalog / Suche") {
                $sichtbar = 4;
            }
            if ($myProductData['sichtbar'] == "Katalog") {
                $sichtbar = 2;
            }
            if ($myProductData['sichtbar'] == "Suche") {
                $sichtbar = 3;
            }
            if ($myProductData['sichtbar'] == "Einzeln nicht sichtbar") {
                $sichtbar = 1;
            }

            /** Kategorien vorbereiten */
            $categories = array(
                $myProductData['art_cat1'],
                $myProductData['art_cat2'],
                $myProductData['art_cat3'],
                $myProductData['art_cat4'],
                $myProductData['art_cat5'],
                $myProductData['art_cat6'],
                $myProductData['art_cat7'],
                $myProductData['art_cat8'],
                $myProductData['art_cat9'],
                $myProductData['art_cat10'],
            );

            /** Categories Baum laden */
            $catIds = $this->getCategoriesTree();

            /** Prüfen ob exportierte Kategorie vorhanden und gesettz */
            foreach ($categories as $categoryId) {
                if ($categoryId != 0) {
                    $categoryIds = $theProductObj->getCategoryIds();
                    if (!in_array($categoryId, $categoryIds)) {
                        if (in_array($categoryId, $catIds)) {
                            $theProductObj->setCategoryIds(array($categories));
                        }
                    }
                }
            }

            /** Prüfen ob gesetzte Kategorien noch gültig ist */
            $categoryIds = $theProductObj->getCategoryIds();
            foreach ($categoryIds as $_categoryId) {
                if (!in_array($_categoryId, $categories)) {
                    $theProductObj->setCategoryIds(array($categories));
                }
            }

            /** Prüfen ob die Spalte 'preis_'default store iso code' existiert */
            $mysqli = $this->db();
            $query = "SHOW COLUMNS FROM `products_temp` LIKE 'preis_" . $this->getStoreIsoCode($this->getDefaultStoreId()) . "'";
            $mysqli->query($query);

            /** Wenn ja dann verwenden. Wenn nein den deutschen Preis nehmen */
            if ($mysqli->affected_rows != 0) {
                /** Prüfen ob das preis Feld gefüllt ist. Wenn 'leer' dann abbrechen und nächsten Artikel nehmen  */
                if ($myProductData['preis_' . $defaulStoreIsoCode] != '0.0000') {
                    $price = $myProductData['preis_' . $defaulStoreIsoCode];
                } else {
                    $message = $myProductData['sku'] . " In der default Sprache [" . $defaulStoreIsoCode . "] ist kein Preis vorhanden!!!";
                    $this->printMessage($message, $logFileHandle);
                    return false;
                }
                $specialPrice = $myProductData['sonderpreis_' . $defaulStoreIsoCode];
                $specialPriceFrom = $myProductData['sp_von_' . $defaulStoreIsoCode];
                $specialPriceTo = $myProductData['sp_bis_' . $defaulStoreIsoCode];
            } else {
                $price = $myProductData['preis_de_DE'];
                $specialPrice = $myProductData['sonderpreis_de_DE'];
                $specialPriceFrom = $myProductData['sp_von_de_DE'];
                $specialPriceTo = $myProductData['sp_bis_de_DE'];
            }

            if ($specialPriceFrom === "30.12.1899") {
                $specialPriceFrom = "01.01.1970";
            }
            if ($specialPriceTo === "30.12.1899") {
                $specialPriceTo = "02.01.1970";
            }

            if ($theProductObj->getId()) {
                $creationStatus = 'geaendert';
                $creationTime = $theProductObj->getCreatedAt();
                $updateTime = strtotime('now');
            } else {
                $creationStatus = 'neu angelegt';
                $creationTime = strtotime('now');
                $updateTime = $theProductObj->getUpdatedAt();
            }

            try {
                $theProductObj
                    ->setAttributeSetId(4)
                    ->setTypeId('simple')
                    ->setCreatedAt($creationTime)
                    ->setUpdatedAt($updateTime)
                    ->setSku($myProductData['sku'])
                    ->setVisibility($sichtbar)
                    /** Als Standard fuer das backend die DE verwenden */
                    ->setName($myProductData['name_de_DE'])
                    ->setDescription($myProductData['description_de_DE'])
                    ->setShortDescription($myProductData['shortdesc_de_DE'])
                    ->setWeight($myProductData['gewicht'])
                    ->setStatus($myProductData['blocked'])
                    ->setTaxClassId(2)
                    ->setBarcode($myProductData['barcode'])
                    /** PREISE */
                    ->setPrice($price)
                    ->setSpecialPrice($specialPrice)
                    ->setSpecialFromDate(date_format(date_create($specialPriceFrom), 'Y-m-d 00:00:00'))
                    ->setSpecialToDate(date_format(date_create($specialPriceTo), 'Y-m-d 00:00:00'))
                    ->setStockData(array(
                            'use_config_manage_stock' => 0,
                            'manage_stock' => 1,
                            'is_in_stock' => $isInStock,
                            'qty' => $myProductData['verf_menge']
                        )
                    )
                    /** KATEGORIEN */
                    ->setCategoryIds(array($categories))
                    /** BILDER */
                    ->setUseExternalImages(1)
                    ->setImageExternalUrl($myProductData['bild1'])
                    ->setSmallImageExternalUrl($myProductData['bild1'])
                    ->setThumbnailExternalUrl($myProductData['bild1'])
                    ->setExternalGallery($externalImageGallery)
                    
                    ->setSmsArtikelgruppe($myProductData['art_gruppe'])
                    ->setSmsArtikelkategorie($myProductData['art_kat'])
                    ->setSmsArtikelkennzeichnung($myProductData['art_kennz'])
                    ->setSmsKettenglieder($myProductData['kettenglieder'])
                    ->setSmsKettenteilung($myProductData['kettenteilung'])
                    ->setSmsMkzHinten($myProductData['mkz_hinten'])
                    ->setSmsMkzVorn($myProductData['mkz_vorn'])
                    ->setSmsModellkennzahl($myProductData['modellkennzahl'])
                    ->setSmsZaehneanzahl($myProductData['zaehnezahl'])
                    ->setErweiterteSuche($myProductData['erw_suche'])
                    ->setXref($myProductData['xref']);
                $theProductObj->save();

                $allStores = $this->getAllStoreViews();

                /** Hier über alle Storeviews laufen */
                foreach ($allStores as $eachStoreId => $val) {
                    /** Auf entsprechenden StoreView umstellen */
                    Mage::app()->setCurrentStore($eachStoreId);

                    /** Artikel für jede Sprache neu laden */
                    $theProductObj = Mage::getModel('catalog/product')->load(Mage::getModel('catalog/product')->getIdBySku($myProductData['sku']));

                    /** Iso Code ermitteln */
                    $storeIsoCode = $this->getStoreIsoCode($eachStoreId);

                    /** Sprachabhängige Felder vorbereiten */
                    $theName = $myProductData['name_' . $storeIsoCode];
                    $theShortDescription = $myProductData['shortdesc_' . $storeIsoCode];
                    $theDescription = $myProductData['description_' . $storeIsoCode];

                    /** Alle relevanten Werte setzen */
                    $theProductObj
                        ->setStoreId($eachStoreId)
                        ->setWebsiteIds(array(
                                Mage::app()->getStore($eachStoreId)->getId()
                            )
                        )
                        ->setName($theName)
                        ->setShortDescription($theShortDescription)
                        ->setDescription($theDescription);

                    /** Status Ausgabe */
                    $message = "Artikel [" . $myProductData['sku'] . "] fuer die Sprache [" . $storeIsoCode . "] erfolgreich " . $creationStatus . ".";
                    $this->printMessage($message, $logFileHandle);
                }
                /** Artikel speichern */
                $theProductObj->save();

            } catch (Exception $e) {
                Mage::log($e->getMessage());
                echo $e->getMessage();
            }
        } else {
            /** Status Ausgabe */
            $message = "Artikel [" . $myProductData['sku'] . "] ist fuer den Store [" . $defaulStoreIsoCode . "] nicht vorgesehen.";
            $this->printMessage($message, $logFileHandle);
        }
    }


    /**
     * @return array
     */
    public function getAllStoreViews()
    {
        return Mage::app()->getStores();
    }


    /**
     * @return mixed
     * @throws Mage_Core_Exception
     */
    public function getDefaultStoreId()
    {
        return Mage::app()
            ->getWebsite()
            ->getDefaultGroup()
            ->getDefaultStoreId();
    }


    /**
     * @param $storeId
     * @return mixed
     */
    public function getStoreIsoCode($storeId)
    {
        return Mage::getStoreConfig('general/locale/code', $storeId);
    }


    /**
     * @return mixed
     */
    public function getCategoriesTree()
    {
        $cat = Mage::getModel('catalog/category');
        $catTree = $cat->getTreeModel()->load();
        return $catTree->getCollection()->getAllIds();
    }


    /**
     * @return mysqli
     */
    public function db()
    {
        $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
        if ($mysqli->connect_errno) {
            printf("Connect failed: %s\n", $mysqli->connect_error);
            exit();
        }
        $mysqli->set_charset("utf8");
        return $mysqli;
    }

}
