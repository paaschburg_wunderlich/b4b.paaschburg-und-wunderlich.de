<?php
/**
* Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Anschriften
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Anschriften
{
	/**
	 * @param Mage_Cusomter_Model_Customer $inCustomer
	 * @param Cateno_ShopSync_Model_Types_Anschriften $inAnschriften
	 * @return array
	 */
    public function setAnschriften($inCustomer, $inAnschriften)
    {
        $theAnschriftenArray = array();

        $theDefaultBillingAddress = $inCustomer->getDefaultBillingAddress();

        foreach($inCustomer->getAddressesCollection() as $theAddress) {

            $theAnschrift = new Cateno_ShopSync_Model_Types_Anschriften($theAddress->getId());
            $theAnschrift->AnsNr = $theAddress->getId();
            $theAnschrift->FreieFelder = Mage::getModel('shopsync/freies_feld')->setFreieFelder($theAddress, $inAnschriften);
            $theAnschrift->LandKennz = $theAddress->getCountryId();

            $theAnschrift->StdReKz = false;
            if($theDefaultBillingAddress != ''){
                $theAnschrift->StdReKz = $theDefaultBillingAddress->getId() == $theAddress->getId();
            }
            //get default billing or default shipping
            if($inCustomer->getData('default_billing') == $theAddress->getId()){
                $theAnschrift->StandardReAns = true;
            }else $theAnschrift->StandardReAns = false;

            if($inCustomer->getData('default_shipping') == $theAddress->getId()){
                $theAnschrift->StandardLiAns = true;
            }else $theAnschrift->StandardLiAns = false;
            $theAnschriftenArray[] = $theAnschrift;
        }

        return $theAnschriftenArray;
    }
}
