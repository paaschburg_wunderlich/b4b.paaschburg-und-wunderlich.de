<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für Sprachen-Übertragung
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Sprachen
{
	/**
	 * Cateno_ShopSync_Model_Types_Sprachen
	 * return array
	 */
	public function getLanguages()
	{
		$theLanguages = array();
		$theLanguages[] = new Cateno_ShopSync_Model_Types_Sprachen(
            'default',
            'default',
            0
        );
		foreach(Mage::getModel('core/website')->getCollection()->joinGroupAndStore() as $theWebsite) {
			$theLocale = Mage::getModel('core/locale');
			$theLocale->emulate($theWebsite->getStoreId());
			$theISOCode = explode('_', $theLocale->getLocale());


			$theLanguages[] = new Cateno_ShopSync_Model_Types_Sprachen(
				$theISOCode[0],
				$theWebsite->getName() . '/' . $theWebsite->getGroupTitle() . '/' . $theWebsite->getStoreTitle(),
				$theWebsite->getStoreId()
			);
		}
		return $theLanguages;
	}
}
