<?php

/**
 * Stendel_OrderComment
 *
 * @category  Stendel
 * @package   Stendel_OrderComment
 * @version   1.0.0
 * @copyright 2015 Stefan Stendel (www.foto-stendel.de)
 */

class Stendel_Shoppartner_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->getLayout()->getBlock('root')->setTemplate("page/2columns-left.phtml");
        $this->renderLayout();
    }

}