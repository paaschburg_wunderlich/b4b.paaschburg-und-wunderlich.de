<?php
/**
 * Cateno
 *
 * @category Single Class
 * @package Cateno_ShopSync
 * Einstiegspunkt für Abruf von Shop/Webserver-Info
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Service_Allgemein
{
    const XML_PRICE_INCLUDE_TAX = 'tax/calculation/price_includes_tax';
	/**
	 *
	 * @return Cateno_ShopSync_Model_Types_Shop_Info
	 */
	public function getShopInfo()
	{
		$theShopInfo = Mage::getSingleton('cateno_shopsync_model_types_shop_info');
		$theShopInfo->DbPrefix = (string) Mage::getConfig()->getTablePrefix();
		$theShopInfo->Tabellen = Mage::getModel('shopsync/tabellen')->getTabellen();
		$theShopInfo->sprachen = Mage::getModel('shopsync/sprachen')->getLanguages();
		$theShopInfo->versandArten = Mage::getModel('shopsync/modul')->getShippingMethods();
		$theShopInfo->zahlungsOptionen = Mage::getModel('shopsync/modul')->getPaymentMethods();
		$theShopInfo->ZusammenfassungsModule = array();//Mage::getSingleton('shopsync/types_zusammenfassungs_module');
        $theShopInfo->vorgangStatus = Mage::getModel('shopsync/vorgang_status')->getVorgangStatus();
		$theShopInfo->shopSystem = 5;

        //tax price
        $theShopInfo->price_includes_tax = array();
		$theShopInfo->price_definition = array();
		$isGlobal = true;
		if(!Mage::helper('catalog')->isPriceGlobal()) {
			$isGlobal = false;
			$theTaxArr = array();
			$theStores = Mage::app()->getStores();
			foreach($theStores as $theStore) {
			    Mage::app()->setCurrentStore($theStore->getId());
			    $theWebSite = Mage::app()->getWebsite();
			    if(!in_array($theWebSite, $theWebSites)){
			        $theWebSites[] = $theWebSite;
			        $theTaxArr[] = array(
	                    'IsIncludeTax'=> Mage::getStoreConfig(self::XML_PRICE_INCLUDE_TAX, $theStore),
	                    'WebSite' => $theWebSite->getId()
	                );
			    }
			}

			$theShopInfo->price_includes_tax = array_reverse($theTaxArr);
		}

		// price scope is global
        if(Mage::getStoreConfig(Cateno_ShopSync_Model_Service_Allgemein::XML_PRICE_INCLUDE_TAX, 0)) {
        	$thePriceDefinitionArr[] = array('IsIncludeTax' => 1, 'IsGlobal' => $isGlobal);
        }else {
        	$thePriceDefinitionArr[] = array('IsIncludeTax' => 0, 'IsGlobal' => $isGlobal);
        }
        $theShopInfo->price_definition = $thePriceDefinitionArr;

		$theShopInfo->ArtikelBilderPfad = false;
		$theShopInfo->artikelPreisGruppen = Mage::getModel('shopsync/artikel_preis_gruppen')->getArtikelPreisGroups();
		$theShopInfo->MultiShops = Mage::getModel('shopsync/multi_shops')->getMultiShops();
		return $theShopInfo;
	}


	/**
	 * Magento auto validate module attribute, so return true.
	 * @return boolean
	 */
	public function PruefeDatenbank()
	{
		return true;
	}

	/**
	 * @param $inUst array('key', 'tax', 'title')
	 * @return boolean
	 */
	public function setUst($inUst)
	{
		if(is_array($inUst) && count($inUst)) {

			Mage::helper('shopsync')->truncateTax();

			foreach($inUst as $theItem) {
				$theProductTaxClass = Mage::getModel('tax/class');
				$theProductTaxClass->setClassId($theUst->Schluessel);
				$theProductTaxClass->setClassType('PRODUCT');
				$theProductTaxClass->setClassName($theItem->Bezeichnung);
				$theProductTaxClass->save();

				$theTaxRate = Mage::getModel('tax/calculation_rate');
				$theTaxRate->setTaxCountryId(Mage::helper('shopsync')->getDefaultTaxCountry());
				$theTaxRate->setTaxRegionId('*');
				$theTaxRate->setTaxPostcode('*');
				$theTaxRate->setCode($theItem->Bezeichnung);
				$theTaxRate->setRate($theItem->Satz);
				$theTaxRate->save();
				/* theTaxRate get tax_calculation_rate_id */

				$theTaxRule = Mage::getModel('tax/calculation_rule');
				$theTaxRule->setCode($theItem->Bezeichnung);
				$theTaxRule->setPriority(1);
				$theTaxRule->setPosition(1);
				$theTaxRule->save();
				/* theTaxRule get tax_calculation_rule_id */

				$theTaxCalculation = Mage::getModel('tax/calculation');
				$theTaxCalculation->setTaxCalculationRateId($theTaxRate->getTaxCalculationRateId());
				$theTaxCalculation->setTaxCalculationRuleId($theTaxRule->getTaxCalculationRuleId());
				$theTaxCalculation->setCustomerTaxClassId(Mage::helper('shopsync')->getDefaultCustomerTaxClass());
				$theTaxCalculation->setProductTaxClassId($theItem->Schluessel);
				$theTaxCalculation->save();
			}
			return true;
		}
		return true;
	}
}
