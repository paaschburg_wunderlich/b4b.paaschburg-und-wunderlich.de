<?php

/**
 * SMS_Shoppartner_Erweiterung
 *
 * @category  Shoppartner
 * @package   Stendel_Shcoppartner
 * @version   1.0.0
 * @copyright 2016 Wolf Schmidt, Schmidt Medienservice (www.schmidt-medien.de)
 */

class Stendel_Shoppartner_SetController extends Mage_Core_Controller_Front_Action
{

    public function idAction()
    {
        $_SESSION['spid'] = "";
        foreach ($this->getRequest()->getParams() as $key => $value) {
            $active = Mage::helper('stendel_shoppartner')->getShoppartnerStatus($value);
            if (!empty($active)) {
                $_SESSION['spid'] = $value;
            }
        }
        $this->_redirect('/');
    }
}