<?php

/**
 * Stendel_Shoppartner
 *
 * @category  Stendel
 * @package   Stendel_Shoppartner
 * @version   1.0.0
 * @copyright 2015 Stefan Stendel (www.foto-stendel.de)
 */

class Stendel_Shoppartner_Model_Observer
{

    /**
     *
     */
    const SHOPPARTNER_FIELD_ID = 'customer_shoppartner';

    /**
     * Check if customer notice should be added to order.
     *
     * @param Varien_Event_Observer $observer
     * @return $this
     */
    public function addCustomerShoppartnerToOrder(Varien_Event_Observer $observer)
    {
        /** @var Mage_Sales_Model_Order $order */
        $order = $observer->getEvent()->getOrder();

        /** @var Mage_Core_Controller_Request_Http $request */
        $request = Mage::app()->getRequest();

        if (!is_object($order) || !$request)
            return $this;

//        $shoppartnerId = strip_tags($request->getParam(self::SHOPPARTNER_FIELD_ID));
        $shoppartnerId = Mage::getSingleton('core/session')->getShoppartnerId();

        if (!empty($shoppartnerId)) {
            $this->_addCustomerShoppartnerIdToOrder($order, $shoppartnerId);
            $this->_addCustomerShoppartnerKdnrToOrder($order, $shoppartnerId);
            $this->_sendEmailToShoppartner($order, $shoppartnerId);
            $this->_addPaymentMethodToOrder($order);
            if (Mage::getSingleton('checkout/session')->getQuote()->getShippingAddress()->getShippingMethod() == "flatrate_flatrate") {
                $this->_addShippingAddressToOrder($order, $shoppartnerId);
            }
        }
        return $this;
    }

    /**
     * @param $order
     * @param $shoppartnerId
     * @return $this
     */
    protected function _addShippingAddressToOrder($order, $shoppartnerId)
    {
        $collection = Mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('entity_id', $shoppartnerId)
            ->addAttributeToFilter('is_shoppartner', true)
            ->addAttributeToFilter('customer_activated', 1)
            ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
            ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
            ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
            ->joinAttribute('billing_telephone', 'customer_address/telephone', 'default_billing', null, 'left')
            ->joinAttribute('billing_country_id', 'customer_address/country_id', 'default_billing', null, 'left')
            ->joinAttribute('billing_region_id', 'customer_address/region_id', 'default_billing', null, 'left')
            ->joinAttribute('billing_region', 'customer_address/region', 'default_billing', null, 'left')
            ->addAttributeToSort('entity_id', 'ASC');

        $shippingAddress = Mage::getModel('sales/order_address')->load($order->getShippingAddress()->getId());

        foreach($collection as $shoppartner) {

            $shippingAddress
                        ->setFirstname($shoppartner->getFirstname())
                        ->setMiddlename($shoppartner->getMiddlename())
                        ->setLastname($shoppartner->getLastname())
                        ->setPrefix($shoppartner->getPrefix())
                        ->setCompany($shoppartner->getStoreName())
                        ->setStreet($shoppartner->getBillingStreet())
                        ->setCity($shoppartner->getBillingCity())
                        ->setCountry_id($shoppartner->getBillingCountryId())
                        ->setEmail($shoppartner->getEmail())
                        ->setRegion($shoppartner->getBillingRegion())
                        ->setRegion_id($shoppartner->getBillingRegionId())
                        ->setPostcode($shoppartner->getBillingPostcode())
                        ->setTelephone($shoppartner->getBillingTelephone())
                        ->save();
        }

        return $this;

    }

    /**
     * Add customer Shoppartner ID to order.
     *
     * @param Mage_Sales_Model_Order $order
     * @param string $shoppartnerId
     * @return $this
     */
    protected function _addCustomerShoppartnerIdToOrder($order, $shoppartnerId)
    {
        $order->setShoppartnerId($shoppartnerId);
        $order->save();
        return $this;
    }

    /**
     * @param $order
     * @return $this
     */
    protected function _addPaymentMethodToOrder($order)
    {
        $order->setPaymentMethod($order->getPayment()->getMethodInstance()->getCode());
        $order->save();
        return $this;
    }

    /**
     * Add customer Shoppartner Kundennummer to order.
     *
     * @param Mage_Sales_Model_Order $order
     * @param string $shoppartnerId
     * @return $this
     */
    protected function _addCustomerShoppartnerKdnrToOrder($order, $shoppartnerId)
    {
        $order->setShoppartnerKdnr(Mage::getModel('customer/customer')->load($shoppartnerId)->getErpCustomernumber());
        $order->setShoppartnerInfo($order->getShoppartnerAddress('order', Mage::getModel('customer/customer')->load($shoppartnerId)->getErpCustomernumber()));
        $order->save();
        return $this;
    }


    /**
     * @param $order
     * @param $shoppartnerId
     * @return $this
     */
    protected function _sendEmailToShoppartner($order, $shoppartnerId)
    {
        /**
         * Mailversand an den Shoppartner
         **/
        $templateId = 2;

        $customer = Mage::getModel('customer/customer')->load($shoppartnerId);

        // TODO: ***** ZUM TESTEN MUSS NOCH ENTFERNT WERDEN *****
        $email = $customer->getEmail();
//        $email = "wolf.schmidt@pwonline.de";
//        $email = "stefan.stendel@googlemail.com";


        $sender = array(
            'name'  => Mage::getStoreConfig('trans_email/ident_general/name'),
            'email' => Mage::getStoreConfig('trans_email/ident_general/email')
        );

        $paymentHtml = $order->getPayment()->getMethodInstance()->getTitle();
        $vars = array(
            'order'                 => $order,
            'destinationCustomer'   => $customer->getId(),
            'orderId'               => $order->getIncrementId(),
            'vorname'               => $customer->getFirstname(),
            'nachname'              => $customer->getLastname(),
            'payment_html'          => $paymentHtml
        );

        Mage::getModel('core/email_template')
            ->sendTransactional(
                $templateId,
                $sender,
                $email,
                $customer->getFirstname() . " " . $customer->getLastname(),
                $vars,
                Mage::app()->getStore('default')->getId()
            );
        return $this;
    }

}