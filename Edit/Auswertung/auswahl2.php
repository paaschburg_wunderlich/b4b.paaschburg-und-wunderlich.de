<?php
$aktuellesJahr = date('Y');
$vorjahr = date('Y') -1;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta name="Author" content="Wolf Schmidt, Paaschburg &amp; Wunderlich GmbH, 7.8.2007" />
    <link href="basic.css" type="text/css" rel="stylesheet" media="all" />
    <title>Shin Yo Shopauswertung</title>
</head>
<body>
<div align="center">
    <table width="900" border="3" cellspacing="2" cellpadding="0">
        <tr>
            <td align="center" valign="top">
                <table width="100%" border="0" cellspacing="2" cellpadding="0" bgcolor="white">
                    <tr>
                        <td colspan="3" align="center" valign="middle" bgcolor="#434040">
                            <H1><br />
                                <font size="+3" color="white">Auswertungen brands4bikes</font></H1>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" valign="middle"><br />
                            <FONT SIZE='3'><STRONG />Umsatz </STRONG></FONT>
                            <SELECT NAME="gesamt" onchange="javascript:parent.show.location.href='auswertung.php?weise=gesamt&jahr='+this.value" STYLE='font-size:11px;border:1px;border-style:solid;background-color:#eceded;'>
                                <OPTION SELECTED=SELECTED>[Bitte auswählen]</OPTION>
                                <OPTION value=2016>2016</OPTION>
                                <OPTION value=2015>2015</OPTION>
                                <OPTION value=2014>2014</OPTION>
                                <OPTION value=2013>2013</OPTION>
                                <OPTION value=2012>2012</OPTION>
                                <OPTION value=alles>Gesamt</OPTION>
                            </SELECT>
                        </td>
                        <td align="center" valign="middle"><br />
                            <FONT SIZE='3'><STRONG />pro Monat </STRONG></FONT>
                            <SELECT NAME='gesamt' onchange="javascript:parent.show.location.href='auswertung.php?weise=monat&jahr='+this.value" STYLE='font-size:11px;border:1px;border-style:solid;background-color:#eceded;'>
                                <OPTION SELECTED=SELECTED>[Bitte Auswählen]</OPTION>
                                <OPTION value=2016>2016</OPTION>
                                <OPTION value=2015>2015</OPTION>
                                <OPTION value=2014>2014</OPTION>
                                <OPTION value=2013>2013</OPTION>
                                <OPTION value=2012>2012</OPTION>
                            </SELECT>
                        </td>
                        <td align="center" valign="middle"><br />
                            <FONT SIZE='3'><STRONG />nach Shoppartnern </STRONG></FONT>
                            <SELECT NAME='gesamt' onchange="javascript:parent.show.location.href='auswertung.php?weise=shoppartner&jahr='+this.value" STYLE='font-size:11px;border:1px;border-style:solid;background-color:#eceded;'>
                                <OPTION SELECTED=SELECTED>[Bitte Ausw&auml;hlen]</OPTION>
                                <OPTION value=2016>2016</OPTION>
                                <OPTION value=2015>2015</OPTION>
                                <OPTION value=2014>2014</OPTION>
                                <OPTION value=2013>2013</OPTION>
                                <OPTION value=2012>2012</OPTION>
                                <OPTION value=Gesamtzeitraum>Gesamt</OPTION>
                            </SELECT>
                        </TD>
                        <br />
                    </TR>
                    <tr>
                        <td align="center" valign="middle"><br />
                            <FONT SIZE='3'><STRONG />Artikel Anzahl </STRONG></FONT>
                            <SELECT NAME='gesamt' onchange="javascript:parent.show.location.href='topseller.php?sort=anzahl&jahr='+this.value" STYLE='font-size:11px;border:1px;border-style:solid;background-color:#eceded;'>
                                <OPTION SELECTED=SELECTED>[Bitte Auswählen]</OPTION>
                                <OPTION value=2016>2016</OPTION>
                                <OPTION value=2015>2015</OPTION>
                                <OPTION value=2014>2014</OPTION>
                                <OPTION value=2013>2013</OPTION>
                                <OPTION value=2012>2012</OPTION>
                                <OPTION value=alles>Gesamt</OPTION>
                            </SELECT>
                        </td>
                        <td align="center" valign="middle"><br />
                            <FONT SIZE='3'><STRONG />Artikel nach Umsatz </STRONG></FONT>
                            <SELECT NAME='gesamt' onchange="javascript:parent.show.location.href='topseller.php?sort=summe&jahr='+this.value" STYLE='font-size:11px;border:1px;border-style:solid;background-color:#eceded;'>
                                <OPTION SELECTED=SELECTED>[Bitte Ausw&auml;hlen]</OPTION>
                                <OPTION value=2016>2016</OPTION>
                                <OPTION value=2015>2015</OPTION>
                                <OPTION value=2014>2014</OPTION>
                                <OPTION value=2013>2013</OPTION>
                                <OPTION value=2012>2012</OPTION>
                                <OPTION value=alles>Gesamt</OPTION>
                            </SELECT>
                        </td>
                        <td align="center" valign="middle"><br />
                            <FONT SIZE='3'><STRONG />nach Warengruppe </STRONG></FONT>
                            <SELECT NAME='gesamt' onchange="javascript:parent.show.location.href='topseller.php?sort=kategorie&jahr='+this.value" STYLE='font-size:11px;border:1px;border-style:solid;background-color:#eceded;'>
                                <OPTION SELECTED=SELECTED>[Bitte Auswählen]</OPTION>
                                <OPTION value=2016>2016</OPTION>
                                <OPTION value=2015>2015</OPTION>
                                <OPTION value=2014>2014</OPTION>
                                <OPTION value=2013>2013</OPTION>
                                <OPTION value=2012>2012</OPTION>
                                <OPTION value=alles>Gesamt</OPTION>
                            </SELECT>
                        </TD>
                        <br />
                    </TR>
                    <tr height="8">
                        <td colspan="3" align="center" valign="middle" bgcolor="#ffffff"></td>
                    </tr>
                    <tr height="13">
                        <td colspan="3" align="center" valign="middle" bgcolor="gray" height="13"></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
</body>
</html>