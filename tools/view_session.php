<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 20.10.2015
 * Time: 19:44
 */
?>

<html lang="de">
<head>
    <meta charset="utf-8">
</head>
<?php
require_once '../app/Mage.php';

UMASK(0);

Mage::app();
Mage::init();

echo "<pre>";

var_dump(Mage::getSingleton('customer/session'));
echo '***';
var_dump(Mage::getSingleton('customer/session'));

echo "</pre>";
?>
</html>
