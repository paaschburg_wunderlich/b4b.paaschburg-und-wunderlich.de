<?php
/*------------------------------------------------------------------------
# $JA#PRODUCT_NAME$ - Version $JA#VERSION$ - Licence Owner $JA#OWNER$
# ------------------------------------------------------------------------
# Copyright (C) 2004-2009 J.O.O.M Solutions Co., Ltd. All Rights Reserved.
# @license - Copyrighted Commercial Software
# Author: J.O.O.M Solutions Co., Ltd
# Websites: http://www.joomlart.com - http://www.joomlancers.com
# This file may not be redistributed in whole or significant part.
-------------------------------------------------------------------------*/
require_once "megamenu/mega.class.php";

class Wavethemes_Jmmegamenu_Block_Jmmegamenu extends Mage_Page_Block_Html_Topmenu
{
	protected $children = null;
	protected $items = null;
	protected $menu = null;
	protected $open = null;
	protected $params = null;

	public function __construct($attributes = array())
	{
		parent::__construct();

		$this->addData(array(
			'cache_lifetime' => null,
		));

		$this->menu = new JAMenuMega();
		$this->params = new stdclass();
		$this->params->megamenu = 1;
		$this->params->startlevel = 0;
		$this->params->endlevel = 10;
	}

//	public function _prepareLayout()
//	{
		//uncomment this if need
//		$headBlock = $this->getLayout()->getBlock('head');
//		$headBlock->addJs('jquery/jquery.1.9.1.min.js');
//		$headBlock->addJs('jquery/jquery.noConflict.js');
//		$headBlock->addJs('joomlart/jmmegamenu/jmmegamenu.js');

//		return parent::_prepareLayout();
//	}

	protected function _toHtml()
	{
		if (!Mage::helper('jmmegamenu')->get('show')) {
			return parent::_toHtml();
		}

		//set template
		$this->setTemplate("joomlart/jmmegamenu/output.phtml");

		//get menu id
		$menuType = $this->getData('menu_type');
		$this->menu->menu_type = $menuType;
		$menuId = Mage::helper('jmmegamenu')->getMenuId($menuType);

		if($menuId)
		{
			//get menu items
			$collections = Mage::getModel('jmmegamenu/jmmegamenu')->getCollection()->setOrder("parent", "ASC")->setOrder("ordering", "ASC")->addFilter("status", 1, "eq")->addFilter("menugroup", $menuId);

			//built menu tree
			$tree = array();
			foreach ($collections as $collection) {
				$collection->tree = array();
				$parent_tree = array();
				if (isset($tree[$collection->parent])) {
					$parent_tree = $tree[$collection->parent];
				}
				//Create tree
				array_push($parent_tree, $collection->menu_id);
				$tree[$collection->menu_id] = $parent_tree;

				$collection->tree = $parent_tree;
			}
			$this->menu->getList($collections);

			ob_start();
			$this->menu->genMenu();
			$menuoutput = ob_get_contents();
			$this->assign('menuoutput', $menuoutput);
			ob_end_clean();
		}
		else{
			echo Mage::helper('jmmegamenu')->__('Menu items not found.');
		}

		return parent::_toHtml();
	}

}
