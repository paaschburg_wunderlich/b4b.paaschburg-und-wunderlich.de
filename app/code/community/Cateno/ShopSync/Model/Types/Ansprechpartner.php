<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Ansprechpartner-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Ansprechpartner
{
	/**
	 * @var string
	 */
	public $AnsNr;

	/**
	 * @var string
	 */
	public $AspNr;

	/**
	 * @var string
	 */
	public $Anr;

	/**
	 * @var string
	 */
	public $Ansp;

	/**
	 * @var string
	 */
	public $Vna;

	/**
	 * @var string
	 */
	public $NNa;

	/**
	 * @var string
	 */
	public $EMail;

	/**
	 * @var string
	 */
	public $Gender;
}
