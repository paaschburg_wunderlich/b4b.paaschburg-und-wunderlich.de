<?php
/**
* Cateno
 *
 * @category Controller
 * @package Cateno_ShopSync
 *
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_ServiceController extends Mage_Core_Controller_Front_Action
{
    protected $helper;
    protected $debug;
    const SOAP_FILE_PATH = 'php://input';

    protected function _construct()
    {
        parent::_construct();
        $this->helper = Mage::helper('shopsync');
        if(!$this->helper->isEnable()) {
            $this->_redirect();
        }
        $this->debug = Mage::helper('shopsync/debug');
    }
    public function preDispatch()
    {
        Mage::app()->getFrontController()->setNoRender();
    }

    public function indexAction()
    {
    }

    /**
     * init this sopa server
     */
    protected function _initServer($inModule)
    {

        if(!Mage::registry('isCateno')) {
			Mage::register('isCateno', true);
        }
  
        ini_set("soap.wsdl_cache_enabled", "0");
        $theUrlModel = Mage::getModel("core/url")->setUseSession(false);
        if (array_key_exists('wsdl',$this->getRequest()->getParams())) {
            $theIO = new Varien_Io_File();
            $theIO->open(array('path' => Mage::getModuleDir('etc', 'Cateno_ShopSync').DS.'wsdl'));
            $theWsdlContent = $theIO->read('Service'.ucfirst($inModule).'.wsdl');

            $theTemplate = Mage::getModel('core/email_template_filter');
            $theWsdlConfig = new Varien_Object();
            $theQueryParams = $this->getRequest()->getQuery();
            if(isset($theQueryParams['wsdl'])) {
                unset($theQueryParams['wsdl']);
            }
            $theWsdlConfig->setUrl(htmlspecialchars($theUrlModel->getUrl('*/*/'.$inModule, array('_query' => $theQueryParams))));
            $theWsdlConfig->setName('Service'.ucfirst($inModule));
            $theWsdlConfig->setHandler($theWsdlConfig->getName());
            $theTemplate->setVariables(array('wsdl' => $theWsdlConfig));
            $this->getResponse()
                    ->setHeader('Content-Type', 'text/xml; charset=utf-8')
                    ->setBody($theTemplate->filter($theWsdlContent));
        } else {
            $theOptions = array('cache_wsdl' => false);
            $theServer = new Zend_Soap_Server(Mage::getUrl('*/*/'.$inModule, array('wsdl'=>1)), $theOptions);
            $theServer->setReturnResponse(1); // return the xml as string and use not echo
            $theServer->setClass('Cateno_ShopSync_Model_Service_'.ucfirst($inModule))->setEncoding('UTF-8');
            $request = null;
            if (isset($_SERVER['HTTP_CONTENT_ENCODING']) && strtolower($_SERVER['HTTP_CONTENT_ENCODING']) == 'gzip') {
                $gzip=file_get_contents(self::SOAP_FILE_PATH);
                $rest = substr($gzip, -4);
                $theUnpack = unpack("V", $rest);
                $theGZFileSize = end($theUnpack);
                $theGzipHandel = gzopen (self::SOAP_FILE_PATH, "r");
                $request = gzread ($theGzipHandel, $theGZFileSize);
                gzclose ($theGzipHandel);
            }elseif(isset($_SERVER['HTTP_CONTENT_ENCODING']) && strtolower($_SERVER['HTTP_CONTENT_ENCODING']) == 'deflate'){
                $deflate = file_get_contents(self::SOAP_FILE_PATH);
                $request = gzuncompress($deflate);
            }
            $theContent = $theServer->handle($request);
            $this->getResponse()->setHeader('Content-Length', strlen($theContent));
            if (isset($_SERVER['HTTP_CONTENT_ENCODING']) && strtolower($_SERVER['HTTP_CONTENT_ENCODING']) == 'gzip') {
            	$this->getResponse()->setHeader('Content-Encoding', 'gzip');
            	$this->getResponse()->setHeader('Content-Transfer-Encoding', 'binary');

            	$theContent = gzencode($theContent, 9, FORCE_GZIP);
            	$this->getResponse()->setHeader('Content-Length', strlen($theContent), true);
            }elseif (isset($_SERVER['HTTP_CONTENT_ENCODING']) && strtolower($_SERVER['HTTP_CONTENT_ENCODING']) == 'deflate') {
            	$this->getResponse()->setHeader('Content-Encoding', 'deflate');
            	$this->getResponse()->setHeader('Content-Transfer-Encoding', 'binary');

            	$theContent = gzcompress($theContent, 9);

            	$this->getResponse()->setHeader('Content-Length', strlen($theContent), true);
            }
            $this->getResponse()
                ->setHeader('Content-Type', 'text/xml; charset=utf-8')
                ->setBody($theContent);

            if($this->debug->isDebugEnable()) {
                $this->debug->createRequestLog($theServer->getLastRequest());
                $this->debug->createResponseLog($theServer->getLastResponse());
            }
        }
    }

    /**
     * Lädt Allgemein controller
     */
    public function allgemeinAction()
    {
        $this->_initServer('allgemein');
    }

    /**
     * Lädt Artikel controller
     */
    public function artikelAction()
    {
        $this->_initServer('artikel');
    }

    /**
     * Lädt Adressen controller
     */
    public function adressenAction()
    {
        $this->_initServer('adressen');
    }

    /**
     * Lädt Vorgang controller
     */
    public function vorgangAction()
    {
        $this->_initServer('vorgang');
    }

    /**
     * URI: shopsync/service/systemcheck
     * @return string
     */
    public function systemcheckAction()
    {
        $theInfo = array();
        $theSystem = Mage::getSingleton('shopsync/systemcheck');
        $theInfo[] = $theSystem->getModuleVersion();
        $theInfo[] = $theSystem->comparePhpVersion();
        $theInfo[] = $theSystem->getPHPVersion();
        $theInfo[] = $theSystem->isOpenSoap();
        $theInfo[] = $theSystem->getShopSystem();
        $theInfo[] = $theSystem->compareMagentoVersion();
        $theInfo[] = $theSystem->getMagentoVersion();
        $this->getResponse()->setBody(implode("\n", $theInfo));
    }
}
