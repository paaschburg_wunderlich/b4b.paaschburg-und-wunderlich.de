<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Erweiterung der Produkt-API
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Api_Product_Api extends Mage_Catalog_Model_Product_Api
{
    /**
     *  Set additional data before product saved
     *
     *  @param    Mage_Catalog_Model_Product $product
     *  @param    array $productData
     *  @return   object
     */
    protected function _prepareDataForSave($product, $productData)
    {
        if (isset($productData['categories'])) {
            $product->setCategoryIds($productData['categories']);
        }

        if (isset($productData['websites']) && is_array($productData['websites'])) {
            foreach ($productData['websites'] as &$website) {
                if (is_string($website)) {
                    try {
                        $website = Mage::app()->getWebsite($website)->getId();
                    } catch (Exception $e) { }
                }
            }
            $product->setWebsiteIds($productData['websites']);
        }

        if (Mage::app()->isSingleStoreMode()) {
            $product->setWebsiteIds(array(Mage::app()->getStore(true)->getWebsite()->getId()));
        }

        if (isset($productData['stock_data']) && is_array($productData['stock_data'])) {
            $product->setStockData($productData['stock_data']);
        } else {
            $product->setStockData(array('use_config_manage_stock' => 0));
        }

        if (isset($productData['tier_price']) && is_array($productData['tier_price'])) {
             $tierPrices = Mage::getModel('catalog/product_attribute_tierprice_api')->prepareTierPrices($product, $productData['tier_price']);
             $product->setData(Mage_Catalog_Model_Product_Attribute_Tierprice_Api::ATTRIBUTE_CODE, $tierPrices);
        }

        $product->setConfigurableProductsData($productData['configurable_products_data']);
        $product->setConfigurableAttributesData($productData['configurable_attributes_data']);
        $product->setCanSaveConfigurableAttributes(1);
        $product->setTierPriceChanged(0);
    }
}
