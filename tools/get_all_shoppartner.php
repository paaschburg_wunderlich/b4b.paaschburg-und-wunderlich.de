<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 20.10.2015
 * Time: 19:44
 */
?>

<html lang="de">
<head>
    <meta charset="utf-8">
</head>
<?php
require_once '../app/Mage.php';

UMASK(0);

Mage::app();
Mage::init();

$collection = mage::getModel('customer/customer')
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('is_shoppartner', true)
    ->addAttributeToFilter('customer_activated', 1)
    ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
    ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
    ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
    ->addAttributeToSort('entity_id', 'ASC');

echo "<pre>";

echo $collection->count() . " aktive Shoppartner\n\n";
echo "Magento ID;Kunden Nr.;Firmenname;Vorname;Name;Strasse;PLZ;Ort;E-Mail;Subtotal\n";
foreach($collection as $shoppartner)
{
    $orders = Mage::getModel('sales/order')->getCollection()
        ->addAttributeToFilter('status', array('neq' => 'canceled'))
        ->addAttributeToFilter('shoppartner_id', $shoppartner->getEntityId());

    $sumSubTotal = 0;
    foreach($orders as $order) {
//        $sumSubTotal = $sumSubTotal + $order->getSubtotal();

        echo '<br />' . $order->getId() . ' - ' . $order->getShippingAddress();

    }

    echo $shoppartner->getEntityId() . ";" .
        $shoppartner->getErpCustomernumber() . ";" .
        $shoppartner->getStoreName() . ";" .
        $shoppartner->getFirstname() . ";" .
        $shoppartner->getName() . ";" .
        $shoppartner->getBillingStreet() . ";" .
        $shoppartner->getBillingPostcode() . ";" .
        $shoppartner->getBillingCity() . ";" .
        $shoppartner->getEmail() . ";" .
        number_format($sumSubTotal, 2, ',', '.') . "\n";

}
echo "</pre>";
?>
</html>
