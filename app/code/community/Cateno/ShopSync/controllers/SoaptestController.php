<?php
/**
* Cateno
 *
 * @category Controller
 * @package Cateno_ShopSync
 * SoapTest Übertragung
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_SoaptestController extends Mage_Core_Controller_Front_Action
{
    protected function _construct()
    {
    }

    public function indexAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();

        $this->getResponse()->setBody(
            '<a href="'.$url.'shopsync/soaptest/getvorgangstatus" target="_blank">getVorgangListeStatus</a><br />'.
            '<a href="'.$url.'shopsync/soaptest/getshopinfo" target="_blank">getShopInfo<br /></a>'.
            '<a href="'.$url.'shopsync/soaptest/pruefedatenbank" target="_blank">PruefeDatenbank<br /></a>'.

            '<a href="'.$url.'shopsync/soaptest/shopbereinigenstarten" target="_blank">ShopBereinigenStarten</a><br />'.
            '<a href="'.$url.'shopsync/soaptest/shopbereinigenbeenden" target="_blank">ShopBereinigenBeenden</a><br />'.
            '<a href="'.$url.'shopsync/soaptest/setartikelbilder" target="_blank">setArtikelBilder<br /></a>'
        );

    }

    public function getvorgangstatusAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();
        $client = new SoapClient(null,
        array(
        "location" => $url."/shopsync/service/vorgang",
        "uri"      => "urn:xmethods-delayed-quotes",
        "style"    => SOAP_RPC,
        "use"      => SOAP_ENCODED,
        'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'trace' => 1,
           ));

        $param = new SoapParam(
                /* Parameter Value */
                "pending",
                /* Parameter Name */
                "status"
        );
        print_r('parameter:');
        print_r($param);

        $result = $client->__soapCall(
        /* SOAP Method Name */
        "getVorgangListeStatus",
        /* Parameters */
        array($param)
        );
        print_r('<br />result:');
        print_r($result);
    }

    public function getshopinfoAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();
        $client = new SoapClient(null,
        array(
        "location" => $url."shopsync/service/allgemein",
        "uri"      => "urn:xmethods-delayed-quotes",
        "style"    => SOAP_RPC,
        "use"      => SOAP_ENCODED,
        'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'trace' => 1,
           ));

        $result = $client->__soapCall(
        /* SOAP Method Name */
        "getShopInfo",
        /* Parameters */
        array()
        );
        //print_r('<br />result:');
        //print_r($result);
    }

    public function shopbereinigenstartenAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();
        $client = new SoapClient(null,
        array(
        "location" => $url."shopsync/service/artikel",
        "uri"      => "urn:xmethods-delayed-quotes",
        "style"    => SOAP_RPC,
        "use"      => SOAP_ENCODED,
        'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'trace' => 1,
           ));

        $result = $client->__soapCall(
        /* SOAP Method Name */
        "ShopBereinigenStarten",
        /* Parameters */
        array()
        );
        print_r('<br />result:');
        print_r($result);
    }

    public function shopbereinigenbeendenAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();
        $client = new SoapClient(null,
        array(
        "location" => $url."shopsync/service/artikel",
        "uri"      => "urn:xmethods-delayed-quotes",
        "style"    => SOAP_RPC,
        "use"      => SOAP_ENCODED,
        'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'trace' => 1,
           ));
        $param = new SoapParam(
                /* Parameter Value */
                "true",
                /* Parameter Name */
                "ArtikelSperren"
        );
        print_r('parameter:');
        print_r($param);
        $result = $client->__soapCall(
        /* SOAP Method Name */
        "ShopBereinigenBeenden",
        /* Parameters */
        array($param)
        );
        print_r('<br />result:');
        print_r($result);
    }

    public function setartikelBilderAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();
        $client = new SoapClient(null,
        array(
        "location" => $url."shopsync/service/artikel",
        "uri"      => "urn:xmethods-delayed-quotes",
        "style"    => SOAP_RPC,
        "use"      => SOAP_ENCODED,
        'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'trace' => 1,
        ));

        $param = new SoapParam(
                /* Parameter Value */
                array('TEST1','TEST2'),
                /* Parameter Name */
                "ArtikelBilder"
        );

        print_r('parameter:');
        print_r($param);
        $result = $client->__soapCall(
        /* SOAP Method Name */
        "setArtikelBilder",
        /* Parameters */
        array($param)
        );
        print_r('<br />result:');
        print_r($result);
    }

    public function pruefedatenbankAction()
    {
        $url = Mage::getModel('core/url')->getBaseUrl();
        $client = new SoapClient(null,
        array(
        "location" => $url."shopsync/service/allgemein",
        "uri"      => "urn:xmethods-delayed-quotes",
        "style"    => SOAP_RPC,
        "use"      => SOAP_ENCODED,
        'compression'=> SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP,
        'trace' => 1,
        ));
        $param = new SoapParam('true','Aendern');

        print_r('parameter:');
        print_r($param);
        $result = $client->__soapCall(
        /* SOAP Method Name */
        "PruefeDatenbank",
        /* Parameters */
        array($param)
        );
        print_r('<br />result:');
        print_r($result);
    }
}
