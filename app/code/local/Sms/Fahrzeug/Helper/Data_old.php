<?php
/**
 * Class Sms_Fahrzeug_Helper_Data
 * Necessary for fahrzeug configuration
 */
class Sms_Fahrzeug_Helper_Data extends Mage_Core_Helper_Abstract
{
    var $text;

    public function __construct() {
        $this->text = Mage::getStoreConfig('fahrzeug/fahrzeugglobalconfig/text',Mage::app()->getStore());
    }

    public function getMessage() {
        return "Mein Text: ".$this->text;
    }

    public function getBike($sms_fahrzeug_id) {
        $bike = array();
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT sms_mkz, sms_hersteller, sms_modell, sms_fahrgestellnr, sms_baujahr_von, sms_baujahr_bis, sms_hubraum, sms_fahrzeugart FROM sms_fahrzeug WHERE sms_mkz = '$sms_fahrzeug_id' LIMIT 1";
        $bike = $readConnection->fetchRow($SQL);

        return $bike;
    }

    public function getVehicletype() {
        $vehicletype = array();
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT sms_fahrzeugart FROM sms_fahrzeug GROUP BY sms_fahrzeugart ORDER BY sms_fahrzeugart";
        $vehicletype = $readConnection->fetchAll($SQL);
        return $vehicletype;
    }
    public function getManufacturer($fahrzeugart) {
        $manufacturer = array();
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        if ($fahrzeugart == "" OR $fahrzeugart == "FAHRZEUGART") {
            $SQL = "SELECT sms_hersteller FROM sms_fahrzeug GROUP BY sms_hersteller ORDER BY sms_hersteller";
        } else {
            $SQL = "SELECT sms_hersteller FROM sms_fahrzeug WHERE sms_fahrzeugart like '$fahrzeugart' GROUP BY sms_hersteller ORDER BY sms_hersteller";
        }
        $manufacturer = $readConnection->fetchAll($SQL);
        return $manufacturer;
    }

    public function getCapacity($fahrzeugart, $hersteller) {
        $capacity = array();
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');

        if ($fahrzeugart <> "FAHRZEUGART" AND $hersteller == "HERSTELLER") {
            $SQL = "SELECT sms_hubraum FROM sms_fahrzeug WHERE sms_fahrzeugart like '$fahrzeugart' GROUP BY sms_hubraum ORDER BY sms_hubraum";
        } elseif ($fahrzeugart <> "FAHRZEUGART" AND $hersteller <> "HERSTELLER") {
            $SQL = "SELECT sms_hubraum FROM sms_fahrzeug
                    WHERE sms_fahrzeugart like '$fahrzeugart' AND sms_hersteller like '$hersteller'
                    GROUP BY sms_hubraum ORDER BY sms_hubraum";
        } elseif ($fahrzeugart === "FAHRZEUGART" AND $hersteller <> "HERSTELLER") {
            $SQL = "SELECT sms_hubraum FROM sms_fahrzeug
                    WHERE sms_hersteller like '$hersteller'
                    GROUP BY sms_hubraum ORDER BY sms_hubraum";
        } else {
            $SQL = "SELECT sms_hubraum FROM sms_fahrzeug WHERE sms_fahrzeugart like '$hersteller' GROUP BY sms_hubraum ORDER BY sms_hubraum";
        }
        $capacity = $readConnection->fetchAll($SQL);
        return $capacity;
    }

    public function getMoto($fahrzeugart, $hersteller, $ccm) {
        $bike = array();
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        if ($fahrzeugart <> "FAHRZEUGART" AND $hersteller === "HERSTELLER" AND $ccm <> "HUBRAUM") {
            $SQL = "SELECT sms_mkz, sms_modell, sms_fahrgestellnr, sms_baujahr_von, sms_baujahr_bis
                    FROM sms_fahrzeug
                    WHERE sms_fahrzeugart like '$fahrzeugart' AND sms_hubraum = '$ccm'
                    ORDER BY sms_hersteller, sms_hubraum, sms_modell, sms_baujahr_von";
        } elseif ($fahrzeugart <> "FAHRZEUGART" AND $hersteller <> "HERSTELLER" AND $ccm <> "HUBRAUM") {
            $SQL = "SELECT sms_mkz, sms_modell, sms_fahrgestellnr, sms_baujahr_von, sms_baujahr_bis
                    FROM sms_fahrzeug
                    WHERE sms_fahrzeugart like '$fahrzeugart' AND sms_hubraum = '$ccm' AND sms_hersteller like '$hersteller'
                    ORDER BY sms_hersteller, sms_hubraum, sms_modell, sms_baujahr_von";
        } elseif ($fahrzeugart <> "FAHRZEUGART" AND $hersteller <> "HERSTELLER" AND $ccm === "HUBRAUM") {
            $SQL = "SELECT sms_mkz, sms_modell, sms_fahrgestellnr, sms_baujahr_von, sms_baujahr_bis
                    FROM sms_fahrzeug
                    WHERE sms_fahrzeugart like '$fahrzeugart' AND sms_hersteller like '$hersteller'
                    ORDER BY sms_hersteller, sms_hubraum, sms_modell, sms_baujahr_von";
        } elseif ($fahrzeugart === "FAHRZEUGART" AND $hersteller <> "HERSTELLER" AND $ccm <> "HUBRAUM") {
            $SQL = "SELECT sms_mkz, sms_modell, sms_fahrgestellnr, sms_baujahr_von, sms_baujahr_bis
                    FROM sms_fahrzeug
                    WHERE sms_hersteller like '$hersteller' AND sms_hubraum like '$ccm'
                    ORDER BY sms_hersteller, sms_hubraum, sms_modell, sms_baujahr_von";
        } else {
            $SQL = "SELECT sms_hubraum FROM sms_fahrzeug WHERE sms_fahrzeugart like '$hersteller' GROUP BY sms_hubraum ORDER BY sms_hubraum";
        }

        $bike = $readConnection->fetchAll($SQL);
        return $bike;
    }

    public function getSelected($moto) {
        $selected = array();
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT sms_mkz, sms_modell, sms_fahrgestellnr, sms_baujahr_von, sms_baujahr_bis
                    FROM sms_fahrzeug
                    WHERE sms_mkz like '$moto'
                    LIMIT 1";
        $selected = $readConnection->fetchAll($SQL);
        return $selected;
    }

    public function selectMoto($moto) {
        $tableName = Mage::getSingleton('core/resource')->getTableName('sms_fahrzeug');
        $resource = Mage::getSingleton('core/resource');
        $readConnection = $resource->getConnection('core_read');
        $SQL = "SELECT *
                    FROM sms_fahrzeug
                    WHERE sms_mkz like '$moto'
                    LIMIT 1";
        $selected = $readConnection->fetchRow($SQL);
        return $selected;
    }

    public function showProducts($bikeselect, $fahrzeugdaten)
    {
        if ($fahrzeugdaten["sms_batterie"] == "") {$fahrzeugdaten["sms_batterie"] = "LEER";}
        if ($fahrzeugdaten["sms_batterie"] == "---") {$fahrzeugdaten["sms_batterie"] = "LEER";}

        $xref = str_replace("-", "", $fahrzeugdaten["sms_batterie"]);
        $xref = "%".$xref."%";
        $batteriebybikeCollection = Mage::getModel('catalog/product')->getCollection();
        $batteriebybikeCollection->addFieldToFilter(array(
            array(
                'attribute'=>'erweiterte_suche',
                'like'=>$xref
            )
        ));

        $kettebybikeCollection = Mage::getModel('catalog/product')->getCollection();
        $kettebybikeCollection->addAttributeToFilter('sms_kettenteilung',(array('like'=>$fahrzeugdaten["sms_kettenteilung"])));
        $kettebybikeCollection->addAttributeToFilter('sms_kettenglieder',(array('like'=>$fahrzeugdaten["sms_kettenglieder"])));

        $kettenradbybikeCollection = Mage::getModel('catalog/product')->getCollection();
        $kettenradbybikeCollection->addAttributeToFilter('sms_artikelkennzeichnung',(array('like'=>$fahrzeugdaten["sms_kettenrad"])));
        $kettenradbybikeCollection->addAttributeToFilter('sms_zaehnezahl',(array('like'=>$fahrzeugdaten["sms_kerazaehne"])));

        $ritzelbybikeCollection = Mage::getModel('catalog/product')->getCollection();
        $ritzelbybikeCollection->addAttributeToFilter('sms_artikelkennzeichnung',(array('like'=>$fahrzeugdaten["sms_ritzel"])));
        $ritzelbybikeCollection->addAttributeToFilter('sms_zaehnezahl',(array('like'=>$fahrzeugdaten["sms_ritzelzaehne"])));

        $filter = "%;".$bikeselect.";%";
        $productbymkzCollection = Mage::getModel('catalog/product')->getCollection();
        $productbymkzCollection->addFieldToFilter(array(
            array(
                'attribute' => 'sms_modellkennzahl',
                'like' => $filter
            )
        ));

        $productbyFahrzeugCollection = Mage::getModel('catalog/product')->getCollection();
        $productbyFahrzeugCollection->addAttributeToFilter(array(
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsbelag_vorn_links"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsbelag_vorn_rechts"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsbelag_hinten"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsbelag_vl_trw"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsbelag_vr_trw"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsbelag_hi_trw"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsscheibe_vl"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsscheibe_vr"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsscheibe_hi"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsscheibe_vl_trw"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsscheibe_vr_trw"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_bremsscheibe_hi_trw"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_oelfilter"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_zuendkerze"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlager_vl"]
            ),array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlager_vr"]
            ),array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlager_hl"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlager_hr"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlager_kr"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlagersatz_vorn"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_radlagersatz_hinten"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_batterie"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_synto_b"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_synto_k"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_verkleidungsscheibe"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_gabelsimm"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_gabelsimmerringsaetze"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_lenkkopflager"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_kupplungen"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_kupplungsfedern"]
            ),
            array(
                'attribute' => 'sms_artikelkennzeichnung',
                'like' => $fahrzeugdaten["sms_zuendkerze_ch"]
            )
        ));

        $merged_ids = array_merge(
            $productbyFahrzeugCollection->getAllIds(),
            $kettebybikeCollection->getAllIds(),
            $kettenradbybikeCollection->getAllIds(),
            $ritzelbybikeCollection->getAllIds(),
            $batteriebybikeCollection->getAllIds(),
            $productbymkzCollection->getAllIds()
        );

        $_productbybikeCollection = Mage::getResourceModel('catalog/product_collection')
            ->addFieldToFilter('entity_id', $merged_ids)
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('status', array('eq' => 1))
            ->setOrder(array('sms_artikelgruppe', 'sms_artikelkategorie'),'asc');

        return $_productbybikeCollection;

    }

}
