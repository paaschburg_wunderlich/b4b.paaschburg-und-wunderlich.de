<?php
error_reporting(E_ALL | E_STRICT);
#ini_set('display_errors', 1);
if($_POST) {
	try {
		$image_url = stripslashes($_POST['IMAGE_URL']);
		$curl_connecttimeout = stripslashes($_POST['CURL_CONNECTTIMEOUT']);
		$curl_timeout = stripslashes($_POST['CURL_TIMEOUT']);
		$curl_maxredir = stripslashes($_POST['CURL_MAXREDIR']);
		$media_root = stripslashes($_POST['MEDIA_ROOT']);
		$image_root = stripslashes($_POST['IMAGE_ROOT']);
		$local_filepath = stripslashes($_POST['LOCAL_FILEPATH']);
		$logging_on = stripslashes($_POST['LOGGING_ON']);
		$ch = curl_init (str_replace(' ','%20',$image_url));
		curl_setopt($ch, CURLOPT_IPRESOLVE, CURL_IPRESOLVE_V4 );
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT,$curl_connecttimeout);
		curl_setopt($ch, CURLOPT_TIMEOUT, $curl_timeout);
		if ($_SERVER['REMOTE_ADDR'] != $_SERVER['SERVER_ADDR']) {
			log_error("Warning:  This php script was accessed from $_SERVER[REMOTE_ADDR].",3,'../var/log/curl_error.log',$logging_on);
			exit;
		}
		$output = curl_exec_follow($ch, $curl_maxredir);
		$info = curl_getinfo($ch);
		if ($output === false || $info['http_code'] != 200) {
			$output = "Error:  No cURL data returned for " . $image_url . " [". $info['http_code']. "]";
			if (curl_error($ch)) {
				$output .= "Error:  ". curl_error($ch);
			}
			log_error($output,3,'../var/log/curl_error.log', $logging_on);
			exit;
		}
		else {
			if (!file_exists($media_root.$image_root)) :
				include('../lib/Varien/Io/Interface.php');
				include('../lib/Varien/Io/Abstract.php');
				include('../lib/Varien/Io/File.php');
				if (!class_exists('Varien_Io_File')) {
					log_error("Error:  Varien includes failed",3,'../var/log/curl_error.log');
					exit;
				}
				$file = new Varien_Io_File();
				$mkDirResult = $file->mkdir($media_root.$image_root);
				if (!$mkDirResult) {
					log_error("Error:  Unable to create directory" . $media_root.$image_root,3,'../var/log/curl_error.log', $logging_on);
					exit;
				}
			endif;
			$fp = @fopen($local_filepath,'x');
			if($fp !== false){
				fwrite($fp, $output);
				fclose($fp);
			}
		}
		curl_close ($ch);
	}
	catch (Exception $e) {
		log_error("Exception:  ". $e,3,'../var/log/curl_error.log', $logging_on);
	}
	exit;
}	
function curl_exec_follow($ch, $maxredirect = null)
{

	  // we emulate a browser here since some websites detect
	  // us as a bot and don't let us do our job
	  $user_agent = "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.7.5)".
				" Gecko/20041107 Firefox/1.0";
	  curl_setopt($ch, CURLOPT_USERAGENT, $user_agent );
	
	  $mr = $maxredirect;
	
	  if (ini_get('open_basedir') == '' && ini_get('safe_mode') == 'Off') {
	
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, $mr > 0);
		curl_setopt($ch, CURLOPT_MAXREDIRS, $mr);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	
	  } else {
		
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false);
	
		if ($mr > 0)
		{
		  $original_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
		  $newurl = $original_url;
		  
		  $rch = curl_copy_handle($ch);
		  
		  curl_setopt($rch, CURLOPT_HEADER, true);
		  curl_setopt($rch, CURLOPT_NOBODY, true);
		  curl_setopt($rch, CURLOPT_FORBID_REUSE, false);
		  do
		  {
			curl_setopt($rch, CURLOPT_URL, $newurl);
			$header = curl_exec($rch);
			if (curl_errno($rch)) {
			  $code = 0;
			} else {
			  $code = curl_getinfo($rch, CURLINFO_HTTP_CODE);
			  if ($code == 301 || $code == 302) {
				preg_match('/Location:(.*?)\n/', $header, $matches);
				$newurl = trim(array_pop($matches));
				
				// if no scheme is present then the new url is a
				// relative path and thus needs some extra care
				if(!preg_match("/^https?:/i", $newurl)){
				  $newurl = $original_url . $newurl;
				}   
			  } else {
				$code = 0;
			  }
			}
		  } while ($code && --$mr);
		  
		  curl_close($rch);
		  
		  if (!$mr)
		  {
			if ($maxredirect === null)
			trigger_error('Too many redirects.', E_USER_WARNING);
			else
			$maxredirect = 0;
			
			return false;
		  }
		  curl_setopt($ch, CURLOPT_URL, $newurl);
		}
	  }
	  return curl_exec($ch);
}		
function log_error($error,$int,$filename,$logEnabled = 0) {
	if($logEnabled) {
		error_log($error . "\n",$int,$filename);
	}
}

?>
