<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Hersteller-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Hersteller extends Varien_Object
{
	/**
	 * @var string
	 */
	public $Name;

	/**
	 * @var string
	 */
	public $URL;

	/**
	 * table attribute
	 * @var array
	 */
	public $Felder;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Seo>)
	 */
	public $SEO;
}
