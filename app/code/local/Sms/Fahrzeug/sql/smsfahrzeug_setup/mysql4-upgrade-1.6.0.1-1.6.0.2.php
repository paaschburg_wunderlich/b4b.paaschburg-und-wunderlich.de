<?php
/**
 * Sms_Fahrzeug
 *
 * @category  Mage
 * @package   SmS_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 */

/**
 * Update script
 *
 * Update script for Sms_Fahrzeug to expand table sms_fahrzeug.
 *
 * @category  Mage
 * @package   Sms_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 * @author    Wolf Schmidtk <wolf.schmidt@schmidt-medien.de>
 * @version   2015-05-28 10:32:14Z
 */
$installer = $this;
$installer->startSetup();

$installer->addAttribute('catalog_product','sms_kettenteilung',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Kettenteilung',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 110,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Kettenteilung', 'sms_kettenteilung');

$installer->addAttribute('catalog_product','sms_kettenglieder',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Kettenglieder',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 120,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Kettenglieder', 'sms_kettenglieder');

$installer->addAttribute('catalog_product','sms_zaehnezahl',array(
    'type'                       => 'text',
    'visible'                    => true,
    'label'                      => 'Zaehnezahl',
    'required'                   => false,
    'default'                    => '',
    'is_global'                  => Mage_Catalog_Model_Resource_Eav_Attribute::SCOPE_STORE,
    'is_html_allowed_on_front'   => false,
    'is_wysiwyg_enabled'         => false,
    'sort_order'                 => 130,
    'user_defined'               => false,
    'visible_on_front'           => false,
));
$installer->addAttributeToGroup('catalog_product', 'Default', 'Zaehnezahl', 'sms_zaehnezahl');

$installer->endSetup();


//$installer = $this;
//$this->startSetup();
//
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var0', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var1', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var2', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var3', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var4', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var5', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var6', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var7', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var8', 'varchar(25) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_var9', 'varchar(25) NOT NULL ');
//
//$this->endSetup();