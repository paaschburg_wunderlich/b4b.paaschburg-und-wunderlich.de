<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von freien Feldern
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_Model_Resource_Freies_Feld
{

	/**
	 *
	 * @param Cateno_ShopSync_Model_Types_Freies_Feld $inFreieFelder
	 * @param array $inData
	 * @return array
	 */
	public function mergeFreiesFeldData($inFreieFelder, $inData)
	{
		if(is_array($inFreieFelder)) {
			foreach($inFreieFelder as $theFeld) {
				$inData = array_merge($inData, array(
					$theFeld->FeldShop => $theFeld->Wert
				));
			}
		}
		return $inData;
	}

	public function setFreieFelder($inObject, $inFreieFelder, $inType = null)
	{
		$theFreieFlederArray = array();
		foreach($inFreieFelder as $theFreieFled) {
		    $theValue = $inObject->getData($theFreieFled->FeldShop);
		    if(strtolower($theFreieFled->FeldShop) == 'gender') {
		        if($inObject->getData($theFreieFled->FeldShop) == 1){
		            $theValue="Male";
		        }elseif($inObject->getData($theFreieFled->FeldShop) == 2){
		            $theValue="Female";
		        }
		    }
			$theFreieFlederArray[] = new Cateno_ShopSync_Model_Types_Freies_Feld(
				$theFreieFled->FeldShop, $theValue
			);
		}
		return $theFreieFlederArray;
	}
}
