if (window.jQuery && jQuery.noConflict && (typeof $('body') == 'object')) {
	jQuery.noConflict();
/**
	 * ja tabs plugin
	 */
	(function($) { 
				$.fn.jaContentTabs = function (_options){
					return this.each( function() {		
						var container = $( this );
						new $.jaContentTabs().setup( this, container );
					} );
				}
				 $.jaContentTabs = function() { 
					var self = this;
					this.lastTab = null;
					this.nextTab=null;
					this.setup=function( obj, o ){
						var contentTabs = o.children( 'div.ja-tab-content' );
						var nav = o.children( 'ul.ja-tab-navigator' );
						contentTabs.children('div').hide();
						nav.children('li:first').addClass('first').addClass( 'active' );	
						contentTabs.children('div:first').show();
						
						nav.children('li').children('a').click( function() {
							self.lastTab = 	nav.children('li.active').children('a').attr('href');										
							nav.children('li.active').removeClass('active')											
							$(this).parent().addClass('active');
							var currentTab = $(this).attr('href'); 
							self.tabActive( contentTabs, currentTab );		
							return false;
						});	
					};
					this.tabActive=function( contentTabs,  currentTab ){
						if(  this.lastTab != currentTab ){
							contentTabs.children( this.lastTab ).hide();	
						}
			
						contentTabs.children( currentTab ).show();
					};
				};
			})(jQuery);
}

function switchTool (ckname, val) {
	createCookie(ckname, val, 365);
	window.location.reload();
}

function createCookie(name,value,days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime()+(days*24*60*60*1000));
		var expires = "; expires="+date.toGMTString();
	}
	else expires = "";
	document.cookie = name+"="+value+expires+"; path=/";
}

String.prototype.trim = function() { return this.replace(/^\s+|\s+$/g, ""); };

function menuFistLastItem () {
	if ((menu = $('nav')) && (children = menu.childElements()) && (children.length)) {
		children[0].addClassName ('first');
		children[children.length-1].addClassName ('last');
	}
}

document.observe("dom:loaded", function() {   
	menuFistLastItem();
	jQuery(".block-content .currently ol").children().last().addClass("last");
	jQuery(".btn-toggle").on("click",function () {
			
			if(jQuery("#jmoverlay").length <= 0){
				jmoverlay = jQuery('<div id="jmoverlay" class="jmoverlay"></div>'); 
				jmoverlay.appendTo('#ja-header .main');
				jmoverlay.bind("click",function(){
					jQuery("#ja-mainnav-inner").css({"left":"0"});							
					jQuery(".has-toggle .btn-toggle").removeClass("active");
					jQuery(".btn-toggle").siblings(".inner-toggle").removeClass("inneractive");	
					this.remove();
				})
			}	
			
			jQuery(".btn-toggle").not(this).siblings(".inner-toggle").removeClass("inneractive");													  	        
			jQuery(this).siblings(".inner-toggle").toggleClass("inneractive");
			jQuery(".btn-toggle").not(this).removeClass("active");													  	            
			jQuery(this).toggleClass("active");
			if(!jQuery(this).hasClass("active") && jQuery("#jmoverlay").length > 0){
				jQuery("#jmoverlay").remove();
			} 
			if(window.sidebarIScroll){
		       window.sidebarIScroll.refresh();
	        }
		});
     wwidth = jQuery(window).width();
		//iscroll section
	  if(jQuery(".megamenu") !== undefined){
		 jQuery("li.haschild").on("mouseover",function(){
           if(wwidth < 985) { return  false; }														 
		   if(jQuery(this).children(".childcontent").length){
			   childcontent = jQuery(this).find(".childcontent-inner-wrap").first();
			   childcontentheight = (childcontent.find(".childcontent-inner").height() < jQuery(window).height())?childcontent.find(".childcontent-inner").height():jQuery(window).height()-100;	
			   childcontent.height(childcontentheight);
			   childcontentid = childcontent.attr("id");
			   if(window.childcontentIScrol == undefined && window.sidebarIScroll == undefined ){
				  window.childcontentIScrol = new iScroll(childcontentid,{vScrollbar: true, useTransform: true,hScrollbar: false});	   
			   }		   
		   } 												  
		 });   
	  }
	   
	  jQuery(window).resize (function() {
		   if(window.shopbyIScrol && window.shopbyIScrol !== undefined)	{
			     window.shopbyIScrol.destroy();
			     window.shopbyIScrol  = null;
			}					  
		   wwidth = jQuery(window).width();	
		   if(wwidth <= 985 && window.sidebarIScroll == undefined){
			  window.sidebarIScroll = new iScroll('ja-mainnav-inner',{vScrollbar: true, useTransform: true,hScrollbar: false});  
		   }else if(window.sidebarIScroll !== undefined && window.sidebarIScroll !== null){
			  jQuery('#jm-megamenu').removeAttr('style');
			  jQuery("#ja-mainnav .btn-toggle").removeClass("active");
			  window.sidebarIScroll.destroy();
			  window.sidebarIScroll = null;
		   }						 
	  });
	  jQuery("#ja-mainnav .btn-toggle").on("click",function(event){
			  event.stopPropagation();
			  wwidth = jQuery(window).width();
			  mainnavos = jQuery("#ja-mainnav-inner").offset();
			  totalwidth = mainnavos.left + jQuery("#ja-mainnav-inner").width();
			  
              if(totalwidth > wwidth){
				  left =  wwidth-totalwidth-5;
				  jQuery("#ja-mainnav-inner").css({"left":left});
			  }
			  if(jQuery(this).attr("class").indexOf("active") < 0){
				  jQuery("#ja-mainnav-inner").css({"left":0});
			  }
			  if(wwidth > 985) { 
			     if(window.childcontentIScrol)
			     return  false;
			  }
			  updatemenuheight();			  
	  });
});

function updatemenuheight(object){
	 
		  mainavheight = (jQuery("#jm-megamenu ul.megamenu:first").height() < jQuery(window).height())?jQuery("#jm-megamenu ul.megamenu:first").height():jQuery(window).height()-120;
		  jQuery("#ja-mainnav-inner").height(mainavheight);										   
		  if(window.sidebarIScroll !== undefined && window.sidebarIScroll !== null){
			 window.sidebarIScroll.refresh();
		  }else{
			 window.sidebarIScroll = new iScroll('ja-mainnav-inner',{vScrollbar: true, useTransform: true,hScrollbar: true});   
		  }			
	
} 

//Add hover event for li - hack for IE6
function navMouseHover () {
	var lis = $$('#nav li');
	if (lis && lis.length) {
		lis.each (function(li){
			li.onMouseover = toggleMenu (li, 1);
			li.onMouseout = toggleMenu (li, 0);
		});
	}
}

toggleMenu = function (el, over) {
	  var iS = false;
    if (Element.childElements(el) && Element.childElements(el).length > 1) {
	    var uL = Element.childElements(el)[1];
			iS = true;
		}
    if (over) {
        Element.addClassName(el, 'over');
				Element.addClassName (el.down('a'), 'over');
        if(iS){ uL.addClassName('shown-sub')};
    }
    else {
        Element.removeClassName(el, 'over');
				Element.removeClassName (el.down('a'), 'over');
        if(iS){ uL.removeClassName('shown-sub')};
    }
}
