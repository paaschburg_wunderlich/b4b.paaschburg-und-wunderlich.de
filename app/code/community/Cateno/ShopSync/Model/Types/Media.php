<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Media-Dateiinformationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Media extends Varien_Object
{
	/**
	 * @var string
	 */
	public $DateiName;

	/**
	 * @var string
	 */
	public $Type;

	/**
	 * @var string
	 */
	public $Klasse;

	/**
	 * @var int
	 */
	public $ID;
}
