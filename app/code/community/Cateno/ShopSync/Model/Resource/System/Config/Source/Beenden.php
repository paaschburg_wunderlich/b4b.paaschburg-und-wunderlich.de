<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung der Beenden-Systemkonfiguration
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_System_Config_Source_Beenden
{
	const BEREINIGEN_OPTIONS_NO_DECIDE = 0;
	const BEREINIGEN_OPTIONS_INACTIVE = 1;
	const BEREINIGEN_OPTIONS_DELETE = 2;

	public function toOptionArray()
	{
		return array(
            array('value' => self::BEREINIGEN_OPTIONS_NO_DECIDE, 'label'=>Mage::helper('shopsync')->__('WaWi Decide')),
            array('value' => self::BEREINIGEN_OPTIONS_INACTIVE, 'label'=>Mage::helper('shopsync')->__('Inactive Product')),
            array('value' => self::BEREINIGEN_OPTIONS_DELETE, 'label'=>Mage::helper('shopsync')->__('Delete Product'))
        );
	}
}
