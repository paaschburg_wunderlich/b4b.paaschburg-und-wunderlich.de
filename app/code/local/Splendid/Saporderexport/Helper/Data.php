<?php class Splendid_Saporderexport_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function getGender($gender)
    {
        switch ($gender) {
            case '1':
                return 'Male';
            case '2':
                return 'Female';
            default:
                return 'Male';
        }
    }

    public function getActiv($activ)
    {
        switch ($activ) {
            case '1':
                return 'Ja';
            case '0':
                return 'Nein';
            default:
                return 'Ja';
        }
    }
}
