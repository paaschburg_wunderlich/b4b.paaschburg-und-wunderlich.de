<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Kundengruppen-Rabattsätzen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Kdgrprabsz extends Varien_Object
{
	/**
	 * @var int
	 */
	public $KdGrp;

	/**
	 * @var float
	 */
	public $RabSz;
}
