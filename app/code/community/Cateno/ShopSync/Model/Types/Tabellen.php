<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Tabellen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Tabellen extends Varien_Object
{
	/**
	 * @var string
	 */
	public $TabellenName;

	/**
	 * @var array
	 */
	public $Felder;

    /**
	 * @var array(<Cateno_ShopSync_Model_Types_FeldInfo>)
	 */        
	public $FeldInfo;

	public function __construct($inTabellenName = null, $inFelder = array(),$feldInfo = null)
	{
		$this->TabellenName = $inTabellenName;
		$this->Felder = $inFelder;
        $this->FeldInfo =  $feldInfo;                
	}
}
