<?php

class Sms_Angebote_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function angebot($angebotid)
    {
        $angebote = new Mage_Catalog_Model_Category();
        $angebote->load($angebotid);
        $angebotproducts = $angebote->getProductCollection();
        $angebotproducts->addAttributeToSelect('*');
        $angebotproducts->getSelect()->order('RAND()');
        $angebotproducts->getSelect()->limit(1);
        $angebote_array = array();
        foreach($angebotproducts as $angebot)
        {
            $angebotimg = $angebot->getImageUrl();
//            $angebotimg = $angebot->getSmallImage();
            $angebotname = $angebot->getName();
            $angebotname = substr($angebotname,0,43);
            $angebotsku = $angebot->getSku();
            $angebotsurl = $angebot->getUrlPath();
            $angebotvk = $angebot->getFinalPrice();
            $angebotsshortdescription = $angebot->getShortDescription();
            $angebotsshortdescription = strip_tags($angebotsshortdescription);
//            $angebotsshortdescription = substr($angebotsshortdescription,0,80);
            array_push($angebote_array, $angebotimg, $angebotname, $angebotsku, $angebotsshortdescription, $angebotsurl, $angebotvk);
        }
        return $angebote_array;
    }

    public function neuheiten ($neuheitenid)
    {
        $neuheiten = new Mage_Catalog_Model_Category();
        $neuheiten->load($neuheitenid);
        $neuheitenproducts = $neuheiten->getProductCollection();
        $neuheitenproducts->addAttributeToSelect('*');
        $neuheitenproducts->getSelect()->order('RAND()');
        $neuheitenproducts->getSelect()->limit(1);
        $neuheiten_array = array();
        foreach($neuheitenproducts as $neuheit)
        {
            $neuheitimg = $neuheit->getImageUrl();
            $neuheitname = $neuheit->getName();
            $neuheitname = substr($neuheitname,0,43);
            $neuheitsku = $neuheit->getSku();
            $neuheiturl = $neuheit->getUrlPath();
            if (Mage::getSingleton('core/session')->getHek() == '1'){
                $neuheitvk = $neuheit->getPrice();
            } else if (Mage::getSingleton('core/session')->getHek() == '0'){
                $neuheitvk = $neuheit->getPrice_uvp();
            }
            $neuheitenshortdescription = $neuheit->getShortDescription();
            $neuheitenshortdescription = strip_tags($neuheitenshortdescription);
            $neuheitenshortdescription = substr($neuheitenshortdescription,0,80);
            array_push($neuheiten_array, $neuheitimg, $neuheitname, $neuheitsku, $neuheitenshortdescription, $neuheiturl, $neuheitvk);
        }
        return $neuheiten_array;
    }

    public function saison ($saisonid)
    {
        $saison = new Mage_Catalog_Model_Category();
        $saison->load($saisonid);
        $saisonproducts = $saison->getProductCollection();
        $saisonproducts->addAttributeToSelect('*');
        $saisonproducts->getSelect()->order('RAND()');
        $saisonproducts->getSelect()->limit(1);
        $saison_array = array();
        foreach($saisonproducts as $saisonart)
        {
            $saisonimg = $saisonart->getImageUrl();
            $saisonname = $saisonart->getName();
            $saisonname = substr($saisonname,0,43);
            $saisonsku = $saisonart->getSku();
            $saisonurl = $saisonart->getUrlPath();
            if (Mage::getSingleton('core/session')->getHek() == '1'){
                $saisonvk = $saisonart->getPrice();
            } else if (Mage::getSingleton('core/session')->getHek() == '0'){
                $saisonvk = $saisonart->getPrice_uvp();
            }
            $saisonshortdescription = $saisonart->getShortDescription();
            $saisonshortdescription = strip_tags($saisonshortdescription);
            $saisonshortdescription = substr($saisonshortdescription,0,80);
            array_push($saison_array, $saisonimg, $saisonname, $saisonsku, $saisonshortdescription, $saisonurl, $saisonvk);
        }
        return $saison_array;
    }

    public function abverkauf ($abverkaufid)
    {
        $abverkauf = new Mage_Catalog_Model_Category();
        $abverkauf->load($abverkaufid);
        $abverkaufproducts = $abverkauf->getProductCollection();
        $abverkaufproducts->addAttributeToSelect('*');
        $abverkaufproducts->getSelect()->order('RAND()');
        $abverkaufproducts->getSelect()->limit(1);
        $abverkauf_array = array();
        foreach($abverkaufproducts as $abverkaufart)
        {
            $abverkaufimg = $abverkaufart->getImageUrl();
            $abverkaufname = $abverkaufart->getName();
            $abverkaufname = substr($abverkaufname,0,43);
            $abverkaufsku = $abverkaufart->getSku();
            $abverkaufurl = $abverkaufart->getUrlPath();
            if (Mage::getSingleton('core/session')->getHek() == '1'){
                $abverkaufvk = $abverkaufart->getPrice();
            } else if (Mage::getSingleton('core/session')->getHek() == '0'){
                $abverkaufvk = $abverkaufart->getPrice_uvp();
            }
            $abverkaufshortdescription = $abverkaufart->getShortDescription();
            $abverkaufshortdescription = strip_tags($abverkaufshortdescription);
            $abverkaufshortdescription = substr($abverkaufshortdescription,0,80);
            array_push($abverkauf_array, $abverkaufimg, $abverkaufname, $abverkaufsku, $abverkaufshortdescription, $abverkaufurl, $abverkaufvk);
        }
        return $abverkauf_array;
    }
  }
