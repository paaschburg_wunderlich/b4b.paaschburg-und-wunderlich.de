<?php
/**
 * Artikel Events for User-Functions
 * Aufrufe für Kundenskripte
 * @category Single
 * @package Cateno_ShopSync
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_Model_Service_Public_Artikel extends Cateno_ShopSync_Model_Types_Abstract
{
	public function NachArtikelEnde($inZaehler)
	{
		Mage::dispatchEvent('NachArtikelEnde', array('counter' => $inZaehler));
		return false;
	}

	public function NachArtikelKategorieEnde($inZaehler)
	{
		Mage::dispatchEvent('NachArtikelKategorieEnde', array('counter' => $inZaehler));
		return false;
	}

	public function NachLagerEnde($inZaehler)
	{
		Mage::dispatchEvent('NachLagerEnde', array('counter' => $inZaehler));
		return false;
	}
}
