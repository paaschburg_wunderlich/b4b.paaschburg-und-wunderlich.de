<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 20.10.2015
 * Time: 19:44
 */
?>

<?php
require_once '../app/Mage.php';

UMASK(0);

Mage::app();
Mage::init();

$collection = mage::getModel('customer/customer')
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('is_shoppartner', true)
    ->addAttributeToFilter('customer_activated', 1)
    ->joinAttribute('billing_street', 'customer_address/street', 'default_billing', null, 'left')
    ->joinAttribute('billing_postcode', 'customer_address/postcode', 'default_billing', null, 'left')
    ->joinAttribute('billing_city', 'customer_address/city', 'default_billing', null, 'left')
    ->addAttributeToSort('entity_id', 'ASC');
//    ->setPageSize(1);

foreach($collection as $shoppartner)
{
//
//    echo $shoppartner->getEntityId() . ";" .
//        $shoppartner->getErpCustomernumber() . ";" .
//        $shoppartner->getStoreName() . ";" .
//        $shoppartner->getName() . ";" .
//        $shoppartner->getBillingStreet() . ";" .
//        $shoppartner->getBillingPostcode() . ";" .
//        $shoppartner->getBillingCity() . ";" .
//        $shoppartner->getEmail() . ";" . "<br>";
    $anrede = $shoppartner->getName();
    $spid = $shoppartner->getEntityId();

    $text='<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<link rel="stylesheet" type="text/css" href="http://www.brands4bikes.de/skin/frontend/default/jm_flannel/css/theme.css" media="all">
</head>
<body>
<h3>Banner für Ihre Homepage</h3>
<h4>Hallo ' . $anrede . ',</h4>
<p>wir haben nachdem Neustart von www.brands4bikes.de zwei Neuerungen eingeführt:</p>
<p>Zum einen kann der Endkunde jetzt auch die Shopabholung wählen, die auch bei geringeren Rechnungsummen keinerlei Versandkosten verursacht.
Hierfür werden ihm die drei Shoppartner zur Auswahl gestellt, die am nächsten zur eingegebenen Rechnungsadresse liegen.
Hierüber wollen wir erreichen, dass Sie Neukunden für die Werkstatt und das Ladengeschäft gewinnen können.</p>
    <p>Zum anderen binden Sie mit den untenstehenden Code einen Werbebanner für Brands4Bikes mit fester Verlinkung ein.
    Die feste Verlinkung sorgt dafür, dass Kunden die über diese Verlinkung auf Ihrer Seite zu Brands4Bikes kommen, Sie als Shoppartner zugeordnet werden.
    Kopieren Sie dafür einfach einen der folgenden Codes und fügen Sie ihn auf Ihrer Homepage an der entsprechenden Stelle ein.
    Wir bieten Ihnen das Banner in drei verschiedenen Größen an.
    Somit sollte für jeden Bedarf ein Banner dabei sein.</p>
    <p>Wir hoffen, dass die Verbesserungen in Ihrem Sinne sind und zu einer weiteren Umsatzsteigerung führen.</p>
    <p>Für Rückfragen stehen wir Ihnen natürlich gerne zur Verfügung.</p>
    <p>Mit besten Grüßen für eine erfolgreiche Saison,<br>
    Ihr Paaschburg & Wunderlich Team</p>
    <h4>Banner Größe 180 x 150 Pixel quer</h4>
    <p>
        <a href="http://www.brands4bikes.de/?id='.$spid.'" target="_blank">
            <img src="http://www.brands4bikes.de/media/banner/180x150_banner.jpg" alt="Brands4Bikes Onlineshop - alles für Dein Motorrad" title="Brands4Bikes Onlineshop - alles für Dein Motorrad">
        </a>
    </p>
        <textarea style="width:728px; height:60px;"><a href="http://www.brands4bikes.de/?id='.$spid.'" target="_blank">
<img src="http://www.brands4bikes.de/media/banner/180x150_banner.jpg" alt="Brands4Bikes Onlineshop - alles für Dein Motorrad" title="Brands4Bikes Onlineshop - alles für Dein Motorrad"></a></textarea>

    <p>&nbsp;</p>
    <h4>Banner Größe 300 x 250 Pixel quer</h4>
    <p>
        <a href="http://www.brands4bikes.de/?id='.$spid.'" target="_blank">
            <img src="http://www.brands4bikes.de/media/banner/300x250_banner.jpg" alt="Brands4Bikes Onlineshop - alles für Dein Motorrad" title="Brands4Bikes Onlineshop - alles für Dein Motorrad">
        </a>
    </p>
    <textarea style="width:728px; height:60px;"><a href="http://www.brands4bikes.de/?id='.$spid.'" target="_blank">
<img src="http://www.brands4bikes.de/media/banner/300x250_banner.jpg" alt="Brands4Bikes Onlineshop - alles für Dein Motorrad" title="Brands4Bikes Onlineshop - alles für Dein Motorrad"></a></textarea>

    <p>&nbsp;</p>
    <h4>Banner Größe 728 x 90 Pixel quer</h4>
    <p>
        <a href="http://www.brands4bikes.de/?id='.$spid.'" target="_blank">
            <img src="http://www.brands4bikes.de/media/banner/728x90_banner.jpg" alt="Brands4Bikes Onlineshop - alles für Dein Motorrad" title="Brands4Bikes Onlineshop - alles für Dein Motorrad">
        </a>
    </p>
    <textarea style="width:728px; height:60px;"><a href="http://www.brands4bikes.de/?id='.$spid.'" target="_blank">
<img src="http://www.brands4bikes.de/media/banner/728x90_banner.jpg" alt="Brands4Bikes Onlineshop - alles für Dein Motorrad" title="Brands4Bikes Onlineshop - alles für Dein Motorrad"></a></textarea>

    </body>
    </html>';
    ?>

    <!-- Mailversand -->

    <?php
    $empfaenger = $shoppartner->getEmail();
//    $empfaenger = "kontakt@ss-bike-event.de";
    $betreff = "Brands4Bikes Banner mit fester Verlinkung";
    $from = "From: verkauf@brands4bikes.de\r\n";
    $from .= "Reply-To: verkauf@brands4bikes.de\r\n";
    $from .= "Bcc: admin@pwonline.info\r\n";
    $from .= "Content-type: text/html; charset=utf-8\r\n";
    mail($empfaenger, $betreff, $text, $from);
    ?>
    <?php echo $empfaenger . "<br>"; ?>
    <?php echo $betreff . "<br>"; ?>
    <?php echo $text . "<br>"; ?>
<?php } ?>
