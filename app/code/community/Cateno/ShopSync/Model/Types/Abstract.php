<?php
/**
 * Cateno
 *
 * @category Abstract Class
 * @package Cateno_ShopSync
 * Abstrakte Basis für Typen-Klassen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Abstract
{
	protected $typeMap = array(
        'related'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_RELATED,
        'up_sell'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_UPSELL,
        'cross_sell'    => Mage_Catalog_Model_Product_Link::LINK_TYPE_CROSSSELL,
        'grouped'       => Mage_Catalog_Model_Product_Link::LINK_TYPE_GROUPED
    );

    protected function _getTypeId($inType)
    {
        if (!isset($this->typeMap[$inType])) {
            $this->_fault('type_not_exists');
        }
        return $this->typeMap[$inType];
    }

    /**
     * Dispatches fault
     *
     * @param string $code
     */
    protected function _fault($inCode, $inMessage = null)
    {
        throw new Mage_Api_Exception($inCode, $inMessage);
    }

    protected function NachArtikel($inProductIds)
    {
    	return false;
    }

    protected function NachArtikelListe($inProductIds)
    {
    	return false;
    }

    protected function NachArtikelKategorie($inCategoryIds)
    {
    	return false;
    }

    protected function NachArtikelKategorieListe($inCategoryIds)
    {
    	return false;
    }
}
