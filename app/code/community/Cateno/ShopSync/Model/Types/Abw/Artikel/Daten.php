<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von abweichenden Artikeldaten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Abw_Artikel_Daten
{
	/**
	 * @var string
	 */
	public $ArtNr;

	/**
	 * @var string
	 */
	public $AbwArtNr;

	/**
	 * @var string
	 */
	public $AbwPr;

	/**
	 * @var int
	 */
	public $KARabKz;

	/**
	 * @var int
	 */
	public $RabKz;

	/**
	 * @var Cateno_ShopSync_Model_Types_Abw_Artikel_Daten_Rab
	 */
	public $Rab;

}
