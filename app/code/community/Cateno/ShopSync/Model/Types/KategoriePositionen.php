<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Kategoriepositionen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_KategoriePositionen extends Varien_Object
{
	/**
	 * @var string
	 */
	public $KategID;

	/**
	 * @var string
	 */
	public $SKU;

	/**
	 * table attribute
	 * @var string
	 */
	public $KategPos;
}
