<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Zusammenfassungsmodul-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Zusammenfassungs_Module extends Varien_Object
{
	public $Name;

	public $Bezeichnung;

	public function __construct($inName = null, $inBezeichnung = null)
	{
		$this->Name = $inName;
		$this->Bezeichnung = $inBezeichnung;
	}
}
