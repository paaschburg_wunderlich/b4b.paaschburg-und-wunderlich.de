<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Feld-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_FeldInfo extends Varien_Object
{
	/**
	 * @var string
	 */
	public $Name;

	/**
	 * @var string
	 */
	public $Typ;

	/**
	 * @var string
	 */
	public $Art;	

	public function __construct($name,$typ,$art)
	{
		$this->Name = $name;
		$this->Typ = $typ;
		$this->Art= $art;
	}
}
