<?php
/**
 *
 * Observer.php
 *
 * @author		Wolf Schmidt
 * @copyright	Schmidt Medienservice
 * @license		Schmidt Medienservice
 * @version		1.6.0.0
 * @since		21.05.2015
**/

class Sms_Fahrzeug_Model_Observer {
    public function fahrzeugSave($observer) {
        $object = $observer->getEvent()->getSave();
        /*  magic */
        return $this;
    }
}