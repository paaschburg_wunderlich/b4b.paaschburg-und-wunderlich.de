<?php
/**
 * Cateno
 *
 * @category Single Class
 * @package Cateno_ShopSync
 * Einstiegspunkt für Vorgang-Sync
 * 
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Service_Vorgang
{
	/**
	 * 
	 * @param array $inWShopIDs
	 * @param Cateno_ShopSync_Model_Types_Freies_Feld $inFreieFelder
	 * @param array $inFreieFelderPositionen
	 * @return array
	 */
	public function getVorgangListe($inWShopIDs, $inFreieFelder, $inFreieFelderPositionen)
	{
		$theVorgangListe = array();

		foreach($inWShopIDs as $theWShopID) {
		     $this->RebuildOrder($theWShopID);
			$theOrder = Mage::getModel('sales/order')->loadByIncrementId($theWShopID);
			if(!$theOrder->getId()){
			    throw new SoapFault('order id fault', $theWShopID.' order increment id not exits');
			}
			$theVorgang = new Cateno_ShopSync_Model_Types_Vorgang();
			$theTaxCollection = Mage::getModel('tax/sales_order_tax')->getCollection()->loadByOrder($theOrder);
			$theCustomer = Mage::getModel('customer/customer')->load($theOrder->getCustomerId());
			$theVorgang->UStId = null;
			if($theCustomer->getId()){
			    $theVorgang->UStId = $theCustomer->getTaxvat();
			}
			
			$theVorgang->ArtPrGrp = $theOrder->getCustomerGroupId();
			
			// Generelle Vorgangsdaten
			$thePayment = $theOrder->getPayment();
			$theVorgang->WShopIDAdresse = $theOrder->getCustomerId();
			$theVorgang->WShopID = $theWShopID;
			$theVorgang->Dat = $theOrder->getCreatedAtStoreDate()->toString("yyyy-M-d H:m:s");
			$theVorgang->ZahlungsOption = $thePayment->getMethod();
			$data = explode('_', $theOrder->getShippingMethod());
            $carrierCode = $data[0];
			$theVorgang->VersandArt = $carrierCode;
			$theVorgang->AspAnsp = '';
            $theVorgang->AspAnr = '';
            $theVorgang->GPreisBt = $theOrder->getBaseGrandTotal();
            $theVorgang->GPreisNt = $theOrder->getGrandTotal() - $theOrder->getTaxAmount();
			
			// Rechnungs- und Lieferanschrift
			/* @var $theBillingAddress Mage_Sales_Model_Order_Address */	
            $theBillingAddress = $theOrder->getBillingAddress();
			$theVorgang->WShopIDReAns = $theBillingAddress->getCustomerAddressId();
			$theVorgang->RePrefix = $theBillingAddress->getPrefix();
			$theVorgang->ReFirstName = $theBillingAddress->getFirstname();
			$theVorgang->ReLastName = $theBillingAddress->getLastname();
			$theVorgang->ReCompany = $theBillingAddress->getCompany();
			$theVorgang->ReStr = $theBillingAddress->getStreet();
			$theVorgang->ReStr =$theVorgang->ReStr[0];
			$theVorgang->ReFax =  $theBillingAddress->getFax();
			$theVorgang->ReLandKennz = $theBillingAddress->getCountryId();
			$theVorgang->RePLZ = $theBillingAddress->getPostcode();
			$theVorgang->ReOrt = $theBillingAddress->getCity();
			$theVorgang->ReTel = $theBillingAddress->getTelephone();
			$theVorgang->ReEmail = $theBillingAddress->getEmail();
                        
			$theVorgang->AspEMail = $theBillingAddress->getEmail();
			
			$theShippingAddress = $theOrder->getShippingAddress();
			//Gastbestellungen haben keine Lieferadresse (Magento 1.7.x specifisch?)
			if(!empty($theShippingAddress)) {
				$theVorgang->WShopIDLiAns = $theShippingAddress->getCustomerAddressId();
				$theVorgang->LiPrefix = $theShippingAddress->getPrefix();
				$theVorgang->LiFirstName = $theShippingAddress->getFirstname();
				$theVorgang->LiLastName = $theShippingAddress->getLastname();
				$theVorgang->LiCompany = $theShippingAddress->getCompany();
				$theVorgang->LiStr = $theShippingAddress->getStreet();
				$theVorgang->LiStr =  $theVorgang->LiStr[0];
				$theVorgang->LiLandKennz = $theShippingAddress->getCountryId();
				$theVorgang->LiPLZ = $theShippingAddress->getPostcode();
				$theVorgang->LiOrt = $theShippingAddress->getCity();
				$theVorgang->LiTel = $theShippingAddress->getTelephone();
				$theVorgang->LiFax =  $theShippingAddress->getFax();
				$theVorgang->LiEmail = $theShippingAddress->getEmail();
			}
			$theVorgang->Waehr = $theOrder->getOrderCurrencyCode();
			$theVorgang->KartInhab = '';
			$theVorgang->KartNr = '';
			$theVorgang->KartGueltig = '';

			// Lastschrift prüfen
			$payment = $theOrder->getPayment();
			if (is_object($payment) ) {
				$paymentData = $payment->getData();
				if ($paymentData['method'] == 'debit') {
					// Daten für Lastschrift
					$theVorgang->KNr = $thePayment->decrypt($thePayment->getCcNumberEnc());
					$theVorgang->Inhab = $thePayment->getCcOwner();
					$theVorgang->BLZ = $thePayment->decrypt($thePayment->getCcType());
                    $theVorgang->BIC = $thePayment['debit_swift'];
                    $theVorgang->IBAN = $thePayment['debit_iban'];
				}
			}

			$theVorgang->Bemerkung = array();
			
			$theComments = $theOrder->getStatusHistoryCollection();
			
			$idx = 0;
			foreach($theComments as $theComment) {
                if($theComment->getComment() != '')
                    $theVorgang->Bemerkung[] = $theComment->getComment();
			}
			$theVorgang->Bemerkung = implode("\n", $theVorgang->Bemerkung);
			$theResultFreieFelder = array();
			foreach($inFreieFelder as $theFreieFeld) {
				$theResultFreieFeld = clone $theFreieFeld;
				$theResultFreieFeld->Wert = $theOrder->getData($theResultFreieFeld->FeldShop);
				$theResultFreieFeld->FeldWarenwirtschaft = null;
				$theResultFreieFeld->FeldArt = null;
				$theResultFreieFeld->Name = null;
				$theResultFreieFeld->Sprache = null;
				$theResultFreieFelder[] = $theResultFreieFeld;
			}
			
			$theVorgang->FreieFelder = $theResultFreieFelder;
			$theVorgang->VorgangPosition = $this->getVorgangPosition($theOrder, $inFreieFelderPositionen, $theVorgang->GPreisBt, $theVorgang->GPreisNt);
			$theVorgangListe [] = $theVorgang;
			unset($theVorgang);
			unset($theResultFreieFelder);
		}
		
		return $theVorgangListe;
	}
	
	protected function getVorgangPosition($theOrder, $inFreieFelder, &$inGPreisBt, &$inGPreisNt)
	{
		// product
		// true include tax(need to - tax = product_price);
		$theIklStKz = Mage::getModel('tax/config')->priceIncludesTax();
		// true shipping include tax(need to - tax = shipping_amount);
		$theShippingPriceIncludeTax = Mage::getModel('tax/config')->shippingPriceIncludesTax();
		
		$theOrderItems = $theOrder->getItemsCollection(array(), true);
		
		$theVorgangPositionArray = array();
		foreach($theOrderItems as $theItem) {
		    
			$theVorgangPosition = new Cateno_ShopSync_Model_Types_Vorgang_Position();
			$theProduct = Mage::getModel('catalog/product')->load($theItem->getProductId());

			// Kindprodukt wenn vorhanden
			$childItem = $theItem->getProductOptions();
			$sku = $childItem['simple_sku'] ? $childItem['simple_sku'] : $theItem->getSku();

			$theVorgangPosition->ArtNr = $sku;
			$theVorgangPosition->Bez = $theItem->getName();
			$theVorgangPosition->Mge = $theItem->getQtyOrdered();
			$theVorgangPosition->UstSz = $theItem->getTaxPercent();
			
			$theResultFreieFelder = array();
            foreach($inFreieFelder as $theFreieFeld) {
                $theResultFreieFeld = new Cateno_ShopSync_Model_Types_Freies_Feld();
                $theResultFreieFeld->FeldShop = $theFreieFeld->FeldShop;
                $theResultFreieFeld->Wert = $theItem->getData($theFreieFeld->FeldShop);
                $theResultFreieFeld->FeldWarenwirtschaft = null;
				$theResultFreieFeld->FeldArt = null;
                $theResultFreieFeld->Name = null;
                $theResultFreieFeld->Sprache = null;
                $theResultFreieFelder[] = $theResultFreieFeld;
            }
			$theVorgangPosition->FreieFelder = $theResultFreieFelder;
			$theVorgangPosition->IstFrachtPosition = false;
			
			$theVorgangPosition->EPrNt = $theItem->getRowTotal();
			$theVorgangPosition->EPrBt = $theItem->getRowTotalInclTax(); // Grand Totals
			$theVorgangPositionArray[] = $theVorgangPosition;
		}
		
		$theShippingMethod = $theOrder->getShippingMethod();
		$theCarrierCode = explode('_', $theShippingMethod);
		
		
		if($theShippingMethod != '') {
		    
		    $theVorgangPosition = new Cateno_ShopSync_Model_Types_Vorgang_Position();
		    $theVorgangPosition->KlassenName = 'Versandkosten';
			$theVorgangPosition->ArtNr = $theCarrierCode[0];
		    $theVorgangPosition->Bez = Mage::getStoreConfig('carriers/'.$theCarrierCode[0].'/name');
		    $theVorgangPosition->Mge = $theOrder->get('total_qty_ordered');
			if ($theOrder->getShippingAmount() != 0)
				$theVorgangPosition->UstSz = number_format($theOrder->getShippingTaxAmount() / $theOrder->getShippingAmount(), 5, ".", " ") * 100;
		    $theResultFreieFelder = array();
            $theVorgangPosition->FreieFelder = $theResultFreieFelder;
            $theVorgangPosition->EPrNt = $theOrder->getShippingAmount();
            $theVorgangPosition->EPrBt = $theOrder->getShippingInclTax();
		    $theVorgangPosition->IstFrachtPosition = true;
		    
		    $theVorgangPositionArray[] = $theVorgangPosition;
		    
		}
		
		// Rabatt
		if ($theOrder->getBaseDiscountAmount() < 0) {
			
			$theVorgangPosition = new Cateno_ShopSync_Model_Types_Vorgang_Position();
		    $theVorgangPosition->KlassenName = 'Rabatt';
			$theVorgangPosition->Bez = 'Rabatt (' . $theOrder->getDiscountDescription() . ')';
		    $theVorgangPosition->Mge = 1;
		    $theVorgangPosition->UstSz = 0;
            $theVorgangPosition->EPrNt = $theOrder->getBaseDiscountAmount();
            $theVorgangPosition->EPrBt = $theOrder->getBaseDiscountAmount();
		    $theVorgangPosition->IstFrachtPosition = true;
		    $theVorgangPositionArray[] = $theVorgangPosition;
		    
		}
		
		// Nachnahme
		$payment = $theOrder->getPayment();
        if (is_object($payment)) {
            $paymentMethod = $payment->getMethodInstance();
            $paymentCode = $paymentMethod->getCode();
        }
        
        if ($paymentCode == 'cashondelivery') {
           
            $costs = $theOrder->getPayment()->getMethodInstance()->getInlandCosts();
            
            $theVorgangPosition = new Cateno_ShopSync_Model_Types_Vorgang_Position();
			$theVorgangPosition->KlassenName = 'Nachnahmegebuehr';
			$theVorgangPosition->Bez = 'Nachnahmegebühr';
            $theVorgangPosition->Mge = 1;
            $theVorgangPosition->UstSz = 0;
            $theVorgangPosition->EPrNt = $costs;
            $theVorgangPosition->EPrBt = $costs;
            $theVorgangPosition->IstFrachtPosition = true;
            $theVorgangPositionArray[] = $theVorgangPosition;
        }
		
		return $theVorgangPositionArray;
		// totals
	}
	
	/**
	 * 
	 * @param string $inStatus
	 * @return array
	 */
	public function getVorgangListeStatus($inStatus)
	{
		return Mage::getModel('shopsync/vorgang_status')->getVorgangListeByStatus($inStatus);
	}
	
	private function RebuildOrder ($orderId)
	{
		$order = Mage::getModel('sales/order')->loadByIncrementId($orderId);
		$this->MergeStreets($order);
		$order->save();
	}
                
	private function MergeStreets ($order)
	{
		$billingAddress = $order->getBillingAddress();
		if(strpos($billingAddress['street'], "\n") !== FALSE) 
		{
			$streets = explode("\n", $billingAddress['street']);
			$billingAddress['street'] = $streets[0] . " " . $streets[1];
			$billingAddress->save();
		}
               
		$shippingAddress = $order->getShippingAddress();
                
		if(strpos($shippingAddress['street'], "\n") !== FALSE) 
		{
			$streets = explode("\n", $shippingAddress['street']);
			$shippingAddress['street'] = $streets[0] . " " . $streets[1];
			$shippingAddress->save();
		}
    }

	/**
     * setVorgangStatus
     *
     * @param string $WShopID
     * @param integer $orders_status
     * @param string $orders_comments
     * @param boolean $customer_notified
     * @return boolean
     */
	public function setVorgangStatus($inWShopID, $inOrdersStatus, $inOrdersComments, $inCustomerNotified)
	{
	    $theOrder = Mage::getModel('sales/order')->loadByIncrementId($inWShopID);
	    if($theOrder->getId()){
	        if($inOrdersStatus != ''){
	            $theOrder->addStatusToHistory($inOrdersStatus, $inOrdersComments, $inCustomerNotified);
	            $theOrder->save();
	        }
	        if($inCustomerNotified){
	            $theOrder->sendOrderUpdateEmail($inCustomerNotified,$inOrdersComments);
	        }
	    }else{
	        throw new SoapFault('order id fault', 'Order Id:'.$inWShopID.' not exist');
	    }
		return true;
	}
}
