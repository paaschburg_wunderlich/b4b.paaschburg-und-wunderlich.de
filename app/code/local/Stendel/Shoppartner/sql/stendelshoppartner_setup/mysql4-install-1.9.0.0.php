<?php

/**
 * Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 */

/**
 * Install script
 *
 * Install script for Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 * @author    Stefan Stendel <stefan.stendel@googlemail.com>
 * @version   2015-05-25 10:32:14Z
 */

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->startSetup();
$installer->addAttribute("order", "shoppartner_id", array(
    'type'              => 'int',
    'input'             => 'text',
    'label'             => 'Shoppartner ID',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
));

/**
 * Das Attribut 'is_shoppartner' hinzufügen
 */
$installer->addAttribute("customer", "is_shoppartner", array(
    'type'              => 'int',
    'input'             => 'text',
    'label'             => 'Kunde Ist Shoppartner',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Kunde ist als Shoppartner registriert.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'is_shoppartner', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'is_shoppartner');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', 1)
    ->setData('sort_order', 100)
;
$attribute->save();


/**
 * Das Attribut 'erp_customernumber' hinzufügen
 */
$installer->addAttribute("customer", "erp_customernumber", array(
    'type'              => 'int',
    'input'             => 'text',
    'label'             => 'Kundennummer',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Kundennummer aus ERP.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'erp_customernumber', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'erp_customernumber');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', true)
    ->setData('sort_order', 100)
;
$attribute->save();


/**
 * Das Attribut 'customers_latitude' hinzufügen
 */
$installer->addAttribute('customer', 'store_lat', array(
    'type'              => 'decimal',
    'input'             => 'text',
    'label'             => 'Latitude',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Latitude des Shoppartner.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'store_lat', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'store_lat');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', 0)
    ->setData('sort_order', 100)
;
$attribute->save();


/**
 * Das Attribut 'customers_longitude' hinzufügen
 */
$installer->addAttribute('customer', 'store_lng', array(
    'type'              => 'decimal',
    'input'             => 'text',
    'label'             => 'Longitude',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Longitude des Shoppartner.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'store_lng', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'store_lng');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', 0)
    ->setData('sort_order', 100)
;
$attribute->save();


/**
 * Das Attribut 'webaddress' hinzufügen
 */
$installer->addAttribute('customer', 'store_web', array(
    'type'              => 'varchar',
    'input'             => 'text',
    'label'             => 'Webadresse',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Webadresse des Shoppartner.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'store_web', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'store_web');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', 0)
    ->setData('sort_order', 100)
;
$attribute->save();


/**
 * Das Attribut 'webaddress' hinzufügen
 */
$installer->addAttribute('customer', 'store_description', array(
    'type'              => 'varchar',
    'input'             => 'text',
    'label'             => 'Beschreibung',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Beschreibung des Shoppartner.'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'store_description', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'store_description');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', 1)
    ->setData('readonly', 0)
    ->setData('sort_order', 100)
;
$attribute->save();

$installer->endSetup();
