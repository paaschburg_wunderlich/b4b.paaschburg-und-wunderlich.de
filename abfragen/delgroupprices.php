<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 18.03.2016
 * Time: 11:52
 */

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

$coreResource = Mage::getSingleton('core/resource');
$dbWrite = $coreResource->getConnection('core_write');

$dbWrite->query("TRUNCATE TABLE `" . $coreResource->getTableName('catalog_product_entity_tier_price') . "`;");
$dbWrite->query("TRUNCATE TABLE `" . $coreResource->getTableName('catalog_product_entity_group_price') . "`;");

