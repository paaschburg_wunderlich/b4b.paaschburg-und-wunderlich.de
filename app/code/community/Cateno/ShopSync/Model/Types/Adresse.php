<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Adress-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Adresse
{
	/**
	 * @var string
	 */
	public $WShopID;

	/**
	 * @var string
	 */
	public $AdrNr;

	/**
	 * @var Cateno_ShopSync_Model_Types_Freies_Feld
	 */
	public $FreieFelder;

	/**
	 * @var Cateno_ShopSync_Model_Types_Anschriften
	 */
	public $Anschriften;

	public function __construct($inWShopID = 0)
	{
		$this->WShopID = $inWShopID;
	}
}
