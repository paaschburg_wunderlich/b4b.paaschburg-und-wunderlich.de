<?php

/**
 * Sms_Fahrzeug
 *
 * @category  Mage
 * @package   Sms_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 */

/**
 * Install script
 *
 * Install script for Sms_Fahrzeug to extend the table sms_fahrzeug and add the customer attrubute "fahrzeug_id"
 *
 * @category  Mage
 * @package   Sms_Fahrzeug
 * @copyright Copyright (c) 2015 Schmidt Medienservice (http://www.schmidt-medien.de/)
 * @author    Wolf Schmidtk <wolf.schmidt@schmidt-medien.de>
 * @version   2015-05-19 10:32:14Z
 */

$installer = $this;

$installer->startSetup();

$table = $installer->getConnection()
    ->newTable($installer->getTable('sms_fahrzeug'))
    ->addColumn('sms_mkz', Varien_Db_Ddl_Table::TYPE_TEXT, array(
        'nullable'  => false,
        'primary'   => true,
        'unique'    => true,
        'length'    => 10,
    ), 'Id');

$installer->getConnection()->createTable($table);

$installer->endSetup();

$this->startSetup();

$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_hersteller', 'varchar(25) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_modell', 'varchar(50) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_baujahr_von', 'year(4) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_baujahr_bis', 'year(4) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_fahrgestellnr', 'varchar(17) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_modelltyp', 'varchar(25) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_hubraum', 'float(4,0) NOT NULL ');
//$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_mkz', 'bigint(4) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsbelag_vorn_links', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsbelag_vorn_rechts', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsbelag_hinten', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_oelfilter', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_zuendkerze', 'varchar(20) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kw', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_fahrzeugart', 'varchar(25) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlager_vl', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlager_vr', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlager_hl', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlager_hr', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_radlager_kr', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsscheibe_vl', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsscheibe_vr', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_bremsscheibe_hi', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_batterie', 'varchar(15) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_synto_b', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_synto_k', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_verkleidungsscheibe', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kettenteilung', 'char(3) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kettenglieder', 'float NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kettenrad', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_kerazaehne', 'float NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_ritzel', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_ritzelzaehne', 'float NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_gabelsimm', 'varchar(5) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_lenkkopflager', 'varchar(10) NOT NULL ');
$this->_conn->addColumn($this->getTable('sms_fahrzeug'), 'sms_zuendkerze_ch', 'varchar(10) NOT NULL ');

$this->endSetup();

//Das Feld sms_fahrzeug_id in die Tabelle customer anfügen

$installer->startSetup();

$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute("customer", "sms_fahrzeug_id",  array(
    "type"     => "varchar",
    "backend"  => "",
    "label"    => "Fahrzeug ID",
    "input"    => "text",
    "source"   => "",
    "visible"  => true,
    "required" => false,
    "default" => "",
    "frontend" => "",
    "unique"     => false,
    "note"       => "Fahrzeug MKZ"

));

$attribute   = Mage::getSingleton("eav/config")->getAttribute("customer", "sms_fahrzeug_id");

$setup->addAttributeToGroup(
    $entityTypeId,
    $attributeSetId,
    $attributeGroupId,
    'sms_fahrzeug_id',
    '15'  //sort_order
);

$used_in_forms=array();

$used_in_forms[]="adminhtml_customer";
//$used_in_forms[]="checkout_register";
//$used_in_forms[]="customer_account_create";
//$used_in_forms[]="customer_account_edit";
//$used_in_forms[]="adminhtml_checkout";
$attribute->setData("used_in_forms", $used_in_forms)
    ->setData("is_used_for_customer_segment", true)
    ->setData("is_system", 0)
    ->setData("is_user_defined", 1)
    ->setData("is_visible", 1)
    ->setData("sort_order", 100)
;
$attribute->save();

$installer->endSetup();