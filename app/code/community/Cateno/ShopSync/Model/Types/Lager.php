<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Lager-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Lager extends Varien_Object
{
	/**
	 * @var string
	 */
	public $Tabelle;

	/**
	 * @var string
	 */
	public $ArtNr;

	/**
	 * @var string
	 */
	public $ArtNrFeld;

	/**
	 * @var array
	 */
	public $Felder;
}
