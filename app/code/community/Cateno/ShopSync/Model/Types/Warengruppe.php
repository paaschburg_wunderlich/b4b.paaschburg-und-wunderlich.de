<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Warengruppen-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Warengruppe extends Varien_Object
{
	/**
	 * @var string
	 */
	public $WgrNr;

	/**
	 * @var string
	 */
	public $Bez;

	/**
	 * @var array(<Cateno_ShopSync_Model_Types_Kdgrprabsz>)
	 */
	public $KdGrpRabSzListe;

	/**
	 * @var array
	 */
	public $Felder;
}
