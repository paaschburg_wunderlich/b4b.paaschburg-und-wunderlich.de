<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Multi-Shop Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Multi_Shops
{
	/**
	 * Cateno_ShopSync_Model_Types_Multi_Shops
	 * @return array
	 */
	public function getMultiShops()
	{
		$theShops = array();
		foreach(Mage::getModel('core/store_group')->getCollection() as $theStore) {
			$theShops[] = new Cateno_ShopSync_Model_Types_Multi_Shops($theStore->getId(), $theStore->getName());
		}
		return $theShops;
	}
}
