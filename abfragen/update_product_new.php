<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 09.06.2016
 * Time: 12:36
 */
$start = microtime(true);

$fileRowCount = 0;
$insertRowCount = 0;
$divDbRowsCount = 0;

require_once __DIR__ . '/../app/Mage.php';
Mage::app();

require_once __DIR__ . '/inc/config.inc.php';
require_once __DIR__ . '/inc/functions.inc.php';
$class = NEW B4b_Abfragen();

$mysqli = $class->db();

















ini_set ('memory_limit','2G');
ini_set ('display_errors', '1');
error_reporting(1);

define ('DB_TABLE_BASE', 'stendel_products_base');
define ('DB_TABLE_TEMP', 'stendel_products_temp');
define ('DB_TABLE_LIVE', 'stendel_products_live');

define ('IMPORT_PATH', Mage::getBaseDir('media') . '/erp-tool/');
define ('IMPORT_FILE', 'test_product_update.sdf');

define ('LOG_PATH', Mage::getBaseDir() . '/var/log/');
define ('LOG_FILE', 'fill_product_db.log');
define ('WRITE_LOGFILE', false);

/** Logfile Vorbereitung */
if(WRITE_LOGFILE == true) {
    $logFileHandle = $class->openFile(LOG_PATH, LOG_FILE, 'a');
} else { $logFileHandle = NULL; }

/** Status Ausgabe */
$message = '* * * START * * *';
$class->printMessage($message, $logFileHandle);

/** Alle Magento Indexe auf Speicherart 'manuell' stellen */
$class->setAllIndexesToManual();

/** Prüfen ob die Tabelle 'products_temp' bereits existiert und wenn 'nein' anlegen */
$query = "SHOW TABLES LIKE '" . DB_TABLE_TEMP . "';";
$mysqli->query($query);
if ($mysqli->affected_rows == 0) {
    $query = file_get_contents(__DIR__ . '/misc/create_products_temp.sql');
    $mysqli->multi_query($query);
    /** Status Ausgabe */
    $message = "Tabelle [" . DB_TABLE_TEMP . "] angelegt";
    $class->printMessage($message, $logFileHandle);
}

/** Prüfen ob die Tabelle 'products_live' bereits existiert und wenn 'nein' anlegen */
$query = "SHOW TABLES LIKE '" . DB_TABLE_LIVE . "';";
$mysqli->query($query);
if ($mysqli->affected_rows == 0) {
    $query = file_get_contents(__DIR__ . '/misc/create_products_live.sql');
    $mysqli->multi_query($query);
    /** Status Ausgabe */
    $message = "Tabelle [" . DB_TABLE_LIVE . "] angelegt";
    $class->printMessage($message, $logFileHandle);
}

/** Tabelle 'stendel_products_live' leeren */
$mysqli->query("TRUNCATE stendel_products_live");

/** Collection über alle Produkte erstellen */
$collection = Mage::getModel('catalog/product')->getCollection()
    ->addAttributeToSelect('sku');

/** Alle SKU in die LIVE Tabelle eintragen */
foreach ($collection as $product) {
    $sql = "INSERT INTO stendel_products_live (`sku`, `hash`, `timestamp`)
    VALUES (
        '" . $product->getSku() . "',
        '',
        CURRENT_TIMESTAMP
    );";
    $mysqli->query($sql);
}
$message = count($collection) . " SKU's in [" . DB_TABLE_LIVE . "] eingefuegt.";
$class->printMessage($message, $logFileHandle);


/** Die Tabelle 'products_root' löschen */
$mysqli->query("DROP TABLE " . DB_TABLE_BASE . ";");

/** Die Tabelle 'products_temp' in 'products_root' umbenennen */
$mysqli->query("RENAME TABLE " . DB_TABLE_TEMP . " TO " . DB_TABLE_BASE);

/** Eine neue 'products_temp' erstellen */
$query = file_get_contents(__DIR__ . '/misc/create_products_temp.sql');
$mysqli->multi_query($query);
$mysqli->close();

/** Eine neue mysql Verbindung aufbauen */
$mysqli = $class->db();

/** Steart des Import Bereiches */
if (($fileHandle = $class->openFile(IMPORT_PATH, IMPORT_FILE, 'r')) !== FALSE) {
    while (($fileData = fgetcsv($fileHandle, 0, ",")) !== FALSE) {
        $fileRowCount++;

        $csvRow = array(
            'sku'                   => $fileData[0],
            'blocked'               => $fileData[1] == 1 ? $fileData[1] = "2" : $fileData[1] = "1",
            'name_de_DE'            => $fileData[2],
            'shortdesc_de_DE'       => $fileData[3],
            'description_de_DE'     => $fileData[4],
            'bild1'                 => $class->extrudeImage($fileData[5]),
            'bild2'                 => $class->extrudeImage($fileData[6]),
            'bild3'                 => $class->extrudeImage($fileData[7]),
            'bild4'                 => $class->extrudeImage($fileData[8]),
            'barcode'               => $fileData[9],
            'erw_suche'             => $fileData[10],
            'verf_menge'            => $fileData[11],
            'gewicht'               => $fileData[12],
            'preis_de_DE'           => $fileData[13],
            'sonderpreis_de_DE'     => $fileData[14],
            'sp_von_de_DE'          => $fileData[15],
            'sp_bis_de_DE'          => $fileData[16],
            'sichtbar'              => $fileData[17],
            'art_gruppe'            => $fileData[18],
            'art_kat'               => $fileData[19],
            'art_kennz'             => $fileData[20],
            'kettenglieder'         => $fileData[21],
            'kettenteilung'         => $fileData[22],
            'mkz_hinten'            => $fileData[23],
            'mkz_vorn'              => $fileData[24],
            'modellkennzahl'        => $fileData[25],
            'zaehnezahl'            => $fileData[26],
            'ausfuehrung'           => $fileData[27],
            'fahrzeughersteller'    => $fileData[28],
            'farbe'                 => $fileData[29],
            'gutachten'             => $fileData[30],
            'material'              => $fileData[31],
            'produktmarke'          => $fileData[32],
            'xref'                  => $fileData[33],
            'store_de_DE'           => $fileData[34],
            'store_en_GB'           => $fileData[35],
            'store_cz_CS'           => $fileData[36],
            'store_pl_PL'           => $fileData[37],
            'store_se_SE'           => $fileData[38],
            'name_en_GB'            => $fileData[39],
            'shortdesc_en_GB'       => $fileData[40],
            'description_en_GB'     => $fileData[41],
            'name_cz_CS'            => $fileData[42],
            'shortdesc_cz_CS'       => $fileData[43],
            'description_cz_CS'     => $fileData[44],
            'name_pl_PL'            => $fileData[45],
            'shortdesc_pl_PL'       => $fileData[46],
            'description_pl_PL'     => $fileData[47],
            'preis_pl_PL'           => $fileData[48],
            'sonderpreis_pl_PL'     => $fileData[49],
            'sp_von_pl_PL'          => $fileData[50],
            'sp_bis_pl_PL'          => $fileData[51],
            'name_se_SE'            => $fileData[52],
            'shortdesc_se_SE'       => $fileData[53],
            'description_se_SE'     => $fileData[54],
            'art_cat1'              => $fileData[55],
            'art_cat2'              => $fileData[56],
            'art_cat3'              => $fileData[57],
            'art_cat4'              => $fileData[58],
            'art_cat5'              => $fileData[59],
            'art_cat6'              => $fileData[60],
            'art_cat7'              => $fileData[61],
            'art_cat8'              => $fileData[62],
            'art_cat9'              => $fileData[63],
            'art_cat10'             => $fileData[64],
            'auf_lager'             => $fileData[65],
        );

        $sql = "INSERT INTO " . DB_TABLE_TEMP . "
      (`id`, `sku`, `blocked`, `name_de_DE`, `shortdesc_de_DE`, `description_de_DE`, `bild1`, `bild2`, `bild3`, `bild4`, `barcode`, `erw_suche`, `verf_menge`, `gewicht`, `preis_de_DE`, `sonderpreis_de_DE`, `sp_von_de_DE`, `sp_bis_de_DE`, `sichtbar`, `art_gruppe`, `art_kat`, `art_kennz`, `kettenglieder`, `kettenteilung`, `mkz_hinten`, `mkz_vorn`, `modellkennzahl`, `zaehnezahl`, `ausfuehrung`, `fahrzeughersteller`, `farbe`, `gutachten`, `material`, `produktmarke`, `xref`, `store_de_DE`, `store_en_GB`, `store_cz_CS`, `store_pl_PL`, `store_se_SE`, `name_en_GB`, `shortdesc_en_GB`, `description_en_GB`, `name_cz_CS`, `shortdesc_cz_CS`, `description_cz_CS`, `name_pl_PL`, `shortdesc_pl_PL`, `description_pl_PL`, `preis_pl_PL`, `sonderpreis_pl_PL`, `sp_von_pl_PL`, `sp_bis_pl_PL`, `name_se_SE`, `shortdesc_se_SE`, `description_se_SE`, `art_cat1`, `art_cat2`, `art_cat3`, `art_cat4`, `art_cat5`, `art_cat6`, `art_cat7`, `art_cat8`, `art_cat9`, `art_cat10`, `auf_lager`, `hash`, `timestamp`)
      VALUES (
          '" . $fileRowCount . "',
          '" . $mysqli->real_escape_string($csvRow['sku']) . "',
      '" . $mysqli->real_escape_string($csvRow['blocked']) . "',
      '" . $mysqli->real_escape_string($csvRow['name_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['shortdesc_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['description_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['bild1']) . "',
      '" . $mysqli->real_escape_string($csvRow['bild2']) . "',
      '" . $mysqli->real_escape_string($csvRow['bild3']) . "',
      '" . $mysqli->real_escape_string($csvRow['bild4']) . "',
      '" . $mysqli->real_escape_string($csvRow['barcode']) . "',
      '" . $mysqli->real_escape_string($csvRow['erw_suche']) . "',
      '" . $mysqli->real_escape_string($csvRow['verf_menge']) . "',
      '" . $mysqli->real_escape_string($csvRow['gewicht']) . "',
      '" . $mysqli->real_escape_string($csvRow['preis_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['sonderpreis_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['sp_von_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['sp_bis_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['sichtbar']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_gruppe']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_kat']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_kennz']) . "',
      '" . $mysqli->real_escape_string($csvRow['kettenglieder']) . "',
      '" . $mysqli->real_escape_string($csvRow['kettenteilung']) . "',
      '" . $mysqli->real_escape_string($csvRow['mkz_hinten']) . "',
      '" . $mysqli->real_escape_string($csvRow['mkz_vorn']) . "',
      '" . $mysqli->real_escape_string($csvRow['modellkennzahl']) . "',
      '" . $mysqli->real_escape_string($csvRow['zaehnezahl']) . "',
      '" . $mysqli->real_escape_string($csvRow['ausfuehrung']) . "',
      '" . $mysqli->real_escape_string($csvRow['fahrzeughersteller']) . "',
      '" . $mysqli->real_escape_string($csvRow['farbe']) . "',
      '" . $mysqli->real_escape_string($csvRow['gutachten']) . "',
      '" . $mysqli->real_escape_string($csvRow['material']) . "',
      '" . $mysqli->real_escape_string($csvRow['produktmarke']) . "',
      '" . $mysqli->real_escape_string($csvRow['xref']) . "',
      '" . $mysqli->real_escape_string($csvRow['store_de_DE']) . "',
      '" . $mysqli->real_escape_string($csvRow['store_en_GB']) . "',
      '" . $mysqli->real_escape_string($csvRow['store_cz_CS']) . "',
      '" . $mysqli->real_escape_string($csvRow['store_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['store_se_SE']) . "',
      '" . $mysqli->real_escape_string($csvRow['name_en_GB']) . "',
      '" . $mysqli->real_escape_string($csvRow['shortdesc_en_GB']) . "',
      '" . $mysqli->real_escape_string($csvRow['description_en_GB']) . "',
      '" . $mysqli->real_escape_string($csvRow['name_cz_CS']) . "',
      '" . $mysqli->real_escape_string($csvRow['shortdesc_cz_CS']) . "',
      '" . $mysqli->real_escape_string($csvRow['description_cz_CS']) . "',
      '" . $mysqli->real_escape_string($csvRow['name_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['shortdesc_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['description_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['preis_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['sonderpreis_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['sp_von_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['sp_bis_pl_PL']) . "',
      '" . $mysqli->real_escape_string($csvRow['name_se_SE']) . "',
      '" . $mysqli->real_escape_string($csvRow['shortdesc_se_SE']) . "',
      '" . $mysqli->real_escape_string($csvRow['description_se_SE']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat1']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat2']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat3']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat4']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat5']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat6']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat7']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat8']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat9']) . "',
      '" . $mysqli->real_escape_string($csvRow['art_cat10']) . "',
      '" . $mysqli->real_escape_string($csvRow['auf_lager']) . "',
      '" . $mysqli->real_escape_string(sha1(join('' , $csvRow))) . "',
       CURRENT_TIMESTAMP
      );";

        if ($mysqli->query($sql) === TRUE) {
            $insertRowCount++;
        } else {
            echo "Error: " . $sql . "\n" . $mysqli->error;
        }
    }

    /** Status Ausgabe */
    $message = "Es wurden [" . $fileRowCount . "] Zeilen aus der Datei eingelesen und [" . $insertRowCount . "] Zeilen in die Temp Tabelle geschrieben.";
    $class->printMessage($message, $logFileHandle);

    /** Prüfung auf Unterschiede in der Spalte 'hash' */
    $stmt = "SELECT * FROM " . DB_TABLE_TEMP . " WHERE (hash) NOT IN (SELECT hash FROM " . DB_TABLE_BASE . ");";
    $result = $mysqli->query($stmt);

    /** Wenn Unterschiede verhanden */
    if($mysqli->affected_rows > 0) {
        /** Status Ausgabe */
        $message = "Es gibt [" . $mysqli->affected_rows . "] Unterschied(e) zwischen den Tabellen [" . DB_TABLE_BASE . "] und [" . DB_TABLE_TEMP . "]";
        $class->printMessage($message, $logFileHandle);

        /** Durch jede Zeile laufen */
        while ($theProductRow = $result->fetch_array()) {
            $divDbRowsCount++;
            /** Update des Products */
//            $class->updateProduct($theProductRow, $logFileHandle);
        }
    } else {
        /** Status Ausgabe */
        $message = "Es gibt keine Unterschiede zwischen den Tabellen. Nichts zu tun ;-)";
        $class->printMessage($message, $logFileHandle);
    }




    $stmt = "SELECT * FROM " . DB_TABLE_TEMP . " WHERE (sku) NOT IN (SELECT sku FROM " . DB_TABLE_LIVE . ") AND `store_" . $class->getStoreIsoCode($class->getDefaultStoreId()) . "` = 1;";
    $result = $mysqli->query($stmt);

    echo "\n" . $stmt . "\n";

    if($mysqli->affected_rows > 0) {
        /** Status Ausgabe */
        $message = "Es gibt [" . $mysqli->affected_rows . "] Unterschied(e) zwischen den Tabellen [" . DB_TABLE_TEMP . "] und [" . DB_TABLE_LIVE . "]";
        $class->printMessage($message, $logFileHandle);
    }


    $end = microtime(true);
    /** Status Ausgabe */
    $message = "Laufzeit [" . number_format($end - $start, 2) . "] Sekunden.";
    $class->printMessage($message, $logFileHandle);

    /** Status Ausgabe */
    $message = '* * * ENDE * * *';
    $class->printMessage($message, $logFileHandle);

    $class->closeFile($fileHandle);

} else {
    /** Status Ausgabe */
    $message = "Input-Datei [" . IMPORT_PATH . IMPORT_FILE . "] nicht gefunden!!!";
    $class->printMessage($message, $logFileHandle);
    exit;
}

