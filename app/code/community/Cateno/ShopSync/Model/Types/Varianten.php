<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Varianten
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Varianten extends Varien_Object
{
	/**
	 * @var int
	 */
	public $ProductsOptionsId;

	/**
	 * @var int
	 */
	public $ProductsOptionsValuesId;

	/**
	 *
	 * @var array
	 */
	public $Felder;
}
