<?php
/**
 * Created by PhpStorm.
 * User: STENDEL
 * Date: 20.10.2015
 * Time: 11:06
 */

//ini_set("memory_limit",'256M');
//ini_set('max_execution_time', 900);

require_once '../app/Mage.php';

UMASK(0);

Mage::app();
Mage::init();

$products = Mage::getModel('catalog/product')
    ->getCollection()
    ->addAttributeToSelect('*')
    ->addAttributeToFilter('image', 'no_selection');

echo "<pre>";

echo $products->count() . " Artikel ohne Bilder!!!\n\n";

foreach($products as $product)
{
    echo  $product->getSku() . " - " . $product->getName() . "\n";
}
echo "</pre>";