<?php

/**
 * Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 */

/**
 * Install script
 *
 * Install script for Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 * @author    Stefan Stendel <stefan.stendel@googlemail.com>
 * @version   2015-05-25 10:32:14Z
 */

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute("order", "payment_method", array(
    'type'              => 'varchar',
    'input'             => 'text',
    'label'             => 'Bezahlart',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
));

/**
 * Das Attribut 'sp_invoicenumber' hinzufügen
 */
$entityTypeId     = $setup->getEntityTypeId('customer');
$attributeSetId   = $setup->getDefaultAttributeSetId($entityTypeId);
$attributeGroupId = $setup->getDefaultAttributeGroupId($entityTypeId, $attributeSetId);

$installer->addAttribute("customer", "sp_invoicenumber", array(
    'type'              => 'int',
    'input'             => 'text',
    'label'             => 'Rechnungsnummer',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
    'note'              => 'Rechnungsnummer'
));
$setup->addAttributeToGroup($entityTypeId, $attributeSetId, $attributeGroupId, 'sp_invoicenumber', '15');
$attribute = Mage::getSingleton('eav/config')->getAttribute('customer', 'sp_invoicenumber');
$used_in_forms = array();
$used_in_forms[] = 'adminhtml_customer';
$attribute->setData('used_in_forms', $used_in_forms)
    ->setData('is_used_for_customer_segment', true)
    ->setData('is_system', 0)
    ->setData('is_user_defined', 1)
    ->setData('is_visible', true)
    ->setData('readonly', true)
    ->setData('sort_order', 100)
;
$attribute->save();

$installer->endSetup();
