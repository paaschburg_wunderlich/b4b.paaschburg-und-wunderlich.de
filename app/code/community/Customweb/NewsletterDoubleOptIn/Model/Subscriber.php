<?php



class Customweb_NewsletterDoubleOptIn_Model_Subscriber extends Mage_Newsletter_Model_Subscriber
{
	const XML_PATH_CONFIRMATION_KNOWN_CUSTOMERS_FLAG = 'newsletter/subscription/confirmation_known_customers';
	const XML_PATH_CONFIR_SEND_CONFIRM_MAIL_FLAG = 'newsletter/subscription/send_confirm_mail';

	public function subscribe($email)
	{
		$isConfirmNeed = (Mage::getStoreConfig(self::XML_PATH_CONFIRMATION_KNOWN_CUSTOMERS_FLAG) == 1) ? true : false;
		
		// Call parent, if we do not change anthing in the way of
		// processing to the default magento.
		if (!$isConfirmNeed) {
			return parent::subscribe($email);
		}
		
		$this->loadByEmail($email);
		$customerSession = Mage::getSingleton('customer/session');

		if(!$this->getId()) {
			$this->setSubscriberConfirmCode($this->randomSequence());
		}

		$isConfirmNeed   = (Mage::getStoreConfig(self::XML_PATH_CONFIRMATION_FLAG) == 1) ? true : false;
		$isOwnSubscribes = false;
		$ownerId = Mage::getModel('customer/customer')
			->setWebsiteId(Mage::app()->getStore()->getWebsiteId())
			->loadByEmail($email)
			->getId();
		$isSubscribeOwnEmail = $customerSession->isLoggedIn() && $ownerId == $customerSession->getId();

		if (!$this->getId() || $this->getStatus() == self::STATUS_UNSUBSCRIBED
			|| $this->getStatus() == self::STATUS_NOT_ACTIVE
		) {
			if ($isConfirmNeed === true) {
				// Set not active in any case.
				$this->setStatus(self::STATUS_NOT_ACTIVE);
			}
			else {
				$this->setStatus(self::STATUS_SUBSCRIBED);
			}
			$this->setSubscriberEmail($email);
		}

		if ($isSubscribeOwnEmail) {
			$this->setStoreId($customerSession->getCustomer()->getStoreId());
			$this->setCustomerId($customerSession->getCustomerId());
		}
		else {
			$this->setStoreId(Mage::app()->getStore()->getId());
			$this->setCustomerId(0);
		}

		$this->setIsStatusChanged(true);

		try {
			$this->save();
			if ($isConfirmNeed === true) {
				$this->sendConfirmationRequestEmail();
			}
			else {
				$this->sendConfirmationSuccessEmail();
			}

			return $this->getStatus();
		}
		catch (Exception $e) {
			throw new Exception($e->getMessage());
		}
	}
	
	public function confirm($code)
	{
		$rs = parent::confirm($code);
		
		$isConfirmNeed = (Mage::getStoreConfig(self::XML_PATH_CONFIR_SEND_CONFIRM_MAIL_FLAG) == 1) ? true : false;
		if ($rs && $isConfirmNeed) {
			$this->sendConfirmationSuccessEmail();
		}
		
		return $rs;
	}


}
