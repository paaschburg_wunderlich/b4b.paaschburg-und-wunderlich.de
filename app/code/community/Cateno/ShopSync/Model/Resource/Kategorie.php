<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Kategorie-Informationen
 *
 * @version 0.5.1
 */
class Cateno_ShopSync_Model_Resource_Kategorie extends Mage_Catalog_Model_Resource_Eav_Mysql4_Category
{
      const XML_PATH_ROOT_ID = 'shopsync/global/root_id';
      const XML_PATH_NAV_MENU = 'shopsync/kategory/nav';
      const XML_PATH_PIC_DIR = 'shopsync/kategory/pic_dir';

    /**
    * @param Cateno_ShopSync_Model_Types_Kategorie $theKategorie
    * @return int
    */
    public function setKategorie($inKategorie)
    {
		if (is_null($inKategorie->Nr)) {
			return;
		}
		if ($inKategorie->KatID == Mage_Catalog_Model_Category::TREE_ROOT_ID) {
			throw new SoapFault('Invalid Category ID', 'Category ID ' . Mage_Catalog_Model_Category::TREE_ROOT_ID . ' is not a valid Category ID, it is reserved for the Magento ROOT category (in ' . __METHOD__ . ' - Line ' . __LINE__ . ')');
			return false;
        }

		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

		$theParentId = (int) $inKategorie->ParentKatID;
		$theStoreId = Mage::app()->getStore()->getId();

		$theNewData = array(
			'image' => $inKategorie->Bild,
			'updated_at' => Mage::getModel('core/date')->gmtDate(),
			'position' => $inKategorie->Index + 1,
			'is_anchor' => 1,
			'is_active' => 1,
			'default_sort_by' => 1,
			'available_sort_by' => 1,
		);

		$theNewData = Mage::getModel('shopsync/tabellen')->mergeFreiesFeldData($inKategorie->FreieFelder, $theNewData);

		$theParentCategory = Mage::getModel('catalog/category')->load($theParentId);

		if ($theParentCategory->getId()) {
			$theNewData['parent_id'] = $theParentCategory->getId();
		} else {
			$theNewData['parent_id'] = Mage::getStoreConfig(self::XML_PATH_ROOT_ID);
        }

        $theCategory = Mage::getModel('catalog/category')->load($inKategorie->KatID);
		if ($theCategory->getCreatedAt() == '') {
			$theNewData['created_at'] = Mage::getModel('core/date')->gmtDate();
        }

		$isNew = true;
		$theCategoryId = '';
		if ($theCategory->getId()) {
          $isNew = false;
          $theCategoryId = $theNewData['entity_id'] = $theCategory->getId();
        }

		$theCategoryApi = Mage::getSingleton('catalog/category_api');

		$do_move = false;
	
		foreach ($inKategorie->Bezeichnungen as $theItem) {
          $theAttributeData = array();
          foreach ($theItem->Felder as $theAttribute) {
                $theAttributeData[$theAttribute->FeldShop] = str_replace('\n', "\n", $theAttribute->Wert);
                }
          if ( isset($theAttributeData['default_sort_by']) && !empty($theAttributeData['default_sort_by']) ) {
                $theAttributeData['available_sort_by'] = array($theAttributeData['default_sort_by']);
                }
          $theAttributeData = array_merge($theNewData, $theAttributeData);

		  try {
			  if ($isNew) {
					$theCategoryId = $theCategoryApi->create($theAttributeData['parent_id'], $theAttributeData, $theAttributeData['store_id']);
					$isNew = false;
			} else {
					$theCategoryApi->update($theCategoryId, $theAttributeData, $theAttributeData['store_id']);
					$do_move = true;
					}
			  } catch (Exception $e) {
			  throw new SoapFault('Catalog  - save data fault (in ' . __METHOD__ . ' - Line ' . __LINE__ . ')', $e->getMessage());
			  }
		}

		$theCategory = Mage::getModel('catalog/category')
			->setStoreId(0)
			->load($theCategoryId);

		try {
			if ($do_move && (int)  $theNewData['parent_id']   >  0 &&  $theNewData['parent_id']  != $theCategory->getParentId() ) 
				$theCategory->move( $theNewData['parent_id'] , null);
		} catch (Exception $e) {
			throw new SoapFault('Category ' . $theCategoryId . ' could not be moved to new parent ' . $inKategorie->ParentKatID .' (in ' . __METHOD__ . ' - Line ' . __LINE__ . ')', $e->getMessage());
		}

		return $theCategory;
    }

    public function setBilder($pictureObject)
    {
		// get the picture import path
		$pictureImportPath = Mage::getBaseDir() . DS . str_replace('/', DS, Mage::getStoreConfig(self::XML_PATH_PIC_DIR)) . DS;
		// get the category picture path
		$categoryImageDir = Mage::getBaseDir('media') . DS . 'catalog' . DS . 'category' . DS;

		// get ID from pictureObject
		$categoryId = $pictureObject->KatNr;

		// try to load a product by the given SKU and check if we get a valid product id, if not exit
		$categoryModel = Mage::getModel('catalog/category')->load($categoryId);

		// exit, if we have no valid product id
		if (!$categoryModel->getId()) {
            throw new SoapFault('Invalid ID', 'ID: ' . $categoryId . ' does not exist. (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')');
        }

		$newPictures = array();
		$thumbnailFilename = $categoryModel->getThumbnail();
		$imageFilename = $categoryModel->getImage();

		// multiple pictures, thumbnail imagenames contain _thumb
		if ($pictureObject->KategorieBilderArray && is_array($pictureObject->KategorieBilderArray)) {
			foreach ($pictureObject->KategorieBilderArray as $position => $picture) {
				if (strpos($picture, '_thumb') !== false) {
					$newPictures['thumbnail']['file'] = trim($picture);
					$newPictures['thumbnail']['update'] = false;
				} else {
					$newPictures['image']['file'] = trim($picture);
					$newPictures['image']['update'] = false;
				}
			}

            try {
                if (!$newPictures['thumbnail']) {
					$categoryModel->setThumbnail('')->save();
					@unlink($categoryImageDir . $thumbnailFilename);
                }
                if (!$newPictures['image']) {
                    $categoryModel->setImage('')->save();
                    @unlink($categoryImageDir . $imageFilename);
                }
            } catch (Exception $e) {
                throw new SoapFault('Category could not be saved (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $e->getMessage());
            }
        } else {
            try {
                $categoryModel
                        ->setImage('')
                        ->setThumbnail('')
                        ->save()
                ;
            } catch (Exception $e) {
                throw new SoapFault('Category could not be saved (Method: ' . __METHOD__ . ' / Line: ' . __LINE__ . ')', $e->getMessage());
            }
            // delete files physically
            if ($thumbnailFilename) {
                @unlink($categoryImageDir . $thumbnailFilename);
            }
            if ($imageFilename) {
                @unlink($categoryImageDir . $imageFilename);
            }

            return $this;
        }

		// scan for updated images
		$updatePictures = scandir($pictureImportPath);
		foreach ($newPictures as $type => $newPicture) {
		if (in_array($newPicture['file'], $updatePictures)) {
			$newPictures[$type]['update'] = true;
        }
    }

    foreach ($newPictures as $type => $newPicture) {
		if ($newPicture['update'] === true) {
			$categoryModel->setData($type, $newPicture['file']);
		}
	}

    $categoryModel->save();
    foreach ($newPictures as $type => $newPicture) {
		if ($newPicture['update'] === true) {
			rename($pictureImportPath . $newPicture['file'], $categoryImageDir . $newPicture['file']);
        }
    }

    if ($categoryModel->getData('image') !== $categoryModel->getOrigData('image')) {
        @unlink($categoryImageDir . $categoryModel->getOrigData('image'));
    }
    if ($categoryModel->getData('thumbnail') !== $categoryModel->getOrigData('thumbnail')) {
        @unlink($categoryImageDir . $categoryModel->getOrigData('image'));
    }

    return $this;
    }
}
