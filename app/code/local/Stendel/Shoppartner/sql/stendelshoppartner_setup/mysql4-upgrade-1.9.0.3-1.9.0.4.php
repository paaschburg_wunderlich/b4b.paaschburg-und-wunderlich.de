<?php

/**
 * Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 */

/**
 * Install script
 *
 * Install script for Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 * @author    Stefan Stendel <stefan.stendel@googlemail.com>
 * @version   2015-05-25 10:32:14Z
 */

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute("order", "shoppartner_info", array(
    'type'              => 'varchar',
    'input'             => 'text',
    'label'             => 'Shoppartner Info',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'visible_on_front'  => 3,
));

$installer->run("ALTER TABLE `sales_flat_order` CHANGE `shoppartner_info` `shoppartner_info` VARCHAR(512) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Shoppartner Info';");

$installer->endSetup();
