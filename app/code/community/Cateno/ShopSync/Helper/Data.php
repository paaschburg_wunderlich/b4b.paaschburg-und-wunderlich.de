<?php
/**
 * Cateno
 *
 * @category Helper
 * @package Cateno_ShopSync
 * Basisklasse für Datenübertragung
 *
 * @version 0.1.0
 */
class Cateno_ShopSync_Helper_Data extends Mage_Core_Helper_Abstract
{
	const XML_ENABLE = 'shopsync/global/enable';
	const XML_DEFAULT_CUSTOMER_TAX_CLASS = 'shopsync/global/customer_tax_class';
	const XML_DEFAULT_TAX_COUNTRY = 'shopsync/global/default_tax_country';

	const XML_STATE_NEW = 'shopsync/global/state_new';
	const XML_STATE_PENDING = 'shopsync/global/state_pending';
	const XML_STATE_PROCESSING = 'shopsync/global/state_processing';
	const XML_STATE_COMPLETE = 'shopsync/global/state_complete';
	const XML_STATE_CLOSED = 'shopsync/global/state_closed';
	const XML_STATE_CANCELED = 'shopsync/global/state_canceled';
	const XML_STATE_HOLDED = 'shopsync/global/state_holded';
	const XML_STATE_PENDING_PAYMENT = 'shopsync/global/state_pending_peyment';
	const XML_STATE_PAYMENT_REVIEW = 'shopsync/global/state_payment_review';

	const XML_FOR_METHOD_ACTION_SHOPBEENDEN = 'shopsync/global/shopbeenden_affected';
	const XML_FOR_METHOD_ACTION_CATEGORY = 'shopsync/global/category_affected';

	/**
	 * get default order status sort
	 * Enter description here ...
	 * @param string $inOrderStateCode
	 * @return int
	 */
	public function getDefaultOrderStatusSort($inOrderStateCode)
	{
		switch ($inOrderStateCode) {
			case Mage_Sales_Model_Order::STATE_NEW:
				return Mage::getStoreConfig(self::XML_STATE_NEW);
				break;
			case Mage_Sales_Model_Recurring_Profile::STATE_PENDING:
				return Mage::getStoreConfig(self::XML_STATE_PENDING);
				break;
			case Mage_Sales_Model_Order::STATE_PROCESSING:
				return Mage::getStoreConfig(self::XML_STATE_PROCESSING);
				break;
			case Mage_Sales_Model_Order::STATE_COMPLETE:
				return Mage::getStoreConfig(self::XML_STATE_COMPLETE);
				break;
			case Mage_Sales_Model_Order::STATE_CLOSED:
				return Mage::getStoreConfig(self::XML_STATE_CLOSED);
				break;
			case Mage_Sales_Model_Order::STATE_CANCELED:
				return Mage::getStoreConfig(self::XML_STATE_CANCELED);
				break;
			case Mage_Sales_Model_Order::STATE_HOLDED:
				return Mage::getStoreConfig(self::XML_STATE_HOLDED);
				break;
			case Mage_Sales_Model_Order::STATE_PENDING_PAYMENT:
				return Mage::getStoreConfig(self::XML_STATE_PENDING_PAYMENT);
				break;
			case Mage_Sales_Model_Order::STATE_PAYMENT_REVIEW:
				return Mage::getStoreConfig(self::XML_STATE_PAYMENT_REVIEW);
				break;
			default:
				return -1;
				break;
		}
	}

	/**
	 * Check this modul is enable.
	 * @return boolean
	 */
	public function isEnable()
	{
		return Mage::getStoreConfig(self::XML_ENABLE);
	}

	/**
	 * get default customer_tax_class
	 * @return string
	 */
	public function getDefaultCustomerTaxClass()
	{
		return Mage::getStoreConfig(self::XML_DEFAULT_CUSTOMER_TAX_CLASS);
	}

	/**
	 * get default tax_country
	 * @return string
	 */
	public function getDefaultTaxCountry()
	{
		return Mage::getStoreConfig(self::XML_DEFAULT_TAX_COUNTRY);
	}

	/**
	 * truncate some tables { 'tax_calculation' | 'tax_calculation_rate', 'tax_calculation_rule' }
	 * delete tax_class where class_type = 'PRODUCT' 's records
	 * return void();
	 */
	public function truncateTax()
	{
		$theTC = Mage::getResourceModel('tax/calculation');
		$theTC->getReadConnection()->truncate($theTC->getMainTable());

		$theTCRT = Mage::getResourceModel('tax/calculation_rate');
		$theTCRT->getReadConnection()->truncate($theTCRT->getMainTable());

		$theTCRL = Mage::getResourceModel('tax/calculation_rule');
		$theTCRL->getReadConnection()->truncate($theTCRL->getMainTable());

		$theTClass = Mage::getResourceModel('tax/class_collection')->setClassTypeFilter('PRODUCT');
		foreach($theTClass as $theItem) {
			$theItem->delete();
		}
	}

	/**
	 * @return int
	 */
	public function getBereinigenBeenden()
	{
		return Mage::getStoreConfig(self::XML_FOR_METHOD_ACTION_SHOPBEENDEN);
	}

	/**
	 * @return int
	 */
	public function getKategorienBereinigen()
	{
		return Mage::getStoreConfig(self::XML_FOR_METHOD_ACTION_CATEGORY);
	}

	/**
	 * compare the magento system customer update , and soapclient $_GET parameters date
	 *
	 * @param datetime $inSysDate
	 * @param datetime $inParamDate
	 * @return boolean
	 */
	public function compareDates($inSysDate, $inParamDate)
	{
		if($inSysDate == '') {
			return true;
		}else{
			list($theSysDate, $theSysPass) = explode(' ', $inSysDate);
			list($theSysYear, $theSysMonth, $theSysDay) = explode('-', $theSysDate);
			list($theSysHour, $theSysMinute, $theSysSecond) = explode(':', $theSysPass);

			$theSysTimestamp = mktime(
				intval($theSysHour),
				intval($theSysMinute),
				intval($theSysSecond),
				intval($theSysMonth),
				intval($theSysDay),
				intval($theSysYear)
			);

			list($theParamDate, $theParamPass) = explode(' ', $inParamDate);
			list($theParamYear, $theParamMonth, $theParamDay) = explode('-', $theParamDate);
			list($theParamHour, $theParamMinute, $theParamSecond) = explode(':', $theParamPass);

			$theParamTimestamp = mktime(
				intval($theParamHour),
				intval($theParamMinute),
				intval($theParamSecond),
				intval($theParamMonth),
				intval($theParamDay),
				intval($theParamYear)
			);

			if($theSysTimestamp < $theParamTimestamp) return true;
			else return false;
		}
	}
}
