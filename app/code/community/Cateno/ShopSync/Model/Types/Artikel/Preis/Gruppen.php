<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Kundengruppen-spezifischen Preisen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Artikel_Preis_Gruppen extends Varien_Object
{
	/**
	 * @var int
	 */
	public $Nr;

	/**
	 * @var string
	 */
	public $Bezeichnung;

	public function __construct($inNr = 0, $inBezeichnung = null)
	{
		$this->Nr = $inNr;
		$this->Bezeichnung = $inBezeichnung;
	}
}
