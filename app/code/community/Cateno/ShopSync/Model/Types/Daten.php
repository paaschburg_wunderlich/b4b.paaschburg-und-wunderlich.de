<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Daten-Feldern
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Daten extends Varien_Object
{
	/**
	 * @var array
	 */
	public $Felder;
}
