<?php
/**
 * Stendel_Shoppartner
 *
 * @category  Stendel
 * @package   Stendel_Shoppartner
 * @version   1.0.0
 * @copyright 2015 Stefan Stendel (www.foto-stendel.de)
 */

class Stendel_Shoppartner_Helper_Data extends Mage_Core_Helper_Abstract
{

    /**
     * @return string
     */
    public function getCustomersDefaultShippingAdress()
    {
        $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultShipping();
        if ($customerAddressId) {
            $address = Mage::getModel('customer/address')->load($customerAddressId);
            $mydatas['name'] = $address->getFirstname() . ' ' . $address->getLastname();
            $mydatas['company'] = $address->getCompany();
            $mydatas['postcode'] = $address->getPostcode();
            $mydatas['city'] = $address->getCity();
            $street = $address->getStreet();
            $mydatas['street'] = $street[0];
            $mydatas['telephone'] = $address->getTelephone();
            $mydatas['fax'] = $address->getFax();
            $mydatas['country'] = $address->getCountry();

            return $mydatas['company'] . " " . $mydatas['name'] . ", " . $mydatas['street'] . ", " . $mydatas['postcode'] . ", " . $mydatas['city'];
        }
    }


    /**
     * @return string
     */
    public function getCustomersDefaultBillingAdress()
    {
        $customerAddressId = Mage::getSingleton('customer/session')->getCustomer()->getDefaultBilling();
        if ($customerAddressId) {
            $address = Mage::getModel('customer/address')->load($customerAddressId);
            $mydatas['name'] = $address->getFirstname() . ' ' . $address->getLastname();
            $mydatas['company'] = $address->getCompany();
            $mydatas['postcode'] = $address->getPostcode();
            $mydatas['city'] = $address->getCity();
            $street = $address->getStreet();
            $mydatas['street'] = $street[0];
            $mydatas['telephone'] = $address->getTelephone();
            $mydatas['fax'] = $address->getFax();
            $mydatas['country'] = $address->getCountry();

            return $mydatas['company'] . " " . $mydatas['name'] . ", " . $mydatas['street'] . ", " . $mydatas['postcode'] . ", " . $mydatas['city'];
        }
    }


    /**
     * @param $address
     * @return mixed
     */
    function getCustomersGeodata($address)
    {
        $url = "http://maps.google.com/maps/api/geocode/json?sensor=false" . "&address=" . urlencode($address);
        $json = file_get_contents($url);
        $data = json_decode($json, TRUE);

        if($data['status']=="OK"){
            return $data['results'];
        }
    }


    /**
     * @param $entity_id
     * @return mixed
     */
    public function getShoppartnerStatus($entity_id)
    {
        $collection = mage::getModel('customer/customer')->getCollection()
            ->addAttributeToFilter('entity_id', $entity_id)
            ->addAttributeToFilter('customer_activated', 1)
            ->addAttributeToFilter('is_shoppartner', 1);

        foreach($collection as $_shoppartner) {
            return $_shoppartner->getId();
        }
    }


    public function getShoppartner($customerAddress, $returnLimit = 1)
    {

    }


    /**
     * @param $address
     * @param int $returnLimit
     * @return array|bool
     */
    public function getCustomersShoppartnerByAddress($address, $returnLimit = 1)
    {
        $geoData = $this->getCustomersGeodata($address);
        $customersLatitude = $geoData[0]['geometry']['location']['lat'];
        $customersLongitude = $geoData[0]['geometry']['location']['lng'];
        if($customersLatitude) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');

            $result = "SELECT DISTINCT entity_id as tmp_entity_id,
            (SELECT @lat := value FROM customer_entity_decimal WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'store_lat') AND entity_id = tmp_entity_id),
            (SELECT @lng := value FROM customer_entity_decimal WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'store_lng') AND entity_id = tmp_entity_id),
            ( 3959 * acos( cos( radians($customersLatitude) ) * cos( radians( @lat ) ) * cos( radians( @lng ) - radians($customersLongitude) ) + sin( radians($customersLatitude) ) * sin( radians( @lat ) ) ) * 1.609344 ) AS distance
            FROM customer_entity_decimal
            ORDER BY distance ASC";

            foreach($readConnection->fetchAll($result) as $row) {
                if($this->getShoppartnerStatus($row['tmp_entity_id']) AND $row['distance'] < 1500) {
                    $shoppartner[] = array(
                        'entity_id' => $row['tmp_entity_id'],
                        'distance'  => $row['distance']
                    );
                }
            }
            return array_splice($shoppartner, 0, $returnLimit);
        }
        return false;
    }


    /**
     * @param $address
     * @param $shoppartnerId
     * @param int $returnLimit
     * @return array|bool
     */
    public function getCustomersShoppartnerById($address, $shoppartnerId, $returnLimit = 1)
    {
        $geoData = $this->getCustomersGeodata($address);
        $customersLatitude = $geoData[0]['geometry']['location']['lat'];
        $customersLongitude = $geoData[0]['geometry']['location']['lng'];
        if($customersLatitude) {
            $resource = Mage::getSingleton('core/resource');
            $readConnection = $resource->getConnection('core_read');

            $result = "SELECT DISTINCT entity_id as tmp_entity_id,
            (SELECT @lat := value FROM customer_entity_decimal WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'store_lat') AND entity_id = tmp_entity_id),
            (SELECT @lng := value FROM customer_entity_decimal WHERE attribute_id = (SELECT attribute_id FROM eav_attribute WHERE attribute_code = 'store_lng') AND entity_id = tmp_entity_id),
            ( 3959 * acos( cos( radians($customersLatitude) ) * cos( radians( @lat ) ) * cos( radians( @lng ) - radians($customersLongitude) ) + sin( radians($customersLatitude) ) * sin( radians( @lat ) ) ) * 1.609344 ) AS distance
            FROM customer_entity_decimal WHERE entity_id = '" . $shoppartnerId . "'
            ORDER BY distance ASC";

            foreach($readConnection->fetchAll($result) as $row) {
                if($this->getShoppartnerStatus($row['tmp_entity_id'])) {
                    $shoppartner[] = array(
                        'entity_id' => $row['tmp_entity_id'],
                        'distance'  => $row['distance']
                    );
                }
            }
            return array_splice($shoppartner, 0, $returnLimit);
        }
        return false;
    }


    /**
     * @param $shoppartnerId
     * @return array
     */
    public function getShoppartnerById($shoppartnerId)
    {
        $collection = mage::getModel('customer/customer')
            ->getCollection()
            ->addAttributeToSelect('*')
            ->addAttributeToFilter('is_shoppartner', true)
            ->addAttributeToFilter('customer_activated', 1)
            ->addAttributeToFilter('entity_id', $shoppartnerId);

        foreach($collection as $row) {
            $shoppartner[] = array(
                'entity_id' => $row['entity_id']
            );
        }
        return $shoppartner;
    }


    /**
     * @return array
     */
    public function getGoogleAddress()
    {
        define ('SELECT_SHOPPARTNER_BY_BILLING_ADDRESS', false);

        /**
         * Wenn der Shoppartner nur aufgrund der Rechnungsadresse zugewiesen werden soll
         */
        if (SELECT_SHOPPARTNER_BY_BILLING_ADDRESS == true)
        {
            /**
             * Customer ist angemeldet
             */
            if (Mage::getSingleton('customer/session')->isLoggedIn())
            {
                /**
                 * Wenn die billing_address_id nicht LEER ist den Adress Datensatz nutzen
                 */
                if (!empty($_POST['billing_address_id']))
                {
                    $address = Mage::getModel('customer/address')->load($_POST['billing_address_id']);
                    $shippingAddressForGoogle = $address->street . "," . $address->postcode . "," . $address->city;
                } else {
                    /**
                     * Wenn die billing_address_id LEER ist die Formularfelder nutzen
                     */
                    $shippingAddressForGoogle = $_POST['billing']['street'][0] . "," . $_POST['billing']['postcode'] . "," . $_POST['billing']['city'];
                }
            } else {
                /**
                 * Customer ist NICHT angemeldet
                 */
                $shippingAddressForGoogle = $_POST['billing']['street'][0] . "," . $_POST['billing']['postcode'] . "," . $_POST['billing']['city'];
            }
            $shoppartnerSourceAddress = $this->__('Billing Address');
        } else {
            /**
             * Wenn der Shoppartner flexibel nach Rechnungs ODER Lieferadresse zugewiesen werden soll
             */

            /**
             * Customer ist angemeldet
             */
            if (Mage::getSingleton('customer/session')->isLoggedIn()) {
                /**
                 * [An diese Adresse verschicken] gewählt UND billing_address_id GEFÜLLT
                 */
                if ($_POST['billing']['use_for_shipping'] == 1 AND !empty($_POST['billing_address_id'])) {
                    $shoppartnerSourceAddress = $this->__('Billing Address');
                    $address = Mage::getModel('customer/address')->load($_POST['billing_address_id']);
                    $shippingAddressForGoogle = $address->street . "," . $address->postcode . "," . $address->city;
                }
                /**
                 * [An diese Adresse verschicken] gewählt UND billing_address_id LEER
                 */
                if ($_POST['billing']['use_for_shipping'] == 1 AND empty($_POST['billing_address_id'])) {
                    $shoppartnerSourceAddress = $this->__('Billing Address');
                    $shippingAddressForGoogle = $_POST['billing']['street'][0] . "," . $_POST['billing']['postcode'] . "," . $_POST['billing']['city'];
                }

                /**
                 * [An diese Adresse verschicken] NICHT gewählt UND shipping_address_id NICHT LEER
                 */
                if ($_POST['billing']['use_for_shipping'] == 0 AND !empty($_POST['shipping_address_id'])) {
                    $shoppartnerSourceAddress = $this->__('Shipping Address');
                    $address = Mage::getModel('customer/address')->load($_POST['shipping_address_id']);
                    $shippingAddressForGoogle = $address->street . "," . $address->postcode . "," . $address->city;
                }

                /**
                 * [An diese Adresse verschicken] NICHT gewählt UND shipping_address_id LEER
                 */
                if ($_POST['billing']['use_for_shipping'] == 0 AND empty($_POST['shipping_address_id'])) {
                    $shoppartnerSourceAddress = $this->__('Shipping Address');
                    $shippingAddressForGoogle = $_POST['shipping']['street'][0] . "," . $_POST['shipping']['postcode'] . "," . $_POST['shipping']['city'];
                }
            } else {
                /**
                 * Customer ist NICHT angemeldet
                 */

                /**
                 * [An diese Adresse verschicken] gewählt die Felder billing verwenden
                 */
                if ($_POST['billing']['use_for_shipping'] == 1) {
                    $shippingAddressForGoogle = $_POST['billing']['street'][0] . "," . $_POST['billing']['postcode'] . "," . $_POST['billing']['city'];
                } else {
                    $shippingAddressForGoogle = $_POST['shipping']['street'][0] . "," . $_POST['shipping']['postcode'] . "," . $_POST['shipping']['city'];
                }
            }
        }
        return array(
            'address'   =>$shippingAddressForGoogle,
            'source'    =>$shoppartnerSourceAddress
        );
    }


    /**
     * @param $shoppartner
     */
    public function printCheck($shoppartner)
    {
        $shippingAddressForGoogle = $this->getGoogleAddress();
        echo "<div style='border: 1px solid red;'><pre>";
        echo "PRÜFAUSGABE MUSS NOCH ENTFERNT WERDEN<br />";
        echo "Billing  Address ID: " . $_POST['billing_address_id'] . "<br />";
        echo "Shipping Address ID: " . $_POST['shipping_address_id'] . "<br />";
        echo "Use for Shipping   : " . $_POST['billing']['use_for_shipping'] . "<br />";
        echo "Shoppartner ID     : " . $shoppartner['store_id'] . "<br />";
        echo "Adresse für Google : " . $shippingAddressForGoogle['address'];
        echo "</pre></div>";
    }


    /**
     * @param $shoppartnerId
     * @return $this
     */
    public function setShoppartnerAddressToQuote($shoppartnerId)
    {

        $shoppartnerObj = Mage::getModel('customer/customer')->load($shoppartnerId);
        $shoppartnerAdressObj = Mage::getModel('customer/address')->load($shoppartnerId);

        $quote = Mage::getSingleton('checkout/session')->getQuote();

        $shippingAddress = Mage::getModel('sales/quote_address')
            ->setAddressType(Mage_Sales_Model_Quote_Address::TYPE_SHIPPING)
            ->setPrefix($shoppartnerAdressObj->getPrefix())
            ->setFirstname($shoppartnerAdressObj->getFirstname())
            ->setMiddlename($shoppartnerAdressObj->getMiddlename())
            ->setLastname($shoppartnerAdressObj->getLastname())
            ->setCompany($shoppartnerObj->getStoreName())
            ->setStreet($shoppartnerAdressObj->getStreet())
            ->setCity($shoppartnerAdressObj->getCity())
            ->setCountry_id("DE")
            ->setRegion("NULL")
            ->setRegion_id("NULL")
            ->setPostcode($shoppartnerAdressObj->getPostcode())
            ->setTelephone($shoppartnerAdressObj->getTelephone());

        $quote->setShippingAddress($shippingAddress);
        $quote->save();

        return $this;
    }


    /**
     * @param $customerId
     * @return bool
     */
    public function getShoppartnerFromLastOrder($customerId)
    {
        $orders = Mage::getModel('sales/order')->getCollection()
            ->setOrder('created_at','DESC')
            ->addFilter('customer_id', $customerId)
            ->setPageSize(1)
            ->setCurPage(1);
        $order = $orders->getFirstItem();
        if ($orders) {
            return $order->getShoppartnerId();
        }
        return false;
    }


    /**
     * @param $style
     * @param $shoppartnerId
     * @return string
     */
    public function getShoppartnerAddress($style, $shoppartnerId)
    {
        $shoppartnerData = Mage::getModel('customer/customer')->load($shoppartnerId)->getData();
        $shoppartnerAddress = Mage::getModel('customer/customer')->load($shoppartnerId)->getPrimaryBillingAddress();

        $storeName = $shoppartnerData['store_name'];
        $storeStreet = $shoppartnerAddress['street'];
        $storePostcode = $shoppartnerAddress['postcode'];
        $storeCity = $shoppartnerAddress['city'];
        $storeTelephone = $shoppartnerAddress['telephone'];
        $storeFax = $shoppartnerAddress['fax'];
        $storeEmail = $shoppartnerData['email'];
        $storeWeb = $shoppartnerData['store_web'];

        switch ($style) {

            case 'mini';
                $content = "<strong>" . $storeName . "</strong><br />";
                $content .= $storeStreet . ", " . $storePostcode . ", " . $storeCity;
                return $content;

            case 'order';
                $content = $storeName . "\n";
                $content .= $storeStreet . ", " . $storePostcode . ", " . $storeCity . "\n";
                if ($storeEmail) $content .= "E-Mail: " . preg_replace('/[\/ ]/', '', $storeEmail) . "\n";
                if ($storeTelephone) $content .= "T: " . preg_replace('/[\/ ]/', '', $storeTelephone);
                if ($storeTelephone AND $storeFax) $content .= ", ";
                if ($storeFax) $content .= "F: " . preg_replace('/[\/ ]/', '', $storeFax);
                return $content;

            case 'agreement';
                if ($storeName) {
                    $content = $storeName . "\n";
                    $content .= $storeStreet . ", " . $storePostcode . ", " . $storeCity . "\n";
                    $content .= preg_replace('/[\/ ]/', '', $storeEmail);
                } else {
                    $content = $this->__('No Shoppartner selected!');;
                }
                return $content;

            case 'normal';
                $content = "<strong>" . $storeName . "</strong><br />";
                $content .= $storeStreet . ", " . $storePostcode . ", " . $storeCity . "<br />";
                if ($storeTelephone) $content .= "T: " . preg_replace('/[\/ ]/', '', $storeTelephone);
                return $content;

            case 'admin';
                $content = "<strong>" . $storeName . "</strong><br />";
                $content .= $storeStreet . ", " . $storePostcode . ", " . $storeCity . "<br />";
                if ($storeTelephone) $content .= "T: " . preg_replace('/[\/ ]/', '', $storeTelephone);
                if ($storeEmail) $content .= "<br />E-Mail: <a href='mailto:" . preg_replace('/[\/ ]/', '', $storeEmail) . "'>" . preg_replace('/[\/ ]/', '', $storeEmail) . "</a>";
                $content .= "<br />Shoppartner ID: <a href='" . Mage::helper('adminhtml')->getUrl('adminhtml/customer/edit/', array('id'=>$shoppartnerData['entity_id'])) . "'>" . $shoppartnerData['entity_id'] . "</a>";
                $content .= " - ERP Kundennummer: <a href='" . Mage::helper('adminhtml')->getUrl('adminhtml/customer/edit/', array('id'=>$shoppartnerData['entity_id'])) . "'>" . $shoppartnerData['erp_customernumber'] . "</a>";

                return $content;

            case 'full';
                $content = "<strong>" . $storeName . "</strong><br />";
                $content .= $storeStreet . ", " . $storePostcode . ", " . $storeCity . "<br />";
                if ($storeTelephone) $content .= "T: " . preg_replace('/[\/ ]/', '', $storeTelephone);
                if ($storeTelephone AND $storeFax) $content .= ", ";
                if ($storeFax) $content .= "F: " . preg_replace('/[\/ ]/', '', $storeFax);
                if ($storeEmail) $content .= "<br />E-Mail: <a href='mailto:" . preg_replace('/[\/ ]/', '', $storeEmail) . "'>" . preg_replace('/[\/ ]/', '', $storeEmail) . "</a>";
                if ($storeWeb) $content .= "<br />Web: <a href='http://" . preg_replace('/[\/ ]/', '', $storeWeb) . "' target='blank'>" . preg_replace('/[\/ ]/', '', $storeWeb) . "</a>";
                return $content;
        }
    }


    /**
     * @param $shoppartnerId
     * @return mixed
     */
    public function getShoppartnerCompany($shoppartnerId)
    {
        $shoppartnerData = Mage::getModel('customer/customer')->load($shoppartnerId)->getData();
        return $shoppartnerData['store_name'];
    }


    /**
     * @param $shoppartnerId
     * @return string
     */
    public function getShoppartnerName($shoppartnerId)
    {
        $shoppartnerData = Mage::getModel('customer/customer')->load($shoppartnerId)->getData();
        return $shoppartnerData['firstname'] . " " . $shoppartnerData['lastname'];
    }

    /**
     * @param $shoppartnerId
     * @return string
     */
    public function getShoppartnerStreet($shoppartnerId)
    {
        $shoppartnerAddress = Mage::getModel('customer/customer')->load($shoppartnerId)->getPrimaryBillingAddress();
        return $shoppartnerAddress['street'];
    }


    /**
     * @param $shoppartnerId
     * @return mixed
     */
    public function getShoppartnerCity($shoppartnerId)
    {
        $shoppartnerAddress = Mage::getModel('customer/customer')->load($shoppartnerId)->getPrimaryBillingAddress();
        return $shoppartnerAddress['postcode'] . ", " . $shoppartnerAddress['city'];
    }

}