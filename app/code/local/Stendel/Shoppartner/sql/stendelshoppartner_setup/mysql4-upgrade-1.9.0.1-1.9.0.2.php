<?php

/**
 * Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 */

/**
 * Install script
 *
 * Install script for Stendel_Shoppartner
 *
 * @category  Mage
 * @package   Stendel_Shoppartner
 * @copyright Copyright (c) 2015 Stefan Stendel
 * @author    Stefan Stendel <stefan.stendel@googlemail.com>
 * @version   2015-05-25 10:32:14Z
 */

$installer = $this;
$setup = new Mage_Eav_Model_Entity_Setup('core_setup');

$installer->startSetup();
$installer->addAttribute("order", "shoppartner_kdnr", array(
    'type'              => 'int',
    'input'             => 'text',
    'label'             => 'Shoppartner Kundennummer',
    'global'            => 1,
    'visible'           => 1,
    'required'          => 0,
    'user_defined'      => 1,
    'default'           => '0',
    'visible_on_front'  => 3,
));
$installer->endSetup();
