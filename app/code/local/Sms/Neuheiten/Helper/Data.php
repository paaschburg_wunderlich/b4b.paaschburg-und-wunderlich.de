<?php

class Sms_Neuheiten_Helper_Data extends Mage_Core_Helper_Abstract
{
    public function neuheiten ($neuheitenid)
    {
        $neuheiten = new Mage_Catalog_Model_Category();
        $neuheiten->load($neuheitenid);
        $neuheitenproducts = $neuheiten->getProductCollection();
        $neuheitenproducts->addAttributeToSelect('*');
        $neuheitenproducts->getSelect()->order('RAND()');
        $neuheitenproducts->getSelect()->limit(1);
        $neuheiten_array = array();
        foreach($neuheitenproducts as $neuheit)
        {
            $neuheitimg = $neuheit->getImageUrl();
            $neuheitname = $neuheit->getName();
            $neuheitname = substr($neuheitname,0,43);
            $neuheitsku = $neuheit->getSku();
            $neuheiturl = $neuheit->getUrlPath();
            $neuheitvk = $neuheit->getFinalPrice();
            $neuheitenshortdescription = $neuheit->getShortDescription();
            $neuheitenshortdescription = strip_tags($neuheitenshortdescription);
            array_push($neuheiten_array, $neuheitimg, $neuheitname, $neuheitsku, $neuheitenshortdescription, $neuheiturl, $neuheitvk);
        }
        return $neuheiten_array;
    }

  }
