<?php

/**
 * Stendel_OrderComment
 *
 * @category  Stendel
 * @package   Stendel_OrderComment
 * @version   1.0.0
 * @copyright 2015 Stefan Stendel (www.foto-stendel.de)
 */

class Stendel_OrderComment_Block_Checkout_Comment extends Mage_Core_Block_Template
{

    /**
     * Get label text for comment field
     *
     * @return string
     */
    public function getLabelText()
    {
        return $this->_getHelper()->__('Customer notice');
    }

    /**
     * Get field id
     *
     * @return string
     */
    public function getCommentFieldId()
    {
        return Stendel_OrderComment_Model_Observer::COMMENT_FIELD_ID;
    }

    /**
     * Get module helper class
     *
     * @return Stendel_OrderComment_Helper_Data
     */
    protected function _getHelper()
    {
        $helper = Mage::helper('ordercomment');

        return $helper;
    }
}