<?php

class Sms_Widget_Block_Text extends Mage_Core_Block_Abstract implements Mage_Widget_Block_Interface {

    protected function _toHtml() {
        $text = $this->getData('mein_text');
        if( trim( $text ) != "" ){
            return $text;
        }
    }
}