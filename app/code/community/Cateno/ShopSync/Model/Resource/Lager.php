<?php
/**
 * Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Ressource für die Übertragung von Lager-Informationen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Lager
{

	/**
	 * set stock by sku
	 * @param Cateno_ShopSync_Model_Types_Lager $inLagerListe
	 * @return boolean
	 */
	public function setLager($inLager)
	{
        //get sku
        $theSku = $inLager->ArtNr;
        //get product id
        $theProductId = Mage::getModel('catalog/product')->getIdBySku($theSku);
        //get product
        $theProductObj = Mage::getModel('catalog/product')->load($theProductId);

        if($theProductObj->getId()){
            $theLagerData = Mage::getModel('shopsync/tabellen')->mergeFreiesFeldData($inLager->Felder, array());

            // Lagerbestand speichern
            $stockData = $theProductObj->getStockData();
			$stockData['qty'] = $theLagerData['qty'];
			$stockData['is_in_stock'] = $theLagerData['is_in_stock'];
			$theProductObj->setStockData($stockData);

			$theProductObj->save();

        }else{
            throw new SoapFault('product sku fault', 'product sku: '.$theSku.' not exist.');
        }
	}
}
