<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von Artikel-Zusätzen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Artikel_Zusaetze extends Varien_Object
{
	/**
	 * @var string
	 */
	public $ArtNr;

	/**
	 * @var array
	 */
	public $Felder;
}
