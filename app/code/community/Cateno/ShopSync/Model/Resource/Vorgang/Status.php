<?php
/**
* Cateno
 *
 * @category Resource Model
 * @package Cateno_ShopSync
 * Übertragung des Vorgangsstatus
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Resource_Vorgang_Status
{
    /**
     * Cateno_ShopSync_Model_Types_Vorgang_Status
     * @return array
     */
	public function getVorgangStatus()
	{
		$theStatus = array();
        $theStatusObj = Mage::getSingleton('sales/order_config')->getStatuses();
        foreach($theStatusObj as $theKey => $theValue){

            $theStatus[] = new Cateno_ShopSync_Model_Types_Vorgang_Status(
                $theKey,
                $theValue
            );
        }
        return $theStatus;
	}

	/**
	 * @param string $inStatus
	 * @return array
	 */
	public function getVorgangListeByStatus($inStatus)
	{
		$theOrderIds = array();
		$theOrderCollection = Mage::getModel('sales/order')->getCollection()
			->addFieldToFilter('status', array('eq' => $inStatus));
		foreach($theOrderCollection as $theOrder) {
			$theOrderIds[] = $theOrder->getIncrementId();
		}

		// als Objekt gekapselt übergeben, um es als Referenz im Observer verändern zu können
        $object = new Varien_Object($theOrderIds);
		Mage::dispatchEvent('shopsync_after_get_vorgang_list_by_status', array('orderIds' => $object));
		$orderIds = $object->getData();

		return $orderIds;
	}
}
