<?php
/**
 * Autor: Schmidt Medienservice, Wolf Schmidt
 * für Paaschburg & Wunderlich
 * 2015-05-20
 * alle Rechte liegen beim Autor
 */
ini_set("memory_limit",'256M');
ini_set('max_execution_time', 900);

define('IMPORTFILE', 'fahrzeuge.sdf');
require_once __DIR__ . '/../app/Mage.php';
Mage::app();

$db_read = Mage::getSingleton('core/resource')->getConnection('core_read');
$db_write = Mage::getSingleton('core/resource')->getConnection('core_write');

if ($importFile = fopen(Mage::getBaseDir('media') . '/sms/' . IMPORTFILE, 'r')) {
    $sql = "TRUNCATE TABLE sms_fahrzeug";
    $db_write->query($sql);
} else {
    echo "[" . date('d.m.Y h:i:s') . "] " . "File " . Mage::getBaseDir('media') . '/sms/' . IMPORTFILE . " not in dirctory!\n";
    exit;
}

$count = 0;
while (($line = fgetcsv($importFile)) !== FALSE) {
    $count++;

    $line[1] = utf8_encode($line[1]);
    $line[2] = utf8_encode($line[2]);
    $line[6] = utf8_encode($line[6]);

    $insertFahrzeugQuery = "INSERT INTO sms_fahrzeug
                            VALUES
                            ('$line[0]' ,
                            '$line[1]' ,
                            '$line[2]' ,
                            '$line[3]' ,
                            '$line[4]' ,
                            '$line[5]' ,
                            '$line[6]' ,
                            '$line[7]' ,
                            '$line[8]' ,
                            '$line[9]' ,
                            '$line[10]' ,
                            '$line[11]' ,
                            '$line[12]' ,
                            '$line[13]' ,
                            '$line[14]' ,
                            '$line[15]' ,
                            '$line[16]' ,
                            '$line[17]' ,
                            '$line[18]' ,
                            '$line[19]' ,
                            '$line[20]' ,
                            '$line[21]' ,
                            '$line[22]' ,
                            '$line[23]' ,
                            '$line[24]' ,
                            '$line[25]' ,
                            '$line[26]' ,
                            '$line[27]' ,
                            '$line[28]' ,
                            '$line[29]' ,
                            '$line[30]' ,
                            '$line[31]' ,
                            '$line[32]' ,
                            '$line[33]' ,
                            '$line[34]' ,
                            '$line[35]' ,
                            '$line[36]' ,
                            '$line[37]' ,
                            '$line[38]' ,
                            '$line[39]' ,
                            '$line[40]' ,
                            '$line[41]' ,
                            '$line[42]' ,
                            '$line[43]' ,
                            '$line[44]' ,
                            '$line[45]' ,
                            '$line[46]'
                            )
                            ";
    $db_write->query($insertFahrzeugQuery);
}
print "Die Datenbanktabelle <B>modelle_neu</B> wurde aktualisiert mit php utf8_encode.";

fclose($importFile);