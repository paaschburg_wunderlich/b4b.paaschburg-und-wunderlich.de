<?php
include "database.inc";
$class = NEW auswertung;
$dbConnection = $class->connectDb();

function sortieren($wert_a, $wert_b)
{

    $a = $wert_a[0];
    $b = $wert_b[0];

    If ($a == $b) {
        return 0;
    }

    return ($a > $b) ? -1 : +1;
}

?>
<HTML>
<HEAD>
    <META HTTP-EQUIV="Content-type" CONTENT="text/html; charset=utf-8">
    <META HTTP-EQUIV="Content-Language" CONTENT="de">
    <link href="basic.css" type="text/css" rel="stylesheet" media="all"/>
    <TITLE>Auswertung</TITLE>

</HEAD>
<BODY>
<DIV ALIGN="center">
    <table width="900" border="1" cellspacing="5" cellpadding="5">

        <?php
        $weise = $_REQUEST["weise"];
        $jahr = $_REQUEST["jahr"];
        $aktuellesJahr = date('Y');

        if (!isset($_REQUEST['sort'])) {
            $sort = "";
        } else {
            $sort = $_REQUEST['sort'];
        }


        switch ($weise) {
        case "gesamt":
        if ($jahr == "alles") {
            $SQL = $dbConnection->query("SELECT * FROM sales_flat_order WHERE store_id = 1 AND status LIKE 'shipped'");
            $SQL2 = $dbConnection->query("SELECT SUM(base_subtotal) AS summe FROM sales_flat_order WHERE store_id = 1 AND status LIKE 'shipped' AND shoppartner_kdnr <> ''");
        } else {
            $SQL = $dbConnection->query("SELECT * FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%Y') = '$jahr' AND store_id = 1 AND status LIKE 'shipped'");
            $SQL2 = $dbConnection->query("SELECT SUM(base_subtotal) AS summe FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%Y') = '$jahr' AND store_id = 1 AND status LIKE 'shipped' AND shoppartner_kdnr <> ''");
        }

        $item2 = $SQL2->fetch_assoc();
        $anzahl_auftr = $SQL->num_rows;
        $Summe_gesamt = number_format($item2['summe'], 2, ',', '.');
        if ($jahr == "alles") {
            $jahr = "";
        }
        ?>
        <TD ALIGN="center"><FONT FACE="Arial Black" SIZE="+2"><BR><?php echo $anzahl_auftr; ?> Aufträge
                und</FONT><BR><BR>
            <FONT FACE="Arial Black" SIZE="+2"><?php echo $Summe_gesamt; ?> Euro Umsatz<BR><BR>
                im gesamten Zeitraum <?php echo $jahr; ?>.<BR><BR></FONT></TD>
        </TR></TABLE>
    <?php
    break;

    /*** Anfang Auswertung nach Shoppartnern ******************************************************************************/
    case "shoppartner":
        print "<FONT SIZE='+1'><DIV ALIGN=CENTER>Umsatz $jahr. Bitte Sortierung auswählen!<BR><BR></DIV></FONT>";
        ?>
        <tr align="center" valign="middle" height="40" bgcolor="#434040">
            <TD ALIGN='center'><A HREF="auswertung.php?weise=shoppartner&jahr=<?php echo $jahr; ?>&sort=kdnr">Kundennummer</A></TD>
            <TD ALIGN='center'><A HREF="auswertung.php?weise=shoppartner&jahr=<?php echo $jahr; ?>&sort=kdname">Name Kunde</A></TD>
            <!--            <TD ALIGN='center'><A HREF="auswertung.php?weise=shoppartner&jahr=--><?php //echo $jahr; ?><!--&sort=plz">PLZ</A></TD>-->
            <!--            <TD ALIGN='center'></TD>-->
            <TD ALIGN='center'><A HREF="auswertung.php?weise=shoppartner&jahr=<?php echo $jahr; ?>&sort=umsatz">Umsatz</A>
            </TD>
            <TD ALIGN='center'><A HREF="auswertung.php?weise=shoppartner&jahr=<?php echo $jahr; ?>&sort=auftraege">Auftr&auml;ge</A>
            </TD>
        </tr>
        <?php

        $SQL1 = "SELECT sfo.shoppartner_id, sfo.shoppartner_kdnr, cev.value
FROM sales_flat_order sfo 
LEFT JOIN customer_entity_varchar cev
ON sfo.shoppartner_id = cev.entity_id AND cev.attribute_id = 199
WHERE sfo.status LIKE 'shipped'
AND sfo.shoppartner_kdnr <> ''
";

        $SQL2 = "";

        if ($jahr != "Gesamtzeitraum") {
            $SQL2 = " AND YEAR (sfo.created_at) = $jahr ";
        }

        $SQL3 = "";
        $SQL4 = "";

        switch ($sort) {
            case "kdnr":
                $SQL4 = " ORDER BY sfr.shoppartner_kdnr asc";
                break;

            case "kdname":
                $SQL4 = " ORDER BY ss.store_name asc";
                break;

            case "plz":
                $SQL4 = " ORDER BY ss.zipcode asc";
                break;

            case "umsatz":
                $SQL4 = " ORDER BY SUM(sfo.base_subtotal) desc";
                break;

            case "auftraege":
                $SQL4 = " ORDER BY count(sfo.entity_id) desc";
                break;
        }
        $SQL5 = "GROUP BY sfo.shoppartner_id ";

        // Abfrage und Ausgabe nur ausfuehren, wenn ein gueltiger Zeitraum ausgewaehlt wurde
        if (substr($jahr, 0, 1) !== "[") {
            $Resultat = $dbConnection->query($SQL1 . $SQL2 . $SQL3 . $SQL5 . $SQL4);
            $gesamt = 0;
            while ($row = $Resultat->fetch_assoc()) {
                extract($row);
                $shoppartner = $row['shoppartner_id'];
                $sql_summe = $dbConnection->query("SELECT SUM(base_subtotal) AS summe 
                                            FROM sales_flat_order 
                                            WHERE DATE_FORMAT(updated_at, '%Y') = '$jahr' 
                                            AND shoppartner_id = $shoppartner 
                                            AND status LIKE 'shipped'");
                $summe = $sql_summe->fetch_assoc();
                $umsatz = number_format($summe['summe'], 2, ',', '.');
                $gesamt = $gesamt + $summe['summe'];

                print "<tr>";
                print "<td>" . $row['shoppartner_kdnr'] . "</td>";
                print "<td>" . $row['value'] . "</td>";
//                print "<td>" . $row['store_zipcode'] . "</td>";
//                print "<td>&nbsp;</td>";
                print "<td align='right'>" . $umsatz . "</td>";
//                print "<td align='right'>" . $row['anzahl_bestellungen'] . "</td>";
                print "</tr>";
            }
            echo "Kontrollsumme: " . number_format($gesamt, 2, ',', '.');
        }

        /*** ENDE Auswertung nach Shoppartnern ******************************************************************************/


//Sortierung innerhalb der Auswertung

        switch ($sort) {
            case "kdnr":
                if ($jahr == 'Gesamtzeitraum') {
                    $SQL = $dbConnection->query("SELECT uName, uFree3, uFree7, uFree8 FROM Users ORDER BY uName");
                    while ($row = $SQL->fetch_assoc()) {

                        $SQL2 = $dbConnection->query("SELECT * FROM ostats WHERE statKndId = $row[uName] GROUP BY statSession");
                        $SQL3 = $dbConnection->query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName]");
                        $Resultat3 = $SQL3->fetch_assoc();

                        if ($Resultat3['summe'] == 0) {
                            $Resultat3['summe'] = 0;
                        }

                        $summe = number_format($Resultat3['summe'], 2, ',', '.');
                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>$row[uName]</STRONG></TD>
			<TD ALIGN='left'>$row[uFree3]</TD>
			<TD ALIGN='left'>$row[uFree7]</TD>
			<TD ALIGN='left'>$row[uFree8]</TD>
			<TD ALIGN='right'><B>$summe Euro</B></TD>
			<TD ALIGN='center'>
		";
                        echo $SQL2->num_rows();
                        print " Aufträge</TD></TR></FONT>";
                    }

                } else {
                    $SQL = $dbConnection->query("SELECT uName, uFree3, uFree7, uFree8 FROM Users WHERE uID > 6 ORDER BY uName");
                    while ($row = $SQL->fetch_assoc()) {

                        $SQL2 = $dbConnection->query("SELECT * FROM ostats WHERE statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr' GROUP BY statSession");
                        $SQL3 = $dbConnection->query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr'");
                        $Resultat3 = $SQL3->fetch_assoc();

                        if ($Resultat3['summe'] == 0) {
                            $Resultat3['summe'] = 0;
                        }

                        $summe = number_format($Resultat3['summe'], 2, ',', '.');
                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>$row[uName]</STRONG></TD>
			<TD ALIGN='left'>$row[uFree3]</TD>
			<TD ALIGN='left'>$row[uFree7]</TD>
			<TD ALIGN='left'>$row[uFree8]</TD>
			<TD ALIGN='right'><B>$summe Euro</B></TD>
			<TD ALIGN='center'>
		";
                        echo mysql_num_rows($Resultat2);
                        print " Aufträge</TD></TR></FONT>";
                    }
                }
                break;

            case "plz":

                if ($jahr == 'Gesamtzeitraum') {
                    $SQL = mysql_query("SELECT uName, uFree3, uFree7, uFree8 FROM Users ORDER BY uFree7");
                    while ($row = mysql_fetch_assoc($SQL)) {

                        $SQL2 = "SELECT * FROM ostats WHERE statKndId = $row[uName] GROUP BY statSession";
                        $Resultat2 = mysql_db_query($tab, $SQL2);

                        $SQL3 = mysql_query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName]");
                        $Resultat3 = mysql_fetch_array($SQL3);

                        if ($Resultat3[summe] == 0) {
                            $Resultat3[summe] = 0;
                        }

                        $summe = number_format($Resultat3[summe], 2, ',', '.');
                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>$row[uName]</STRONG></TD>
			<TD ALIGN='left'>$row[uFree3]</TD>
			<TD ALIGN='left'>$row[uFree7]</TD>
			<TD ALIGN='left'>$row[uFree8]</TD>
			<TD ALIGN='right'><B>$summe Euro</B></TD>
			<TD ALIGN='center'>
		";
                        echo mysql_num_rows($Resultat2);
                        print " Aufträge</TD></TR></FONT>";
                    }
                } else {
                    $SQL = mysql_query("SELECT uName, uFree3, uFree7, uFree8 FROM Users WHERE uID > 6 ORDER BY uFree7");
                    while ($row = mysql_fetch_assoc($SQL)) {

                        $SQL2 = "SELECT * FROM ostats WHERE statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr' GROUP BY statSession";
                        $Resultat2 = mysql_db_query($tab, $SQL2);

                        $SQL3 = mysql_query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr'");
                        $Resultat3 = mysql_fetch_array($SQL3);

                        if ($Resultat3[summe] == 0) {
                            $Resultat3[summe] = 0;
                        }

                        $summe = number_format($Resultat3[summe], 2, ',', '.');
                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>$row[uName]</STRONG></TD>
			<TD ALIGN='left'>$row[uFree3]</TD>
			<TD ALIGN='left'>$row[uFree7]</TD>
			<TD ALIGN='left'>$row[uFree8]</TD>
			<TD ALIGN='right'><B>$summe Euro</B></TD>
			<TD ALIGN='center'>
		";
                        echo mysql_num_rows($Resultat2);
                        print " Aufträge</TD></TR></FONT>";
                    }
                }
                break;

            case "umsatz":

                $umsatz = array();
                if ($jahr == "Gesamtzeitraum") {
                    $SQL = mysql_query("SELECT uName, uFree3, uFree7, uFree8 FROM Users ORDER BY uName");
                    while ($row = mysql_fetch_assoc($SQL)) {
                        extract($row);

                        $SQL2 = "SELECT statSession, statKndId FROM ostats WHERE statKndId = $row[uName] GROUP BY statSession ORDER BY statKndId";
                        $Resultat2 = mysql_db_query($tab, $SQL2);
                        $count = mysql_num_rows($Resultat2);

                        $SQL3 = mysql_query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName]");
                        $Resultat3 = mysql_fetch_array($SQL3);

                        if ($Resultat3[summe] == 0) {
                            $Resultat3[summe] = 0;
                        }

                        array_unshift($row, $Resultat3[summe], $count);
                        array_unshift($umsatz, $row);
                    }
                    usort($umsatz, 'sortieren');


                    for ($i = 0; $i < sizeof($umsatz); $i++) {

                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>";
                        echo $umsatz[$i][uName];
                        echo "</STRONG></TD>
			<TD ALIGN='left'>";
                        echo $umsatz[$i][uFree3];
                        echo "</TD>
			<TD ALIGN='left'>";
                        echo $umsatz[$i][uFree7];
                        echo "</TD>
			<TD ALIGN='left'>";
                        echo $umsatz[$i][uFree8];
                        echo "</TD>
			<TD ALIGN='right'><B>";
                        echo number_format($umsatz[$i][0], 2, ',', '.');
                        echo " Euro</B></TD>
			<TD ALIGN='center'>";
                        echo $umsatz[$i][1];
                        echo " Aufträge</TD></TR></FONT>";
                    }
                } else {
//                    $SQL = $dbConnection->query("SELECT uName, uFree3, uFree7, uFree8 FROM Users WHERE uID > 6 ORDER BY uName");
//                    while ($row = $SQL->fetch_assoc()) {
//                        extract($row);
//
//                        $SQL2 = "SELECT statSession, statKndId FROM ostats WHERE statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr' GROUP BY statSession ORDER BY statKndId";
//                        $Resultat2 = mysql_db_query($tab, $SQL2);
//                        $count = mysql_num_rows($Resultat2);
//
//                        $SQL3 = mysql_query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr'");
//                        $Resultat3 = mysql_fetch_array($SQL3);
//
//                        if ($Resultat3[summe] == 0) {
//                            $Resultat3[summe] = 0;
//                        }
//
//                        array_unshift($row, $Resultat3[summe], $count);
//                        array_unshift($umsatz, $row);
//                    }
//                    usort($umsatz, 'sortieren');
//
//
//                    for ($i = 0; $i < sizeof($umsatz); $i++) {
//
//                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>";
//                        echo $umsatz[$i][uName];
//                        echo "</STRONG></TD>
//			<TD ALIGN='left'>";
//                        echo $umsatz[$i][uFree3];
//                        echo "</TD>
//			<TD ALIGN='left'>";
//                        echo $umsatz[$i][uFree7];
//                        echo "</TD>
//			<TD ALIGN='left'>";
//                        echo $umsatz[$i][uFree8];
//                        echo "</TD>
//			<TD ALIGN='right'><B>";
//                        echo number_format($umsatz[$i][0], 2, ',', '.');
//                        echo " Euro</B></TD>
//			<TD ALIGN='center'>";
//                        echo $umsatz[$i][1];
//                        echo " Aufträge</TD></TR></FONT>";
//                    }
                }
                break;

            default:
                if ($jahr == 'Gesamtzeitraum') {
                    $SQL = mysql_query("SELECT uName, uFree3, uFree7, uFree8 FROM Users ORDER BY uName");
                    while ($row = mysql_fetch_assoc($SQL)) {

                        if (isset($jahr)) {
                            $SQL2 = "SELECT * FROM ostats WHERE statKndId = $row[uName] GROUP BY statSession";
                            $Resultat2 = mysql_db_query($tab, $SQL2);

                            $SQL3 = mysql_query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName]");
                            $Resultat3 = mysql_fetch_array($SQL3);
                        } else {
                            $SQL2 = "SELECT * FROM ostats WHERE statKndId = $row[uName] GROUP BY statSession";
                            $Resultat2 = mysql_db_query($tab, $SQL2);

                            $SQL3 = mysql_query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName]");
                            $Resultat3 = mysql_fetch_array($SQL3);

                        }
                        if ($Resultat3[summe] == 0) {
                            $Resultat3[summe] = 0;
                        }

                        $summe = number_format($Resultat3[summe], 2, ',', '.');
                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>$row[uName]</STRONG></TD>
			<TD ALIGN='left'>$row[uFree3]</TD>
			<TD ALIGN='left'>$row[uFree7]</TD>
			<TD ALIGN='left'>$row[uFree8]</TD>
			<TD ALIGN='right'><B>$summe Euro</B></TD>
			<TD ALIGN='center'>
		";
                        echo mysql_num_rows($Resultat2);
                        print " Aufträge</TD></TR></FONT>";
                    }
                } else {
//                    $SQL = $dbConnection->query("SELECT uName, uFree3, uFree7, uFree8 FROM Users WHERE uID > 6 ORDER BY uName");
//                    while ($row = $SQL->fetch_assoc()) {
//
//                        if (isset($jahr)) {
//                            $SQL2 = $dbConnection->query("SELECT * FROM ostats WHERE statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr' GROUP BY statSession");
//                            $SQL3 = $dbConnection->query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName] AND DATE_FORMAT(statArtDate, '%Y') = '$jahr'");
//                            $Resultat3 = $SQL3->fetch_assoc();
//                        } else {
//                            $SQL2 = $dbConnection->query("SELECT * FROM ostats WHERE statKndId = $row[uName] GROUP BY statSession");
//                            $SQL3 = $dbConnection->query("SELECT SUM(statPosSum) AS summe FROM ostats WHERE statArtGrp <> 'Versandkosten' AND statKndId = $row[uName]");
//                            $Resultat3 = $SQL3->fetch_assoc();
//
//                        }
//                        if ($Resultat3[summe] == 0) {
//                            $Resultat3[summe] = 0;
//                        }
//
//                        $summe = number_format($Resultat3[summe], 2, ',', '.');
//                        echo "	<TR ALIGN='center' VALIGN='middle' HEIGHT='40'><TD ALIGN='center'><FONT SIZE=+1><STRONG>$row[uName]</STRONG></TD>
//			<TD ALIGN='left'>$row[uFree3]</TD>
//			<TD ALIGN='left'>$row[uFree7]</TD>
//			<TD ALIGN='left'>$row[uFree8]</TD>
//			<TD ALIGN='right'><B>$summe Euro</B></TD>
//			<TD ALIGN='center'>
//		";
//                        echo mysql_num_rows($Resultat2);
//                        print " Aufträge</TD></TR></FONT>";
//                    }
                }
        }
        break;
    case "monat":

        $SPALTEN = 6; //Anzahl Spalten der Tabelle
        $num = 0;
        ?>
        <tr align="center" valign="middle" BGCOLOR="#434040" height="35">
            <td colspan="6"><font style="font-weight:bold; color:white; font-size:24px;">Umsatzauswertung nach
                    Monat</font></td>


            <?php

            //Jahressumme abfragen
            if (!isset ($jahr)) {

                $Jahresumsatz = $dbConnection->query("SELECT SUM(base_subtotal) AS summe FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%Y') = '$aktuellesJahr' AND store_id = 1 AND status LIKE 'shipped'");
                $Jahresumsatzergebnis = $Jahresumsatz->fetch_assoc();
                if ($Jahresumsatzergebnis['summe'] == 0) {
                    $Jahresumsatzergebnis['summe'] = 'kein Umsatz';
                }
            } else {
                $Jahresumsatz = $dbConnection->query("SELECT SUM(base_subtotal) AS summe FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%Y') = '$jahr' AND store_id = 1 AND status LIKE 'shipped' AND shoppartner_kdnr <> ''");
                $Jahresumsatzergebnis = $Jahresumsatz->fetch_assoc();
                if ($Jahresumsatzergebnis['summe'] == 0) {
                    $Jahresumsatzergebnis['summe'] = 'kein Umsatz';
                }
            }
            $row = array("01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12");
            //	while ($row = mysql_fetch_assoc($SQL)) {

            foreach ($row as $monat) {

                if (!isset ($jahr)) {
                    $SQL2 = $dbConnection->query("SELECT SUM(base_subtotal) AS summe 
FROM sales_flat_order 
WHERE DATE_FORMAT(updated_at, '%m') = $monat 
AND DATE_FORMAT(updated_at, '%Y') = $aktuellesJahr 
AND store_id = 1 
AND status LIKE 'shipped'");
                    $SQL3 = $dbConnection->query("SELECT * FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%m') = $monat AND DATE_FORMAT(updated_at, '%Y') = $aktuellesJahr AND store_id = 1 AND status LIKE 'shipped'");
                } else {
                    $SQL2 = $dbConnection->query("SELECT SUM(base_subtotal) AS summe FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%m') = $monat AND DATE_FORMAT(updated_at, '%Y') = '$jahr' AND store_id = 1 AND status LIKE 'shipped'");
                    $SQL3 = $dbConnection->query("SELECT * FROM sales_flat_order WHERE DATE_FORMAT(updated_at, '%m') = $monat AND DATE_FORMAT(updated_at, '%Y') = '$jahr' AND store_id = 1 AND status LIKE 'shipped'");
                }


                $Resultat2 = $SQL2->fetch_assoc();
                $Resultat3 = $SQL3->fetch_assoc();
                $Value = number_format($Resultat2['summe'], 2, ',', '.');
                if ($num % $SPALTEN == 0)
                    echo "</TR><TR>";

                echo "
			<TD ALIGN='center'><FONT FACE='Arial' SIZE='+1'><B>$monat</B></FONT><BR><BR>
		$Value Euro<BR>
		";

                echo $SQL3->num_rows;
                print " Aufträge</TD>";
                $num++;
            }
            if ($num % $SPALTEN != 0) {
                echo "<td colspan='" . ($SPALTEN % $num) . "'>&nbsp;</TD>";
            }
            $Jahresvalue = number_format($Jahresumsatzergebnis['summe'], 2, ',', '.');
            ?>
        </TR>
        <TR>
            <TD colspan="6" ALIGN="center" BGCOLOR="silver"><FONT SIZE="+2"><STRONG/>Gesamtsumme für <?php echo $_REQUEST["jahr"]; ?>: <?php echo $Jahresvalue; ?></STRONG></FONT></TD>
        </TR>
        </TABLE>
        <?php
        break;

    default:

        echo "<TD><FONT FACE='Arial Black' SIZE='+2'>Nix ausgew&auml;hlt</FONT></TD></TR></TABLE>";
    }
    ?>
</DIV>
</BODY>
</HTML>