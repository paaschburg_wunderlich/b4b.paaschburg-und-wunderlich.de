<?php
/**
 * Cateno
 *
 * @category Class
 * @package Cateno_ShopSync
 * Übertragung von zu leerenden Tabellen
 *
 * @version 0.2.0
 */
class Cateno_ShopSync_Model_Types_Tabellen_Loeschen extends Varien_Object
{
	/**
	 * @var string
	 */
	public $TabellenName;

	/**
	 * @var string
	 */
	public $FeldName;
}
